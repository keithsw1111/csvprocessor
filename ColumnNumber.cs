// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.Diagnostics;
using System.Collections.Generic;

namespace CSVProcessor
{
    /// <summary>
    /// Can read a string or create a column
    /// </summary>
    public class StringOrColumn
    {
        #region Member Variables
        Column _c = null; // the column
        string _s = string.Empty; // the string value
        #endregion

        #region Constructions

        /// <summary>
        /// Create a string
        /// </summary>
        /// <param name="s"></param>
        public StringOrColumn(string s)
        {
            _s = s;
        }

        /// <summary>
        /// Create a string or a column from an XML node
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        /// <param name="od"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        public StringOrColumn(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            // Loook at all the child nodes
            foreach (XmlNode n1 in n.ChildNodes)
            {
                // if it is a text/cdata node save it as a string
                if (n1.NodeType == XmlNodeType.Text || n1.NodeType == XmlNodeType.CDATA)
                {
                    _s = n1.InnerText;
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "   " + _s);
                    }
                }
                else if (n1.NodeType != XmlNodeType.Comment)
                {
                    // create the output column
                    _c = Column.Create(n1, j, p, od, tracexml, prefix + "        ");
                }
                else
                {
                    // do nothing it was a comment
                }
            }
        }
        #endregion

        #region Public Functions
        public bool IsColumn
        {
            get
            {
                return _c != null;
            }
        }

        public void SetStringValue(string s)
        {
            _s = s;
            _c = null;
        }
        /// <summary>
        /// Get the value of the string or the column
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public string Value(ColumnLine l)
        {
            // a column takes priority
            if (_c != null && l != null)
            {
                return _c.Process(l).ToString(false);
            }
            else
            {
                return _s;
            }
        }

        public string Value()
        {
            if (_c != null)
            {
                return _c.Process(new ColumnLine()).ToString(false);
            }
            else
            {
                return _s;
            }
        }
        #endregion
    }

    public class ColumnNumberOrColumn
    {
        #region Member Variables
        ColumnNumber _cn = null;
        Column _c = null;
        CSVProcess _p = null;
        #endregion

        #region Constructors
        public ColumnNumberOrColumn(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            _p = p;

            if (n == null)
            {
                _cn = new ColumnNumber(j, "0");
            }
            else
            {
                foreach (XmlNode n1 in n.ChildNodes)
                {
                    if (n1.NodeType == XmlNodeType.Text || n1.NodeType == XmlNodeType.CDATA)
                    {
                        if (p == null)
                        {
                            _cn = new ColumnNumber(j, n1.InnerText);
                        }
                        else
                        {
                            _cn = new ColumnNumber(p.Job, n1.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  " + _cn.ToString());
                        }
                    }
                    else if (n1.NodeType != XmlNodeType.Comment)
                    {
                        // create the output column
                        _c = Column.Create(n1, j, p, od, tracexml, prefix + "        ");
                    }
                    else
                    {
                        // do nothing it was a comment
                    }
                }
            }
        }
        #endregion

        #region Overridden Functions
        public override string ToString()
        {
            if (_c != null)
            {
                return _c.ToString();
            }
            else if (_cn != null)
            {
                if (_cn.Number == int.MaxValue)
                {
                    return _cn.Name;
                }
                else
                {
                    return _cn.Number.ToString();
                }
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region Accessors
        public ColumnNumber ColumnNumber
        {
            get
            {
                return _cn;
            }
        }

        public Column Column
        {
            get
            {
                return _c;
            }
        }

        public bool IsValid
        {
            get
            {
                return (_cn != null || _c != null);
            }
        }
        #endregion

        #region Public Functions

        public bool IsColumnNumber()
        {
            return _cn != null;
        }
        public string Value(ColumnLine l)
        {
            if (_cn != null)
            {
                // get the column value
                string cv = string.Empty;
                if (_cn.TagValue)
                {
                    return _p.Tags.GetTagValue(_cn.Name, l);
                }
                else if (_cn.Variable)
                {
                    try
                    {
                        return _p.JobParameters.PromptVariable(_cn.Name);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            return Constants.Constant(_cn.Name);
                        }
                        catch (Exception)
                        {
                            try
                            {
                                return RuntimeVariable.GetValue(_cn.Name);
                            }
                            catch (Exception)
                            {
                                return string.Empty;
                            }
                        }
                    }
                }
                else
                {
                    string rc = string.Empty;
                    try
                    {
                        rc = l[_cn];
                    }
                    catch
                    {
                        // if the column does not exist then just use an empty column
                        rc = string.Empty;
                    }
                    return rc;
                }
            }
            else if (_c != null)
            {
                return _c.Process(l)[0];
            }
            else
            {
                return "Column Number and Column not set!";
            }
        }
        #endregion
    }

    /// <summary>
    /// Class used to hold and interpret a column number
    /// </summary>
    public class ColumnNumber
    {
        #region static Variables
        static Dictionary<string, ColumnLine> __columnNames = new Dictionary<string, ColumnLine>(); // holds the column names from the first line of the file
        static int __resetindex = 0;
        static bool __columnNamesSet = false;
        #endregion

        #region Member variables
        string _defn = string.Empty; // the original column number text
        string _name = string.Empty; // name of this column
        int _number = int.MinValue; // number of this column (0 based) -1 = last column
        int _resetindex = -1;
        bool _all = false; // if true this column represents all columns
        bool _tagValue = false; // true if tag value name
        bool _variable = false;
        CSVJob _job = null;
        //bool _dontresetonfilechange = false;
        #endregion

        #region Column Name Functions
        /// <summary>
        /// Called to pass in the first line from the file
        /// </summary>
        /// <param name="line">First line in the file</param>
        public static void SetColNames(CSVJob job, ColumnLine line, bool replace)
        {
            if (replace)
            {
                __columnNames[job.Id] = line;
                __resetindex++;
            }
            else
            {
                // only do this if we dont already have column names
                if (__columnNames.ContainsKey(job.Id))
                {
                    __columnNames[job.Id].AddBeginning(line);
                }
                else
                {
                    __columnNames[job.Id] = line;
                }
            }
            __columnNamesSet = true;
        }

        static public ColumnLine GetColNames(CSVJob job)
        {
            return __columnNames[job.Id];
        }

        public static string ColumnName(CSVJob job, int i)
        {
            if (__columnNames.ContainsKey(job.Id))
            {
                if (i == int.MaxValue - 1)
                {
                    return "Raw";
                }
                else if (i < 0)
                {
                    return __columnNames[job.Id][__columnNames.Count + i];
                }
                else
                {
                    return __columnNames[job.Id][i];
                }
            }
            else
            {
                return string.Empty;
            }
        }

        static public bool ColumnNameExists(CSVJob job, string _name)
        {
            if (__columnNames.ContainsKey(job.Id))
            {
                foreach (string colname in __columnNames[job.Id].Cols)
                {
                    if (colname.ToLower().Trim() == _name)
                    {
                        return true;
                    }
                }
                // if we are looking for the special !FOREACH! tag and it isnt there ... add it to the end
                if (_name == "!foreach!")
                {
                    __columnNames[job.Id].AddEnd("!foreach!");
                }
            }

            return false;
        }

        /// <summary>
        /// Add a column name to the column names
        /// </summary>
        /// <param name="name">Name to add</param>
        public static void Add(CSVJob job, string name)
        {
            if (__columnNames.ContainsKey(job.Id))
            {
                // Add the column name to the end
                __columnNames[job.Id].AddEnd(name);
            }
            else
            {
                __columnNames[job.Id] = new CSVFileLine(name, 0);
                __columnNames[job.Id].Data = false;
                __columnNames[job.Id].Parse();
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Get the original column name
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _defn;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Common constructor function
        /// </summary>
        /// <param name="col">the column</param>
        void Construct(CSVJob job, string col)
        {
            _job = job;

            // trim the column name down
            string c = col.Trim().ToLower();

            // If this is an excel format column definition
            if (c.StartsWith("excel:"))
            {
                // This code takes something like aa and turns it into 26
                int val = 0;
                int multiplier = 1;

                string s = c.ToUpper().Substring(6).Trim();

                for (int i = s.Length - 1; i >= 0; i--)
                {
                    val = val + (Convert.ToByte(s[i]) - 64) * multiplier;
                    multiplier = multiplier * 10;
                }

                _number = val - 1;
                _defn = c;
            }
            // If this is a column name that should match a column in the first line of the file
            else if (c.StartsWith("colname:"))
            {
                _name = c.Substring(8).Trim();
                _defn = c;
            }
            // If this is a filter column name then it should also match a column in the first line of the file
            else if (c.StartsWith("filtercolumn:"))
            {
                _name = c.Substring(13).Trim();
                _defn = c;
            }
            else if (c.StartsWith("tagvalue:"))
            {
                _name = c.Substring(9).Trim();
                _tagValue = true;
                _defn = c;
            }
            else if (c.StartsWith("variable:") || c.StartsWith("constant:"))
            {
                _name = c.Substring(9).Trim();
                _variable = true;
                _defn = c;
            }
            // If this is a * then it means all columns
            else if (c == "*")
            {
                _all = true;
                _defn = c;
            }
            else if (c == "raw")
            {
                _number = int.MaxValue - 1;
                _defn = "raw";
            }
            // Should be a number
            else
            {
                try
                {
                    _number = Convert.ToInt32(c);
                    _resetindex = __resetindex;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error reading source column : " + col, ex);
                }
                _defn = _number.ToString();
            }
        }

        /// <summary>
        /// Create a column number from a string
        /// </summary>
        /// <param name="s">Column name string</param>
        public ColumnNumber(CSVJob job, string s)
        {
            Construct(job, s);
        }

        /// <summary>
        /// Parses a column number node.
        /// </summary>
        /// <param name="n">Node to parse</param>
        public ColumnNumber(CSVJob job, XmlNode n)
        {
            Construct(job, n.InnerText);
        }
        #endregion

        List<string> ParseNames(string _name)
        {
            List<string> names = new List<string>();

            string s = _name;

            while (s.Contains("|colname:"))
            {
                names.Add(s.Substring(0, s.IndexOf("|colname:")));
                s = s.Substring(s.IndexOf("|colname:") + 9);
            }

            List<string> res = new List<string>();
            res.Add(s);
            res.AddRange(names);

            return res;
        }

        #region Accessors
        /// <summary>
        /// Get the column number
        /// </summary>
        public int Number
        {
            get
            {
                // If it is an all then you cant ask for it
                if (_all)
                {
                    throw new ApplicationException("Column does not have a number ... all columns specified");
                }

                if (_tagValue)
                {
                    throw new ApplicationException("Column does not have a number ... tagvalue specified");
                }

                if (_variable)
                {
                    throw new ApplicationException("Column does not have a number ... variable specified");
                }

                if (_number < 0 && _number != int.MinValue)
                {
                    if (!__columnNamesSet)
                    {
                        throw new ApplicationException("Can't convert negative column number unless the column titles are already set.");
                    }
                    else
                    {
                        if ((_number) * -1 > __columnNames[_job.Id].Count)
                        {
                            throw new ApplicationException("Input column titles did not have enough columns for " + _number.ToString() + " to be a valid column number.\n" + __columnNames.ToString());
                        }
                        int old = _number;
                        _number = __columnNames[_job.Id].Count + _number;
                        CSVProcessor.DisplayError(ANSIHelper.Information + "Column Number '" + old.ToString() + "' re-interpreted as " + _number.ToString() + ".\n" + ANSIHelper.Normal);
                    }
                }

                // If we have no number or if the column names have been reset then try to decode the name
                if (_number == int.MinValue || __resetindex > _resetindex)
                {
                    if (_defn.Contains("colname:") || _defn.Contains("filtercolumn:"))
                    {
                        _number = int.MinValue;

                        if (_job != null)
                        {
                            List<string> lookupnames = ParseNames(_name);

                            foreach (string lookupname in lookupnames)
                            {
                                // search the first line columns
                                int col = 0;
                                foreach (string colname in __columnNames[_job.Id].Cols)
                                {
                                    if (colname.ToLower().Trim() == lookupname)
                                    {
                                        _number = col;
                                        _resetindex = __resetindex;
                                        CSVProcessor.DisplayError(ANSIHelper.Information + "Column Number '" + _name + "' --> '" + lookupname + "' assigned to column " + _number.ToString() + ".\n" + ANSIHelper.Normal);
                                        break;
                                    }
                                    else
                                    {
                                        col++;
                                    }
                                }

                                // Dynamically add !FOREACH! to the end if necessary
                                if (_number == int.MinValue && lookupname == "!foreach!")
                                {
                                    __columnNames[_job.Id].AddEnd("!FOREACH!");
                                    _number = col;
                                    _resetindex = __resetindex;
                                    CSVProcessor.DisplayError(ANSIHelper.Information + "Column Number '" + _name + "' --> '" + lookupname + "' assigned to column " + _number.ToString() + ".\n" + ANSIHelper.Normal);
                                }

                                if (_number != int.MinValue)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                // If we did not find a match
                if (_number == int.MinValue)
                {
                    if (_job.JobParameters.RejectOnUnknownColumn)
                    {
                        throw new ApplicationException("Error decoding column name : " + _name);
                    }
                    else
                    {
                        _number = int.MaxValue;
                        _resetindex = __resetindex;
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Column Number '" + _defn + "' is not known. A blank column will always be returned.\n" + ANSIHelper.Normal);
                    }
                }

                // We found it
                return _number;
            }
        }

        public bool TagValue
        {
            get
            {
                return _tagValue;
            }
        }

        public bool Variable
        {
            get
            {
                return _variable;
            }
        }

        public string GetVariableValue(CSVProcess p)
        {
            if (Name.Length == 2 && Name.ToLower().StartsWith("v"))
            {
                return systemvariable.GetVariable(Name);
            }
            else
            {
                try
                {
                    return p.ProcessParameters.PromptVariable(Name);
                }
                catch
                {
                    try
                    {
                        return p.JobParameters.PromptVariable(Name);
                    }
                    catch (Exception ex2)
                    {
                        try
                        {
                            return Constants.Constant(Name);
                        }
                        catch (Exception)
                        {
                            try
                            {
                                return RuntimeVariable.GetValue(Name);
                            }
                            catch (Exception)
                            {
                                // we dont recognise that variable
                                throw new ApplicationException("Variable name not recognised : " + Name, ex2);
                            }
                        }
                    }
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Check if this column represents all columns
        /// </summary>
        public bool All
        {
            get
            {
                return _all;
            }
        }
        #endregion
    }
}
