using System;
using System.Xml;

namespace CSVProcessor
{
	/// <summary>
	/// Summary description for FTPFile.
	/// </summary>
	public class FTPFile
	{
		static string __defaultUser;
		static string __defaultPassword;
		string _path = string.Empty;
		string _dir = string.Empty;
		string _pattern = string.Empty;
		string _user = string.Empty;
		string _password = string.Empty;

		void Download()
		{
		}

		public static void SetDefaultUser(string user)
		{
			__defaultUser = user;
		}

		public static void SetDefaultPassword(string password)
		{
			__defaultPassword = password;
		}

		public FTPFile(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
		{
			foreach (XmlNode n1 in node.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "path")
					{
						_path = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Path>" + _path + "</Path>");
						}
					}
					else if (n1.Name.ToLower() == "downloaddirectory")
					{
						_dir = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<DownloadDirectory>" + _dir + "</DownloadDirectory>");
						}
					}
					else if (n1.Name.ToLower() == "pattern")
					{
						_pattern = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Pattern>" + _pattern + "</Pattern>");
						}
					}
					else if (n1.Name.ToLower() == "user")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "    <User>");
						}
						StringOrColumn sc = new StringOrColumn(n1, j, null, null, tracexml, prefix + "   ");
						_user = sc.Value();
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "    </User>");
						}
					}
					else if (n1.Name.ToLower() == "password")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "    <Password>");
						}
						StringOrColumn sc = new StringOrColumn(n1, j, null, null, tracexml, prefix + "   ");
						_password = sc.Value();
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "    </Password>");
						}
					}
				}
			}
		}
	}
}
