using System;
using System.Xml;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

namespace CSVProcessor
{
    /// <summary>
    /// Class holding all our tag values
    /// </summary>
    public class TagValues
    {
        CSVProcess _p = null; // owning processor
        Dictionary<string, TagValue> _tagValues = new Dictionary<string, TagValue>(); // the table of tag values
        List<string> _tagvaluesinorder = new List<string>(); // the order in which to process tag values - required because hash tables dont define retrieval order

        /// <summary>
        /// The state the line is in when it is passed in for tag value processing
        /// </summary>
        public enum State
        {
            FIRSTLINE, PROCESSED, FILTERED, FIRSTLINEOFFIRSTFILE
        };

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            foreach (TagValue tv in _tagValues.Values)
            {
                dotWriter.WriteLine(prefix + tv.DotName + " [shape=trapezium];");
                dotWriter.WriteLine(prefix + tv.DotName + " [label=\"" + tv.Name + "\"];");
            }
        }

        #region Accessors

        /// <summary>
        /// Get the value of a tag value
        /// </summary>
        public string GetTagValue(string index, ColumnLine l)
        {
            // if we have a tag value with that name return it
            try
            {
                TagValue o = _tagValues[index];
                if (o.HasKey)
                {
                    string key = ((TagValue)o).UseKey(l);
                    return ((TagValue)o).KeyedValue(key);
                }
                else
                {
                    return ((TagValue)o).Value;
                }
            }
            catch
            {
                return "Unknown tag value : " + index;
            }
        }

        #endregion

        #region Public Functions

        public void SetOutputDefinition(OutputDefinition od)
        {
            foreach (string tvn in _tagvaluesinorder)
            {
                // ask the tag value to process the line
                ((TagValue)(_tagValues[tvn])).SetOutputDefinition(od);
            }
        }

        public void ClearIfAppropriate(ColumnLine l)
        {
            // check if any of our tag values are interested in this line
            foreach (string tvn in _tagvaluesinorder)
            {
                // ask the tag value to process the line
                ((TagValue)(_tagValues[tvn])).ClearIfAppropriate(l);
            }
        }

        /// <summary>
        /// Process a line
        /// </summary>
        /// <param name="l"></param>
        /// <param name="line"></param>
        /// <param name="s"></param>
        public void Process(ColumnLine l, long line, State s)
        {
            // check if any of our tag values are interested in this line
            foreach (string tvn in _tagvaluesinorder)
            {
                // ask the tag value to process the line
                ((TagValue)(_tagValues[tvn])).Process(l, line, s);
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Create a tag value
        /// </summary>
        /// <param name="p"></param>
        public TagValues(CSVProcess p)
        {
            // save the owning processor
            _p = p;
        }

        /// <summary>
        /// Create a tag value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="p"></param>
        public TagValues(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<TagValues>");
            }

            // save the owning processor
            _p = p;

            // process each node
            foreach (XmlNode n1 in node.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "tagvalue")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TagValue>");
                        }
                        try
                        {
                            // create the tag value
                            TagValue tv = new TagValue(n1, j, p, tracexml, prefix + "    ");

                            // add it to our list
                            _tagValues.Add(tv.Name, tv);
                            _tagvaluesinorder.Add(tv.Name);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing Tag Value", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </TagValue>");
                        }
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "TagValues::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</TagValues>");
            }
        }
        #endregion
    }

    /// <summary>
    /// A single tag value.
    /// </summary>
    public class TagValue
    {
        CSVProcess _p = null; // owning processor
        bool _processFiltered = false; // true if we should process filtered lines
        bool _processFirstLine = false; // true if we should process the first line of the file
        bool _processFirstLineOfFirstFile = false; // true if we should process the first line of the first file
        string _name = string.Empty; // the name of the tage value
        Regex _colRegex = null; // the regular expression to apply against the provided column to determine if the tag value should be processed
        ColumnNumber _col = null; // the column to apply the colRegex to
        Regex _lineRegex = null; // the regular expression to apply against the raw line
        Regex _clearAfterRegex = null; // the regular expression to apply against the raw line to see if the value should be cleared
        Column _output = null; // the outputcolumn to use to extract the tag value
        Column _sourcekey = null; // the outputcolumn to use to extract the key for the tag value when saving it
        Column _usekey = null; // the outputcolumn to use to extract the key for the tag value when acessing it
        string _value = string.Empty; // the current value of the tag value
        Dictionary<string, string> _values = null; // hashtable to store keyed values
        bool _trace = false;
        string _id = string.Empty;
        int _initialCapacity = 1000;
        bool _useSpecialHashtable = false;
        bool _timePerformance = false;
        protected long starttime = 0;
        string _perfcounter = string.Empty;
        string _default = "";

        public string DotName
        {
            get
            {
                return "TagValue_" + Name;
            }
        }

        #region Accessors
        /// <summary>
        /// The tag values name
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
        }

        public bool HasKey
        {
            get
            {
                return (_sourcekey != null);
            }
        }

        /// <summary>
        /// The tag values current value
        /// </summary>
        public string Value
        {
            get
            {
                return _value;
            }
        }

        public string UseKey(ColumnLine l)
        {
            return _usekey.Process(l).ToString(false);
        }

        public string KeyedValue(string key)
        {
            PreAccess();
            string rc = _default;
            try
            {
                rc = _values[key];
            }
            catch
            {
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "TagValue accessed :" + _name + ":" + Id + "  [" + key + "] -> '" + rc + "'" + ANSIHelper.White);
            }

            PostAccess();
            return rc;
        }
        #endregion

        #region Public Functions

        public void ClearIfAppropriate(ColumnLine l)
        {
            if (_clearAfterRegex != null && _clearAfterRegex.IsMatch(l.Raw))
            {
                if (_sourcekey != null)
                {
                    string key = _sourcekey.Process(l).ToString(false);
                    _values[key] = _default;
                }
                else
                {
                    // process it to get our new value
                    _value = _default;
                }
            }
        }

        public void SetOutputDefinition(OutputDefinition od)
        {
            _output.SetOutputDefinition(od);
            if (_sourcekey != null)
            {
                _sourcekey.SetOutputDefinition(od);
            }
            if (_usekey != null)
            {
                _usekey.SetOutputDefinition(od);
            }
        }

        protected void PreProcess()
        {
            Performance.QueryPerformanceCounter(ref starttime);
        }

        protected void PostProcess()
        {
            if (_timePerformance)
            {
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                Performance.IncrementBy("TagValue Store Average Duration", _perfcounter, endtime - starttime);
                Performance.Increment("TagValue Store Average Duration Base", _perfcounter);
            }
        }

        protected void PreAccess()
        {
            Performance.QueryPerformanceCounter(ref starttime);
        }

        protected void PostAccess()
        {
            if (_timePerformance)
            {
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                Performance.IncrementBy("TagValue Access Average Duration", _perfcounter, endtime - starttime);
                Performance.Increment("TagValue Access Average Duration Base", _perfcounter);
            }
        }

        /// <summary>
        /// Process a line to see if we need to extract a new tag value
        /// </summary>
        /// <param name="l"></param>
        /// <param name="line"></param>
        /// <param name="s"></param>
        public void Process(ColumnLine l, long line, TagValues.State s)
        {
            PreProcess();

            string trace = string.Empty;

            // if this is a line we are to process
            if (s == TagValues.State.PROCESSED || (s == TagValues.State.FILTERED && _processFiltered) ||
                (s == TagValues.State.FIRSTLINE && _processFirstLine) || (s == TagValues.State.FIRSTLINEOFFIRSTFILE && _processFirstLineOfFirstFile))
            {
                // if one of our regular expressions matches ... or none are set
                if ((_colRegex != null && _colRegex.IsMatch(l[_col.Number])) ||
                    (_lineRegex != null && _lineRegex.IsMatch(l.Raw)) ||
                    (_colRegex == null & _lineRegex == null))
                {
                    // if the have a column definition
                    if (_output != null)
                    {
                        if (_sourcekey != null)
                        {
                            string key = _sourcekey.Process(l).ToString(false);
                            string s1 = _output.Process(l).ToString(false);
                            _values[key] = s1;
                            trace = "[" + key + "] = '" + s1 + "'";
                            if (_timePerformance)
                            {
                                Performance.SetCounter("Tagged Values", _perfcounter, _values.Count);
                            }
                        }
                        else
                        {
                            // process it to get our new value
                            _value = _output.Process(l).ToString(false);
                            trace = _value;
                        }
                    }
                    else
                    {
                        // no so just the default
                        _value = _default;
                    }

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "TagValue set :" + _name + ":" + Id + "  " + trace + ANSIHelper.White);
                    }
                }
            }
            else
            {
                // we can ignore this line
            }

            PostProcess();
        }
        public string Id
        {
            get
            {
                return "{" + _id + "}";
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Create a tag value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="p"></param>
        public TagValue(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            // save the owning processor
            _p = p;

            // process each node
            foreach (XmlNode n1 in node.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "name")
                    {
                        // save the name
                        _name = n1.InnerText.ToLower();
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Name>" + _name + "</Name>");
                        }
                    }
                    else if (n1.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<TimePerformance/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "default")
                    {
                        _default = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Default>" + _default + "</Default>");
                        }
                    }
                    else if (n1.Name.ToLower() == "initialcapacity")
                    {
                        try
                        {
                            _initialCapacity = Convert.ToInt32(n1.InnerText);
                            _useSpecialHashtable = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InitialCapacity>" + _initialCapacity.ToString() + "</InitialCapacity>");
                            }
                        }
                        catch (Exception e)
                        {
                            throw new ApplicationException("TagValue::Constructor error parsing initial capacity.", e);
                        }
                    }
                    else if (n1.Name.ToLower() == "sourcekey")
                    {
                        foreach (XmlNode n2 in n1.ChildNodes)
                        {
                            if (n2.NodeType != XmlNodeType.Comment)
                            {
                                // check this has not already been defined
                                if (_sourcekey != null)
                                {
                                    throw new ApplicationException("Only one key can be specified in a Tag Value.");
                                }
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "<SourceKey>");
                                }

                                // save the output
                                _sourcekey = Column.Create(n2, j, p, null, tracexml, prefix + "  ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "</SourceKey>");
                                }
                            }
                        }
                    }
                    else if (n1.Name.ToLower() == "accesskey")
                    {
                        foreach (XmlNode n2 in n1.ChildNodes)
                        {
                            if (n2.NodeType != XmlNodeType.Comment)
                            {
                                // check this has not already been defined
                                if (_usekey != null)
                                {
                                    throw new ApplicationException("Only one key can be specified in a Tag Value.");
                                }

                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "<AccessKey>");
                                }
                                // save the output
                                _usekey = Column.Create(n2, j, p, null, tracexml, prefix + "  ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "</AccessKey>");
                                }
                            }
                        }
                    }
                    else if (n1.Name.ToLower() == "colregex")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<ColRegex>");
                        }

                        foreach (XmlNode n2 in n1.ChildNodes)
                        {
                            if (n2.NodeType != XmlNodeType.Comment)
                            {
                                if (n2.Name.ToLower() == "column")
                                {
                                    // save the column
                                    _col = new ColumnNumber(_p.Job, n2.InnerText);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  <Column>" + _col.ToString() + "</Column>");
                                    }
                                }
                                else if (n2.Name.ToLower() == "regex")
                                {
                                    // save the regular expression to apply to the column
                                    try
                                    {
                                        _colRegex = new Regex(n2.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                                    }
                                    catch (Exception e)
                                    {
                                        throw new ApplicationException("TagValue error parsing column regex : " + n2.InnerText, e);
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  <Regex>" + _colRegex.ToString() + "</Regex>");
                                    }
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</ColRegex>");
                        }
                    }
                    else if (n1.Name.ToLower() == "lineregex")
                    {
                        try
                        {
                            // save the regular expression to apply to the line
                            _lineRegex = new Regex(n1.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<LineRegex>" + _lineRegex.ToString() + "</LineRegex>");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing lineregex : " + n1.InnerText, ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "clearafterregex")
                    {
                        try
                        {
                            // save the regular expression to apply to the line
                            _clearAfterRegex = new Regex(n1.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<ClearAfterRegex>" + _lineRegex.ToString() + "</ClearAfterRegex>");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing clearafterregex : " + n1.InnerText, ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "output")
                    {
                        foreach (XmlNode n2 in n1.ChildNodes)
                        {
                            if (n2.NodeType != XmlNodeType.Comment)
                            {
                                // check this has not already been defined
                                if (_output != null)
                                {
                                    throw new ApplicationException("Only one output can be specified in a Tag Value.");
                                }
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "<Output>");
                                }

                                // save the output
                                _output = Column.Create(n2, j, p, null, tracexml, prefix + "  ");

                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "</Output>");
                                }
                            }
                        }
                    }
                    else if (n1.Name.ToLower() == "processfiltered")
                    {
                        _processFiltered = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<ProcessFiltered/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "processfirstline")
                    {
                        _processFirstLine = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<ProcessFirstLine/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "processfirstlineoffirstfile")
                    {
                        _processFirstLineOfFirstFile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<ProcessFirstLineOfFirstFile/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "TagValue::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
                    }
                }
            }

            if (_sourcekey != null && _usekey == null)
            {
                throw new ApplicationException("TagValue::Constructor : If you have a SourceKey then you must have an AccessKey.");
            }

            if (_output == null)
            {
                throw new ApplicationException("TagValue::Constructor : No output specified.");
            }

            _value = _default;

            if (_useSpecialHashtable)
            {
                _values = new Dictionary<string, string>(_initialCapacity);
            }
            else
            {
                _values = new Dictionary<string, string>();
            }

            if (_timePerformance)
            {
                _perfcounter = _p.Id + "_" + _name;
                Performance.CreateCounterInstance("CSVProcessor_TagValues", "Tagged Values", _perfcounter);
                Performance.CreateCounterInstance("CSVProcessor_TagValues", "TagValue Store Average Duration", _perfcounter);
                Performance.CreateCounterInstance("CSVProcessor_TagValues", "TagValue Store Average Duration Base", _perfcounter);
                Performance.CreateCounterInstance("CSVProcessor_TagValues", "TagValue Access Average Duration", _perfcounter);
                Performance.CreateCounterInstance("CSVProcessor_TagValues", "TagValue Access Average Duration Base", _perfcounter);
            }
        }
        #endregion
    }
}
