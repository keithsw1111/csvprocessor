// Code last tidied up September 4, 2010

using System;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.IO;

namespace CSVProcessor
{
	/// <summary>
	/// A column evaluated before filters
	/// </summary>
	class FilterColumn
	{
		#region Member Variables
		string _name = string.Empty; // name of the column
		Column _col = null; // the output column that produces the value
		bool _trace = false; // true if we are to trace output
		string _id = null; // id of the filter column
		#endregion

		public void WriteDot(StreamWriter dotWriter, string prefix)
		{
			dotWriter.WriteLine(prefix + "   subgraph filtercolumn_" + Id + " { color=lightgrey; label = \"" + Id + "\";");
			_col.WriteDot(dotWriter, prefix + "   ");
			dotWriter.WriteLine(prefix + "   }");
		}


		#region Accessors
		/// <summary>
		/// Filter column name
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}
		public string Id
		{
			get
			{
				return _id;
			}
		}
		#endregion

		#region Public Functions
		/// <summary>
		/// Generate a value for this filter variable
		/// </summary>
		/// <param name="line">input line to use</param>
		/// <returns>The filter variable value</returns>
		public string Process(ColumnLine line)
		{
			string s = _col.Process(line).ToString(false);

			if (_trace)
			{
				CSVProcessor.DisplayError(ANSIHelper.Trace + "FilterColumn {" + _id + "}: " + _name + " = '" + s + "'" + ANSIHelper.White);
			}

			// generate it using the column definition
			return s;
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Filter Column Constructor
		/// </summary>
		/// <param name="node">XML node containing the definition</param>
		public FilterColumn(CSVJob j, XmlNode node, bool tracexml, string prefix)
		{
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "<FilterColumn>");
			}

			foreach (XmlNode n in node.ChildNodes)
			{
				if (n.NodeType != XmlNodeType.Comment)
				{
					if (n.Name.ToLower() == "name")
					{
						// save the filter column name
						_name = n.InnerText.ToLower();

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Name>" + _name + "</Name>");
						}
					}
					else if (n.Name.ToLower() == "id")
					{
						// save the filter column name
						_id = n.InnerText.ToLower();

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
						}
					}
					else if (n.Name.ToLower() == "trace")
					{
						_trace = true;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Trace/>");
						}

					}
					else
					{
						if (_col != null)
						{
							throw new ApplicationException("{" + _id + "}Only one column definition allowed in a filter column.");
						}
						// create a column
						_col = Column.Create(n, j, null, null, tracexml, prefix + "  ");
					}
				}
			}

			// check we have a name
			if (_name == string.Empty)
			{
				throw new ApplicationException("{" + _id + "} Filter column must have a name.");
			}

			// check we have a column
			if (_col == null)
			{
				throw new ApplicationException("{" + _id + "} Filter column " + _name + " must have a column definition.");
			}

			// add it to the list
			ColumnNumber.Add(j, _name);

			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "</FilterColumn>");
			}
		}
		#endregion
	}

	/// <summary>
	/// Holder of all the filter variables.
	/// </summary>
	public class FilterVars
	{
		List<FilterColumn> _filtercols = new List<FilterColumn>(); // holds the filter column definitions

		#region Public Functions
		/// <summary>
		/// Process all the filter columns
		/// </summary>
		/// <param name="l">Input line to use</param>
		public void Process(ColumnLine l)
		{
			// Process each filter column
			foreach (FilterColumn c in _filtercols)
			{
				// add its value to the end of the input line
				l.AddEnd(c.Process(l));
			}
		}

		public void WriteDot(StreamWriter dotWriter, string prefix)
		{
			dotWriter.WriteLine(prefix + "   subgraph filtercolumns { color=lightgrey; label = \"FilterColumns\";");

			foreach (FilterColumn fc in _filtercols)
			{
				fc.WriteDot(dotWriter, prefix + "   ");
			}

			dotWriter.WriteLine(prefix + "   }");
		}

		#endregion

		#region Constructors
		/// <summary>
		/// Create all the filter variables
		/// </summary>
		/// <param name="doc">Xml document to create the filter variables from</param>
		public FilterVars(CSVJob j, XmlNode node, bool tracexml, string prefix)
		{
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "<FilterColumns>");
			}

			foreach (XmlNode n3 in node.ChildNodes)
			{
				if (n3.NodeType != XmlNodeType.Comment)
				{
					if (n3.Name.ToLower() == "filtercolumn")
					{
						// create a filter column
						FilterColumn fc = new FilterColumn(j, n3, tracexml, prefix + "  ");

						// add it to the list
						_filtercols.Add(fc);
					}
					else
					{
						CSVProcessor.DisplayError(ANSIHelper.Warning + "FilterVars::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
					}
				}
			}

			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "</FilterColumns>");
			}

		}
		#endregion
	}
}
