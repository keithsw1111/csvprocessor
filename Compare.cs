using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;

namespace CSVProcessor
{
    /// <summary>
    /// Summary description for Totals.
    /// </summary>
    public class Compare
    {
        #region Member Variables
        static ColumnLine UPDATE = new CSVFileLine("UPDATE", 0);
        static ColumnLine INSERT = new CSVFileLine("INSERT", 0);
        static ColumnLine DELETE = new CSVFileLine("DELETE", 0);
        static ColumnLine MATCH = new CSVFileLine("MATCH", 0);
        static ColumnLine BLANK = new CSVFileLine(string.Empty, 0);
        static ColumnLine LT = new CSVFileLine("<", 0);
        static ColumnLine GT = new CSVFileLine(">", 0);
        static ColumnLine LTLT = new CSVFileLine("<<", 0);
        static ColumnLine GTGT = new CSVFileLine(">>", 0);
        static ColumnLine EQ = new CSVFileLine("==", 0);

        FilterDefinition _fd = null;
        string _compareTo = string.Empty;
        List<ColumnNumberOrColumn> _key = new List<ColumnNumberOrColumn>();
        bool _sortedByKey = false;
        bool _deltaRecords = false;
        bool _diffRecords = false;
        bool _diffFilenames = false;
        bool _differenceAddedToColumn = false;
        bool _trace = false;
        bool _showNew = false;
        bool _showDeleted = false;
        bool _showMatched = false;
        bool _showUpdated = false;
        bool _applyblankcolfix = false;
        bool _trimkeys = false;
        long _inserted = 0;
        long _deleted = 0;
        long _updated = 0;
        long _matched = 0;
        bool _showStats = false;
        bool _showStatsAll = false;
        List<ColumnNumber> _ignoreColumns = new List<ColumnNumber>();
        string _id = string.Empty;
        CSVProcess _csvProcess = null;
        DecodeTable _lookup = null;
        StreamReader _compareToFile = null;
        CSVFileLine _currentLine = null;
        string _currentKey = string.Empty;
        bool _caseSensitive = false;
        bool _blankIgnoredColumns = false;
        ColumnLine _newFile = null;
        ColumnLine _oldFile = null;
        int[] _countMismatch = null;
        int _maxcols = 0;
        bool _showMismatchColumnStats = false;
        bool _useinputrules = false;
        bool _failonmismatch = false;
        int _unique = UniqueIdClass.Allocate();
        #endregion

        public string UniqueId
        {
            get
            {
                return "COMP" + _unique.ToString();
            }
        }

        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   " + UniqueId + " [label=\"Compare::" + Id + "\"];");
        }

        #region Accessors
        /// <summary>
        /// The totals Id
        /// </summary>
        public string Id
        {
            get
            {
                if (_id != null && _id != string.Empty)
                {
                    return _id;
                }
                else
                {
                    return _csvProcess.Id;
                }
            }
        }
        #endregion

        #region Constructors
        static Compare()
        {
            INSERT.Parse();
            DELETE.Parse();
            UPDATE.Parse();
            MATCH.Parse();
            BLANK.Parse();
            GT.Parse();
            GTGT.Parse();
            LT.Parse();
            LTLT.Parse();
            EQ.Parse();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="doc">Xml Document holding the output definition</param>
        public Compare(XmlNode node, CSVJob j, CSVProcess process, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Compare>");
            }

            _csvProcess = process;

            // look at the children
            foreach (XmlNode n3 in node.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "keycols")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <KeyCols>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "column")
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Column>");
                                    }
                                    _key.Add(new ColumnNumberOrColumn(n4, j, process, null, tracexml, prefix + "   "));
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     </Column>");
                                    }
                                }
                                else
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Compare::Constructor Ignoring xml node : " + n4.Name + ANSIHelper.White);
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </KeyCols>");
                        }
                    }
                    else if (n3.Name.ToLower() == "id")
                    {
                        _id = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</id>");
                        }
                    }
                    else if (n3.Name.ToLower() == "compareto")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "   <CompareTo>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, _csvProcess, null, tracexml, prefix + "   ");
                        _compareTo = sc.Value();
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "   </CompareTo>");
                        }
                    }
                    else if (n3.Name.ToLower() == "sortedbykey")
                    {
                        _sortedByKey = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SortedByKey/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "deltarecords")
                    {
                        _deltaRecords = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DeltaRecords/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "diffrecords")
                    {
                        _diffRecords = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DiffRecords/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "failonmismatch")
                    {
                        _failonmismatch = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnMismatch/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "difffilenames")
                    {
                        _diffFilenames = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DiffFilenames/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "trimkeys")
                    {
                        _trimkeys = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TrimKeys/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "adddifference")
                    {
                        _differenceAddedToColumn = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <AddDifference/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Trace/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "shownew")
                    {
                        _showNew = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowNew/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showstats")
                    {
                        _showStats = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowStats/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showstatsall")
                    {
                        _showStats = true;
                        _showStatsAll = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowStatsAll/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showmismatchedcolumnstats")
                    {
                        _showMismatchColumnStats = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowMismatchedColumnStats/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "casesensitive")
                    {
                        _caseSensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <CaseSensitive/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "applyblankcolfix")
                    {
                        _applyblankcolfix = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ApplyBlankColFix/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showdeleted")
                    {
                        _showDeleted = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowDeleted/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showmatched")
                    {
                        _showMatched = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowMatched/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "showupdated")
                    {
                        _showUpdated = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ShowUpdated/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "useinputparsingrules")
                    {
                        _useinputrules = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <UseInputParsingRules/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "ignorecolumns")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <IgnoreColumns>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "column")
                                {
                                    _ignoreColumns.Add(new ColumnNumber(_csvProcess.Job, n4.InnerText));
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Column>" + n4.InnerText + "</Column>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "blankignoredcolumns")
                                {
                                    _blankIgnoredColumns = true;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <BlankIgnoredColumns/>");
                                    }
                                }
                                else
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Compare::Constructor Ignoring xml node : " + n4.Name + ANSIHelper.White);
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </IgnoreColumns>");
                        }
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Compare::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Compare>");
            }
            if (_deltaRecords && _diffRecords)
            {
                throw new ApplicationException("Compare can't have both <DeltaRecords/> and <DiffRecords/>");
            }
            if (_deltaRecords && _diffFilenames)
            {
                throw new ApplicationException("Compare can't have both <DeltaRecords/> and <DiffFilenames/>");
            }
            if (_diffFilenames && _diffRecords)
            {
                throw new ApplicationException("Compare can't have both <DiffFilenames/> and <DiffRecords/>");
            }
            else if (!_deltaRecords && !_diffRecords && !_diffFilenames)
            {
                _diffRecords = true;
                CSVProcessor.DisplayError(prefix + ANSIHelper.Information + "</DiffRecords/> assumed." + ANSIHelper.Normal);
            }
            if (_compareTo == string.Empty)
            {
                throw new ApplicationException("Compare needs a <CompareTo> file.");
            }
            if (_sortedByKey)
            {
                _compareToFile = new StreamReader(_compareTo);

                if (_csvProcess.ProcessParameters.PassThroughFirstLine)
                {
                    _compareToFile.ReadLine();
                }
                else if (_csvProcess.ProcessParameters.SkipNLines > 0)
                {
                    for (long l = 0; l < _csvProcess.ProcessParameters.SkipNLines; l++)
                    {
                        _compareToFile.ReadLine();
                    }
                }

                ReadNext();
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Loading " + _compareTo + ANSIHelper.Normal);
                List<string> kk = new List<string>();
                foreach (var k in _key)
                {
                    kk.Add(k.ColumnNumber.Number.ToString());
                }
                if (_useinputrules)
                {
                    _lookup = new DecodeTable(_compareTo, kk, false, _caseSensitive, process.Input.Delimiter, process.Input.Quote, process.Input.QuoteEscape);
                }
                else
                {
                    _lookup = new DecodeTable(_compareTo, kk, false, _caseSensitive);
                }
                CSVProcessor.DisplayError(ANSIHelper.Information + _compareTo + " Loaded." + ANSIHelper.Normal);
            }

            _newFile = new CSVFileLine(_compareTo, 0);
            _newFile.Parse();

        }
        #endregion

        #region Public Methods

        public void SetFilter(FilterDefinition fd)
        {
            _fd = fd;
        }

        void DisplayInserted(ColumnLine l)
        {
            l = BlankIgnoredColumns(l);
            if (_deltaRecords)
            {
                l.AddBeginning(INSERT);
                _csvProcess.DisplayOutput(l);
            }
            else if (_diffRecords)
            {
                l.AddBeginning(GTGT);
                _csvProcess.DisplayOutput(l);
            }
            else
            {
                l.AddBeginning(_newFile);
                l.AddBeginning(GTGT);
                _csvProcess.DisplayOutput(l);
            }

            if (_failonmismatch)
            {

                Environment.Exit(99);
            }
        }

        void DisplayDeleted(ColumnLine l)
        {
            l = BlankIgnoredColumns(l);
            if (_deltaRecords)
            {
                l.AddBeginning(DELETE);
                _csvProcess.DisplayOutput(l);
            }
            else if (_diffRecords)
            {
                l.AddBeginning(LTLT);
                _csvProcess.DisplayOutput(l);
            }
            else
            {
                l.AddBeginning(_oldFile);
                l.AddBeginning(LTLT);
                _csvProcess.DisplayOutput(l);
            }

            if (_failonmismatch)
            {
                Environment.Exit(99);
            }
        }

        ColumnLine BlankIgnoredColumns(ColumnLine l)
        {
            if (_blankIgnoredColumns)
            {
                foreach (ColumnNumber cn in _ignoreColumns)
                {
                    l[cn] = string.Empty;
                }
            }

            return l;
        }

        void ProcessMatched(string key, ColumnLine in1, ColumnLine lookup)
        {
            in1 = BlankIgnoredColumns(in1);
            if (_deltaRecords)
            {
                in1.AddBeginning(MATCH);
                _csvProcess.DisplayOutput(in1);
            }
            else if (_diffRecords)
            {
                in1.AddBeginning(EQ);
                _csvProcess.DisplayOutput(in1);
            }
            else
            {
                in1.AddBeginning(BLANK);
                in1.AddBeginning(EQ);
                _csvProcess.DisplayOutput(in1);
            }
        }

        void ProcessUpdated(string key, ColumnLine in1, ColumnLine lookup)
        {
            in1 = BlankIgnoredColumns(in1);
            lookup = BlankIgnoredColumns(lookup);
            if (_deltaRecords)
            {
                in1.AddBeginning(UPDATE);
                _csvProcess.DisplayOutput(in1);
            }
            else if (_diffRecords)
            {
                lookup.AddBeginning(LT);
                _csvProcess.DisplayOutput(lookup);

                in1.AddBeginning(GT);
                _csvProcess.DisplayOutput(in1);
            }
            else
            {
                lookup.AddBeginning(_newFile);
                lookup.AddBeginning(LT);
                _csvProcess.DisplayOutput(lookup);

                in1.AddBeginning(_oldFile);
                in1.AddBeginning(GT);
                _csvProcess.DisplayOutput(in1);
            }

            if (_failonmismatch)
            {
                Environment.Exit(99);
            }
        }

        void ReadNext()
        {
            if (_compareToFile.EndOfStream)
            {
                _currentKey = char.MaxValue.ToString();
            }
            else
            {
                _currentLine = new CSVFileLine(_compareToFile.ReadLine(), 0);
                _currentLine.Parse();
                _currentKey = string.Empty;
                foreach (ColumnNumberOrColumn cn in _key)
                {
                    string k = cn.Value(_currentLine);

                    if (_trimkeys)
                    {
                        k = k.Trim();
                    }

                    if (!_caseSensitive)
                    {
                        k = k.ToLower();
                    }

                    if (_applyblankcolfix && k == string.Empty)
                    {
                        // i do this
                        _currentKey += " ";
                    }
                    else
                    {
                        _currentKey += k;
                    }
                }
            }
        }

        /// <summary>
        /// This function takes an input line and adds it to the totals
        /// </summary>
        /// <param name="l">CSV input line</param>
        public void Process(ColumnLine ll)
        {
            if (_countMismatch == null)
            {
                _countMismatch = new int[ll.Count + 100];
            }

            if (ll.Count > _maxcols)
            {
                _maxcols = ll.Count;
            }

            if (_oldFile == null)
            {
                _oldFile = new CSVFileLine(ll.FileName, 0);
                _oldFile.Parse();
            }

            // copy the columnline to protect it
            ColumnLine l = new ColumnLine(ll);

            try
            {
                string key = string.Empty;
                foreach (ColumnNumberOrColumn cn in _key)
                {
                    string k = cn.Value(l);

                    if (!_caseSensitive)
                    {
                        k = k.ToLower();
                    }

                    if (_trimkeys)
                    {
                        k = k.Trim();
                    }

                    if (_applyblankcolfix && k == string.Empty)
                    {
                        key += " ";
                    }
                    else
                    {
                        key += k;
                    }
                }

                if (_sortedByKey)
                {
                    bool one = false;
                    while (key.CompareTo(_currentKey) >= 0 && _currentKey != char.MaxValue.ToString())
                    {
                        if (key == _currentKey)
                        {
                            one = true;

                            //match
                            bool changed = false;
                            for (int i = 0; changed == false && i < l.Count; i++)
                            {
                                bool skip = false;
                                foreach (ColumnNumber cn in _ignoreColumns)
                                {
                                    if (cn.Number == i)
                                    {
                                        skip = true;
                                    }
                                }

                                if (!skip)
                                {
                                    if ((!_caseSensitive && (l[i].ToLower() != _currentLine[i].ToLower())) ||
                                        (_caseSensitive && (l[i] != _currentLine[i])))
                                    {
                                        if (_trace)
                                        {
                                            CSVProcessor.DisplayError(ANSIHelper.Information + key + ": " + ColumnNumber.ColumnName(_csvProcess.Job, i) + " : " + i.ToString() + " : " + l[i] + " <> " + _currentLine[i] + ANSIHelper.Normal);
                                        }
                                        _countMismatch[i]++;
                                        if (_differenceAddedToColumn)
                                        {
                                            ColumnLine diff = new CSVFileLine(ColumnNumber.ColumnName(_csvProcess.Job, i) + " : " + i.ToString() + " : " + l[i] + " <> " + _currentLine[i], 0);
                                            diff.Parse();
                                            l.AddEnd(diff);
                                        }
                                        changed = true;
                                    }
                                }
                            }

                            if (changed)
                            {
                                _updated++;
                                if (_showUpdated)
                                {
                                    ProcessUpdated(key, l, _currentLine);
                                }
                            }
                            else
                            {
                                _matched++;
                                if (_showMatched)
                                {
                                    ProcessMatched(key, l, _currentLine);
                                }
                            }

                            ReadNext();
                        }
                        else if (key.CompareTo(_currentKey) < 0)
                        {
                            _inserted++;
                            //insert
                            if (_showNew)
                            {
                                DisplayInserted(l);
                            }
                        }
                        else
                        {
                            _deleted++;
                            //delete
                            if (_showDeleted)
                            {
                                if (_fd.In(_currentLine))
                                {
                                    DisplayDeleted(_currentLine);
                                }
                            }
                            ReadNext();
                        }
                    }

                    if (!one && key.CompareTo(_currentKey) < 0)
                    {
                        _inserted++;
                        //insert
                        if (_showNew)
                        {
                            DisplayInserted(l);
                        }
                    }
                }
                else
                {
                    ColumnLine lookup = null;
                    try
                    {
                        lookup = new ColumnLine((ColumnLine)_lookup.Decode(key, false));
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine("Not found in lookup: " + key);
                        lookup = null;
                    }
                    if (lookup == null)
                    {
                        _inserted++;
                        // insert
                        if (_showNew)
                        {
                            DisplayInserted(l);
                        }
                    }
                    else
                    {
                        // match
                        bool changed = false;
                        for (int i = 0; changed == false && i < l.Count; i++)
                        {
                            bool skip = false;
                            foreach (ColumnNumber cn in _ignoreColumns)
                            {
                                if (cn.Number == i)
                                {
                                    skip = true;
                                }
                            }

                            if (!skip)
                            {
                                if ((!_caseSensitive && (l[i].ToLower() != lookup[i].ToLower()) ||
                                    (_caseSensitive && (l[i] != lookup[i])))
                                   )
                                {
                                    if (_trace)
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Information + key + ": " + ColumnNumber.ColumnName(_csvProcess.Job, i) + " : " + i.ToString() + " : " + l[i] + " <> " + lookup[i] + ANSIHelper.Normal);
                                    }
                                    _countMismatch[i]++;
                                    if (_differenceAddedToColumn)
                                    {
                                        ColumnLine diff = new CSVFileLine(ColumnNumber.ColumnName(_csvProcess.Job, i) + " : " + i.ToString() + " : " + l[i] + " <> " + lookup[i], 0);
                                        diff.Parse();
                                        l.AddEnd(diff);
                                    }
                                    changed = true;
                                }
                            }
                        }
                        if (changed)
                        {
                            _updated++;
                            if (_showUpdated)
                            {
                                ProcessUpdated(key, l, lookup);
                            }
                        }
                        else
                        {
                            _matched++;
                            if (_showMatched)
                            {
                                ProcessMatched(key, l, lookup);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing line through compare.", ex);
            }
        }
        public void CloseUp()
        {
            CSVProcessor.DisplayError(ANSIHelper.Information + "Starting to write output." + ANSIHelper.Normal);
            if (_sortedByKey)
            {
                while (!_compareToFile.EndOfStream)
                {
                    if (_fd.In(_currentLine))
                    {
                        _deleted++;
                        if (_showDeleted)
                        {
                            DisplayDeleted(_currentLine);
                        }
                    }
                    ReadNext();
                }
                _compareToFile.Close();
            }
            else
            {
                // process each lookup entry that was not looked up ... delete
                // but only if they pass the filter
                foreach (DecodeTableEntry dte in _lookup.UnaccessedCodes)
                {
                    if (_fd.In((ColumnLine)dte.Value))
                    {
                        _deleted++;
                        if (_showDeleted)
                        {
                            DisplayDeleted((ColumnLine)dte.Value);
                        }
                    }
                }
            }

            if (_showStats)
            {
                ColumnLine clMismatch = new CSVFileLine("Compare Stats - Start", 0);
                clMismatch.Data = false;
                clMismatch.Parse();
                _csvProcess.DisplayOutput(clMismatch);

                if (_showStatsAll || _showNew)
                {
                    ColumnLine clInserted = new CSVFileLine("Inserted," + _inserted.ToString(), 0);
                    clInserted.Data = false;
                    clInserted.Parse();
                    _csvProcess.DisplayOutput(clInserted);
                }
                if (_showStatsAll || _showDeleted)
                {
                    ColumnLine clDeleted = new CSVFileLine("Deleted," + _deleted.ToString(), 0);
                    clDeleted.Data = false;
                    clDeleted.Parse();
                    _csvProcess.DisplayOutput(clDeleted);
                }
                if (_showStatsAll || _showUpdated)
                {
                    ColumnLine clUpdated = new CSVFileLine("Updated," + _updated.ToString(), 0);
                    clUpdated.Data = false;
                    clUpdated.Parse();
                    _csvProcess.DisplayOutput(clUpdated);
                }
                if (_showStatsAll || _showMatched)
                {
                    ColumnLine clMatched = new CSVFileLine("Matched," + _matched.ToString(), 0);
                    clMatched.Data = false;
                    clMatched.Parse();
                    _csvProcess.DisplayOutput(clMatched);
                }

                clMismatch = new CSVFileLine("Compare Stats - End", 0);
                clMismatch.Data = false;
                clMismatch.Parse();
                _csvProcess.DisplayOutput(clMismatch);
            }

            if (_showMismatchColumnStats)
            {
                ColumnLine clMismatch = new CSVFileLine("Compare Mismatched Column Stats - Start", 0);
                clMismatch.Data = false;
                clMismatch.Parse();
                _csvProcess.DisplayOutput(clMismatch);

                for (int i = 0; i < _maxcols; i++)
                {
                    clMismatch = new CSVFileLine("Column " + i.ToString() + "," + ColumnNumber.ColumnName(_csvProcess.Job, i) + "," + _countMismatch[i].ToString(), 0);
                    clMismatch.Data = false;
                    clMismatch.Parse();
                    _csvProcess.DisplayOutput(clMismatch);
                }

                clMismatch = new CSVFileLine("Compare Mismatched Column Stats - End", 0);
                clMismatch.Data = false;
                clMismatch.Parse();
                _csvProcess.DisplayOutput(clMismatch);
            }
            CSVProcessor.DisplayError(ANSIHelper.Information + "Output written." + ANSIHelper.Normal);
        }
        #endregion
    }
}