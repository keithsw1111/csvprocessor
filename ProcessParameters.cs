// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections;
using System.IO;

namespace CSVProcessor
{
    /// <summary>
    /// This class holds output file parameters.
    /// </summary>
    public class ProcessParameters
    {
        #region Member Variables
        long _skipnlines = 0; // should we skip the first line in the input file
        bool _skipblanklines = false; // should we ignore blank lines in the input file
        bool _passthroughfirstline = false; // should we pass through the first line to the output file
        bool _passthroughfirstlineoffirstfile = false; // should we pass through the first line to the output file of the first file
        string _givenfirstline = string.Empty; // A string to use for the first line
        string _givenlastline = string.Empty; // A string to use for the last line
        bool _disabled = false; // true if the processor is disabled
        Hashtable _promptVars = new Hashtable(); // prompt variables
        bool _failonfilter = false; // should we fail if a record gets filtered out
        bool _failonprocessingerror = false; // should we fail if we get an unexpected error procssing a record
        bool _usecolumntitles = false; // should we query the columns for their titles?
        #endregion

        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            foreach (PromptVariable p in _promptVars.Values)
            {
                dotWriter.WriteLine(p.DotName + " [shape=Msquare];");
                dotWriter.WriteLine(p.DotName + " [label=\"" + p.Name + "\"];");
            }
        }

        #region Accessors

        /// <summary>
        /// Should we use the tiles from the column definitions
        /// </summary>
        public bool UseColumTitles
        {
            get
            {
                return _usecolumntitles;
            }
        }

        /// <summary>
        /// Processor diabled
        /// </summary>
        public bool Disabled
        {
            get
            {
                return _disabled;
            }
            set
            {
                _disabled = value;
            }
        }

        /// <summary>
        /// Should we fail if a record is filtered out
        /// </summary>
        public bool FailOnFilter
        {
            get
            {
                return _failonfilter;
            }
        }

        /// <summary>
        /// Should we fail if we get an unexpected error processing an input record
        /// </summary>
        public bool FailOnProcessingError
        {
            get
            {
                return _failonprocessingerror;
            }
        }

        /// <summary>
        /// Get a prompt variable.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string PromptVariable(string name)
        {
            if (_promptVars.ContainsKey(name.ToLower()))
            {
                return (string)_promptVars[name.ToLower()];
            }
            else
            {
                throw new ApplicationException("Prompt Variable " + name + " not defined.");
            }
        }

        /// <summary>
        /// Should we skip the first line
        /// </summary>
        public long SkipNLines
        {
            get
            {
                return _skipnlines;
            }
        }

        /// <summary>
        /// Should we skip blank lines
        /// </summary>
        public bool SkipBlankLines
        {
            get
            {
                return _skipblanklines;
            }
        }

        /// <summary>
        /// What is our first line
        /// </summary>
        public string GivenFirstLine
        {
            get
            {
                return _givenfirstline;
            }
        }

        /// <summary>
        /// What is the last line
        /// </summary>
        public string GivenLastLine
        {
            get
            {
                return _givenlastline;
            }
        }

        /// <summary>
        /// Should we pass through the first line
        /// </summary>
        public bool PassThroughFirstLine
        {
            get
            {
                return _passthroughfirstline;
            }
        }

        /// <summary>
        /// Should we pass through the first line of the first file
        /// </summary>
        public bool PassThroughFirstLineOfFirstFile
        {
            get
            {
                return _passthroughfirstlineoffirstfile;
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Default Process Parameters
        /// </summary>
        public ProcessParameters()
        {
        }

        /// <summary>
        /// Load the FileParameters from the XML file
        /// </summary>
        /// <param name="n">Xml Node</param>
        public ProcessParameters(XmlNode n, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<ProcessParameters>");
            }

            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    // Skip the first line
                    if (n3.Name.ToLower() == "skipfirstline")
                    {
                        _skipnlines = 1;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipFirstLine/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "skipnlines")
                    {
                        try
                        {
                            _skipnlines = long.Parse(n3.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing lines to skip. " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipNLines>" + _skipnlines.ToString() + "</SkipNLines>");
                        }
                    }
                    // Pass through the first line
                    else if (n3.Name.ToLower() == "passthroughfirstline")
                    {
                        _passthroughfirstline = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PassThroughFirstLine/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "usecolumntitles")
                    {
                        _usecolumntitles = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <UseColumnTitles/>");
                        }
                    }
                    // Pass through the first line of the first file
                    else if (n3.Name.ToLower() == "passthroughfirstlineoffirstfile")
                    {
                        _passthroughfirstlineoffirstfile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PassThroughFirstLineOfFirstFile/>");
                        }
                    }
                    // Disabled processor
                    else if (n3.Name.ToLower() == "disable" || n3.Name.ToLower() == "disabled")
                    {
                        _disabled = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Disabled/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "failonfilter")
                    {
                        _failonfilter = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnFilter/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "failonprocessingerror")
                    {
                        _failonprocessingerror = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnProcessingError/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "promptvariables")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PromptVariables>");
                        }

                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                // prompt user for variable
                                if (n4.Name.ToLower() == "promptvariable")
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    <PromptVariable>");
                                    }

                                    string name = string.Empty;
                                    string message = string.Empty;
                                    string regex = string.Empty;
                                    string defaultValue = string.Empty;
                                    bool mask = false;

                                    foreach (XmlNode n5 in n4.ChildNodes)
                                    {
                                        if (n5.NodeType != XmlNodeType.Comment)
                                        {
                                            // read in the number of bytes to skip
                                            if (n5.Name.ToLower() == "name")
                                            {
                                                name = n5.InnerText.ToLower();
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Name>" + name + "</Name>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "message")
                                            {
                                                message = n5.InnerText;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Message>" + message + "</Message>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "mask")
                                            {
                                                mask = true;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Mask/>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "validationregex")
                                            {
                                                regex = n5.InnerText;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <ValidationRegex>" + regex + "</ValidationRegex>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "default")
                                            {
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Default>");
                                                }
                                                StringOrColumn sc = new StringOrColumn(n5, j, p, null, tracexml, prefix + "   ");
                                                defaultValue = sc.Value();
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      </Default>");
                                                }
                                            }
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </PromptVariable>");
                                        }
                                    }

                                    PromptVariable pv = new PromptVariable(message, new Regex(regex, RegexOptions.Singleline), defaultValue, mask);

                                    CSVProcessor.DisplayError(ANSIHelper.Information + "Prompting for variable: " + name + " : " + message + "." + ANSIHelper.White);

                                    pv.ShowDialog(null);

                                    if (systemvariable.IsReservedName(name))
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + name + " cannot be used by PromptVariable. Internal value will be used." + ANSIHelper.White);
                                    }

                                    _promptVars[name] = pv.EnteredValue;

                                    if (!mask)
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Information + prefix + "   PromptVariable " + name + " = " + pv.EnteredValue + ANSIHelper.White);
                                    }
                                    else
                                    {
                                        string pwd = string.Empty;
                                        for (int i = 0; i < pv.EnteredValue.Length; i++)
                                        {
                                            pwd += "*";
                                        }
                                        CSVProcessor.DisplayError(ANSIHelper.Information + prefix + "   PromptVariable " + name + " = " + pwd + ANSIHelper.White);
                                    }
                                }
                            }
                        }
                    }
                    // Skip blank lines
                    else if (n3.Name.ToLower() == "skipblanklines")
                    {
                        _skipblanklines = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipBlankLines/>");
                        }
                    }
                    // Get the output file name
                    // Given first line
                    else if (n3.Name.ToLower() == "givenfirstline")
                    {
                        _givenfirstline = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <GivenFirstLine>" + _givenfirstline + "</GivenFirstLine>");
                        }
                    }
                    // Given last line
                    else if (n3.Name.ToLower() == "givenlastline")
                    {
                        _givenlastline = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <GivenLastLine>" + _givenlastline + "</GivenLastLine>");
                        }
                    }
                    else if (n3.Name.ToLower() == "runtimevariable")
                    {
                        RuntimeVariable _var = new RuntimeVariable(n3, j, p, null, tracexml, prefix);
                        _var.Process(null);
                        // already processed
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "ProcessParameters::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</ProcessParameters>");
            }
        }
        #endregion
    }
}