// Code last tidied up September 4, 2010

using System;

namespace CSVProcessor
{
	/// <summary>
	/// Elapsed timer
	/// </summary>
	public static class UniqueIdClass
	{
		#region Static Member Variables
		static int __uniqueId = 0;
		static readonly object sync = new object(); // Object used to synchronize the allocation of ids.
		#endregion

		#region Static Functions
		public static int Allocate()
		{
			lock (sync)
			{
				return __uniqueId++;
			}
		}
		#endregion

	}
}
