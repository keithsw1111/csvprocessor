﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace CSVProcessor
{
    class Constants
    {
        static Dictionary<string, string> __constants = new Dictionary<string, string>();

        public static void LoadConstants(XmlNode n3, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  <Constants>");
            }

            foreach (XmlNode n4 in n3.ChildNodes)
            {
                if (n4.NodeType != XmlNodeType.Comment)
                {
                    // prompt user for variable
                    if (n4.Name.ToLower() == "constant")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "    <Constant>");
                        }

                        string name = string.Empty;
                        string value = string.Empty;

                        foreach (XmlNode n5 in n4.ChildNodes)
                        {
                            if (n5.NodeType != XmlNodeType.Comment)
                            {
                                // read in the number of bytes to skip
                                if (n5.Name.ToLower() == "name")
                                {
                                    name = n5.InnerText.ToLower().Trim();
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "      <Name>" + name + "</Name>");
                                    }
                                }
                                else if (n5.Name.ToLower() == "value")
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "      <Value>");
                                    }

                                    StringOrColumn sc = new StringOrColumn(n5, j, p, null, tracexml, prefix + "   ");
                                    value = sc.Value();

                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "      </Value>");
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    </PromptVariable>");
                            }
                        }

                        if (systemvariable.IsReservedName(name))
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + name + " cannot be used by Constant. Internal value will be used." + ANSIHelper.White);
                        }

                        if (name != string.Empty)
                        {
                            __constants[name] = value;
                        }

                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  </Constants>");
            }
        }

        public static string Constant(string name)
        {
            if (__constants.ContainsKey(name.ToLower().Trim()))
            {
                return __constants[name.ToLower().Trim()];
            }
            else
            {
                throw new ApplicationException("Constant '" + name.Trim() + "' not defined.");
            }
        }
    }
}
