// Code last tidied up September 4, 2010

using System;

namespace CSVProcessor
{
	/// <summary>
	/// Elapsed timer
	/// </summary>
	public class Timer
	{
		#region Member Variables
		DateTime _start = DateTime.Now; // the start time
		#endregion

		#region Constructors
		public Timer()
		{
		}
		#endregion

		#region Static Functions
		/// <summary>
		/// Format a timespan for display - why doesn't .NET have one of these
		/// </summary>
		/// <param name="span">Timespan to display</param>
		/// <returns>HH:MM:SS.000</returns>
		public static string FormatTimeSpan(TimeSpan span)
		{
			return
				span.Hours.ToString("00") + ":" +
				span.Minutes.ToString("00") + ":" +
				span.Seconds.ToString("00") + "." +
				span.Milliseconds.ToString("000");
		}

		public static string FormatTimeSpan(TimeSpan span, string outputFormat)
		{
			string result = outputFormat;

			result = result.Replace("ttttt", span.Milliseconds.ToString().Trim());
			result = result.Replace("tttt", span.Milliseconds.ToString().Trim());
			result = result.Replace("ttt", span.Milliseconds.ToString("000").Trim());
			result = result.Replace("tt", span.Milliseconds.ToString("00").Trim());
			result = result.Replace("t", span.Milliseconds.ToString("0").Trim());
			result = result.Replace("sss", span.TotalSeconds.ToString().Trim());
			result = result.Replace("ss", span.Seconds.ToString("00").Trim());
			result = result.Replace("mmm", span.TotalMinutes.ToString().Trim());
			result = result.Replace("mm", span.Minutes.ToString("00").Trim());
			result = result.Replace("HHH", span.TotalHours.ToString().Trim());
			result = result.Replace("HH", span.Hours.ToString("00").Trim());
			result = result.Replace("ddd", span.TotalDays.ToString().Trim());
			result = result.Replace("dd", span.Days.ToString().Trim());
			if (span < TimeSpan.Zero)
			{
			}
			else
			{
				result = result.Replace("-", " ");
			}

			return result;
		}

		#endregion

		#region Public Functions
		/// <summary>
		/// Reset the timer
		/// </summary>
		public void Reset()
		{
			// now is the current time
			_start = DateTime.Now;
		}
		#endregion

		#region Accessors
		/// <summary>
		/// How long the timer has been running
		/// </summary>
		public TimeSpan Duration
		{
			get
			{
				// return time since started
				return DateTime.Now - _start;
			}
		}

		/// <summary>
		/// How long the timer has been running as a string
		/// </summary>
		public string DurationAsString
		{
			get
			{
				return Timer.FormatTimeSpan(Duration);
			}
		}
		#endregion
	}
}
