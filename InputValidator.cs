// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Queues;
using System.Reflection;

namespace CSVProcessor
{
    public class InputValidation
    {
        protected InputValidator _v = null;
        protected string _id = string.Empty; // a id for the column
        protected bool _trace = false; // trace instruction
        string _name = string.Empty;
        string _message = string.Empty;
        protected bool _fail = false;
        bool _pause = false;

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        protected void Fail(string message)
        {
            CSVProcessor.DisplayError(ANSIHelper.Warning + message + ANSIHelper.White);
            if (_fail)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + _message + ANSIHelper.White);
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + _message + ANSIHelper.White);
            }

            if (_pause)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Press any key to continue ..." + ANSIHelper.White);
                Console.ReadKey(true);
            }
        }

        protected InputValidation()
        {
        }

        public InputValidation(XmlNode n, InputValidator v, bool tracexml, string prefix)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "message")
                    {
                        _message = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Message>" + _message + "</Message>");
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "fail")
                    {
                        _fail = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Fail/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "pause")
                    {
                        _pause = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Pause/>");
                        }
                    }
                    else
                    {
                        bool found = false;

                        object[] o = { n1.Name.ToLower() };
                        // walk the type hierachy. from here up asking each class what values they support
                        Type t = this.GetType();
                        while (!found && t != typeof(InputValidation))
                        {
                            MethodInfo[] mi = t.GetMethods();
                            foreach (MethodInfo m in mi)
                            {
                                if (m.Name == "Supports" && m.IsStatic)
                                {
                                    try
                                    {
                                        if ((bool)m.Invoke(null, o))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                    }
                                }
                            }

                            t = t.BaseType;
                        }

                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "InputValidation::Constructor Ignoring xml node : " + n1.Name + " when creating " + this.GetType().Name + ANSIHelper.White);
                        }
                    }
                }
            }

            // save the processor we belong to
            _v = v;
        }

        /// <summary>
        /// Get a list of supported Columns
        /// </summary>
        /// <returns></returns>
        public static List<string> SupportsCols()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if (t.BaseType.Name == "InputValidation")
                {
                    s.Add(t.Name);
                }
            }

            return s;
        }

        public static bool Supports(string s)
        {
            switch (s)
            {
                case "id":
                case "trace":
                    return true;
                default:
                    return false;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Id
        {
            get
            {
                return _id;
            }
        }

        public virtual bool Process(CSVJob j)
        {
            throw new ApplicationException("Derived classes from InputValidation must provide their own Process() implementation.");
        }

        public static InputValidation Create(XmlNode n, CSVJob j, InputValidator v, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                // now construct the object passing in our xml node
                InputValidation o = (InputValidation)Activator.CreateInstance(t, new object[] { n, j, v, tracexml, prefix });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("InputValidation::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
    }

    public class minfilesvalidate : InputValidation
    {
        int _minvalue = int.MaxValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "minvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public minfilesvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MinFilesValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "minvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MinValue>");
                            }
                            try
                            {
                                _minvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing minimum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MinValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.FileCount < _minvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.FileCount.ToString() + ") not greater than or equal to " + _minvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class maxfilesvalidate : InputValidation
    {
        int _maxvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "maxvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public maxfilesvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MaxFilesValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "maxvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MaxValue>");
                            }
                            try
                            {
                                _maxvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing maximum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MaxValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.FileCount > _maxvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.FileCount.ToString() + ") not less than or equal to " + _maxvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class exactfilesvalidate : InputValidation
    {
        int _exactvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "exactvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public exactfilesvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "ExactFilesValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "exactvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ExactValue>");
                            }
                            try
                            {
                                _exactvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing exact value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </ExactValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.FileCount != _exactvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.FileCount.ToString() + ") not equal to " + _exactvalue);
                return !_fail;
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "ExactFilesValidate Success!" + ANSIHelper.Normal);
                return true;
            }
        }
        #endregion
    }

    public class mininputrowsvalidate : InputValidation
    {
        int _minvalue = int.MaxValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "minvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public mininputrowsvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MinInputRowsValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "minvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MinValue>");
                            }
                            try
                            {
                                _minvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing minimum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MinValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.InputLineCount < _minvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.InputLineCount.ToString() + ") not greater than or equal to " + _minvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class maxinputrowsvalidate : InputValidation
    {
        int _maxvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "maxvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public maxinputrowsvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MaxInputRowsValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "maxvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MaxValue>");
                            }
                            try
                            {
                                _maxvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing maximum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MaxValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.InputLineCount > _maxvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.InputLineCount.ToString() + ") not less than or equal to " + _maxvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class exactinputrowsvalidate : InputValidation
    {
        int _exactvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "exactvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public exactinputrowsvalidate(XmlNode node, InputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "ExactInputRowsValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "exactvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ExactValue>");
                            }
                            try
                            {
                                _exactvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing exact value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </ExactValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(CSVJob j)
        {
            if (j.InputLineCount != _exactvalue)
            {
                Fail("InputValidation:" + Name + ": {" + Id + "} failed files (" + j.InputLineCount.ToString() + ") not equal to " + _exactvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    /// <summary>
    /// This class represents a process of a CSV file.
    /// </summary>
    public class InputValidator
    {
        #region Member Variables
        Input _i = null;
        string _id = string.Empty; // Id of the validator
        List<InputValidation> _validations = new List<InputValidation>();
        int _failed = 0;
        #endregion

        #region Accessors
        /// <summary>
        /// Get the id of this process
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
        }

        /// <summary>
        /// Get the prcoess name
        /// </summary>
        public string Name
        {
            get
            {
                return Id;
            }
        }

        /// <summary>
        /// Get the process result code
        /// </summary>
        public int Failed
        {
            get
            {
                int rc = 0;

                lock (this)
                {
                    rc = _failed;
                }

                return rc;
            }
        }

        public Input Input
        {
            get
            {
                return _i;
            }
        }

        #endregion

        #region Private Functions
        /// <summary>
        /// Notify that we have a new file
        /// </summary>
        void NewFile()
        {
        }

        /// <summary>
        /// Process an input line
        /// </summary>
        /// <param name="l">Line to process</param>
        /// <param name="line">Line number</param>
        /// <param name="firstfile">First file flag</param>
        public int Process(CSVJob j)
        {
            // TODO this is where we need tokenise do real work
            foreach (InputValidation v in _validations)
            {
                try
                {
                    if (!v.Process(j))
                    {
                        return 9;
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException("InputValidator encountered error.", e);
                }
            }

            return 0;
        }

        /// <summary>
        /// This function is called after all input records have been processed to close up the file
        /// </summary>
        void Close()
        {
            // if we did not fail
            if (Failed == 0)
            {
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a CSVProcess object
        /// </summary>
        /// <param name="forceSTDOUT">Force output to stdout</param>
        /// <param name="n">XML node to create it from</param>
        public InputValidator(XmlNode n, CSVJob j, bool tracexml, string prefix, Input i)
        {
            // save the job parameters
            _i = i;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<InputValidate>");
            }

            // process each node
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else
                    {
                        _validations.Add(InputValidation.Create(n1, j, this, tracexml, prefix + "  "));
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</InputValidate>");
            }
        }
        #endregion

        public string DotName
        {
            get
            {
                return ("InputValidate_" + Id).Replace(" ", "").Replace(".", "").Replace("\\", "");
            }
        }
        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph inputvalidate_" + Id + " { color=lightgrey; label = \"" + Id + "\";");

            foreach (InputValidation v in _validations)
            {
                v.WriteDot(dotWriter, prefix + "   ");
            }

            dotWriter.WriteLine(prefix + "   }");
        }
    }
}
