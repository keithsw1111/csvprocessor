using System;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSVProcessor
{
    #region Base Class For Holiday Definition Types
    abstract class HolidayBase
    {
        #region Constructors
        public HolidayBase()
        {
        }
        #endregion

        #region Static Functions
        public static HolidayBase Create(XmlNode n, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            // create a fully specified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type to create
                Type t = Type.GetType(classname);

                // create the object
                HolidayBase o = (HolidayBase)Activator.CreateInstance(t, new object[] { n, j, p, tracexml, prefix + "  " });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("HolidayBase::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
        #endregion

        #region Overridable Functions
        public abstract bool Is(DateTime dt, Holiday h);
        #endregion
    }
    #endregion

    #region Holiday Definition Types
    class dayofmonth : HolidayBase
    {
        int _day = 0;
        int _month = 0;
        bool _weekdayafter = false;
        bool _weekdayafterholiday = false;
        bool _weekdaybefore = false;

        public override bool Is(DateTime dt, Holiday h)
        {
            DateTime dtcheck = DateTime.MinValue;
            if (_month == 0)
            {
                dtcheck = new DateTime(dt.Year, dt.Month, _day);
            }
            else
            {
                dtcheck = new DateTime(dt.Year, _month, _day);
            }

            if (dtcheck.DayOfWeek == DayOfWeek.Saturday)
            {
                dtcheck = dtcheck.AddDays(2);
            }
            if (dtcheck.DayOfWeek == DayOfWeek.Sunday)
            {
                dtcheck = dtcheck.AddDays(1);
            }

            if (_weekdayafterholiday)
            {
                Holiday h2 = new Holiday(h);
                h2.RemoveHoliday(this);

                DateTime dtcheckstart = dtcheck;

                while (h2.IsAHoliday(dtcheck))
                {
                    dtcheck = dtcheck.AddDays(1);

                    if (dtcheck.DayOfWeek == DayOfWeek.Saturday)
                    {
                        dtcheck = dtcheck.AddDays(2);
                    }
                    if (dtcheck.DayOfWeek == DayOfWeek.Sunday)
                    {
                        dtcheck = dtcheck.AddDays(1);
                    }

                    if (dtcheck - dtcheckstart > new TimeSpan(15, 0, 0, 0))
                    {
                        throw new ApplicationException("Holiday definition <WeekdayAfterHoliday> has caused a holiday to move by > 15 days.");
                    }
                }

                if (dt.Date == dtcheck.Date)
                {
                    return true;
                }
            }
            else if (_weekdayafter || _weekdaybefore)
            {

                if (dtcheck.DayOfWeek == DayOfWeek.Saturday)
                {
                    if (_weekdayafter)
                    {
                        dtcheck = dtcheck.AddDays(2);
                    }
                    else
                    {
                        dtcheck = dtcheck.AddDays(-1);
                    }
                }
                if (dtcheck.DayOfWeek == DayOfWeek.Sunday)
                {
                    if (_weekdayafter)
                    {
                        dtcheck = dtcheck.AddDays(1);
                    }
                    else
                    {
                        dtcheck = dtcheck.AddDays(-2);
                    }
                }

                if (dt.Date == dtcheck.Date)
                {
                    return true;
                }
            }
            else
            {
                if (_month == 0)
                {
                    if (dt.Day == _day)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dt.Day == _day && dt.Month == _month)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public dayofmonth(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix) : base()
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   <DayOfMonth>");
            }

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "day")
                    {
                        _day = Convert.ToInt32(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Day>" + _day.ToString() + "</Day>");
                        }
                    }
                    else if (n.Name.ToLower() == "month")
                    {
                        _month = Convert.ToInt32(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Month>" + _month.ToString() + "</Month>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekdayafter")
                    {
                        _weekdayafter = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <WeekdayAfter/>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekdayafterholiday")
                    {
                        _weekdayafterholiday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <WeekdayAfterHoliday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekdaybefore")
                    {
                        _weekdaybefore = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <WeekdayBefore/>");
                        }
                    }
                }
            }

            if (_day < 1 || _day > 31)
            {
                throw new ApplicationException("<DayOfMonth> <Day> must be 1-31.");
            }

            if (_month < 0 || _month > 12)
            {
                throw new ApplicationException("<DayOfMonth> <Month> must be 1-12 or not specified.");
            }

            if (_weekdayafter && _weekdaybefore)
            {
                throw new ApplicationException("<DayOfMonth> Can't be both <WeekdayAfter/> and <WeekdayBefore/>.");
            }

            if (_weekdayafterholiday && _weekdaybefore)
            {
                throw new ApplicationException("<DayOfMonth> Can't be both <WeekdayAfterHoliday/> and <WeekdayBefore/>.");
            }

            if (_weekdayafter && _weekdayafterholiday)
            {
                throw new ApplicationException("<DayOfMonth> Can't be both <WeekdayAfter/> and <WeekdayAfterHoliday/>.");
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   </DayOfMonth>");
            }
        }
    }

    class weekofmonth : HolidayBase
    {
        int _week = 0;
        int _month = 0;
        DayOfWeek _dayofweek = DayOfWeek.Friday;
        bool _dayofweekspecified = false;

        bool IsLastWeekday(DateTime dt)
        {
            DayOfWeek wd = dt.DayOfWeek;
            int m = dt.Month;

            DateTime d = dt.AddDays(1);

            while (d.Month == m)
            {
                if (d.DayOfWeek == wd)
                {
                    return false;
                }
                d = d.AddDays(1);
            }

            return true;
        }

        int CountWeekdayInMonth(DateTime dt)
        {
            int count = 0;

            DateTime d = new DateTime(dt.Year, dt.Month, 1);

            while (d.Date <= dt.Date)
            {
                if (d.DayOfWeek == dt.DayOfWeek)
                {
                    count++;
                }
                d = d.AddDays(1);
            }

            return count;
        }

        public override bool Is(DateTime dt, Holiday h)
        {
            if (_month != 0)
            {
                if (dt.Month != _month)
                {
                    return false;
                }
            }

            if (dt.DayOfWeek != _dayofweek)
            {
                return false;
            }

            if (_week == 6)
            {
                // last
                return IsLastWeekday(dt);
            }
            else
            {
                if (_week == CountWeekdayInMonth(dt))
                {
                    return true;
                }
            }

            return false;
        }

        public weekofmonth(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
           : base()
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   <WeekOfMonth>");
            }

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "week")
                    {
                        if (n.InnerText.Trim().ToLower() == "last")
                        {
                            _week = 6;
                        }
                        else
                        {
                            _week = Convert.ToInt32(n.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Week>" + _week.ToString() + "</Week>");
                        }
                    }
                    else if (n.Name.ToLower() == "month")
                    {
                        _month = Convert.ToInt32(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Month>" + _month.ToString() + "</Month>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekday")
                    {
                        _dayofweekspecified = true;

                        switch (n.InnerText.Trim().ToLower())
                        {
                            case "m":
                            case "mo":
                            case "mon":
                            case "monday":
                                _dayofweek = DayOfWeek.Monday;
                                break;
                            case "tu":
                            case "tue":
                            case "tues":
                            case "tuesday":
                                _dayofweek = DayOfWeek.Tuesday;
                                break;
                            case "w":
                            case "we":
                            case "wed":
                            case "wednesday":
                                _dayofweek = DayOfWeek.Wednesday;
                                break;
                            case "th":
                            case "thu":
                            case "thur":
                            case "thurs":
                            case "thursday":
                                _dayofweek = DayOfWeek.Thursday;
                                break;
                            case "f":
                            case "fr":
                            case "fri":
                            case "friday":
                                _dayofweek = DayOfWeek.Friday;
                                break;
                            case "sa":
                            case "sat":
                            case "saturday":
                                _dayofweek = DayOfWeek.Saturday;
                                break;
                            case "su":
                            case "sun":
                            case "sunday":
                                _dayofweek = DayOfWeek.Sunday;
                                break;
                            default:
                                throw new ApplicationException("<WeekOfMonth> <Weekday> not recogised " + n.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Weekday>" + _dayofweek.ToString() + "</Weekday>");
                        }
                    }
                }
            }

            if (_week < 1 || _week > 6)
            {
                throw new ApplicationException("<WeekOfMonth> <Week> must be 1-5 or Last.");
            }

            if (_month < 0 || _month > 12)
            {
                throw new ApplicationException("<WeekOfMonth> <Month> must be 1-12 or not specified.");
            }

            if (!_dayofweekspecified)
            {
                throw new ApplicationException("<WeekOfMonth> <Weekday> must be specified.");
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   </WeekOfMonth>");
            }
        }
    }

    class relativeholiday : HolidayBase
    {
        int _relative = 0;
        HolidayBase _relativeTo = null;

        public override bool Is(DateTime dt, Holiday h)
        {
            DateTime d = dt.AddDays(_relative * -1);
            return _relativeTo.Is(d, h);
        }

        public relativeholiday(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
           : base()
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   <RelativeHoliday>");
            }

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "days")
                    {
                        _relative = Convert.ToInt32(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Days>" + _relative.ToString() + "</Days>");
                        }
                    }
                    else if (n.Name.ToLower() == "relativeto")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <RelativeTo>");
                        }
                        foreach (XmlNode n1 in n.ChildNodes)
                        {
                            if (n1.NodeType != XmlNodeType.Comment)
                            {
                                if (_relativeTo != null)
                                {
                                    throw new ApplicationException("<RelativeHoliday> can only have one holiday in the <RelativeTo>.");
                                }
                                _relativeTo = HolidayBase.Create(n1, j, p, tracexml, prefix + "   ");
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      </RelativeTo>");
                        }
                    }
                }
            }
            if (_relativeTo == null)
            {
                throw new ApplicationException("<RelativeHoliday> must have a <RelativeTo>.");
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   </RelativeHoliday>");
            }
        }
    }

    class eastersunday : HolidayBase
    {
        public override bool Is(DateTime dt, Holiday h)
        {
            if (dt.Date == easter(dt.Year).Date)
            {
                return true;
            }

            return false;
        }

        private DateTime easter(int y)
        {
            int a = y % 19;
            int b = y / 100;
            int c = y % 100;
            int d = b / 4;
            int e = b % 4;
            int f = (b + 8) / 25;
            int g = (b - f + 1) / 3;
            int h = (19 * a + b - d - g + 15) % 30;
            int i = c / 4;
            int k = c % 4;
            int l = (32 + 2 * e + 2 * i - h - k) % 7;
            int m = (a + 11 * h + 22 * l) / 451;
            int easterMonth = (h + l - 7 * m + 114) / 31;
            int p = (h + l - 7 * m + 114) % 31;
            int easterDay = p + 1;
            DateTime est = new DateTime(y, easterMonth, easterDay);
            return new DateTime(y, easterMonth, easterDay);
        }

        public eastersunday(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
           : base()
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "   <EasterSunday/>");
            }
        }
    }

    #endregion

    #region Holiday Manager
    class Holiday
    {
        #region Static Variables
        static Dictionary<string, List<HolidayBase>> __tables = new Dictionary<string, List<HolidayBase>>(); // a hashtable of loaded tables. This allows us to reuse the same table if it is referenced multiple times.
        #endregion

        #region Member Variables
        List<HolidayBase> _holidays = new List<HolidayBase>();
        #endregion

        #region Constructors
        void CommonConstructor(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Holidays>");
            }

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    _holidays.Add(HolidayBase.Create(n, j, p, tracexml, prefix + "   "));
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Holidays>");
            }
        }

        public Holiday(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            CommonConstructor(node, j, p, tracexml, prefix);
        }

        public void RemoveHoliday(HolidayBase hb)
        {
            foreach (HolidayBase h in _holidays)
            {
                if (hb == h)
                {
                    _holidays.Remove(h);
                    break;
                }
            }
        }

        public Holiday(Holiday h)
        {
            foreach (HolidayBase hb in h._holidays)
            {
                _holidays.Add(hb);
            }
        }

        public Holiday(string file, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            // If we have already loaded this lookup file
            if (__tables.ContainsKey(file))
            {
                // copy it
                _holidays = __tables[file];
            }
            else
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(file);

                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "holidays")
                        {
                            CommonConstructor(n, j, p, tracexml, prefix);
                            return;
                        }
                        else if (n.Name.ToLower() == "csvprocessor")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "holidays")
                                    {
                                        CommonConstructor(n1, j, p, tracexml, prefix);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                // save it in our list of tables
                __tables[file] = _holidays;
            }
        }
        #endregion

        #region Public Functions
        public bool IsAHoliday(DateTime dt)
        {
            foreach (HolidayBase h in _holidays)
            {
                if (h.Is(dt, this))
                {
                    return true;
                }
            }

            return false;
        }
        #endregion
    }
    #endregion
}
