using System;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
//using Microsoft.SqlServer.Management.Trace;
using Microsoft.Win32;
using System.Data.OleDb;
using System.Reflection;
//using Microsoft.Office.Interop.Outlook;
using System.Management;
using System.Windows.Input;
using System.Runtime.InteropServices;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;

namespace CSVProcessor
{
    public class CSVFileInfo
    {
        string _filename = string.Empty;
        DateTime _created = DateTime.MinValue;
        DateTime _updated = DateTime.MinValue;
        long _size = 0;
        FileAttributes _fa = 0;
        string _attribs = string.Empty;

        public CSVFileInfo()
        {
        }

        public CSVFileInfo(string file)
        {
            _filename = file;
            if (File.Exists(file))
            {
                FileInfo fi = new FileInfo(file);
                _created = fi.CreationTime;
                _updated = fi.LastWriteTime;
                _size = fi.Length;
                _fa = fi.Attributes;
            }
        }

        public CSVFileInfo(string file, DateTime created, DateTime lastUpdated, long size, string attribs)
        {
            _filename = file;
            _created = created;
            _updated = lastUpdated;
            _size = size;
            _attribs = attribs;
        }

        public CSVFileInfo(FileInfo fi)
        {
            _filename = fi.FullName;
            _created = fi.CreationTime;
            _updated = fi.LastWriteTime;
            _size = fi.Length;
            _fa = fi.Attributes;
        }

        public CSVFileInfo(DirectoryInfo di)
        {
            _filename = di.FullName;
            _created = di.CreationTime;
            _updated = di.LastWriteTime;
            _fa = di.Attributes;
        }
        public string FullFileName
        {
            get
            {
                return _filename;
            }
        }
        public DateTime Created
        {
            get
            {
                return _created;
            }
        }
        public DateTime Updated
        {
            get
            {
                return _updated;
            }
        }
        public long Size
        {
            get
            {
                return _size;
            }
        }
        public string Attributes
        {
            get
            {
                string val = _attribs;
                if (_fa.HasFlag(FileAttributes.Archive))
                {
                    val = val + "A";
                }
                if (_fa.HasFlag(FileAttributes.Compressed))
                {
                    val = val + "C";
                }
                if (_fa.HasFlag(FileAttributes.Directory))
                {
                    val = val + "D";
                }
                if (_fa.HasFlag(FileAttributes.Encrypted))
                {
                    val = val + "E";
                }
                if (_fa.HasFlag(FileAttributes.Hidden))
                {
                    val = val + "H";
                }
                if (_fa.HasFlag(FileAttributes.ReadOnly))
                {
                    val = val + "R";
                }
                if (_fa.HasFlag(FileAttributes.System))
                {
                    val = val + "S";
                }

                return val;
            }
        }
    }

    #region Base Class
    /// <summary>
    /// Base class for Input.
    /// </summary>
    public class Input
    {
        #region Member Variables
        protected CSVJob _job = null; // the job we are passing the data to
        Thread _thread = null; // the input thread
        long _progress = 0; // where we are up to
        protected long _inputrecords = 0; // number of records we have accepted as passed along for processing
        long _size = 0; // the size of the input (may be lines or bytes)
        bool _supportsProgress = true; // indicates if the input supports % progress
        bool _supportsSeek = true; // indicates if the inpur source supports seeking
        protected long _progressUpdateInterval = 100; // how many records to process before displaying an update
        protected FilterDefinition _fd = null; // input filter
        protected FilterDefinition _ppfd = null; // input filter
        protected InputParser _parser = null; // the parser to extract columns from the input line
        protected CSVFileInfo _fi = null;
        protected InputValidator _inputValidator = null;
        string _id;
        int _unique = UniqueIdClass.Allocate();
        int _rc = 0;
        List<string> _triggerFiles = new List<string>();
        bool _triggersToday = false;
        bool _triggersSinceStart = false;
        int _triggersSinceStartMinusMinutes = 0;
        protected long _inputRecordLimit = -1;
        protected bool _abort = false;
        protected bool _trace = false;
        #endregion

        public string UniqueId
        {
            get
            {
                return "I" + _unique.ToString();
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        #region Accessors
        /// <summary>
        /// Get the delimiter from the delimitedparser if appropriate
        /// </summary>
        public char Delimiter
        {
            get
            {
                if (!ParserRequired)
                {
                    throw new ApplicationException("Input type does not support parsers.");
                }

                // if this is a delimited parser
                if (_parser.GetType() == typeof(delimitedparser))
                {
                    return ((delimitedparser)_parser).Delimiter;
                }
                else
                {
                    return delimitedparser.DefaultDelimiter;
                }
            }
        }
        /// <summary>
        /// Get the quote from the delimitedparser if appropriate
        /// </summary>
        public char Quote
        {
            get
            {
                if (!ParserRequired)
                {
                    throw new ApplicationException("Input type does not support quote parsers.");
                }

                if (_parser.GetType() == typeof(delimitedparser))
                {
                    return ((delimitedparser)_parser).Quote;
                }
                else
                {
                    return delimitedparser.DefaultQuote;
                }
            }
        }
        /// <summary>
        /// Get the quoteescape from the delimitedparser if appropriate
        /// </summary>
        public char QuoteEscape
        {
            get
            {
                if (!ParserRequired)
                {
                    throw new ApplicationException("Input type does not support parsers.");
                }

                if (_parser.GetType() == typeof(delimitedparser))
                {
                    return ((delimitedparser)_parser).QuoteEscape;
                }
                else
                {
                    return delimitedparser.DefaultQuoteEscape;
                }
            }
        }
        /// <summary>
        /// True if the class supports % progress updates
        /// </summary>
        protected bool SupportsProgress
        {
            set
            {
                _supportsProgress = value;
            }
            get
            {
                return _supportsProgress;
            }
        }

        /// <summary>
        /// True if the class supports seeking
        /// </summary>
        protected bool SupportsSeek
        {
            set
            {
                _supportsSeek = value;
            }
            get
            {
                return _supportsSeek;
            }
        }

        public int RC
        {
            get
            {
                return _rc;
            }
        }

        /// <summary>
        /// The progress through the stream
        /// </summary>
        protected long Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = value;
            }
        }

        /// <summary>
        /// The size of the file that progress is building towards
        /// </summary>
        protected long Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }

        /// <summary>
        /// Progress as a percentage of the total
        /// </summary>
        public float ProgressPct
        {
            get
            {
                return (float)(((double)_progress) / (double)_size);
            }
        }

        #endregion

        #region Public Functions

        public bool IsAtLimit()
        {
            // if limit is 0 or lower then no limit
            if (_inputRecordLimit <= 0) return false;

            return _inputrecords > _inputRecordLimit;
        }

        // This is really ugly ... i have to put this in a post create as it relies on the job knowing about the input to function
        public void PostCreate(XmlNode n, bool tracexml, string prefix)
        {
            // parse the configuration items
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment && n2.Name.ToLower() != "inputvalidate")
                {
                    foreach (XmlNode n1 in n2.ChildNodes)
                    {
                        if (n1.NodeType != XmlNodeType.Comment && n1.NodeType != XmlNodeType.CDATA && n1.NodeType != XmlNodeType.Text)
                        {
                            bool found = false;

                            object[] o = { n1.Name.ToLower() };
                            // walk the type hierachy. from here up asking each class what values they support
                            Type t = this.GetType();
                            while (!found && t != typeof(Input))
                            {
                                MethodInfo[] mi = t.GetMethods();
                                foreach (MethodInfo m in mi)
                                {
                                    if (m.Name == "Supports" && m.IsStatic)
                                    {
                                        try
                                        {
                                            if ((bool)m.Invoke(null, o))
                                            {
                                                found = true;
                                                break;
                                            }
                                        }
                                        catch (System.Exception ex)
                                        {
                                            throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                        }
                                    }
                                }

                                t = t.BaseType;
                            }

                            if (!found)
                            {
                                // must be an input parser
                                if (_parser != null)
                                {
                                    throw new ApplicationException("Multiple parsers not permitted : " + n1.Name + ". " + _parser.ToString() + " already set as the parser.");
                                }
                                if (!ParserRequired)
                                {
                                    throw new ApplicationException("Input type does not support parsers." + n1.Name);
                                }

                                // create it
                                _parser = InputParser.Create(_job, n1, tracexml, prefix, _fi);
                            }
                        }
                    }
                }
            }

            // if we dont have an input parser
            if (_parser == null && ParserRequired)
            {
                // create the default one
                _parser = InputParser.CreateDefault(_job, _fi);
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the base object
        /// </summary>
        /// <param name="j">Owning job</param>
        /// <param name="n">XML defining the input</param>
        /// <param name="tracexml">true if we are to trace out the configuration</param>
        /// <param name="prefix">padding for xml display</param>
        public Input(CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // save the job
            _job = j;

            // parse the configuration items
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    bool found = false;

                    object[] o = { n1.Name.ToLower() };
                    // walk the type hierachy. from here up asking each class what values they support
                    Type t = this.GetType();
                    while (!found && t != typeof(Input))
                    {
                        MethodInfo[] mi = t.GetMethods();
                        foreach (MethodInfo m in mi)
                        {
                            if (m.Name == "Supports" && m.IsStatic)
                            {
                                try
                                {
                                    if ((bool)m.Invoke(null, o))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                catch (System.Exception ex)
                                {
                                    throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                }
                            }
                        }

                        t = t.BaseType;
                    }

                    if (n1.Name.ToLower() == "progressupdateinterval")
                    {
                        // read in the progress update frequency
                        try
                        {
                            _progressUpdateInterval = Convert.ToInt32(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading ProgressUpdateInterval parameter : " + n1.InnerText, ex);
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ProgressUpdateInterval>" + _progressUpdateInterval.ToString() + "</ProgressUpdateInterval>");
                        }
                    }
                    else if (n1.Name.ToLower() == "recordlimit")
                    {
                        try
                        {
                            _inputRecordLimit = Convert.ToInt32(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading RecordLimit parameter : " + n1.InnerText, ex);
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RecordLimit>" + _inputRecordLimit.ToString() + "</RecordLimit>");
                        }
                    }
                    else if (n1.Name.ToLower() == "inputfilter")
                    {
                        // extract the line filter definition from the configuration file
                        try
                        {
                            _fd = new FilterDefinition(n1, j, null, tracexml, prefix + "  ");
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error extracting the inputfilter definition from the configuration file", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "triggerfile")
                    {
                        _triggerFiles.Add(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TriggerFile>" + n1.InnerText + "</TriggerFile>");
                        }
                    }
                    else if (n1.Name.ToLower() == "triggerfiledatedtoday")
                    {
                        _triggersToday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TriggerFileDatedToday/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "triggerfileafterjobstart")
                    {
                        _triggersSinceStart = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TriggerFileAfterJobStart/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "triggerfileminutesbeforejobstart")
                    {
                        try
                        {
                            _triggersSinceStartMinusMinutes = Convert.ToInt32(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading TriggerFileMinutesBeforeJobStart parameter : " + n1.InnerText, ex);
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TriggerFileMinutesBeforeJobStart>" + _triggersSinceStartMinusMinutes.ToString() + "</TriggerFileMinutesBeforeJobStart>");
                        }
                    }
                    else if (n1.Name.ToLower() == "preparseinputfilter")
                    {
                        // extract the line filter definition from the configuration file
                        try
                        {
                            _ppfd = new FilterDefinition(n1, j, null, tracexml, prefix + "  ");
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error extracting the preparseinputfilter definition from the configuration file", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                    }
                    // if not a known parameter to ignore
                    else if (!found)
                    {
                        // this is a parser ... ignore it here
                    }
                }
            }

            if ((_triggersToday = true && _triggersSinceStart) || (_triggersSinceStart && _triggersSinceStartMinusMinutes != 0) || (_triggersToday && _triggersSinceStartMinusMinutes != 0))
            {
                throw new ApplicationException("Only one Trigger File instruction among the following can be specified: TriggerFileDatedToday, TriggerFileAfterJobStart, & TriggerFileMinutesBeforeJobStart.");
            }

            // if no finder definition was created then create a null one
            if (_fd == null)
            {
                _fd = new FilterDefinition(null);
            }
            if (_ppfd == null)
            {
                _ppfd = new FilterDefinition(null);
            }
        }
        #endregion

        bool AllTriggerFilesExist
        {
            get
            {
                foreach (string file in _triggerFiles)
                {
                    if (!File.Exists(file))
                    {
                        return false;
                    }
                }

                DateTime start = _job.StartTime;

                if (_triggersToday)
                {
                    DateTime today = start.Date;
                    foreach (string file in _triggerFiles)
                    {
                        if (File.GetCreationTime(file).Date < today)
                        {
                            return false;
                        }
                    }
                }
                else if (_triggersSinceStart)
                {
                    foreach (string file in _triggerFiles)
                    {
                        if (File.GetCreationTime(file) < start)
                        {
                            return false;
                        }
                    }
                }
                else if (_triggersSinceStartMinusMinutes != 0)
                {
                    DateTime test = start - new TimeSpan(0, _triggersSinceStartMinusMinutes, 0);
                    foreach (string file in _triggerFiles)
                    {
                        if (File.GetCreationTime(file) < test)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        #region Thread Management Functions
        /// <summary>
        /// Start the input thread
        /// </summary>
        public void Start()
        {
            if (_triggerFiles.Count > 0)
            {
                if (!AllTriggerFilesExist)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Input {" + Id + "} Waiting for trigger files to exist." + ANSIHelper.White);

                    while (!AllTriggerFilesExist)
                    {
                        Thread.Sleep(1000);
                    }

                    CSVProcessor.DisplayError(ANSIHelper.Information + "Input {" + Id + "} Trigger files all now exist." + ANSIHelper.White);
                }
            }

            // Create the thread
            _thread = new Thread(new ThreadStart(Run));

            _thread.Name = "Input:" + Name;

            // start it
            _thread.Start();

            // Sleep to give it a chance to run
            Thread.Sleep(0);
        }

        /// <summary>
        /// Wait until this thread ends
        /// </summary>
        public void Wait()
        {
            Debug.WriteLine("Waiting for input {" + Id + "} to end");

            // if we have a thread
            if (_thread != null)
            {
                // wait for it
                _thread.Join();
            }

            Debug.WriteLine("Input {" + Id + "} ended");
        }
        #endregion

        #region Message Passing Functions
        /// <summary>
        /// Pause until the record has been processed
        /// </summary>
        public void Synch()
        {
            Debug.WriteLine("Input {" + Id + "} sending synch");
            _job.Synch();
        }

        /// <summary>
        /// Report an error
        /// </summary>
        /// <param name="error"></param>
        public void ReportError(string error)
        {
            _job.ReportError(error);
        }

        /// <summary>
        /// Report progress through the file
        /// </summary>
        public void ReportProgress()
        {
            if (SupportsProgress)
            {
                _job.ReportProgressPct(Name, ProgressPct);
            }
            else
            {
                _job.ReportProgress(Name, Progress);
            }
        }

        /// <summary>
        /// Tell the job we are done
        /// </summary>
        public void Stop()
        {
            if (_inputValidator != null)
            {
                _rc = _inputValidator.Process(_job);
            }
            Debug.WriteLine("Input {" + Id + "} sending stop");
            _job.Stop();
        }

        /// <summary>
        /// Tell the job we are starting a new file
        /// </summary>
        public void NewFile(string file)
        {
            _job.NewFile(file);
        }

        /// <summary>
        /// Send a column line to the job
        /// </summary>
        /// <param name="l">The column line</param>
        public void Send(ColumnLine l)
        {
            if (!_abort)
            {
                _inputrecords++;
                _job.Send(l);

                if (IsAtLimit())
                {
                    _abort = true;
                    CSVProcessor.DisplayError(ANSIHelper.Information + "Input {" + Id + "} Aborting because record limit of " + _inputRecordLimit.ToString() + " reached." + ANSIHelper.White);
                }
            }
        }

        public void AddValidator(InputValidator iv)
        {
            _inputValidator = iv;
        }

        /// <summary>
        /// Send a rejected record to the job
        /// </summary>
        /// <param name="l"></param>
        public void Reject(string l)
        {
            _job.Reject(l);
        }
        #endregion

        #region Functions to be overridden
        /// <summary>
        /// Read the file
        /// </summary>
        public virtual void Run()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get the input name
        /// </summary>
        public virtual string Name
        {
            get
            {
                return Id;
            }
        }
        public string Id
        {
            get
            {
                return _id;
            }
        }
        public virtual bool ParserRequired
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Static functions
        /// <summary>
        /// Check if this function supports that parameter
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "progressupdateinterval":
                case "inputfilter":
                case "id":
                case "preparseinputfilter":
                case "inputvalidate":
                case "recordlimit":
                case "trace":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Create the appropriate input
        /// </summary>
        /// <param name="j">Owning job</param>
        /// <param name="n">XML node containing the input</param>
        /// <param name="tracexml">true if we are to trace out the configuration</param>
        /// <param name="prefix">padding to use to display the xml</param>
        /// <param name="files">files/directories passed in on the command line</param>
        /// <returns></returns>
        static public Input Create(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
        {
            string inputName = string.Empty; // the name of the input
            Input input = null; // the input we create
            XmlNode node = null; // the xml node that contains the input we are using
            XmlNode iv = null;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Input>");
            }

            // process the configutation
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    // check this is out first input
                    if (n1.Name.ToLower() == "inputvalidate")
                    {
                        iv = n1;
                    }
                    else if (inputName == string.Empty)
                    {
                        // save the input node name
                        inputName = n1.Name.ToLower();

                        // save the XML
                        node = n1;
                    }
                    else
                    {
                        // warn the user we are ignoring this
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple inputs are not supported : " + n1.Name.ToLower() + " ignored.\n" + ANSIHelper.White);
                    }
                }
            }

            // work out the fully classified class name
            string classname = "CSVProcessor." + inputName;

            try
            {
                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "  <" + inputName + ">");
                }

                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                // now construct the object passing in our xml node
                input = (Input)Activator.CreateInstance(t, new object[] { j, node, tracexml, prefix + "  ", files });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "  </" + inputName + ">");
                }

                if (iv != null)
                {
                    input.AddValidator(new InputValidator(iv, j, tracexml, prefix + "   ", input));
                }
            }
            catch (System.Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("Input::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly.", ex);
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Input>");
            }

            // return the object we created
            return input;
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// Read from std input
    /// </summary>
    class stdinput : TextReaderInput
    {
        #region Constructor
        /// <summary>
        /// Create a reader from STDINPUT
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public stdinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            FirstFile = true;

            // set the file name
            _fi = new CSVFileInfo("STDIN");

            // set the stream
            _tr = Console.In;

            // initialise progress
            Size = 0;
            Progress = 0;

            // STDIN does not support progress
            SupportsProgress = false;

            // STDIN does not support seeking
            SupportsSeek = false;

            if (_blocksize != 0)
            {
                throw new ApplicationException("STDINPUT does not support BLOCKSIZE OR VARABLEBLOCKSIZE.");
            }
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// Check if a parameter is understood by this Input
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            return Input.Supports(s);
        }
        #endregion

        #region Override
        /// <summary>
        /// Close the stream
        /// </summary>
        protected override void Close()
        {
            // do nothing
        }

        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                return "STDIN";
            }
        }

        #endregion
    }

    /// <summary>
    /// Read from the event log
    /// </summary>
    class eventloginput : Input
    {
        #region Member Variables
        bool _monitor = false; // true if we are to keep monitoring
        EventLog _eventLog = null; // our open event log
        string _logName = string.Empty; // name of the log to open
        string _machineName = "."; // name of the machine to get the event log from
        long _line = 1; // input line number
        string _newLineReplacement = "\n"; // char to replace new lines with
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"EventLogInput::" + _machineName + "." + _logName + "\"];");
        }

        #region Static Functions
        /// <summary>
        /// Check if a parameter is understood by this input type
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "log":
                case "machine":
                case "newline":
                case "monitor":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public eventloginput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "log")
                    {
                        _logName = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Log>" + _logName + "</Log>");
                        }
                    }
                    else if (n1.Name.ToLower() == "machine")
                    {
                        _machineName = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Machine>" + _machineName + "</Machine>");
                        }
                    }
                    else if (n1.Name.ToLower() == "newline")
                    {
                        _newLineReplacement = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NewLine>" + _newLineReplacement + "</NewLine>");
                        }
                    }
                    else if (n1.Name.ToLower() == "monitor")
                    {
                        _monitor = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Monitor />");
                        }
                    }
                }
            }

            // open the event log
            try
            {
                _eventLog = new EventLog(_logName, _machineName);
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening event log " + Name, ex);
            }

            _fi = new CSVFileInfo(Name);
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Handle an event log written event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnEntryWritten(object source, EntryWrittenEventArgs e)
        {
            // increment the input count
            _line++;

            // create a column line from it
            ColumnLine cle = GetEventLogEntry(e.Entry, _line++, /*Name*/_fi);

            if (_fd.In(cle) && _ppfd.In(cle))
            {
                //cle.Parse();

                // send it to the job
                _job.Send(cle);
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Convert an event log entry into a CSV line
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        ColumnLine GetEventLogEntry(EventLogEntry e, long line, CSVFileInfo fi)
        {
            ColumnLine cl = new ColumnLine(line, fi);
            //string s = string.Empty;

            cl.AddEnd(e.TimeGenerated.ToString("dd/MM/yyyy"));
            cl.AddEnd(e.TimeGenerated.ToString("HH:mm:ss.fff"));
            cl.AddEnd(e.EntryType.ToString());
            if (e.UserName == null)
            {
                cl.AddEnd(string.Empty);
            }
            else
            {
                cl.AddEnd(e.UserName);
            }
            cl.AddEnd(e.MachineName);
            cl.AddEnd(e.Source);
            try
            {
                cl.AddEnd(e.Category);
            }
            catch
            {
                cl.AddEnd("");
            }

            cl.AddEnd(e.InstanceId.ToString());
            string msg = e.Message.Replace("\r\n", _newLineReplacement);
            msg = msg.Replace("\n", _newLineReplacement);
            msg = msg.Replace("\r", _newLineReplacement);
            cl.AddEnd(msg);

            return cl;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                return _machineName + "." + _logName;
            }
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            // send the header line
            CSVFileLine cl = new CSVFileLine("Date,Time,Type,User,Computer,Source,Category,EventID,Description", _line, /*Name*/_fi);
            if (_fd.In(cl) && _ppfd.In(cl))
            {
                cl.Parse();
                cl.Data = false;
                _job.Send(cl);
                _line++;
            }

            // If we are to monitor
            if (_monitor)
            {
                // set up the event handler
                _eventLog.EntryWritten += new EntryWrittenEventHandler(OnEntryWritten);
                _eventLog.EnableRaisingEvents = true;

                // do nothing for ever
                while (true && !_abort)
                {
                    Thread.Sleep(1000);
                }

                if (_abort)
                {
                    _eventLog.EnableRaisingEvents = false;
                }
            }
            else
            {
                // Process each entry from the event log
                foreach (EventLogEntry ele in _eventLog.Entries)
                {
                    // parse it
                    CSVFileInfo fi = new CSVFileInfo(Name);
                    ColumnLine cle = GetEventLogEntry(ele, _line++, /*Name*/fi);

                    if (_fd.In(cle) && _ppfd.In(cle))
                    {
                        //cle.Parse();

                        // send it to the job
                        _job.Send(cle);
                    }

                    if (_abort)
                    {
                        break;
                    }
                }

                // tell the job we are done
                _job.Stop();
            }
        }
        #endregion
    }

    class directorylistinput : Input
    {
        #region Member Variables
        List<string> _startfolder = new List<string>(); // name of the log to open
        bool _recurse = false; // do we recurse into folders
        bool _includefolders = false; // do we include an entry for the folder
        bool _includehidden = false; // do we include hidden files
        bool _includesystem = false; // do we include system files
        bool _treatarchivesasfolders = false; // do we open archives as if they were folders
        long _line = 1; // input line number
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.Write(prefix + UniqueId + " [label=\"DirectoryListInput::");
            foreach (string s in _startfolder)
            {
                dotWriter.Write(s.Replace("\\", "\\\\") + "\\n");
            }
            dotWriter.WriteLine("\"];");
        }

        #region Static Functions
        /// <summary>
        /// Do we support a given parameter
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "startfolder":
                case "recurse":
                case "treatarchivesasfolders":
                case "includefolders":
                case "includehidden":
                case "includesystem":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public directorylistinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "startfolder")
                    {
                        _startfolder.Add(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StartFolder>" + n1.InnerText + "</StartFolder>");
                        }
                    }
                    else if (n1.Name.ToLower() == "recurse")
                    {
                        _recurse = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Recurse/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "treatarchivesasfolders")
                    {
                        _treatarchivesasfolders = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TreatArchivesAsFolders/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "includefolders")
                    {
                        _includefolders = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <IncludeFolders/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "includehidden")
                    {
                        _includehidden = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <IncludeHidden/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "includesystem")
                    {
                        _includesystem = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <IncludeSystem/>");
                        }
                    }
                }
            }

            if (_startfolder.Count == 0)
            {
                throw new ApplicationException("DirectoryListInput::Constructor You must specify at least one StartFolder.");
            }
        }
        #endregion

        #region PrivateFunctions
        /// <summary>
        /// Safe access to a string that may be null
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        string NullSafe(string s)
        {
            if (s == null)
            {
                return string.Empty;
            }
            else
            {
                return s;
            }
        }

        /// <summary>
        /// Process a zip file as if it was a folder
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="file"></param>
        /// <param name="zip"></param>
        void ProcessArchive(string dir, string file, string zip)
        {
            // open the zip file
            using (ZipArchive archive = ZipArchive.OpenOnFile(zip))
            {
                // look at each file inside
                foreach (ZipArchive.ZipFileInfo zfi in archive.Files)
                {
                    // if it is a file
                    if (!zfi.FolderFlag)
                    {
                        // open the file
                        using (Stream s = zfi.GetStream())
                        {
                            // if it is a zip file within a zip file
                            if (Path.GetExtension(file + "\\" + zfi.Name).ToLower() == ".zip" && _recurse)
                            {
                                // write out details about the zip file
                                _fi = new CSVFileInfo(Path.GetFullPath(file) + "\\" + zfi.Name, zfi.LastModFileDateTime, zfi.LastModFileDateTime, s.Length, "");
                                CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                                csv.AddEnd(Path.GetFullPath(file) + "\\" + zfi.Name);
                                csv.AddEnd(Path.GetDirectoryName(file) + "\\" + zfi.Name);
                                csv.AddEnd(Path.GetFileName(file) + "\\" + zfi.Name);
                                csv.AddEnd("ArchiveFile");
                                csv.AddEnd(s.Length.ToString());
                                csv.AddEnd("");
                                csv.AddEnd("");
                                csv.AddEnd("");
                                csv.AddEnd(zfi.LastModFileDateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                                _job.Send(csv);

                                // extract the zip file ... then recursively process it somehow dummying the path
                                string _filename = Path.GetTempFileName();
                                FileStream streamWriter = File.Create(_filename);
                                int size = 2048;
                                byte[] data = new byte[2048];
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        streamWriter.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                streamWriter.Close();

                                // Now recursively process this zip file
                                ProcessArchive(Path.GetDirectoryName(file + "\\" + zfi.Name), file + "\\" + zfi.Name, _filename);

                                // now delete the temporary file
                                File.Delete(_filename);
                            }
                            else
                            {
                                // write out the details of a regular file
                                _fi = new CSVFileInfo(Path.GetFullPath(file) + "\\" + zfi.Name, zfi.LastModFileDateTime, zfi.LastModFileDateTime, s.Length, "");
                                CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                                csv.AddEnd(Path.GetFullPath(file) + "\\" + zfi.Name);
                                csv.AddEnd(Path.GetDirectoryName(file) + "\\" + zfi.Name);
                                csv.AddEnd(Path.GetFileName(file) + "\\" + zfi.Name);
                                csv.AddEnd("ArchiveFile");
                                csv.AddEnd(s.Length.ToString());
                                csv.AddEnd("");
                                csv.AddEnd("");
                                csv.AddEnd("");
                                csv.AddEnd(zfi.LastModFileDateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                                _job.Send(csv);
                            }
                        }
                    }
                    else
                    {
                        // if we are meant to write out folder details
                        if (_includefolders)
                        {
                            // send the folder details
                            _fi = new CSVFileInfo(Path.GetFullPath(file) + "\\" + zfi.Name, zfi.LastModFileDateTime, zfi.LastModFileDateTime, 0, "D");
                            CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                            csv.AddEnd(Path.GetFullPath(file) + "\\" + zfi.Name);
                            csv.AddEnd(Path.GetDirectoryName(file) + "\\" + zfi.Name);
                            csv.AddEnd(Path.GetFileName(file) + "\\" + zfi.Name);
                            csv.AddEnd("ArchiveDir");
                            csv.AddEnd("0");
                            csv.AddEnd("");
                            csv.AddEnd("");
                            csv.AddEnd("");
                            csv.AddEnd(zfi.LastModFileDateTime.ToString("yyyy/MM/dd HH:mm:ss"));

                            _job.Send(csv);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process a directory
        /// </summary>
        /// <param name="dir"></param>
        void ProcessDirectory(string dir)
        {
            string full = Path.GetFullPath(dir); // the path

            // look at each file
            foreach (string f in Directory.GetFiles(full))
            {
                if (_abort)
                {
                    break;
                }

                // get the file details
                FileInfo fi = new FileInfo(f);

                // check if we should include it
                if (((fi.Attributes & FileAttributes.Hidden) != 0 && !_includehidden) || ((fi.Attributes & FileAttributes.System) != 0 && !_includesystem))
                {
                    // skip this file
                }
                // If it is an archive file
                else if (fi.Extension.ToLower() == ".zip" && _treatarchivesasfolders)
                {
                    // send the details of the archive file
                    _fi = new CSVFileInfo(Path.GetFullPath(f), fi.CreationTime, fi.LastWriteTime, fi.Length, "D");
                    CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                    csv.AddEnd(Path.GetFullPath(f));
                    csv.AddEnd(Path.GetDirectoryName(f));
                    csv.AddEnd(Path.GetFileName(f));
                    csv.AddEnd("File");
                    csv.AddEnd(fi.Length.ToString());
                    csv.AddEnd(fi.Attributes.ToString());
                    csv.AddEnd(fi.CreationTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(fi.LastAccessTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(fi.LastWriteTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(f);
                    if (fvi != null)
                    {
                        csv.AddEnd(NullSafe(fvi.FileVersion));
                        csv.AddEnd(NullSafe(fvi.ProductVersion));
                        csv.AddEnd(NullSafe(fvi.InternalName));
                        csv.AddEnd(NullSafe(fvi.ProductName));
                        csv.AddEnd(NullSafe(fvi.CompanyName));
                        csv.AddEnd(NullSafe(fvi.PrivateBuild));
                        csv.AddEnd(NullSafe(fvi.SpecialBuild));
                        csv.AddEnd(NullSafe(fvi.Comments));
                        csv.AddEnd(NullSafe(fvi.FileDescription));
                    }
                    _job.Send(csv);

                    // now process the archive
                    ProcessArchive(f, f, f);
                }
                else
                {
                    // process a regular file
                    _fi = new CSVFileInfo(fi);
                    CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                    csv.AddEnd(Path.GetFullPath(f));
                    csv.AddEnd(Path.GetDirectoryName(f));
                    csv.AddEnd(Path.GetFileName(f));
                    csv.AddEnd("File");
                    csv.AddEnd(fi.Length.ToString());
                    csv.AddEnd(fi.Attributes.ToString());
                    csv.AddEnd(fi.CreationTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(fi.LastAccessTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(fi.LastWriteTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(f);
                    if (fvi != null)
                    {
                        csv.AddEnd(NullSafe(fvi.FileVersion));
                        csv.AddEnd(NullSafe(fvi.ProductVersion));
                        csv.AddEnd(NullSafe(fvi.InternalName));
                        csv.AddEnd(NullSafe(fvi.ProductName));
                        csv.AddEnd(NullSafe(fvi.CompanyName));
                        csv.AddEnd(NullSafe(fvi.PrivateBuild));
                        csv.AddEnd(NullSafe(fvi.SpecialBuild));
                        csv.AddEnd(NullSafe(fvi.Comments));
                        csv.AddEnd(NullSafe(fvi.FileDescription));
                    }
                    _job.Send(csv);
                }
            }

            // look at each directory
            foreach (string d in Directory.GetDirectories(full))
            {
                if (_abort)
                {
                    break;
                }

                // if we should include folders
                if (_includefolders)
                {
                    // send details of the folder
                    DirectoryInfo di = new DirectoryInfo(d);
                    _fi = new CSVFileInfo(di);
                    CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                    csv.AddEnd(Path.GetFullPath(d));
                    csv.AddEnd(Path.GetDirectoryName(d));
                    csv.AddEnd(Path.GetFileName(d));
                    csv.AddEnd("Dir");
                    csv.AddEnd("0");
                    csv.AddEnd(di.Attributes.ToString());
                    csv.AddEnd(di.CreationTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(di.LastAccessTime.ToString("yyyy/MM/dd HH:mm:ss"));
                    csv.AddEnd(di.LastWriteTime.ToString("yyyy/MM/dd HH:mm:ss"));

                    _job.Send(csv);
                }

                // if we are meant to recursively process folders
                if (_recurse)
                {
                    // process the directory
                    ProcessDirectory(d);
                }
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                string s = "DirectoryList::";
                if (_startfolder.Count > 0)
                {
                    s = s + _startfolder[0];
                }

                return s;
            }
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            // send the header line
            CSVFileInfo fi = new CSVFileInfo(Name);
            CSVFileLine cl = new CSVFileLine("FullName,Path,Filename,Type,Size,Attributes,CreationTime,LastAccessTime,LastWriteTime,FileVersion,ProductVersion,InternalName,ProductName,CompanyName,PrivateBuild,SpecialBuild,Comments,FileDescription", _line, /*Name*/fi);
            if (_fd.In(cl) && _ppfd.In(cl))
            {
                cl.Parse();
                cl.Data = false;
                _job.Send(cl);
                _line++;
            }

            foreach (string start in _startfolder)
            {
                ProcessDirectory(start);

                if (_abort)
                {
                    break;
                }
            }

            // tell the job we are done
            _job.Stop();
        }
        #endregion
    }

    class pdfinput : Input
    {
        #region Member Variables
        List<string> _filenames = new List<string>();
        List<string> _folders = new List<string>();
        List<string> _patterns = new List<string>();
        bool _allpagesoneline = false;
        bool _allelementsoneline = false;
        char _elementseparator = '|';
        char _pageseparator = '~';
        long _line = 1; // input line number
        bool _processsubfolders = false;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.Write(prefix + UniqueId + " [label=\"PDFInput::");
            foreach (var fn in _filenames)
            {
                dotWriter.Write(fn + "\\n");
            }
            foreach (var d in _folders)
            {
                foreach (var p in _patterns)
                {
                    dotWriter.Write(d + "\\\\" + p + "\\n");
                }
            }
            dotWriter.WriteLine("\"];");
        }

        #region Static Functions
        /// <summary>
        /// Do we support a given parameter
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "allpagesoneline":
                case "allelementsoneline":
                case "pageseparator":
                case "pageseperator":
                case "elementsseparator":
                case "elementsseperator":
                case "filename":
                case "directory":
                case "filenamepattern":
                case "processsubfolders":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public pdfinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "filename")
                    {
                        _filenames.Add(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Filename>" + n1.InnerText + "</Filename>");
                        }
                    }
                    else if (n1.Name.ToLower() == "directory")
                    {
                        _folders.Add(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Directory>" + n1.InnerText + "</Directory>");
                        }
                    }
                    else if (n1.Name.ToLower() == "filenamepattern")
                    {
                        _patterns.Add(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FilenamePattern>" + n1.InnerText + "</FilenamePattern>");
                        }
                    }
                    else if (n1.Name.ToLower() == "allpagesoneline")
                    {
                        _allpagesoneline = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <AllPagesOneLine/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "allelementsoneline")
                    {
                        _allelementsoneline = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <AllELementsOneLine/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "processsubfolders")
                    {
                        _processsubfolders = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ProcessSubfolders/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "pageseparator" || n1.Name.ToLower() == "pageseperator")
                    {
                        string separator = "";
                        string s = n1.InnerText;
                        if (s == "\\n")
                        {
                            separator = "\\n";
                            _pageseparator = '\n';
                        }
                        else if (s == "\\t")
                        {
                            separator = "\\t";
                            _pageseparator = '\t';
                        }
                        else if (s == "\\r")
                        {
                            separator = "\\r";
                            _pageseparator = '\r';
                        }
                        else if (s == "")
                        {
                            separator = "";
                            _pageseparator = (char)0x01;
                        }
                        else
                        {
                            separator += s[0];
                            _pageseparator = s[0];
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PageSeparator>" + separator + "</PageSeparator>");
                        }
                    }
                    else if (n1.Name.ToLower() == "elementseparator" || n1.Name.ToLower() == "elementseperator")
                    {
                        string separator = "";
                        string s = n1.InnerText;
                        if (s == "\\n")
                        {
                            separator = "\\n";
                            _elementseparator = '\n';
                        }
                        else if (s == "\\t")
                        {
                            separator = "\\t";
                            _elementseparator = '\t';
                        }
                        else if (s == "\\r")
                        {
                            separator = "\\r";
                            _elementseparator = '\r';
                        }
                        else if (s == "")
                        {
                            separator = "";
                            _elementseparator = (char)0x01;
                        }
                        else
                        {
                            separator += s[0];
                            _elementseparator = s[0];
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ElementSeparator>" + separator + "</ElementSeparator>");
                        }
                    }
                }
            }

            if (_filenames.Count == 0 && _folders.Count == 0)
            {
                throw new ApplicationException("PDFInput::Constructor You must specify a filename or a folder.");
            }
            if (_patterns.Count == 0 && _folders.Count > 0)
            {
                _patterns.Add("*.*");
            }
            if (_allpagesoneline) _allelementsoneline = true;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                string s = "PDF::";
                if (_filenames.Count > 0)
                {
                    return s + _filenames[0];
                }
                if (_folders.Count > 0)
                {
                    return s + _folders[0];
                }
                return "PDF::";
            }
        }

        void ProcessDirectory(string dir)
        {
            string full = Path.GetFullPath(dir); // the path

            // look at each file
            foreach (string p in _patterns)
            {
                foreach (string f in Directory.GetFiles(full, p))
                {
                    try
                    {
                        ProcessFile(f);
                    }
                    catch(Exception ex)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Error + "PDFInput unable to read file " + f + " : " + ex.Message + "\n" + ANSIHelper.White);
                    }
                }
            }
            if (_processsubfolders)
            {
                foreach (string d in Directory.GetDirectories(full))
                {
                    ProcessDirectory(d);
                }
            }
        }

        void ProcessFile(string file)
        {
            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "PDFInput processing file  " + file + "\n" + ANSIHelper.White);
            }

            CSVFileInfo fi = new CSVFileInfo(file);
            string allpagesoneline = "";

            PdfReader pdfreader = new PdfReader(file);
            PdfDocument reader = new PdfDocument(pdfreader);
            PdfDocumentContentParser parser = new PdfDocumentContentParser(reader);
            ITextExtractionStrategy strategy;
            for (int i = 1; i <= reader.GetNumberOfPages(); i++)
            {
                strategy = parser.ProcessContent(i, new SimpleTextExtractionStrategy());
                if (_allpagesoneline)
                {
                    if (allpagesoneline != "")
                    {
                        if (_pageseparator != (char)0x01)
                        {
                            allpagesoneline += _pageseparator;
                        }
                    }

                    StringReader sr = new StringReader(strategy.GetResultantText());
                    string allelementsoneline = "";
                    string s = sr.ReadLine();
                    while (s != null)
                    {
                        if (allelementsoneline != "")
                        {
                            if (_elementseparator != (char)0x01)
                            {
                                allelementsoneline += _elementseparator;
                            }
                        }
                        allelementsoneline += s;
                        s = sr.ReadLine();
                    }

                    allpagesoneline += allelementsoneline;
                }
                else
                {
                    var texts = strategy.GetResultantText();

                    if (_allelementsoneline)
                    {
                        StringReader sr = new StringReader(texts);
                        string allelementsoneline = "";
                        string s = sr.ReadLine();
                        while (s != null)
                        {
                            if (allelementsoneline != "")
                            {
                                if (_elementseparator != (char)0x01)
                                {
                                    allelementsoneline += _elementseparator;
                                }
                            }
                            allelementsoneline += s;
                            s = sr.ReadLine();
                        }
                        CSVFileLine csv = new CSVFileLine("", _line++, fi);
                        csv.AddEnd(allelementsoneline);
                        _job.Send(csv);
                    }
                    else
                    {
                        StringReader sr = new StringReader(texts);

                        string s = sr.ReadLine();
                        while (s != null)
                        {
                            CSVFileLine csv = new CSVFileLine("", _line++, fi);
                            csv.AddEnd(s);
                            _job.Send(csv);
                            s = sr.ReadLine();
                        }
                    }
                }
            }

            if (_allpagesoneline)
            {
                CSVFileLine csv = new CSVFileLine("", _line++, fi);
                csv.AddEnd(allpagesoneline);
                _job.Send(csv);
            }

            reader.Close();
            pdfreader.Close();
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            // send the header line
            CSVFileInfo fi = new CSVFileInfo(Name);
            CSVFileLine cl = new CSVFileLine("PDFContent", _line, /*Name*/fi);
            if (_fd.In(cl) && _ppfd.In(cl))
            {
                cl.Parse();
                cl.Data = false;
                _job.Send(cl);
                _line++;
            }

            foreach (var f in _filenames)
            {
                ProcessFile(f);
            }

            foreach (var d in _folders)
            {
                ProcessDirectory(d);

                if (_abort)
                {
                    break;
                }
            }

            // tell the job we are done
            _job.Stop();
        }
        #endregion
    }

    class registrylistinput : Input
    {
        #region Member Variables
        List<string> _startkey = new List<string>(); // name of the keys to open
        List<RegistryKey> _hives = new List<RegistryKey>(); // hives to open
        bool _recurse = false; // should i recurse through keys
        bool _includekeys = false; // should i include the keys themselves
        long _line = 1; // input line number
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.Write(prefix + UniqueId + " [label=\"RegistryListInput::");
            for (int i = 0; i < _hives.Count; i++)
            {
                dotWriter.Write(_hives[i].Name + "\\\\" + _startkey[i].Replace("\\", "\\\\") + "\\n");
            }
            dotWriter.WriteLine("\"];");
        }

        #region Static Functions
        /// <summary>
        /// Check if the input supports a given parameter
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "startkey":
                case "recurse":
                case "includekeys":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public registrylistinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "startkey")
                    {
                        string hive = string.Empty;

                        if (n1.InnerText.IndexOf('\\') == -1)
                        {
                            hive = n1.InnerText.Trim().ToLower();
                            _startkey.Add("");
                        }
                        else
                        {
                            string s = n1.InnerText.Trim().ToLower();

                            while (s != string.Empty && s[0] == '\\')
                            {
                                s = s.Substring(1);
                            }

                            if (s.IndexOf('\\') == -1)
                            {
                                hive = s;
                                _startkey.Add("");
                            }
                            else
                            {
                                hive = s.Substring(0, s.IndexOf('\\'));
                                if (s == "\\")
                                {
                                    _startkey.Add("");
                                }
                                else
                                {
                                    _startkey.Add(s.Substring(s.IndexOf('\\') + 1));
                                }
                            }
                        }

                        switch (hive)
                        {
                            case "hkey_classes_root":
                                _hives.Add(Registry.ClassesRoot);
                                break;
                            case "hkey_current_config":
                                _hives.Add(Registry.CurrentConfig);
                                break;
                            case "hkey_current_user":
                                _hives.Add(Registry.CurrentUser);
                                break;
#if Framework35
                     case "hkey_dyn_data":
                        _hives.Add(Registry.DynData);
                        break;
#endif
                            case "hkey_local_machine":
                                _hives.Add(Registry.LocalMachine);
                                break;
                            case "hkey_performance_data":
                                _hives.Add(Registry.PerformanceData);
                                break;
                            case "hkey_users":
                                _hives.Add(Registry.Users);
                                break;
                            default:
                                throw new ApplicationException("RegistryListInput::Constructor Unknown Hive '" + hive + "' : " + n1.InnerText);
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StartKey>" + n1.InnerText + "</StartKey>");
                        }
                    }
                    else if (n1.Name.ToLower() == "recurse")
                    {
                        _recurse = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Recurse/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "includekeys")
                    {
                        _includekeys = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <IncludeKeys/>");
                        }
                    }
                }
            }

            if (_startkey.Count == 0)
            {
                throw new ApplicationException("RegistryListInput::Constructor You must specify at least one StartKey.");
            }

            _fi = new CSVFileInfo(Name);
        }
        #endregion

        #region PrivateFunctions
        void ProcessKey(string hive, string path, RegistryKey key, string fullname)
        {
            if (key == null)
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + "RegistryListInput skipping nonexistent key : " + fullname + ANSIHelper.White);
                return;
            }

            // for each key
            foreach (string v in key.GetValueNames())
            {
                if (_abort) break;

                // get the kind of value
                RegistryValueKind rvk = key.GetValueKind(v);
                object o = key.GetValue(v, null);
                string value = string.Empty;
                if (o != null)
                {
                    switch (rvk)
                    {
                        case RegistryValueKind.Binary:
                        case RegistryValueKind.Unknown:
                            byte[] barray = (byte[])o;

                            foreach (byte b in barray)
                            {
                                value = value + fixedparser.ColumnParsingRule.Hex((b & 0xF0) >> 4) + fixedparser.ColumnParsingRule.Hex((b & 0x0F));
                            }
                            break;
                        case RegistryValueKind.DWord:
                            Int32 i = (Int32)o;
                            value = fixedparser.ColumnParsingRule.Hex((int)((i & 0xF0000000) >> 28)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x0F000000) >> 24)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x00F00000) >> 20)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x000F0000) >> 16)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x0000F000) >> 12)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x00000F00) >> 8)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x000000F0) >> 4)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((i & 0x0000000F) >> 0)).ToString();
                            break;
                        case RegistryValueKind.ExpandString:
                            value = (string)o;
                            break;
                        case RegistryValueKind.MultiString:
                            string[] sarray = (string[])o;
                            foreach (string s in sarray)
                            {
                                if (value == string.Empty)
                                {
                                    value = s;
                                }
                                else
                                {
                                    value = value + "|" + s;
                                }
                            }
                            break;
                        case RegistryValueKind.QWord:
                            ulong l = (ulong)o;
                            value = fixedparser.ColumnParsingRule.Hex((int)((l & 0xF000000000000000) >> 60)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x0F00000000000000) >> 56)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x00F0000000000000) >> 52)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x000F000000000000) >> 48)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x0000F00000000000) >> 44)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x00000F0000000000) >> 40)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x000000F000000000) >> 36)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x0000000F00000000) >> 32)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x00000000F0000000) >> 28)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x000000000F000000) >> 24)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x0000000000F00000) >> 20)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x00000000000F0000) >> 16)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x000000000000F000) >> 12)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x0000000000000F00) >> 8)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x00000000000000F0) >> 4)).ToString() +
                                    fixedparser.ColumnParsingRule.Hex((int)((l & 0x000000000000000F) >> 0)).ToString();
                            break;
                        case RegistryValueKind.String:
                            value = (string)o;
                            break;
                    }
                }

                // create the output line
                CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/_fi);
                csv.AddEnd(hive);
                csv.AddEnd(path);
                if (key.Name.IndexOf('\\') == -1)
                {
                    csv.AddEnd(key.Name);
                }
                else
                {
                    csv.AddEnd(key.Name.Substring(key.Name.LastIndexOf('\\') + 1));
                }
                csv.AddEnd("Value");
                if (v == string.Empty)
                {
                    csv.AddEnd("(Default)");
                }
                else
                {
                    csv.AddEnd(v);
                }
                csv.AddEnd(rvk.ToString());
                csv.AddEnd(value);
                _job.Send(csv);
            }

            // now process each of the subkeys
            foreach (string k in key.GetSubKeyNames())
            {
                RegistryKey subKey = null;

                // get the key details
                try
                {
                    subKey = key.OpenSubKey(k);

                    // if we are including key details then send it
                    if (_includekeys)
                    {
                        CSVFileInfo fi = new CSVFileInfo(Name);
                        CSVFileLine csv = new CSVFileLine("", _line++, /*Name*/fi);
                        csv.AddEnd(hive);
                        csv.AddEnd(path);
                        csv.AddEnd(k);
                        csv.AddEnd("Key");
                        _job.Send(csv);
                    }

                }
                catch
                {
                    // ignore security errors
                }

                // if we successfully opened the key and we are recursing
                if (subKey != null && _recurse)
                {
                    // process the sub key
                    ProcessKey(hive, path + "\\" + k, subKey, "Not required because by definition this must exist");
                }
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                string s = "RegistryList::";
                if (_startkey.Count > 0)
                {
                    s = s + _startkey[0];
                }

                return s;
            }
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            // send the header line
            CSVFileInfo fi = new CSVFileInfo(Name);
            CSVFileLine cl = new CSVFileLine("Hive,Path,Key,Type,ValueName,ValueType,Value", _line, /*Name*/fi);
            if (_fd.In(cl) && _ppfd.In(cl))
            {
                cl.Parse();
                cl.Data = false;
                _job.Send(cl);
                _line++;
            }

            // process each of the starter keys
            for (int i = 0; i < _startkey.Count; i++)
            {
                if (_abort) break;

                if (_startkey[i] == string.Empty)
                {
                    ProcessKey(_hives[i].Name, "\\" + _hives[i].Name, _hives[i], "\\" + _hives[i].Name);
                }
                else
                {
                    ProcessKey(_hives[i].Name, "\\" + _hives[i].Name, _hives[i].OpenSubKey(_startkey[i]), "\\" + _hives[i].Name + "\\" + _startkey[i]);
                }
            }

            // tell the job we are done
            _job.Stop();
        }
        #endregion
    }

    /// <summary>
    /// Read a queue from another job
    /// </summary>
    class queueinput : Input
    {
        #region Member Variables
        NamedCSVMessageQueue _nmq = null; // the queue to read from
        string _name = string.Empty; // name of the queue
        // bool _noDelimiter = false; // true if we are to ignore delimiters
        bool _failonreject = false; // true if we are to abandon reading on reject
        Column _preprocess = null; // preprocess line as a single column
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"QueueInput::" + _name + "\"];");
        }

        #region Static Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "name":
                case "preprocessline":
                case "failonreject":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create the input queue
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public queueinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            SupportsProgress = false;

            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "name")
                    {
                        _name = n1.InnerText.ToLower();
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Name>" + _name.ToString() + "</Name>");
                        }

                        _name = "Queue::" + _name;
                    }
                    else if (n1.Name.ToLower() == "preprocessline")
                    {
                        // a line preprocessor
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PreprocessLine>");
                        }
                        try
                        {
                            // create the column processor to use to pre-process lines in the file
                            _preprocess = Column.Create(n1, j, null, null, tracexml, prefix + "    ");
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating preprocess routine", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </PreprocessLine>");
                        }
                    }
                    else if (n1.Name.ToLower() == "failonreject")
                    {
                        // fail if we find a rejected record
                        _failonreject = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnReject/>");
                        }
                    }
                }
            }

            // get the input queue
            _nmq = NamedCSVMessageQueue.GetQueue(_name);
            Debug.WriteLine("Input queue found named " + _nmq.Name);

            _fi = new CSVFileInfo(Name);
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input queue name
        /// </summary>
        public override string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Run the input queue
        /// </summary>
        public override void Run()
        {
            bool fContinue = true; // true to continue looping
            int line = 0;

            // loop processing messages
            do
            {
                // get the next message
                CSVMessage msg = (CSVMessage)_nmq.Queue.Dequeue();
                //Debug.WriteLine("Dequeued message on queue {" + Id + "} " + _nmq.Queue.Count.ToString() + " left.");

                if (msg.Status == CSVMessage.STATUS.STOP)
                {
                    Debug.WriteLine("InputQueue {" + Id + "} received a STOP ... telling our owning job we are done.");
                    _job.Stop();
                    fContinue = false;
                }
                else if (msg.Status == CSVMessage.STATUS.LINE)
                {
                    string l = msg.String;
                    bool failed = false;

                    // increment our line count
                    line++;

                    // if we are on a line number that is a mulitple of a <ProgressUpdateInterval> issue a status update to the user
                    if (line % _progressUpdateInterval == 0)
                    {
                        // progress based on line
                        Progress = line;
                        ReportProgress();
                    }

                    try
                    {
                        // if we have a preprocess
                        if (_preprocess != null)
                        {
                            // parse the line
                            ColumnLine cl = new ColumnLine(l);

                            // add the whole line as one column
                            cl.AddEnd(l);

                            // process it
                            l = _preprocess.Process(cl)[0]; // i refer to [0] but raw might also work. ToString() would be a bad idea
                        }
                    }
                    catch (System.Exception ex)
                    {
                        // we failed on this record
                        failed = true;

                        // reject it
                        Reject(l);

                        // tell the user
                        ReportError("Problem preprocessing the line : " + ex.Message);
                    }

                    if (!failed)
                    {
                        ColumnLine ppcsv = new ColumnLine(l);

                        if (_ppfd.In(ppcsv))
                        {
                            // parse the line
                            ColumnLine csv = null;
                            try
                            {
                                // if it has an input parser
                                if (_parser != null)
                                {
                                    csv = _parser.Parse(l, line);
                                    if (_fd.In(csv))
                                    {
                                        Send(csv);
                                    }
                                }
                            }
                            catch (System.Exception ex)
                            {
                                // tell the user
                                ReportError(ANSIHelper.Warning + "Problem parsing the input line # : " + line.ToString() + ANSIHelper.Information + " : " + ex.Message + ANSIHelper.White);
                                ReportError(l);

                                // reject the record
                                Reject(l);

                                // record that we failed
                                failed = true;
                            }
                        }
                    }


                    // if we failed and we are to stop
                    if (failed && _failonreject)
                    {
                        // tell the user
                        ReportError(ANSIHelper.Red + "Stopping due to rejected record." + ANSIHelper.White);
                        Stop();
                        fContinue = false;

                        // no line
                        l = null;
                    }
                }
            } while (fContinue); // loop forever

            Progress = line;
            ReportProgress();

            Debug.WriteLine("InputQueue {" + Id + "} is done.");
        }
        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }
        #endregion
    }

    /// <summary>
    /// Read from multiple files
    /// </summary>
    class multifileinput : Input
    {
        #region Sub Classes
        /// <summary>
        /// A sortable filename
        /// </summary>
        public class SortableFileName : IComparable
        {
            #region Member Variables
            static bool __reverse = false;
            string _fullfile = string.Empty; // the full file name
            string _fileonly = string.Empty; // the file name only
            string _zipfile = string.Empty; // tje zip file name
            long _length = 0;
            string _realfilename = string.Empty; // the full file name
            string _sortname = string.Empty; // the full file name
            #endregion

            static public bool Reverse
            {
                get
                {
                    return __reverse;
                }
                set
                {
                    __reverse = true;
                }
            }

            public bool IsDirectory
            {
                get
                {
                    return Directory.Exists(_fullfile);
                }
            }

            #region Accessors
            /// <summary>
            /// Get the file length
            /// </summary>
            public long Length
            {
                get
                {
                    return _length;
                }
            }

            /// <summary>
            /// Get the filename only
            /// </summary>
            public string FilenameOnly
            {
                get
                {
                    return _fileonly;
                }
            }

            public string SortName
            {
                get
                {
                    return _sortname;
                }
            }

            /// <summary>
            /// Get the full file name
            /// </summary>
            public string FullFilename
            {
                get
                {
                    return _fullfile;
                }
            }

            /// <summary>
            /// Get the zip file name
            /// </summary>
            public string ZipFile
            {
                get
                {
                    return _zipfile;
                }
            }
            #endregion

            #region IComparable
            /// <summary>
            /// Comparator used to compare 2 files
            /// </summary>
            /// <param name="o">object to compare - must be a sortable file</param>
            /// <returns></returns>
            public int CompareTo(object o)
            {
                SortableFileName sfn = o as SortableFileName;
                if (_sortname == null)
                {
                    if (__reverse)
                    {
                        return string.Compare(sfn.FilenameOnly, FilenameOnly);
                    }
                    else
                    {
                        return string.Compare(FilenameOnly, sfn.FilenameOnly);
                    }
                }
                else
                {
                    if (__reverse)
                    {
                        return string.Compare(sfn.SortName, _sortname);
                    }
                    else
                    {
                        return string.Compare(_sortname, sfn.SortName);
                    }
                }
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Create a sortable line
            /// </summary>
            /// <param name="s">The line</param>
            /// <param name="keycols">The key columns</param>
            public SortableFileName(string sortname, string realfilename, long length)
            {
                _length = length;
                _sortname = sortname;

                // save the file name
                if (realfilename.IndexOf("::") != -1)
                {
                    _zipfile = realfilename.Substring(0, realfilename.IndexOf("::"));
                    string s = realfilename.Substring(realfilename.IndexOf("::") + 2);
                    _fileonly = Path.GetFileName(s.Substring(0, s.IndexOf("::")));
                    _fullfile = s.Substring(0, s.IndexOf("::"));
                }
                else
                {
                    _fileonly = Path.GetFileName(realfilename);
                    _fullfile = realfilename;
                }
            }
            #endregion
        }
        #endregion

        #region Member Variables
        List<SortableFileName> _files = new List<SortableFileName>(); // the list of files to process
        bool _sortFileNames = false; // true if file names need sorting
        bool _reversesortFileNames = false; // true if file names need sorting
        bool _lookInArchives = false; // true if we are to process zip files
        bool _processSubdirectories = false; // true if we are to recurse through subdirectories
        List<string> _filePattern = new List<string>(); // the file pattern to match when processing a directory
        string _origPattern = "*.*"; // the default pattern
        XmlNode _baseNode = null; // the xml node that holds the config settings we will be reusing
        bool _tracexml = false; // save the tracexml flag
        Column _sortfilename = null;
        LogicFilterNode _filenamefilter = new AndFilterNode(); // holds the default AND filter
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.Write(prefix + UniqueId + " [label=\"MultiFileInput::");
            foreach (SortableFileName sfn in _files)
            {
                if (sfn.IsDirectory)
                {
                    if (_filePattern.Count == 0)
                    {
                        dotWriter.Write(sfn.FullFilename.Replace("\\", "\\\\") + "\\\\" + _origPattern + "\\n");
                    }
                    else
                    {
                        foreach (string p in _filePattern)
                        {
                            dotWriter.Write(sfn.FullFilename.Replace("\\", "\\\\") + "\\\\" + p + "\\n");
                        }
                    }
                }
                else
                {
                    dotWriter.Write(sfn.FullFilename.Replace("\\", "\\\\") + "\\n");
                }
            }
            dotWriter.WriteLine("\"];");
        }

        #region Private Functions
        /// <summary>
        /// Extract files from a zip file
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        List<SortableFileName> ProcessZip(string file)
        {
            List<SortableFileName> al = new List<SortableFileName>(); // list of files to return

            foreach (string pat in _filePattern)
            {
                // create the regex to check the file name
                string regexs = "^" + pat + "$";
                regexs = regexs.Replace(".", "\\.");
                regexs = regexs.Replace("*", ".*?");
                Regex regex = new Regex(regexs, RegexOptions.Singleline | RegexOptions.IgnoreCase);

                // open the archive
                using (ZipArchive archive = ZipArchive.OpenOnFile(file))
                {
                    // look at each file in the archive
                    foreach (ZipArchive.ZipFileInfo zfi in archive.Files)
                    {
                        // if it is a file
                        if (!zfi.FolderFlag)
                        {
                            // open the file stream
                            using (Stream s = zfi.GetStream())
                            {
                                // add the file to the list of files to process
                                SortableFileName sfn = null;
                                if (_sortfilename != null)
                                {
                                    ColumnLine cl = new ColumnLine(zfi.Name);
                                    cl.Parse();
                                    sfn = new SortableFileName(_sortfilename.Process(cl).Items[0], file + "::" + zfi.Name + "::ZIP", s.Length);
                                }
                                else
                                {
                                    sfn = new SortableFileName(null, file + "::" + zfi.Name + "::ZIP", s.Length);
                                }
                                if (regex.IsMatch(sfn.FilenameOnly))
                                {
                                    ColumnLine filenameline = new ColumnLine();
                                    filenameline.AddEnd(sfn.FullFilename);
                                    filenameline.AddEnd(Path.GetDirectoryName(file));
                                    filenameline.AddEnd(Path.GetFileNameWithoutExtension(file));
                                    filenameline.AddEnd(Path.GetExtension(file));
                                    filenameline.AddEnd(Path.GetFileName(file));
                                    filenameline.AddEnd(zfi.Name);
                                    if (_filenamefilter.Evaluate(filenameline))
                                    {
                                        if (al.Contains(sfn))
                                        {
                                            CSVProcessor.DisplayError(
                                                ANSIHelper.Warning +
                                                "Duplicate filenames found in zip file. Only one will be processed. " +
                                                sfn.FullFilename + "\n" + ANSIHelper.White);
                                        }
                                        else
                                        {
                                            al.Add(sfn);
                                            Size += s.Length;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return al;
        }

        /// <summary>
        /// Process input files in a directory
        /// </summary>
        /// <param name="d">directory to process</param>
        List<SortableFileName> ProcessDirectory(DirectoryInfo d)
        {
            List<SortableFileName> al = new List<SortableFileName>(); // list of files to return
            List<string> files = new List<string>();

            // get the files in the directory
            foreach (string s in _filePattern)
            {
                foreach (string f in Directory.GetFiles(d.ToString(), s))
                {
                    ColumnLine filenameline = new ColumnLine();
                    filenameline.AddEnd(f);
                    filenameline.AddEnd(Path.GetDirectoryName(f));
                    filenameline.AddEnd(Path.GetFileNameWithoutExtension(f));
                    filenameline.AddEnd(Path.GetExtension(f));
                    filenameline.AddEnd(Path.GetFileName(f));
                    filenameline.AddEnd("");
                    if (_filenamefilter.Evaluate(filenameline))
                    {
                        files.Add(f);
                    }
                }
            }

            // look at each file
            foreach (string f in files)
            {
                // add the size of the files to the work we need to do
                FileInfo fi = new FileInfo(f);
                Size += fi.Length;

                // add it to my list
                if (_sortfilename != null)
                {
                    ColumnLine cl = new ColumnLine(f);
                    cl.Parse();
                    al.Add(new SortableFileName(_sortfilename.Process(cl).Items[0], f, fi.Length));
                }
                else
                {
                    al.Add(new SortableFileName(null, f, fi.Length));
                }
            }

            if (_lookInArchives)
            {
                string[] zips = Directory.GetFiles(d.ToString(), "*.zip");

                // look at each file
                foreach (string f in zips)
                {
                    if (new FileInfo(f).Length == 0)
                    {
                        CSVProcessor.DisplayError(
                            ANSIHelper.Warning +
                            "Zero length zip file found. Zip file will be ignored. " +
                            f + "\n" + ANSIHelper.White);
                    }
                    else
                    {
                        // add it to my list
                        try
                        {
                            var ff = ProcessZip(f);
                            foreach (var fff in ff)
                            {
                                if (al.Contains(fff))
                                {
                                    CSVProcessor.DisplayError(
                                        ANSIHelper.Warning +
                                        "Duplicate filenames found in zip file. Only one will be processed. " +
                                        fff.FullFilename + "\n" + ANSIHelper.White);
                                }
                                else
                                {
                                    al.Add(fff);
                                }
                            }
                        }
                        catch (System.Exception e)
                        {
                            if (e.InnerException.Message.Contains("Item has already been added. Key in dictionary"))
                            {
                                CSVProcessor.DisplayError(
                                    ANSIHelper.Warning +
                                    "Duplicate filenames found in zip file causing exception opening it. Zip file will be ignored. " +
                                    f + "\n" + ANSIHelper.White);
                            }
                            else
                            {
                                CSVProcessor.DisplayError(
                                    ANSIHelper.Error +
                                    "Error processing zip file. It will be ignored. " +
                                    f + " " + e.InnerException.Message + "\n" + ANSIHelper.White);

                                //throw new ApplicationException("Error accessing ZIP file " + f, e);
                            }
                        }
                    }
                }
            }

            // are we to process subdirectories
            if (_processSubdirectories)
            {
                // look at each subdirectory
                foreach (string subdir in Directory.GetDirectories(d.ToString()))
                {
                    // recurse
                    al.AddRange(ProcessDirectory(new DirectoryInfo(subdir)));
                }
            }

            // return the list of files
            return al;
        }

        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, null, tracexml, prefix + "  ")));
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if the file looks like it is a zip file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        bool IsZip(string file)
        {
            if (file.EndsWith(".zip") || file.EndsWith(".gz") || file.EndsWith(".tar") || file.EndsWith(".z") || file.EndsWith(".gzip"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Static Functions
        static bool __recurse = false;

        public new static bool Supports(string s)
        {
            if (__recurse)
            {
                return false;
            }

            switch (s)
            {
                case "file":
                case "inputfile":
                case "sortfilenames":
                case "sortfilename":
                case "reversesortfilenames":
                case "lookinarchives":
                case "processsubdirectories":
                case "filenamepattern":
                case "filenamefilter":
                    return true;
                default:
                    __recurse = true;
                    bool f = fileinput.Supports(s);
                    __recurse = false;
                    return f;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create an input which processes multiple files and/or directories
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public multifileinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            List<string> allFiles = files; // the list of files to scan

            // save the trace flag
            _tracexml = tracexml;

            // save the base xml node. I will need to look at this each time to get the parameters
            _baseNode = n;

            // now add any in the xml file
            foreach (XmlNode n4 in n.ChildNodes)
            {
                // If this is a text node then they have just given us a file name
                if (n4.NodeType != XmlNodeType.Comment)
                {
                    // add a file or a directory
                    if (n4.Name.ToLower() == "file" || n4.Name.ToLower() == "inputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "    <File>");
                        }

                        StringOrColumn sc = new StringOrColumn(n4/*n5*/, j, null, null, tracexml, prefix + "   ");
                        allFiles.Add(sc.Value());
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "    " + sc.Value());
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "    </File>");
                        }
                    }
                    else if (n4.Name.ToLower() == "sortfilenames")
                    {
                        // save that we are to sort files
                        _sortFileNames = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SortFilenames/>");
                        }
                    }
                    else if (n4.Name.ToLower() == "reversesortfilenames")
                    {
                        // save that we are to sort files
                        _reversesortFileNames = true;
                        SortableFileName.Reverse = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ReverseSortFilenames/>");
                        }
                    }
                    else if (n4.Name.ToLower() == "sortablename")
                    {
                        // save that we are to sort files
                        try
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <SortableName>");
                            }

                            foreach (XmlNode n5 in n4.ChildNodes)
                            {
                                if (n5.NodeType != XmlNodeType.Comment)
                                {
                                    if (_sortfilename != null)
                                    {
                                        throw new ApplicationException("MultiFileInput::Constructor only one <SortableName> is permitted.");
                                    }

                                    _sortfilename = Column.Create(n5, j, null, null, tracexml, prefix + "   ");
                                }
                            }

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </SortableNameRegex>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("MultiFileInput::Constructor error parsing <SortableName>.", ex);
                        }
                    }
                    else if (n4.Name.ToLower() == "lookinarchives")
                    {
                        // save that we are to sort files
                        _lookInArchives = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <LookInArchives/>");
                        }
                    }
                    else if (n4.Name.ToLower() == "processsubdirectories")
                    {
                        // save that we are to process subdirectories
                        _processSubdirectories = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ProcessSubdirectories/>");
                        }
                    }
                    else if (n4.Name.ToLower() == "filenamefilter")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FileNameFilter>");
                        }

                        ParseFilter(_filenamefilter, j, n4, tracexml, prefix + "    ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </FileNameFilter>");
                        }
                    }
                    else if (n4.Name.ToLower() == "filenamepattern")
                    {
                        // look at the children
                        foreach (XmlNode n5 in n4.ChildNodes)
                        {
                            // If this is a text node then they have just given us a file name
                            if (n5.NodeType == XmlNodeType.Text || n5.NodeType == XmlNodeType.CDATA)
                            {
                                _origPattern = n5.InnerText;

                                string[] pats = n5.InnerText.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string s in pats)
                                {
                                    _filePattern.Add(s);
                                }
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <FileNamePattern>" + n5.InnerText + "</FileNamePattern>");
                                }
                            }
                            else if (n5.NodeType != XmlNodeType.Comment)
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <FileNamePattern>");
                                }
                                // create the output column
                                Column c = Column.Create(n5, j, null, null, tracexml, prefix + "    ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </FileNamePattern>");
                                }

                                // Use the output column to generate the file name
                                _filePattern.Add(c.Process(new ColumnLine()).ToString(false));
                            }
                        }
                    }
                }
            }

            if (_sortFileNames && _reversesortFileNames)
            {
                throw new ApplicationException("MultiFileInput::Constructor Cannot specify both <SortFileNames/> and <ReverseSortFileNames/>.");
            }

            // if we have no pattern then add the default.
            if (_filePattern.Count == 0)
            {
                _filePattern.Add(_origPattern);
            }

            // for each file
            foreach (string file in allFiles)
            {
                // if it is a directory
                if (Directory.Exists(file))
                {
                    _files.AddRange(ProcessDirectory(new DirectoryInfo(file)));
                }
                else if (IsZip(file.ToLower()) && File.Exists(file) && _lookInArchives)
                {
                    _files.AddRange(ProcessZip(file));
                }
                // if it is a file
                else if (File.Exists(file))
                {
                    FileInfo fi = new FileInfo(file);
                    Size += fi.Length;

                    if (_sortfilename != null)
                    {
                        ColumnLine cl = new ColumnLine(file);
                        cl.Parse();
                        _files.Add(new SortableFileName(_sortfilename.Process(cl).Items[0], file, fi.Length));
                    }
                    else
                    {
                        _files.Add(new SortableFileName(null, file, fi.Length));
                    }
                }
                else
                {
                    // not a valid file
                    CSVProcessor.DisplayError(ANSIHelper.Red + "Error processing file : " + file + " : Not Found : CWD : " + Directory.GetCurrentDirectory() + ANSIHelper.White);
                }
            }

            // if we are to sort filenames
            if (_sortFileNames)
            {
                // do the sort
                _files.Sort();
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Read the file
        /// </summary>
        public override void Run()
        {
            bool first = true;

            // for each file we are to process
            foreach (SortableFileName sfn in _files)
            {
                if (_abort) break;

                // let them know we are processing a new file
                NewFile(sfn.FullFilename);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "MultiFileInput processing file " + sfn.FullFilename + "\n" + ANSIHelper.White);
                }

                // create a file input to read it
                fileinput fi = new fileinput(_job, _baseNode, _tracexml, "", sfn);
                fi.PostCreate(_baseNode.ParentNode, _tracexml, "");

                fi.FirstFile = first;
                first = false;

                // tell file input to suppress the stop message as there may be more files
                fi.SuppressStop = true;

                // start up the file input
                fi.Start();

                // wait for it to finish
                fi.Wait();

                // update our progress
                Progress += sfn.Length;

                ReportProgress();
            }

            // we need to send a dummy new file
            NewFile(string.Empty);

            // now send the stop message
            Stop();
        }

        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                return "MultiFile::" + _origPattern;
            }
        }

        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }

        #endregion
    }

    //class sqltraceinput : Input
    //{
    //    #region Member Variables
    //    //string _filename = string.Empty; // the name of the file
    //    TraceFile _traceFile = new TraceFile(); // the trace file we are processing
    //    #endregion

    //    public override void WriteDot(StreamWriter dotWriter, string prefix)
    //    {
    //        dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _fi.FullFileName.Replace("\\", "\\\\") + "\"];");
    //    }

    //    #region Private Functions
    //    /// <summary>
    //    /// OPen the trace file
    //    /// </summary>
    //    void Open()
    //    {
    //        try
    //        {
    //            _traceFile.InitializeAsReader(/*_filename*/_fi.FullFileName);
    //        }
    //        catch (System.Exception ex)
    //        {
    //            throw new ApplicationException("Problem opening SQL Trace File : " + _fi.FullFileName, ex);
    //        }
    //    }

    //    /// <summary>
    //    /// Close the trace file
    //    /// </summary>
    //    void Close()
    //    {
    //        _traceFile.Close();
    //    }
    //    #endregion

    //    #region Static Functions
    //    public new static bool Supports(string s)
    //    {
    //        switch (s)
    //        {
    //            case "file":
    //            case "inputfile":
    //                return true;
    //            default:
    //                return Input.Supports(s);
    //        }
    //    }
    //    #endregion

    //    #region Constructors
    //    public sqltraceinput(CSVJob j, XmlNode n, bool tracexml, string prefix, multifileinput.SortableFileName file)
    //        : base(j, n, tracexml, prefix)
    //    {
    //        _fi = new CSVFileInfo(Path.GetFullPath(file.FullFilename));
    //        Open();
    //    }

    //    public sqltraceinput(CSVJob j, XmlNode n, bool tracexml, string prefix, string file)
    //        : base(j, n, tracexml, prefix)
    //    {
    //        _fi = new CSVFileInfo(Path.GetFullPath(file));
    //        Open();
    //    }

    //    public sqltraceinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
    //        : base(j, n, tracexml, prefix)
    //    {
    //        string file = string.Empty;

    //        // process the configuration to look for files
    //        foreach (XmlNode n1 in n.ChildNodes)
    //        {
    //            if (n1.NodeType != XmlNodeType.Comment)
    //            {
    //                if (n1.Name.ToLower() == "file" || n1.Name.ToLower() == "inputfile")
    //                {
    //                    // check we dont already have one
    //                    if (file != string.Empty)
    //                    {
    //                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an SQLTraceInput input processor. Ignored file " + n1.InnerText + "\n" + ANSIHelper.White);
    //                    }
    //                    else
    //                    {
    //                        if (tracexml)
    //                        {
    //                            CSVProcessor.DisplayError(prefix + "    <File>");
    //                        }
    //                        StringOrColumn sc = new StringOrColumn(n1, j, null, null, tracexml, prefix + "   ");
    //                        file = sc.Value();
    //                        if (tracexml)
    //                        {
    //                            CSVProcessor.DisplayError(prefix + "    </File>");
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        // check we dont have a file + command line files
    //        if (file != string.Empty && files.Count > 0)
    //        {
    //            foreach (string s in files)
    //            {
    //                CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an SQLTraceInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
    //            }
    //        }

    //        if (file == string.Empty && files.Count == 0)
    //        {
    //            throw new ApplicationException("SQLTraceInput No file specified");
    //        }

    //        if (file == string.Empty)
    //        {
    //            // save the first file
    //            file = (string)files[0];

    //            // if there is more than 1 file
    //            if (files.Count > 1)
    //            {
    //                // display the errors
    //                foreach (string s in files)
    //                {
    //                    if (s != file)
    //                    {
    //                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an SQLTraceInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
    //                    }
    //                }
    //            }
    //        }

    //        // save the filename
    //        _fi = new CSVFileInfo(new FileInfo(file));

    //        Open();
    //    }
    //    #endregion

    //    #region Override
    //    /// <summary>
    //    /// Get the input name
    //    /// </summary>
    //    public override string Name
    //    {
    //        get
    //        {
    //            return _fi.FullFileName;
    //        }
    //    }

    //    /// <summary>
    //    /// Process the file
    //    /// </summary>
    //    public override void Run()
    //    {
    //        long line = 0;

    //        // while we have trace entries to read
    //        while (_traceFile.Read())
    //        {
    //            if (_abort) break;

    //            line++;

    //            // if we are on a line number that is a mulitple of a <ProgressUpdateInterval> issue a status update to the user
    //            if (line % _progressUpdateInterval == 0)
    //            {
    //                // progress based on line
    //                Progress = line;
    //                ReportProgress();
    //            }

    //            ColumnLine csv = new ColumnLine("", line, /*_filename*/_fi);

    //            for (int i = 0; i < _traceFile.FieldCount; i++)
    //            {
    //                csv.AddEnd(_traceFile.GetString(i));
    //            }

    //            Send(csv);
    //        }

    //        // progress based on line
    //        Progress = line;
    //        ReportProgress();

    //        Close();

    //        // synchronise
    //        Synch();

    //        Stop();
    //    }
    //    #endregion
    //}

    /// <summary>
    /// Read from a single file
    /// </summary>
    class fileinput : TextReaderInput
    {
        #region Member Variables
        string _zipfile = string.Empty; // the zip file name
        string _fileinzipfile = string.Empty; // the name of the file in a zip file
        bool _fReadStreamDirectly = true; // should we not write zip to a temp file
        string _tempfilename = string.Empty;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"FileInput::" + _fi.FullFileName.Replace("\\", "\\\\") + "\"];");
        }

        #region Static Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "usetempfiles":
                case "file":
                case "inputfile":
                    return true;
                default:
                    return TextReaderInput.Supports(s) || multifileinput.Supports(s);
            }
        }
        #endregion

        #region Constructor
        void CommonConstructor(XmlNode n, bool tracexml, string prefix)
        {
            FirstFile = true;

            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "usetempfiles")
                    {
                        _fReadStreamDirectly = false;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <UseTempFiles/>");
                        }
                    }
                }
            }
        }

        public fileinput(CSVJob j, XmlNode n, bool tracexml, string prefix, multifileinput.SortableFileName file)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            if (file.ZipFile != string.Empty)
            {
                _zipfile = file.ZipFile;
                _fileinzipfile = file.FullFilename;
                //_filename = Path.GetTempFileName();
                _tempfilename = Path.GetTempFileName();

                // open the zip file
                ZipArchive za = ZipArchive.OpenOnFile(_zipfile);
                Stream s = null;

                // look at each file in the zip file
                foreach (ZipArchive.ZipFileInfo zfi in za.Files)
                {
                    // if it is the file we want
                    if (zfi.Name == _fileinzipfile)
                    {
                        // get the file
                        s = zfi.GetStream();
                        _fi = new CSVFileInfo(Path.GetFullPath(_zipfile) + "\\" + zfi.Name, zfi.LastModFileDateTime, zfi.LastModFileDateTime, s.Length, "");

                        // if we are reading it without writing it to a temporary file
                        if (_fReadStreamDirectly)
                        {
                            // create the right sort of reader
                            if (_blocksize == 0)
                            {
                                _tr = new StreamReader(s, true);
                            }
                            else
                            {
                                _br = new BinaryReader(s);
                            }
                        }
                        else
                        {
                            // write the file to a tempory file
                            FileStream streamWriter = File.Create(_tempfilename);
                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            streamWriter.Close();
                        }
                        break;
                    }
                }

                // if we are not reading it directly then we can close the zip file
                if (!_fReadStreamDirectly)
                {
                    s.Close();
                    za.Dispose();
                }

                // if we are not reading it directly then we can close the zip file
                if (!_fReadStreamDirectly)
                {
                    // open the file
                    try
                    {
                        if (_blocksize == 0)
                        {
                            _tr = new StreamReader(_tempfilename);
                        }
                        else
                        {
                            _br = new BinaryReader(new FileStream(_tempfilename, FileMode.Open, FileAccess.Read));
                        }
                    }
                    catch (System.Exception ex)
                    {
                        throw new ApplicationException("Error opening file : " + _zipfile + "::" + _fileinzipfile, ex);
                    }
                }

                // set the size of the file
                Size += file.Length;
            }
            else
            {
                // save the filename
                string filename = file.FullFilename;
                _fi = new CSVFileInfo(new FileInfo(filename));

                // open the file
                try
                {
                    int tries = 0;
                    while (_tr == null && _br == null)
                    {
                        try
                        {
                            if (_blocksize == 0)
                            {
                                _tr = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                            }
                            else
                            {
                                _br = new BinaryReader(new FileStream(filename, FileMode.Open, FileAccess.Read));
                            }
                            if (tries > 0)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Information + "FileInput::OpenFile " + filename + " successfully opened." + ANSIHelper.White);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            if (ex.Message.Contains("used by another process"))
                            {
                                // display error every 5 seconds
                                if (tries % 50 == 0)
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Error + "FileInput::OpenFile Error opening " + filename + " because it is in use by another process. Close it to continue." + ANSIHelper.White);
                                }
                                tries++;
                                Thread.Sleep(100);
                            }
                            else
                            {
                                throw ex;
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    throw new ApplicationException("Error opening file : " + filename, ex);
                }

                // set the size of the file
                Size += _fi.Size;
            }
        }

        /// <summary>
        /// Create an input file
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="file"></param>
        public fileinput(CSVJob j, XmlNode n, bool tracexml, string prefix, string file)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            // save the filename
            _fi = new CSVFileInfo(new FileInfo(file));

            // open the file
            try
            {
                if (_blocksize == 0)
                {
                    _tr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                }
                else
                {
                    _br = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read));
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening file : " + file, ex);
            }

            // set the size of the file
            Size += _fi.Size;
        }

        /// <summary>
        /// Create an input file
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public fileinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            string file = string.Empty; // file we are going to process

            // process the configuration to look for files
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "file" || n1.Name.ToLower() == "inputfile")
                    {
                        // check we dont already have one
                        if (file != string.Empty)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileInput input processor. Ignored file " + n1.InnerText + "\n" + ANSIHelper.White);
                        }
                        else
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    <File>");
                            }
                            StringOrColumn sc = new StringOrColumn(n1, j, null, null, tracexml, prefix + "   ");
                            file = sc.Value();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    </File>");
                            }
                        }
                    }
                }
            }

            // check we dont have a file + command line files
            if (file != string.Empty && files.Count > 0)
            {
                foreach (string s in files)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
                }
            }

            if (file == string.Empty && files.Count == 0)
            {
                throw new ApplicationException("No file specified");
            }

            if (file == string.Empty)
            {
                // save the first file
                file = (string)files[0];

                // if there is more than 1 file
                if (files.Count > 1)
                {
                    // display the errors
                    foreach (string s in files)
                    {
                        if (s != file)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
                        }
                    }
                }
            }

            // save the filename
            FileInfo fi = new FileInfo(file);
            if (!fi.Exists)
            {
                throw new ApplicationException("Error opening file : " + fi.FullName);
            }

            _fi = new CSVFileInfo(fi);

            // open the file
            try
            {
                if (_blocksize == 0)
                {
                    _tr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                }
                else
                {
                    _br = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read));
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening file : " + file, ex);
            }
        }
        #endregion

        #region Override
        /// <summary>
        /// Close the input
        /// </summary>
        protected override void Close()
        {
            if (_blocksize == 0)
            {
                _tr.Close();
            }
            else
            {
                _br.Close();
            }
            // if this came from a zip file
            if (_zipfile != string.Empty)
            {
                try
                {
                    // delete the temporary file
                    File.Delete(_tempfilename);
                }
                catch
                {
                    // ignore errors
                }
            }
        }

        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                if (_zipfile != string.Empty)
                {
                    return _zipfile + "::" + _fileinzipfile;
                }
                else
                {
                    return _fi.FullFileName;
                }
            }
        }
        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Read from a single file
    /// </summary>
    class filemonitorinput : TextReaderInput
    {
        #region Member Variables
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"FileMonitorInput::" + _fi.FullFileName.Replace("\\", "\\\\") + "\"];");
        }

        #region Static Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "file":
                case "inputfile":
                    return true;
                default:
                    return TextReaderInput.Supports(s);
            }
        }
        #endregion

        #region Constructor
        void CommonConstructor(XmlNode n, bool tracexml, string prefix)
        {
            FirstFile = true;
            _monitor = true;
            SupportsProgress = false;
        }

        public filemonitorinput(CSVJob j, XmlNode n, bool tracexml, string prefix, multifileinput.SortableFileName file)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            // save the filename
            _fi = new CSVFileInfo(new FileInfo(file.FullFilename));

            // open the file
            try
            {
                if (_blocksize == 0)
                {
                    _tr = new StreamReader(new FileStream(file.FullFilename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                }
                else
                {
                    _br = new BinaryReader(new FileStream(file.FullFilename, FileMode.Open, FileAccess.Read));
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening file : " + file.FullFilename, ex);
            }

            // set the size of the file
            Size += _fi.Size;
        }

        /// <summary>
        /// Create an input file
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="file"></param>
        public filemonitorinput(CSVJob j, XmlNode n, bool tracexml, string prefix, string file)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            // save the filename
            _fi = new CSVFileInfo(new FileInfo(file));

            // open the file
            try
            {
                if (_blocksize == 0)
                {
                    _tr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                }
                else
                {
                    _br = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read));
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening file : " + file, ex);
            }

            // set the size of the file
            Size += _fi.Size;
        }

        /// <summary>
        /// Create an input file
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public filemonitorinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            CommonConstructor(n, tracexml, prefix);

            string file = string.Empty; // file we are going to process

            // process the configuration to look for files
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "file" || n1.Name.ToLower() == "inputfile")
                    {
                        // check we dont already have one
                        if (file != string.Empty)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileMonitorInput input processor. Ignored file " + n1.InnerText + "\n" + ANSIHelper.White);
                        }
                        else
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    <File>");
                            }
                            StringOrColumn sc = new StringOrColumn(n1, j, null, null, tracexml, prefix + "   ");
                            file = sc.Value();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    </File>");
                            }
                        }
                    }
                }
            }

            // check we dont have a file + command line files
            if (file != string.Empty && files.Count > 0)
            {
                foreach (string s in files)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileMonitorInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
                }
            }

            if (file == string.Empty && files.Count == 0)
            {
                throw new ApplicationException("No file specified");
            }

            if (file == string.Empty)
            {
                // save the first file
                file = (string)files[0];

                // if there is more than 1 file
                if (files.Count > 1)
                {
                    // display the errors
                    foreach (string s in files)
                    {
                        if (s != file)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Multiple input files found for an FileMonitorInput input processor. Ignored file " + s + "\n" + ANSIHelper.White);
                        }
                    }
                }
            }

            // save the filename
            _fi = new CSVFileInfo(new FileInfo(file));

            // open the file
            try
            {
                if (_blocksize == 0)
                {
                    _tr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                }
                else
                {
                    _br = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read));
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error opening file : " + file, ex);
            }
        }
        #endregion

        #region Override
        /// <summary>
        /// Close the input
        /// </summary>
        protected override void Close()
        {
            if (_blocksize == 0)
            {
                _tr.Close();
            }
            else
            {
                _br.Close();
            }
        }

        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                return _fi.FullFileName;
            }
        }

        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Base class for any text reader
    /// </summary>
    abstract class TextReaderInput : Input
    {
        #region Member Variables
        protected TextReader _tr = null; // the stream to read
        protected BinaryReader _br = null; // the stream to read
        Regex _newLineRegex = null; // regular expression used to detect start of a new line
        bool _suppressMaxLine = false; // if true max line size checking is disabled
        Regex _endLineRegex = null; // regular expression used to detect end of a line
        string _newLineSeparator = string.Empty; // chars to insert where a line is joined
        bool _suppressStop = false; // true if we should not raise a stop message
        long _skipBytes = 0; // bytes to skip at the start of the file
        long _stopBytes = 0; // where to stop reading
        bool _failonreject = false; // true if we are to abandon reading on reject
        Column _preprocess = null; // preprocess line as a single column
        bool _displaystartstopline = true; // should we show the user where we started and stopped
        protected int _blocksize = 0; // default block size read
        Column _variableblocksize = null;
        const int MAXVARIABLEBLOCKSIZE = 16000;
        protected bool _monitor = false; // should we monitor the file for changes
        LogicFilterNode _skipuntil = null;
        bool _includeskipendrow = false;
        bool _includeskipendrowinfirstfileonly = false;
        bool _skipuntilfound = false;
        bool _firstfile = false;
        #endregion

        #region Accessors
        /// <summary>
        /// Tell the input not to raise the stop event
        /// </summary>
        public bool SuppressStop
        {
            set
            {
                _suppressStop = value;
            }
        }
        public bool FirstFile
        {
            set
            {
                _firstfile = value;
            }
        }
        #endregion

        #region Abstract Functions
        protected abstract void Close();
        #endregion

        #region Private Functions
        /// <summary>
        /// Prepare a line and send it off
        /// </summary>
        /// <param name="line"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        bool DispatchLine(ref long line, string l)
        {
            bool failed = false;

            // if we are on a line number that is a mulitple of a <ProgressUpdateInterval> issue a status update to the user
            if (line % _progressUpdateInterval == 0)
            {
                // progress based on line
                Progress = line;
                ReportProgress();
            }

            try
            {
                // if we have a preprocess
                if (_preprocess != null)
                {
                    // parse the line
                    ColumnLine cl = new ColumnLine(l);

                    // add the whole line as one column
                    cl.AddEnd(l);

                    // process it
                    l = _preprocess.Process(cl)[0];
                }
            }
            catch (System.Exception ex)
            {
                // we failed on this record
                failed = true;

                // reject it
                Reject(l);

                // tell the user
                ReportError("Problem preprocessing the line : " + ex.Message);
            }

            // if everything is ok
            if (!failed)
            {
                ColumnLine ppcsv = new ColumnLine(l);

                if (_ppfd.In(ppcsv))
                {

                    // parse the line
                    ColumnLine csv = null;
                    try
                    {
                        // if we have a parser
                        if (_parser != null)
                        {
                            csv = _parser.Parse(l, line);
                            if (_fd.In(csv))
                            {
                                if (_skipuntil != null && !_skipuntilfound)
                                {
                                    _skipuntilfound = _skipuntil.Evaluate(csv);

                                    if (_skipuntilfound && ((_includeskipendrowinfirstfileonly && _firstfile) || _includeskipendrow))
                                    {
                                        // now that we are past our skip this is line 1
                                        csv.OverrideLineNumber(1);
                                        line = 1;
                                        Send(csv);
                                    }
                                    else
                                    {
                                        // next line will be line 1
                                        line = 0;
                                    }
                                }
                                else
                                {
                                    Send(csv);
                                }
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        // tell the user
                        ReportError(ANSIHelper.Error + "Problem parsing the input line # : " + line.ToString() + " : " + ex.Message + ANSIHelper.White);
                        ReportError(l);

                        // reject the record
                        Reject(l);

                        // record that we failed
                        failed = true;
                    }
                }
            }

            return failed;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Handle a file changed event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnChanged(object source, FileSystemEventArgs e)
        {
            string l = string.Empty; // the line
            byte[] buffer = null; // a buffer to read blocks into

            if (_blocksize > 0)
            {
                if (_variableblocksize == null)
                {
                    buffer = new byte[_blocksize];
                }
                else
                {
                    buffer = new byte[MAXVARIABLEBLOCKSIZE];
                }
            }

            // read the first line
            if (_blocksize != 0)
            {
                l = string.Empty;
                int read = _br.Read(buffer, 0, _blocksize);

                if (read == 0)
                {
                    l = null;
                }
                else
                {
                    for (int i = 0; i < read; i++)
                    {
                        l += (char)buffer[i];
                    }
                }

                if (_variableblocksize != null)
                {
                    int variablesize = 0;

                    //calculate the variable size
                    ColumnLine cl = new ColumnLine(l);
                    string vbs = "";
                    try
                    {
                        vbs = _variableblocksize.Process(cl)[0];
                        variablesize = int.Parse(vbs);
                        if (variablesize > MAXVARIABLEBLOCKSIZE)
                        {
                            throw new ApplicationException("Error - Variable block size " + variablesize.ToString() + " greater than maximum allowed " + MAXVARIABLEBLOCKSIZE.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error extracting variable block size from '" + vbs + "' line UNKNOWN", ex);
                    }

                    // read the remaining in
                    read = _br.Read(buffer, 0, variablesize);
                    for (int i = 0; i < read; i++)
                    {
                        l += (char)buffer[i];
                    }
                }
            }
            else
            {
                l = _tr.ReadLine();
            }
            string readAhead = string.Empty; // read ahead buffer
            long line = 0; // line number

            // if we have a regex to identify new lines
            if (_newLineRegex != null && _blocksize == 0)
            {
                // if we are not already at end of file
                if (l != null)
                {
                    // read ahead a line
                    readAhead = _tr.ReadLine();

                    // while not at end of line and not a new line match
                    while (readAhead != null && !_newLineRegex.IsMatch(readAhead))
                    {
                        // add it to our line
                        l = l + _newLineSeparator + readAhead;

                        if (!_suppressMaxLine)
                        {
                            if (l.Length > 20000)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Error + "First 1000 characters : '" + l.Substring(0, 1000) + "'" + ANSIHelper.White);
                                throw new ApplicationException("NewLineRegex resulted in line > 20K characters. To enable super long line add <SuppressMaxLine/> to your input config.");
                            }
                        }

                        // read another line ahead
                        readAhead = _tr.ReadLine();

                        // if we are meant to stop after a given number of bytes
                        if (_stopBytes != 0)
                        {
                            // check we are not using STDIN
                            if (SupportsSeek)
                            {
                                Stream sr = null;
                                if (_tr != null)
                                {
                                    sr = ((StreamReader)_tr).BaseStream;
                                }
                                else
                                {
                                    sr = _br.BaseStream;
                                }

                                // if we are too far in
                                if (sr.Position > _stopBytes)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            ReportError("");

            // While we have a line to process
            while (l != null)
            {
                bool failed = false; // set to true if things fail

                if (_endLineRegex == null)
                {
                    // increment our line count
                    line++;

                    // handle
                    failed = DispatchLine(ref line, l);
                }
                else
                {
                    int lastend = -1;
                    string rest = string.Empty;

                    foreach (Match m in _endLineRegex.Matches(l))
                    {
                        // increment our line count
                        line++;

                        string partialline = l.Substring(lastend + 1, m.Index - lastend - 1 + m.Length);
                        lastend = m.Index + m.Length;
                        if (m.Index + m.Length < l.Length)
                        {
                            rest = l.Substring(m.Index + m.Length);
                        }
                        else
                        {
                            rest = string.Empty;
                        }
                        failed |= DispatchLine(ref line, partialline);
                    }

                    if (rest != string.Empty)
                    {
                        // increment our line count
                        line++;
                        failed |= DispatchLine(ref line, rest);
                    }
                }

                // if we failed and we are to stop
                if (failed && _failonreject)
                {
                    // tell the user
                    ReportError(ANSIHelper.Red + "Stopping due to rejected record." + ANSIHelper.White);

                    // tell job we have stopped
                    if (!_suppressStop)
                    {
                        Stop();
                    }

                    // no line
                    l = null;
                }
                else
                {
                    // if we are using a new line regex
                    if (_newLineRegex != null && _blocksize == 0)
                    {
                        // take from the read ahead buffer
                        l = readAhead;

                        // if it was not end of file
                        if (l != null)
                        {
                            // read ahead a line
                            readAhead = _tr.ReadLine();

                            // while this does not match the newline regex
                            while (readAhead != null && !_newLineRegex.IsMatch(readAhead))
                            {
                                // add it to our line
                                l = l + _newLineSeparator + readAhead;

                                if (!_suppressMaxLine)
                                {
                                    if (l.Length > 20000)
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Error + "First 1000 characters : '" + l.Substring(0, 1000) + "'" + ANSIHelper.White);
                                        throw new ApplicationException("NewLineRegex resulted in line > 20K characters. To enable super long line add <SuppressMaxLine/> to your input config.");
                                    }
                                }


                                // read ahead more
                                readAhead = _tr.ReadLine();
                            }
                        }
                    }
                    else
                    {
                        if (_blocksize == 0)
                        {
                            // read the next line
                            l = _tr.ReadLine();
                        }
                        else
                        {
                            l = string.Empty;
                            int read = _br.Read(buffer, 0, _blocksize);
                            if (read == 0)
                            {
                                l = null;
                            }
                            else
                            {
                                for (int i = 0; i < read; i++)
                                {
                                    l += (char)buffer[i];
                                }
                            }

                            if (_variableblocksize != null)
                            {
                                int variablesize = 0;

                                //calculate the variable size
                                ColumnLine cl = new ColumnLine(l);
                                string vbs = "";
                                try
                                {
                                    vbs = _variableblocksize.Process(cl)[0];
                                    variablesize = int.Parse(vbs);
                                    if (variablesize > MAXVARIABLEBLOCKSIZE)
                                    {
                                        throw new ApplicationException("Error - Variable block size " + variablesize.ToString() + " greater than maximum allowed " + MAXVARIABLEBLOCKSIZE.ToString());
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new ApplicationException("Error extracting variable block size from '" + vbs + "' line " + line.ToString(), ex);
                                }

                                // read the remaining in
                                read = _br.Read(buffer, 0, variablesize);
                                for (int i = 0; i < read; i++)
                                {
                                    l += (char)buffer[i];
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Override Functions
        /// <summary>
        /// Process the file
        /// </summary>
        public override void Run()
        {
            try
            {
                string l = string.Empty; // the line
                byte[] buffer = null; // block read buffer

                if (_blocksize > 0)
                {
                    if (_variableblocksize == null)
                    {
                        buffer = new byte[_blocksize];
                    }
                    else
                    {
                        buffer = new byte[MAXVARIABLEBLOCKSIZE];
                    }
                }

                // if we are meant to skip to the middle of the file
                if (_skipBytes != 0)
                {
                    // check we are not using STDIN
                    if (SupportsSeek)
                    {
                        // jump the indicated number of bytes
                        Stream sr = null;
                        if (_tr != null)
                        {
                            sr = ((StreamReader)_tr).BaseStream;
                        }
                        else
                        {
                            sr = _br.BaseStream;
                        }
                        sr.Seek(_skipBytes, SeekOrigin.Begin);

                        if (_blocksize == 0)
                        {
                            // read to the end of whatever line we are in the middle of
                            l = _tr.ReadLine();

                            // keep going until we find a new line
                            if (_newLineRegex != null)
                            {
                                while (!_newLineRegex.IsMatch(l))
                                {
                                    l = _tr.ReadLine();
                                }
                            }
                        }
                    }
                    else
                    {
                        ReportError(ANSIHelper.Red + "Can't skip bytes as input does not support seeking." + ANSIHelper.White);
                    }
                }

                // check we support seek
                if (SupportsSeek && !_monitor)
                {
                    // set the size to be the file length
                    Size = _fi.Size;
                }
                else
                {
                    // if we are meant to stop at a given number of bytes into the file then check we are not using STDIN. This just forces this error early
                    if (_stopBytes != 0)
                    {
                        ReportError(ANSIHelper.Red + "Can't stop after bytes when input does not support seek." + ANSIHelper.White);
                    }
                }

                // read the first line
                if (_blocksize != 0)
                {
                    l = string.Empty;
                    int read = _br.Read(buffer, 0, _blocksize);

                    if (read == 0)
                    {
                        l = null;
                    }
                    else
                    {
                        for (int i = 0; i < read; i++)
                        {
                            l += (char)buffer[i];
                        }
                    }

                    if (_variableblocksize != null)
                    {
                        int variablesize = 0;

                        //calculate the variable size
                        ColumnLine cl = new ColumnLine(l);
                        string vbs = "";
                        try
                        {
                            vbs = _variableblocksize.Process(cl)[0];
                            variablesize = int.Parse(vbs);
                            if (variablesize > MAXVARIABLEBLOCKSIZE)
                            {
                                throw new ApplicationException("Error - Variable block size " + variablesize.ToString() + " greater than maximum allowed " + MAXVARIABLEBLOCKSIZE.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error extracting variable block size from '" + vbs + "' line UNKNOWN", ex);
                        }

                        // read the remaining in
                        read = _br.Read(buffer, 0, variablesize);
                        for (int i = 0; i < read; i++)
                        {
                            l += (char)buffer[i];
                        }
                    }
                }
                else
                {
                    l = _tr.ReadLine();
                }
                string readAhead = string.Empty; // read ahead buffer
                long line = 0; // line number

                // if we have a regex to identify new lines
                if (_newLineRegex != null && _blocksize == 0)
                {
                    // if we are not already at end of file
                    if (l != null)
                    {
                        // read ahead a line
                        readAhead = _tr.ReadLine();

                        // while not at end of line and not a new line match
                        while (readAhead != null && !_newLineRegex.IsMatch(readAhead))
                        {
                            // add it to our line
                            l = l + _newLineSeparator + readAhead;

                            if (!_suppressMaxLine)
                            {
                                if (l.Length > 20000)
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Error + "First 1000 characters : '" + l.Substring(0, 1000) + "'" + ANSIHelper.White);
                                    throw new ApplicationException("NewLineRegex resulted in line > 20K characters. To enable super long line add <SuppressMaxLine/> to your input config.");
                                }
                            }


                            // read another line ahead
                            readAhead = _tr.ReadLine();

                            // if we are meant to stop after a given number of bytes
                            if (_stopBytes != 0)
                            {
                                // check we are not using STDIN
                                if (SupportsSeek && !_monitor)
                                {
                                    Stream sr = null;
                                    if (_tr != null)
                                    {
                                        sr = ((StreamReader)_tr).BaseStream;
                                    }
                                    else
                                    {
                                        sr = _br.BaseStream;
                                    }

                                    // if we are too far in
                                    if (sr.Position > _stopBytes)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                ReportError("");

                // While we have a line to process
                while (l != null)
                {
                    if (_abort) break;

                    bool failed = false; // set to true if things fail

                    // if we skipped in then print out the first record. This is helpful as otherwise the user does not know exactly where they ended up
                    if (line == 0 && _skipBytes != 0 && _blocksize != 0)
                    {
                        if (_displaystartstopline)
                        {
                            ReportError(ANSIHelper.Green + "First record after SkipBytes : " + l + ANSIHelper.White);
                            ReportError("");
                        }
                    }

                    // if we are on a line number that is a mulitple of a <ProgressUpdateInterval> issue a status update to the user
                    if (line % _progressUpdateInterval == 0)
                    {
                        // if the job supports progress
                        if (SupportsProgress)
                        {
                            // progress based on position
                            if (_tr != null)
                            {
                                Progress = ((StreamReader)_tr).BaseStream.Position;
                            }
                            else
                            {
                                Progress = _br.BaseStream.Position;
                            }
                            ReportProgress();
                        }
                        else
                        {
                            // dont need to do this as DispatchLine will do it
                        }
                    }

                    if (_endLineRegex == null)
                    {
                        // increment our line count
                        line++;

                        // handle
                        failed = DispatchLine(ref line, l);
                    }
                    else
                    {
                        int lastend = -1;
                        string rest = string.Empty;

                        foreach (Match m in _endLineRegex.Matches(l))
                        {
                            // increment our line count
                            line++;

                            string partialline = l.Substring(lastend + 1, m.Index - lastend - 1 + m.Length);
                            lastend = m.Index + m.Length;
                            if (m.Index + m.Length < l.Length)
                            {
                                rest = l.Substring(m.Index + m.Length);
                            }
                            else
                            {
                                rest = string.Empty;
                            }
                            failed |= DispatchLine(ref line, partialline);
                        }

                        if (rest != string.Empty)
                        {
                            // increment our line count
                            line++;
                            failed |= DispatchLine(ref line, rest);
                        }
                    }

                    // if we failed and we are to stop
                    if (failed && _failonreject)
                    {
                        // tell the user
                        ReportError(ANSIHelper.Red + "Stopping due to rejected record." + ANSIHelper.White);

                        // tell job we have stopped
                        if (!_suppressStop)
                        {
                            Stop();
                        }

                        // no line
                        l = null;
                    }
                    else
                    {
                        // if we are using a new line regex
                        if (_newLineRegex != null && _blocksize == 0)
                        {
                            // take from the read ahead buffer
                            l = readAhead;

                            // if it was not end of file
                            if (l != null)
                            {
                                // read ahead a line
                                readAhead = _tr.ReadLine();

                                // while this does not match the newline regex
                                while (readAhead != null && !_newLineRegex.IsMatch(readAhead))
                                {
                                    // add it to our line
                                    l = l + _newLineSeparator + readAhead;

                                    if (!_suppressMaxLine)
                                    {
                                        if (l.Length > 20000)
                                        {
                                            CSVProcessor.DisplayError(ANSIHelper.Error + "First 1000 characters : '" + l.Substring(0, 1000) + "'" + ANSIHelper.White);
                                            throw new ApplicationException("NewLineRegex resulted in line > 20K characters. To enable super long line add <suppressMaxLine/> to your config.");
                                        }
                                    }


                                    // read ahead more
                                    readAhead = _tr.ReadLine();

                                    // if we are meant to stop after a given number of bytes
                                    if (_stopBytes != 0)
                                    {
                                        // check we are not using STDIN
                                        if (SupportsSeek && !_monitor)
                                        {
                                            StreamReader sr = (StreamReader)_tr;

                                            // if we are too far in
                                            if (sr.BaseStream.Position > _stopBytes)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            if (_blocksize == 0)
                            {
                                // read the next line
                                l = _tr.ReadLine();
                            }
                            else
                            {
                                l = string.Empty;
                                int read = _br.Read(buffer, 0, _blocksize);
                                if (read == 0)
                                {
                                    l = null;
                                }
                                else
                                {
                                    for (int i = 0; i < read; i++)
                                    {
                                        l += (char)buffer[i];
                                    }
                                }
                                if (_variableblocksize != null)
                                {
                                    int variablesize = 0;

                                    //calculate the variable size
                                    ColumnLine cl = new ColumnLine(l);
                                    string vbs = "";
                                    try
                                    {
                                        vbs = _variableblocksize.Process(cl)[0];
                                        variablesize = int.Parse(vbs);
                                        if (variablesize > MAXVARIABLEBLOCKSIZE)
                                        {
                                            throw new ApplicationException("Error - Variable block size " + variablesize.ToString() + " greater than maximum allowed " + MAXVARIABLEBLOCKSIZE.ToString());
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("Error extracting variable block size from '" + vbs + "' line " + line.ToString(), ex);
                                    }

                                    // read the remaining in
                                    read = _br.Read(buffer, 0, variablesize);
                                    for (int i = 0; i < read; i++)
                                    {
                                        l += (char)buffer[i];
                                    }
                                }
                            }
                        }
                    }

                    // if we are meant to stop after a given number of bytes
                    if (_stopBytes != 0)
                    {
                        // check we are not using STDIN
                        if (SupportsSeek && !_monitor)
                        {
                            Stream sr = null;

                            if (_tr != null)
                            {
                                sr = ((StreamReader)_tr).BaseStream;
                            }
                            else
                            {
                                sr = _br.BaseStream;
                            }

                            // if we are too far in
                            if (sr.Position > _stopBytes)
                            {
                                if (_displaystartstopline && _blocksize == 0)
                                {
                                    // tell the user why and where we are stopping
                                    ReportError(ANSIHelper.Green + "Stopping at byte " + sr.Position.ToString());
                                    ReportError("Next line would have been : " + l + ANSIHelper.White);
                                }
                                break;
                            }
                        }
                    }
                }

                if (_monitor)
                {
                    FileSystemWatcher watcher = new FileSystemWatcher();

                    watcher.Path = Path.GetFullPath(_fi.FullFileName);                                    // set the path
                    watcher.Filter = _fi.FullFileName;                                  // set the file
                    watcher.NotifyFilter = NotifyFilters.Size;                // look for size change
                    watcher.Changed += new FileSystemEventHandler(OnChanged); // Add event handler(s).
                    watcher.EnableRaisingEvents = true;                       // Begin watching. 

                    // is this right? What should this thread do while waiting for events.
                    while (true)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        Thread.Sleep(100);
                        if (_abort)
                        {
                            watcher.EnableRaisingEvents = false;
                            break;
                        }
                    }
                }

                // if the job supports progress
                if (SupportsProgress)
                {
                    if (_tr != null)
                    {
                        // progress based on position
                        Progress = ((StreamReader)_tr).BaseStream.Position;
                    }
                    else
                    {
                        Progress = _br.BaseStream.Position;
                    }
                }
                else
                {
                    // progress based on line
                    Progress = line;
                }
                ReportProgress();
            }
            catch (Exception ex)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + "TextReaderInput::Run problem reading file so aborting : " + ex.Message + ANSIHelper.White);
                Exception e = ex.InnerException;
                while (e != null)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "*** " + e.Message + ANSIHelper.White + "\n");
                    e = e.InnerException;
                }
                CSVProcessor.DisplayError(ANSIHelper.Error + "Stack Trace : " + ex.StackTrace + ANSIHelper.White);
            }

            // close the file
            Close();

            // synchronise
            Synch();

            // send stop message
            if (!_suppressStop)
            {
                Stop();
            }
        }
        public override bool ParserRequired
        {
            get
            {
                return true;
            }
        }

        #endregion

        #region Static Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "newlineregex":
                case "suppressmaxline":
                case "newlineseparator":
                case "preprocessline":
                case "endlineregex":
                case "skipbytes":
                case "blocksize":
                case "variableblocksize":
                case "stopbytes":
                case "supressdisplaystartstopline":
                case "failonreject":
                case "skipuntil":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a text reader input stream
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        public TextReaderInput(CSVJob j, XmlNode n, bool tracexml, string prefix)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "newlineregex")
                    {
                        // regex that matches a new line
                        try
                        {
                            _newLineRegex = new Regex(n1.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating newline regex : " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NewLineRegex>" + _newLineRegex.ToString() + "</NewLineRegex>");
                        }
                    }
                    else if (n1.Name.ToLower() == "suppressmaxline")
                    {
                        _suppressMaxLine = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "   <SuppressMaxLine/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "endlineregex")
                    {
                        // regex that matches a new line
                        try
                        {
                            _endLineRegex = new Regex(n1.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating endline regex : " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EndLineRegex>" + _endLineRegex.ToString() + "</EndLineRegex>");
                        }
                    }
                    else if (n1.Name.ToLower() == "skipuntil")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipUntil>");
                        }
                        try
                        {
                            foreach (XmlNode n2 in n1.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "includeskipuntilrow")
                                    {
                                        _includeskipendrow = true;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <IncludeSkipUntilRow/>");
                                        }
                                    }
                                    else if (n2.Name.ToLower() == "includeskipuntilrowinfirstfileonly")
                                    {
                                        _includeskipendrowinfirstfileonly = true;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <IncludeSkipUntilRowInFirstFileOnly/>");
                                        }
                                    }
                                    else if (n2.Name.ToLower() == "condition")
                                    {
                                        foreach (XmlNode n3 in n2.ChildNodes)
                                        {
                                            if (n3.NodeType != XmlNodeType.Comment)
                                            {
                                                if (_skipuntil == null)
                                                {
                                                    _skipuntil = new AndFilterNode();
                                                }
                                                _skipuntil.Add(new FilterNode(Condition.Create(n3, j, null, tracexml, prefix + "      ")));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating skipuntil", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </SkipUntil>");
                        }
                    }
                    else if (n1.Name.ToLower() == "newlineseparator")
                    {
                        _newLineSeparator = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NewLineSeparator>" + _newLineSeparator + "</NewLineSeparator>");
                        }
                    }
                    else if (n1.Name.ToLower() == "preprocessline")
                    {
                        // a line preprocessor
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PreprocessLine>");
                        }
                        try
                        {
                            foreach (XmlNode n2 in n1.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (_preprocess != null)
                                    {
                                        throw new ApplicationException("Can't have multiple PreprocessLine items.");
                                    }

                                    // create the column processor to use to pre-process lines in the file
                                    _preprocess = Column.Create(n2, j, null, null, tracexml, prefix + "    ");
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating preprocess routine", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </PreprocessLine>");
                        }
                    }
                    else if (n1.Name.ToLower() == "skipbytes")
                    {
                        // skip bytes
                        try
                        {
                            _skipBytes = Convert.ToInt64(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading SkipBytes parameter : " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipBytes>" + _skipBytes.ToString() + "</SkipBytes>");
                        }
                    }
                    else if (n1.Name.ToLower() == "blocksize")
                    {
                        // block size
                        try
                        {
                            _blocksize = Convert.ToInt32(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading BlockSize parameter : " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <BlockSize>" + _blocksize.ToString() + "</BlockSize>");
                        }
                    }
                    else if (n1.Name.ToLower() == "variableblocksize")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <VariableBlockSize>");
                        }
                        try
                        {
                            foreach (XmlNode n2 in n1.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "baseblocksize")
                                    {
                                        try
                                        {
                                            _blocksize = Convert.ToInt32(n2.InnerText);
                                        }
                                        catch (System.Exception ex)
                                        {
                                            throw new ApplicationException("Error reading BaseBlockSize parameter : " + n2.InnerText, ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <BaseBlockSize>" + _blocksize.ToString() + "</BaseBlockSize>");
                                        }
                                    }
                                    else if (n2.Name.ToLower() == "variablesizecolumn")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <VariableSizeColumn>");
                                        }
                                        foreach (XmlNode n3 in n2.ChildNodes)
                                        {
                                            if (n3.NodeType != XmlNodeType.Comment)
                                            {
                                                if (_variableblocksize != null)
                                                {
                                                    throw new ApplicationException("Only one VariableSizeColumn permitted.");
                                                }
                                                _variableblocksize = Column.Create(n3, j, null, null, tracexml, prefix + "       ");
                                            }
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     </VariableSizeColumn>");
                                        }
                                    }
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error creating variable block size", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </SkipUntil>");
                        }
                    }
                    else if (n1.Name.ToLower() == "stopbytes")
                    {
                        // read in the number of bytes to read up to
                        try
                        {
                            _stopBytes = Convert.ToInt64(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading StopBytes parameter : " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StopBytes>" + _stopBytes.ToString() + "</StopBytes>");
                        }
                    }
                    else if (n1.Name.ToLower() == "failonreject")
                    {
                        // fail if we find a rehjected record
                        _failonreject = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnReject/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "supressdisplaystartstopline")
                    {
                        _displaystartstopline = false;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SupressDisplayStartStopLine/>");
                        }
                    }
                }
            }
        }
        #endregion
    }


    /// <summary>
    /// Read from an excel file
    /// </summary>
    class excelinput : Input
    {
        #region Member Variables
        string _filename = string.Empty; // the excel file name
        string _worksheet = string.Empty; // the worksheet to read from
        int _worksheetindex = 0; // index of the worksheet
        int _startrow = 1; // row to start on
        int _endrow = -1; // row to end on
        int _startcol = 1; // column to start at
        int _endcol = -1; // column to end at
        long _line = 1; // input line number
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (_worksheet == string.Empty)
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"ExcelInput::" + _filename.Replace("\\", "\\\\") + "::" + _worksheetindex.ToString() + "\"];");
            }
            else
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"ExcelInput::" + _filename.Replace("\\", "\\\\") + "::" + _worksheet + "\"];");
            }
        }


        #region Static Functions
        /// <summary>
        /// Check if a parameter is understood by this input type
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "filename":
                case "worksheet":
                case "worksheetindex":
                case "startrow":
                case "endrow":
                case "endrowonblank":
                case "startcol":
                case "endcol":
                case "endcolonblank":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public excelinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // only grab the first file
            if (files.Count > 0)
            {
                _filename = files[0];
            }

            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "filename")
                    {
                        _filename = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FileName>" + _filename + "</FileName>");
                        }
                    }
                    else if (n1.Name.ToLower() == "worksheet")
                    {
                        _worksheet = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Worksheet>" + _worksheet + "</Worksheet>");
                        }
                    }
                    else if (n1.Name.ToLower() == "worksheetindex")
                    {
                        try
                        {
                            _worksheetindex = int.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <WorksheetIndex>" + _worksheetindex + "</WorksheetIndex>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error parsing ExcelInput WorksheetIndex", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "startrow")
                    {
                        try
                        {
                            _startrow = int.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <StartRow>" + _startrow + "</StartRow>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error parsing ExcelInput StartRow", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "endrow")
                    {
                        try
                        {
                            _endrow = int.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <EndRow>" + _endrow + "</EndRow>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error parsing ExcelInput EndRow", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "endrowonblank")
                    {
                        _endrow = -1;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EndRowOnBlank/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "endcolonblank")
                    {
                        _endcol = -1;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EndColOnBlank/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "startcol")
                    {
                        try
                        {
                            _startcol = int.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <StartCol>" + _startcol + "</StartCol>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error parsing ExcelInput StartCol", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "endcol")
                    {
                        try
                        {
                            _endcol = int.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <EndCol>" + _endcol + "</EndCol>");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error parsing ExcelInput EndCol", ex);
                        }
                    }
                }
            }

            if (!File.Exists(_filename))
            {
                throw new ApplicationException("Error opening excel file " + _filename);
            }

            _fi = new CSVFileInfo(Name);
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                if (_worksheet == string.Empty)
                {
                    return _filename + "[" + _worksheetindex.ToString() + "]";
                }
                else
                {
                    return _filename + "[" + _worksheet + "]";
                }
            }
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            string connection;
            if (Path.GetExtension(_filename).ToLower() == ".xls")
            {
                connection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + _filename + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;MAXSCANROWS=15;READONLY=TRUE\"";
            }
            else
            {
                connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + _filename + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1;MAXSCANROWS=15;READONLY=TRUE\"";
            }

            OleDbConnection conObj = new OleDbConnection(connection);
            conObj.Open();

            // get the worksheet names
            DataTable dtSchema = null;
            dtSchema = conObj.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            if (_worksheet != null)
            {
                DataRow[] dr = dtSchema.Select("TABLE = '" + _worksheet + "'");
                if (dr.GetLength(0) == 0)
                {
                    throw new ApplicationException("Excel file " + _filename + " does not contain worksheet " + _worksheet);
                }
            }
            else
            {
                if (dtSchema.Rows.Count < _worksheetindex)
                {
                    throw new ApplicationException("Excel file " + _filename + " does not contain " + _worksheetindex.ToString() + " worksheets.");
                }
                _worksheet = (string)dtSchema.Rows[_worksheetindex - 1][0];
                if (_worksheet.EndsWith("$"))
                {
                    _worksheet = _worksheet.Substring(0, _worksheet.Length - 1);
                }
            }

            string cmds = "select * from '" + _worksheet + "'";
            OleDbCommand cmd = new OleDbCommand(cmds);
            cmd.Connection = conObj;
            OleDbDataAdapter adpt = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpt.Fill(ds, _worksheet);

            DataTable ws = ds.Tables[0];

            if (_endcol < 0)
            {
                int r = _startrow;
                while (((string)ws.Rows[r][0]) != string.Empty)
                {
                    if (_abort) break;

                    ColumnLine cl = new ColumnLine(_line, _fi);

                    if (_endcol < 0)
                    {
                        int c = _startcol;

                        while (c <= _endcol)
                        {
                            cl.AddEnd((string)ws.Rows[r][c]);
                            c++;
                        }
                    }
                    else
                    {
                        for (int c = _startcol; c <= _endcol; c++)
                        {
                            cl.AddEnd((string)ws.Rows[r][c]);
                        }
                    }

                    if (_fd.In(cl) && _ppfd.In(cl))
                    {
                        //cl.Parse();
                        _job.Send(cl);
                        _line++;
                    }

                    r++;
                }
            }
            else
            {
                for (int r = _startrow; r <= _endrow; r++)
                {
                    if (_abort) break;

                    ColumnLine cl = new ColumnLine("", _line, _fi);

                    if (_endcol < 0)
                    {
                        int c = _startcol;

                        while (c <= _endcol)
                        {
                            cl.AddEnd((string)ws.Rows[r][c]);
                            c++;
                        }
                    }
                    else
                    {
                        for (int c = _startcol; c <= _endcol; c++)
                        {
                            cl.AddEnd((string)ws.Rows[r][c]);
                        }
                    }

                    if (_fd.In(cl) && _ppfd.In(cl))
                    {
                        //cl.Parse();
                        _job.Send(cl);
                        _line++;
                    }
                }
            }

            conObj.Close();

            // tell the job we are done
            _job.Stop();
        }
        #endregion
    }

    //class outlookinput : Input
    //{
    //    #region Member Variables
    //    string _foldername = string.Empty;
    //    bool _processSubFolders = false;
    //    Column _preprocess = null; // preprocess message body
    //    long _line = 1;
    //    string _newLineSeparator = string.Empty;
    //    #endregion

    //    public override void WriteDot(StreamWriter dotWriter, string prefix)
    //    {
    //        dotWriter.WriteLine(prefix + UniqueId + " [label=\"OutlookInput::" + _foldername.Replace("\\", "\\\\") + "\"];");
    //    }

    //    #region Static Functions
    //    /// <summary>
    //    /// Check if a parameter is understood by this input type
    //    /// </summary>
    //    /// <param name="s"></param>
    //    /// <returns></returns>
    //    public new static bool Supports(string s)
    //    {
    //        switch (s)
    //        {
    //            case "folder":
    //            case "processsubfolders":
    //            case "preprocessemail":
    //            case "newlineseparator":
    //                return true;
    //            default:
    //                return Input.Supports(s);
    //        }
    //    }
    //    #endregion

    //    #region Constructor
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="j"></param>
    //    /// <param name="n"></param>
    //    /// <param name="tracexml"></param>
    //    /// <param name="prefix"></param>
    //    /// <param name="files"></param>
    //    public outlookinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
    //        : base(j, n, tracexml, prefix)
    //    {
    //        // process the parameters
    //        foreach (XmlNode n1 in n.ChildNodes)
    //        {
    //            if (n1.NodeType != XmlNodeType.Comment)
    //            {
    //                if (n1.Name.ToLower() == "folder")
    //                {
    //                    _foldername = n1.InnerText;
    //                    if (tracexml)
    //                    {
    //                        CSVProcessor.DisplayError(prefix + "  <Folder>" + _foldername + "</Folder>");
    //                    }
    //                }
    //                else if (n1.Name.ToLower() == "processsubfolders")
    //                {
    //                    _processSubFolders = true;
    //                    if (tracexml)
    //                    {
    //                        CSVProcessor.DisplayError(prefix + "  <ProcessSubFolders/>");
    //                    }
    //                }
    //                else if (n1.Name.ToLower() == "newlineseparator")
    //                {
    //                    _newLineSeparator = n1.InnerText;
    //                    if (tracexml)
    //                    {
    //                        CSVProcessor.DisplayError(prefix + "  <NewLineSeparator>" + _newLineSeparator + "</NewLineSeparator>");
    //                    }
    //                }
    //                else if (n1.Name.ToLower() == "preprocessemail")
    //                {
    //                    // a line preprocessor
    //                    if (tracexml)
    //                    {
    //                        CSVProcessor.DisplayError(prefix + "  <PreprocessEMail>");
    //                    }
    //                    try
    //                    {
    //                        foreach (XmlNode n2 in n1.ChildNodes)
    //                        {
    //                            if (n2.NodeType != XmlNodeType.Comment)
    //                            {
    //                                if (_preprocess != null)
    //                                {
    //                                    throw new ApplicationException("Can't have multiple PreprocessEMail items.");
    //                                }

    //                                // create the column processor to use to pre-process lines in the file
    //                                _preprocess = Column.Create(n2, null, null, tracexml, prefix + "    ");
    //                            }
    //                        }
    //                    }
    //                    catch (System.Exception ex)
    //                    {
    //                        throw new ApplicationException("Error creating preprocess routine", ex);
    //                    }
    //                    if (tracexml)
    //                    {
    //                        CSVProcessor.DisplayError(prefix + "  </PreprocessEMail>");
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    #endregion

    //    #region Overrides
    //    /// <summary>
    //    /// Get the input name
    //    /// </summary>
    //    public override string Name
    //    {
    //        get
    //        {
    //            return _foldername;
    //        }
    //    }

    //    void ProcessFolder(Microsoft.Office.Interop.Outlook.MAPIFolder folder, string parent)
    //    {
    //        // process each mail item
    //        foreach (object o in folder.Items)
    //        {
    //            if (_abort) break;
    //            CSVProcessor.DisplayError(ANSIHelper.Trace + "post item type name " + o.GetType().Name + ANSIHelper.Normal);
    //            if (o.GetType() == typeof(Microsoft.Office.Interop.Outlook.MailItem))
    //            {
    //                MailItem mi = (MailItem)o;
    //                ColumnLine cl = new ColumnLine("", _line, parent);
    //                try
    //                {
    //                    cl.AddEnd(mi.SenderName);
    //                    if (mi.To == null)
    //                    {
    //                        cl.AddEnd(string.Empty);
    //                    }
    //                    else
    //                    {
    //                        cl.AddEnd(mi.To);
    //                    }
    //                    cl.AddEnd(mi.SentOn.ToString("yyyy/MM/dd HH:mm:ss"));
    //                    if (mi.Subject == null)
    //                    {
    //                        cl.AddEnd(string.Empty);
    //                    }
    //                    else
    //                    {
    //                        cl.AddEnd(mi.Subject);
    //                    }

    //                    // string out new line characters
    //                    Regex regex = new Regex("(\r\n|\r|\n)", RegexOptions.Multiline | RegexOptions.Compiled);
    //                    cl.AddEnd(regex.Replace(mi.Body, _newLineSeparator));

    //                    if (_preprocess != null)
    //                    {
    //                        cl[4] = _preprocess.Process(cl)[0];
    //                    }
    //                }
    //                catch (System.Exception ex)
    //                {
    //                    throw new ApplicationException("Error accessing MailItem attributes for message " + _line.ToString() + " in folder " + parent + ".", ex);
    //                }

    //                if (_fd.In(cl) && _ppfd.In(cl))
    //                {
    //                    //cl.Parse();
    //                    _job.Send(cl);
    //                    _line++;
    //                }
    //            }
    //        }

    //        if (_processSubFolders)
    //        {
    //            // now process any subdirectories
    //            foreach (Microsoft.Office.Interop.Outlook.MAPIFolder f in folder.Folders)
    //            {
    //                ProcessFolder(f, parent + "\\" + f.Name);
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Process the input
    //    /// </summary>
    //    public override void Run()
    //    {
    //        Microsoft.Office.Interop.Outlook.Application app = null;
    //        Microsoft.Office.Interop.Outlook._NameSpace ns = null;

    //        try
    //        {
    //            app = new Microsoft.Office.Interop.Outlook.Application();
    //            ns = app.GetNamespace("MAPI");
    //            ns.Logon(null, null, false, false);
    //        }
    //        catch (System.Exception ex)
    //        {
    //            throw new ApplicationException("Error connecting to Outlook.", ex);
    //        }

    //        // parse out the folder name
    //        char[] separators = { '\\', '/' };
    //        string[] folderbits = _foldername.Split(separators);

    //        Microsoft.Office.Interop.Outlook.MAPIFolder folder = null;

    //        if (folderbits[0].ToLower() == "inbox")
    //        {
    //            // if it is inbox open that
    //            folder = ns.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);
    //        }
    //        else
    //        {
    //            // else open the PST
    //            foreach (Microsoft.Office.Interop.Outlook.Store s in ns.Stores)
    //            {
    //                if (s.FilePath != null && s.FilePath != string.Empty)
    //                {
    //                    if (s.DisplayName.ToLower() == folderbits[0].ToLower() || Path.GetFileName(s.FilePath).ToLower() == folderbits[0].ToLower())
    //                    {
    //                        folder = s.GetRootFolder();
    //                    }
    //                }
    //            }

    //            if (folder == null)
    //            {
    //                throw new ApplicationException("Root folder not found " + folderbits[0]);
    //            }
    //        }

    //        if (folder == null)
    //        {
    //            throw new ApplicationException("Unknown PST file: " + folderbits[0]);
    //        }

    //        // now navigate to the right folder
    //        for (int i = 1; i < folderbits.Length; i++)
    //        {
    //            foreach (Microsoft.Office.Interop.Outlook.Folder f in folder.Folders)
    //            {
    //                CSVProcessor.DisplayError(ANSIHelper.Trace + "Found folder.Name: " + f.Name + " looking for: " + folderbits[i] + ANSIHelper.Normal);
    //                if (f.Name.ToLower() == folderbits[i].ToLower())
    //                {
    //                    folder = f;
    //                    break;
    //                }

    //                throw new ApplicationException("Folder not found " + folderbits[i]);
    //            }
    //        }

    //        // Send a header line
    //        ColumnLine cl = new ColumnLine("", _line, _foldername);
    //        cl.AddEnd("From");
    //        cl.AddEnd("To");
    //        cl.AddEnd("Timestamp");
    //        cl.AddEnd("Subject");
    //        cl.AddEnd("Message");
    //        cl.Data = false;

    //        if (_fd.In(cl) && _ppfd.In(cl))
    //        {
    //            //cl.Parse();
    //            _job.Send(cl);
    //            _line++;
    //        }

    //        // now process each item in that folder
    //        // this is done in a function so we can easily recurse
    //        ProcessFolder(folder, _foldername);

    //        // tell the job we are done
    //        _job.Stop();
    //    }
    //    #endregion
    //}

    class wmiinput : Input
    {
        #region Member Variables
        string _query = string.Empty;
        long _rerunsecs = -1; // run once only
        long _line = 1;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"WMIInput::" + _query + "\"];");
        }

        [DllImport("user32.dll")]
        public static extern ushort GetKeyState(short nVirtKey);

        public const ushort keyDownBit = 0x80;

        public static bool IsKeyPressed(short key)
        {
            return ((GetKeyState((short)key) & keyDownBit) == keyDownBit);
        }

        #region Static Functions
        /// <summary>
        /// Check if a parameter is understood by this input type
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "query":
                case "rerunsecs":
                    return true;
                default:
                    return Input.Supports(s);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="files"></param>
        public wmiinput(CSVJob j, XmlNode n, bool tracexml, string prefix, List<string> files)
            : base(j, n, tracexml, prefix)
        {
            // process the parameters
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "query")
                    {
                        _query = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Query>" + _query + "</Query>");
                        }
                    }
                    else if (n1.Name.ToLower() == "rerunsecs")
                    {
                        try
                        {
                            _rerunsecs = long.Parse(n1.InnerText);
                        }
                        catch (System.Exception ex)
                        {
                            throw new ApplicationException("Error reading RerunSecs option on WMI input. " + n1.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RerunSecs>" + _rerunsecs.ToString() + "</RerunSecs>");
                        }
                    }
                }
            }

            if (_rerunsecs >= 0)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "WMI Query will be rerun every " + _rerunsecs.ToString() + " seconds." + ANSIHelper.Normal);
                CSVProcessor.DisplayError(ANSIHelper.Warning + "This process will continue to run until killed by pressing and holding CTRL-Q." + ANSIHelper.Normal);
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Get the input name
        /// </summary>
        public override string Name
        {
            get
            {
                return "WMI:" + _query;
            }
        }

        /// <summary>
        /// Process the input
        /// </summary>
        public override void Run()
        {
            bool first = true;

            do
            {
                if (_abort) break;

                ManagementObjectSearcher searcher = null;

                try
                {
                    searcher = new ManagementObjectSearcher(_query);
                }
                catch (System.Exception ex)
                {
                    throw new ApplicationException("Error running WMI query " + _query, ex);
                }

                string timestamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

                foreach (ManagementObject o in searcher.Get())
                {
                    if (_abort) break;

                    if (o != null)
                    {
                        if (first)
                        {
                            ColumnLine clh = new ColumnLine("", _line, _query);
                            clh.Data = false;
                            clh.AddEnd("Timestamp");
                            foreach (PropertyData pd in o.Properties)
                            {
                                clh.AddEnd(pd.Name);
                            }

                            if (_fd.In(clh) && _ppfd.In(clh))
                            {
                                //clh.Parse();
                                _job.Send(clh);
                                _line++;
                            }
                            first = false;
                        }

                        ColumnLine cl = new ColumnLine("", _line, _query);
                        cl.AddEnd(timestamp);

                        foreach (PropertyData pd in o.Properties)
                        {
                            if (pd.Value == null)
                            {
                                cl.AddEnd("NULL");
                            }
                            else
                            {
                                cl.AddEnd(pd.Value.ToString());
                            }
                        }

                        if (_fd.In(cl) && _ppfd.In(cl))
                        {
                            //cl.Parse();
                            _job.Send(cl);
                            _line++;
                        }
                    }
                }

                // free searcher up before we sleep
                searcher = null;

                // sleep if we need to
                if (_rerunsecs > 0)
                {
                    for (long l = 0; l < _rerunsecs; l++)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            Thread.Sleep(100);

                            if (IsKeyPressed(0x51/*Q*/) && (IsKeyPressed(0xA2 /*LCTRL*/) || IsKeyPressed(0xA3 /*RCTRL*/)))
                            {
                                _rerunsecs = -1;
                                break;
                            }
                        }
                    }
                }
                // run again if we need to
            } while (_rerunsecs >= 0);

            // tell the job we are done
            _job.Stop();
        }
        #endregion
    }

    #region Unimplemented Inputs
    //	class SQLInput : Input
    //	{
    //		SqlConnection _conn = new SqlConnection();
    //		SqlCommand _cmd = null;
    //		SqlDataReader _reader = null;
    //
    //    public override void WriteDot(StreamWriter dotWriter, string prefix)
    //    {
    //        dotWriter.WriteLine(prefix + UniqueId + " [label=\"SQLInput::" + _conn + "::" + _cmd + "\"];");
    //    }
    //		public SQLInput(XmlNode n, string connectionstring, string SQL) : base(n)
    //		{
    //			if (_p.SQLFile)
    //			{
    //				Timer FileTimer = new Timer();
    //				FileTimer.Reset();
    //
    //				// Let the user know what is happening
    //				DisplayError(ANSIHelper.Yellow + "SQL : " + _p.SQL + ANSIHelper.White);
    //
    //				// Process the file
    //				try
    //				{
    //					SQLReader sr = new SQLReader(_p.ConnectionString, _p.SQL);
    //
    //					rc = Process(sr, "sql", false);
    //				}
    //				catch(Exception ex)
    //				{
    //					DisplayError(ANSIHelper.Red + "Error processing sql : " + _p.SQL + " : " + ex.Message + ANSIHelper.White);
    //				}
    //
    //				// display total file time
    //				DisplayError(ANSIHelper.Blue + "SQL processed in " + FormatTimeSpan(FileTimer.Duration) + ANSIHelper.White);
    //			}
    //
    //
    //			_conn.ConnectionString = connectionstring;
    //			CSVProcessor.DisplayError(ANSIHelper.Yellow + "Connecting to database.\n" + ANSIHelper.White);
    //			_conn.Open();
    //			_cmd = _conn.CreateCommand();
    //			CSVProcessor.DisplayError(ANSIHelper.Yellow + "Connected.\n" + ANSIHelper.White);
    //			_cmd.CommandText = SQL;
    //			_reader = _cmd.ExecuteReader();
    //		}
    //
    //		public string ReadLine()
    //		{
    //			_reader.Read();
    //
    //			string res = string.Empty;
    //
    //			object[] cols = {};
    //			_reader.GetValues(cols);
    //
    //			foreach(object o in cols)
    //			{
    //				if (res != string.Empty)
    //				{
    //					res = res + ",";
    //				}
    //				
    //				res = res + o.ToString();
    //			}
    //
    //			return res;
    //		}
    //
    //		public void Close()
    //		{
    //			_reader.Close();
    //			_cmd.Cancel();
    //			_conn.Close();
    //		}
    //	}
    //					else if (n3.Name.ToLower() == "sql")
    //					{
    //						if (tracexml)
    //						{
    //							CSVProcessor.DisplayError(prefix + "  <Sql>");
    //						}
    //
    //						_sqlfile = true;
    //
    //						// add files to list
    //						foreach (XmlNode n4 in n3.ChildNodes)
    //						{
    //							// If this is a text node then they have just given us a file name
    //							if (n4.NodeType != XmlNodeType.Comment)
    //							{
    //								// add a file or a directory
    //								if (n4.Name.ToLower() == "connectionstring")
    //								{
    //									// Example
    //									// Server=192.168.6.4;Database=VM;User ID=sa;Password=;Trusted_Connection=False;
    //											if (tracexml)
    //											{
    //												CSVProcessor.DisplayError(prefix + "    <ConnectionString>");
    //											}
    //                               StringOrColumn sc = new StringOrColumn(n4, null, null, tracexml, prefix + "   ");
    //                               _connectionstring = sc.Value();
    //											if (tracexml)
    //											{
    //												CSVProcessor.DisplayError(prefix + "    </ConnectionString>");
    //											}
    //								}
    //									// sql string
    //								else if (n4.Name.ToLower() == "sql")
    //								{
    //									_sql = n4.InnerText;
    //									if (tracexml)
    //									{
    //										CSVProcessor.DisplayError(prefix + "    <Sql>" + _sql + "</Sql>");
    //									}
    //								}
    //							}
    //						}
    //						if (tracexml)
    //						{
    //							CSVProcessor.DisplayError(prefix + "  </Sql>");
    //						}
    //
    //					}
    //					else if (n3.Name.ToLower() == "defaultftpuser")
    //					{
    //						FTPFile.SetDefaultUser(n3.InnerText);
    //						if (tracexml)
    //						{
    //							CSVProcessor.DisplayError(prefix + "  <DefaultFtpUser>" + n3.InnerText + "</DefaultFtpUser>");
    //						}
    //					}
    //					else if (n3.Name.ToLower() == "defaultftppassword")
    //					{
    //						FTPFile.SetDefaultPassword(n3.InnerText);
    //						if (tracexml)
    //						{
    //							CSVProcessor.DisplayError(prefix + "  <DefaultFtpPassword>" + n3.InnerText + "</DefaultFtpPassword>");
    //						}
    //					}
    #endregion
}
