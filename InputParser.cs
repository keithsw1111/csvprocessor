﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace CSVProcessor
{
    #region InputParser Base Class
    /// <summary>
    /// The base class for Input Parsers
    /// </summary>
    public abstract class InputParser
    {
        #region Member Variables
        //protected string _filename = string.Empty; // The name of the file being parsed
        protected CSVFileInfo _fi = null;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="filename"></param>
        /// <param name="supported"></param>
        public InputParser(CSVJob j, XmlNode n, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi, string[] supported)
        {
            // save the filename
            //_filename = filename;
            _fi = fi;

            // Process the configuration
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    // check if the node belongs
                    bool insupported = false;
                    foreach (string s in supported)
                    {
                        if (s == n3.Name.ToLower())
                        {
                            insupported = true;
                            break;
                        }
                    }

                    // If it does not belong warn the user
                    if (!insupported)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "InputParser::Constructor Ignoring xml node : " + n3.Name + " when creating " + this.GetType().Name + "\n" + ANSIHelper.White);
                    }
                }
            }
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Parse the line into columns
        /// </summary>
        /// <param name="line"></param>
        /// <param name="inputline"></param>
        /// <returns></returns>
        public abstract ColumnLine Parse(string line, long inputline);
        #endregion

        #region Static Methods
        /// <summary>
        /// Create the default parser
        /// </summary>
        /// <param name="j"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static InputParser CreateDefault(CSVJob j, CSVFileInfo fi)
        {
            // Mock up the XML
            XmlDocument doc = new XmlDocument();
            XmlNode n = doc.CreateNode(XmlNodeType.Element, "delimitedparser", "");

            // create the input parser
            InputParser ip = InputParser.Create(j, n, false, "", fi);

            // warn the user
            CSVProcessor.DisplayError(ANSIHelper.Warning + "Using default InputParser : " + ip.GetType().Name + "\n" + ANSIHelper.White);

            return ip;
        }

        /// <summary>
        /// Create the named InputParser
        /// </summary>
        /// <param name="j"></param>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static InputParser Create(CSVJob j, XmlNode n, bool tracexml, string prefix, CSVFileInfo fi)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                if (t == null)
                {
                    throw new ApplicationException("InputParser::Create - " + n.Name + " is not a valid inputparser type.");
                }

                // now construct the object passing in our xml node
                InputParser ip = (InputParser)Activator.CreateInstance(t, new object[] { j, n, tracexml, prefix, fi });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return ip;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("InputParser::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// Parser which reads each line as a single column
    /// </summary>
    class nodelimiterparser : InputParser
    {
        #region Constructors
        public nodelimiterparser(CSVJob j, XmlNode n, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, n, tracexml, prefix, /*filename*/fi, new string[] { })
        {
        }
        #endregion

        #region Overriden Methods
        /// <summary>
        /// Parse the input line
        /// </summary>
        /// <param name="line"></param>
        /// <param name="inputline"></param>
        /// <returns></returns>
        public override ColumnLine Parse(string line, long inputline)
        {
            //Create the line with the whole line in a single column
            ColumnLine cl = new ColumnLine(line, inputline, /*_filename*/_fi);
            cl.AddEnd(line);

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Use regular expressions to break a line into columns
    /// </summary>
    class regexparser : InputParser
    {
        #region Child Classes
        /// <summary>
        /// Represent a regular expression parsed column
        /// </summary>
        class RegexCol
        {
            #region Member Variables
            Regex _regex = null; // the regular expression
            string _name = string.Empty; // the group to extract from the regular expression
            bool _ignore; // flag if to ignore the column when building the line to process
            #endregion

            #region Accessors
            /// <summary>
            /// Should I ignore the column
            /// </summary>
            public bool Ignore
            {
                get
                {
                    return _ignore;
                }
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Create a regular expression column
            /// </summary>
            /// <param name="regex"></param>
            /// <param name="name"></param>
            /// <param name="ignore"></param>
            public RegexCol(string regex, string name, bool ignore)
            {
                try
                {
                    _regex = new Regex(regex, RegexOptions.Compiled & RegexOptions.Singleline);
                }
                catch (Exception e)
                {
                    throw new ApplicationException("RegexCol error parsing regex : " + regex, e);
                }
                _name = name;
                _ignore = ignore;
            }
            #endregion

            #region Public Methods
            /// <summary>
            /// Apply the regex column to extract the value from a line
            /// </summary>
            /// <param name="line"></param>
            /// <returns></returns>
            public string ExtractCol(string line)
            {
                return _regex.Match(line).Groups[_name].Value;
            }
            #endregion
        }
        #endregion

        #region Member Variables
        List<RegexCol> _regexes = new List<RegexCol>(); // list of columns
        bool _incremental = false; // if true apply the regular expressions removing the characters from the left
        #endregion

        #region Constructors
        public regexparser(CSVJob j, XmlNode n, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, n, tracexml, prefix, /*filename*/fi, new string[] { "column", "incremental" })
        {
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "column")
                    {
                        string regex = string.Empty;
                        string name = string.Empty;
                        bool ignore = false;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }

                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "regex")
                                {
                                    regex = n4.InnerText;

                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Regex>" + regex + "</Regex>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "name")
                                {
                                    name = n4.InnerText.Trim();
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Name>" + name + "</Name>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "ignore")
                                {
                                    ignore = true;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Ignore/>");
                                    }
                                }
                                else
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "RegexParser::Constructor Ignoring xml node : " + n4.Name + "\n" + ANSIHelper.White);
                                }
                            }
                        }

                        if (regex == string.Empty)
                        {
                            throw new ApplicationException("RegexParser::Constructor column cant have a blank regex.");
                        }

                        if (name == string.Empty)
                        {
                            throw new ApplicationException("RegexParser::Constructor column cant have a blank name.");
                        }

                        // add the column to the list
                        _regexes.Add(new RegexCol(regex, name, ignore));

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n3.Name.ToLower() == "incremental")
                    {
                        _incremental = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Incremental/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Overriden Methods
        /// <summary>
        /// Parse an input line
        /// </summary>
        /// <param name="line"></param>
        /// <param name="inputline"></param>
        /// <returns></returns>
        public override ColumnLine Parse(string line, long inputline)
        {
            // copy the line in case i am doing incremental processing as I will need to alter it
            string l = line;

            // create the output line
            ColumnLine cl = new ColumnLine(line, inputline, /*_filename*/_fi);

            // precess each column
            foreach (RegexCol rc in _regexes)
            {
                // get the column value
                string c = rc.ExtractCol(l);

                // if we are not to ignore it then add it to the output
                if (!rc.Ignore)
                {
                    cl.AddEnd(c);
                }

                // if we are doing incremental processing then remove the chars from the start of the string
                if (_incremental)
                {
                    l = l.Substring(c.Length);
                }
            }

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Fixed column parser
    /// </summary>
    class fixedparser : InputParser
    {
        #region Child Classes
        /// <summary>
        /// This class is used by FixedColLine to represent the rules to extract a column from the file
        /// </summary>
        public class ColumnParsingRule
        {
            #region Member Variables
            int _start = 0; // start character
            int _length = -1; // length of the column
            string _name = string.Empty; // name of the column
            bool _ebcdic = false; // true if we are to do EBCDIC -> ASCII character mapping
            List<char> _stopchars = new List<char>(); // chars which will stop the column short
            int _comp = -1; // -1 if no processing required else the type of COBOL comp unpacking to do
            int _wholedigits = -1; // for comp unpacking digits before the decimal point
            int _fractiondigits = -1; // for comp unpacking digits after the decimal point
            bool _signed = false; // for comp unpacking indicator of whether there is a sign
            string _format = string.Empty; // format to use to display COMP numbers
            #endregion

            #region Static Variables
            // table for converting EBCDIC to ASCII
            static int[] __EtoA = new int[] {
                   0x00,0x01,0x02,0x03,0x9C,0x09,0x86,0x7F,0x97,0x8D,0x8E,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x9D,0x85,0x08,0x87,0x18,0x19,0x92,0x8F,0x1C,0x1D,0x1E,0x1F,
                   0x80,0x81,0x82,0x83,0x84,0x0A,0x17,0x1B,0x88,0x89,0x8A,0x8B,0x8C,0x05,0x06,0x07,0x90,0x91,0x16,0x93,0x94,0x95,0x96,0x04,0x98,0x99,0x9A,0x9B,0x14,0x15,0x9E,0x1A,
                   0x20,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xD5,0x2E,0x3C,0x28,0x2B,0x7C,0x26,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0x21,0x24,0x2A,0x29,0x3B,0x5E,
                   0x2D,0x2F,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xE5,0x2C,0x25,0x5F,0x3E,0x3F,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0x60,0x3A,0x23,0x40,0x27,0x3D,0x22,
                   0xC3,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,
                   0xD1,0x7E,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0xD2,0xD3,0xD4,0x5B,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0x5D,0xE6,0xE7,
                   0x7B,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0x7D,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,
                   0x5C,0x9F,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF};
            static char[] __Hex = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            static Regex __Repeat = new Regex("\\((?'val'[0-9]*?)\\)", RegexOptions.Singleline); // regex to match (x) where x is 0-9
            #endregion

            #region Public Static Functions
            /// <summary>
            /// Turn a string into a hex string
            /// </summary>
            /// <param name="cv"></param>
            /// <param name="break1"></param>
            /// <param name="break4"></param>
            /// <param name="break16"></param>
            /// <returns></returns>
            public static string HexString(string cv, string break1, string break4, string break16)
            {
                string s = string.Empty; // our output string
                int i = 0; // keep track of how far through the string we are so we know when to insert breaks

                // look at each character
                foreach (char c in cv)
                {
                    // add the break every 16 characters
                    if (i % 16 == 0 && i != 0)
                    {
                        s = s + break16;
                    }

                    // add the break every 4 characters
                    if (i % 4 == 0 & i != 0)
                    {
                        s = s + break4;
                    }

                    // add the every character break
                    s = s + break1;

                    // convert the character to hex
                    s = s + ColumnParsingRule.Hex(((int)c & 0xF0) >> 4) + ColumnParsingRule.Hex((int)c & 0x0F);

                    // next character
                    i++;
                }

                // return the resulting hex string
                return s;
            }

            /// <summary>
            /// returns count-1 repetion of s
            /// </summary>
            /// <param name="count"></param>
            /// <param name="s"></param>
            /// <returns></returns>
            public static string Nines(string count, string s)
            {

                // convert count to integer
                int c = Convert.ToInt32(count) - 1;

                // now call the alternate nines function
                return Nines(c, s);
            }

            /// <summary>
            /// returns count-1 repition of s
            /// </summary>
            /// <param name="c"></param>
            /// <param name="s"></param>
            /// <returns></returns>
            public static string Nines(int c, string s)
            {
                string n = ""; // our return string

                // add the required number of s
                for (int i = 0; i < c; i++)
                {
                    n = n + s;
                }

                // return the resulting string
                return n;
            }

            /// <summary>
            /// convert an EBCDIC string to an ASCII string
            /// </summary>
            /// <param name="s"></param>
            /// <returns></returns>
            public static string EBCDICtoASCII(string s)
            {
                string r = string.Empty; // our result string

                // look at each character
                foreach (char c in s.ToCharArray())
                {
                    // convert it ot ASCII and add it to our result
                    r = r + Convert.ToChar(__EtoA[Convert.ToByte(c)]);
                }

                // return the result
                return r;
            }

            /// <summary>
            /// Parse a COBOL COMP format
            /// </summary>
            /// <param name="s"></param>
            /// <param name="wholedigits"></param>
            /// <param name="fractiondigits"></param>
            /// <param name="signed"></param>
            public static void ParseComp(string s, out int wholedigits, out int fractiondigits, out bool signed)
            {
                signed = false; // assume signed is false

                // remove all whitespace            	
                string work = s.Replace(" ", "").ToUpper();
                work = work.Replace("\t", "");

                // if they included PIC remove it
                if (work.StartsWith("PIC"))
                {
                    work = work.Substring(4);
                }

                // expand any repeat counts in brackets
                foreach (Match m in __Repeat.Matches(work))
                {
                    work = work.Replace(m.Value, ColumnParsingRule.Nines(m.Groups["val"].Value, "9"));
                }

                // check for a sign
                if (work.StartsWith("S"))
                {
                    signed = true;
                    work = work.Substring(1);
                }

                // now work out the number of digits before and after decimal point
                wholedigits = 0;
                fractiondigits = 0;
                bool dec = false;
                foreach (char c in work)
                {
                    if (c == '9')
                    {
                        if (dec)
                        {
                            fractiondigits++;
                        }
                        else
                        {
                            wholedigits++;
                        }
                    }
                    else if (c == 'V')
                    {
                        dec = true;
                    }
                    else
                    {
                        throw new ApplicationException("ParseComp problem parsing PIC clause " + s);
                    }
                }
            }

            /// <summary>
            /// get the hex char for a given number 0-15
            /// </summary>
            /// <param name="i"></param>
            /// <returns></returns>
            public static char Hex(int i)
            {
                // i must be 0-15
                Debug.Assert(i >= 0 && i <= 15);

                // force i to be 0-15 and get the Hex Char
                return ColumnParsingRule.__Hex[i & 0xF];
            }

            /// <summary>
            /// Generate a format string given number of whole and fraction digits
            /// </summary>
            /// <param name="wholedigits"></param>
            /// <param name="fractiondigits"></param>
            /// <returns></returns>
            public static string GenerateCompFormat(int wholedigits, int fractiondigits)
            {
                string format = string.Empty; // our format result

                // if we have no whole digits then we start with a blank if not zero
                if (wholedigits == 0)
                {
                    format = "#";
                }
                else
                {
                    // add required number of # followed by a single 0
                    for (int i = 0; i < wholedigits - 1; i++)
                    {
                        format = format + "#";
                    }
                    format = format + "0";
                }

                // if we have fraction digits
                if (fractiondigits > 0)
                {
                    // add a decimal point
                    format = format + ".";

                    // now add the fraction digits as 0
                    for (int i = 0; i < fractiondigits; i++)
                    {
                        format = format + "0";
                    }
                }

                return format;
            }

            /// <summary>
            /// convert a COMP field into the number
            /// </summary>
            /// <param name="comptype"></param>
            /// <param name="_wholedigits"></param>
            /// <param name="_fractiondigits"></param>
            /// <param name="_signed"></param>
            /// <param name="s"></param>
            /// <returns></returns>
            public static string COMPtoNumber(int comptype, int _wholedigits, int _fractiondigits, bool _signed, string s, string outformat)
            {
                // if the string is empty then return 0
                if (s.Length == 0)
                {
                    return "0";
                }

                string convert = string.Empty; // our result
                string format = outformat; // the format string to use for formatting the outpu

                // if we were not given a format generate one
                if (format == string.Empty)
                {
                    format = GenerateCompFormat(_wholedigits, _fractiondigits);
                }

                // different comp types need differt parsers
                if (comptype == 3) // COMP-3 also known as Binary Coded Decimal
                {
                    string all = ""; // holds the BCD decoded digits

                    // unpack the BCD
                    char[] cc = s.ToCharArray();
                    foreach (char b in cc)
                    {
                        all = all + Hex(((b & 0xF0) >> 4)) + Hex((b & 0x0F));
                    }

                    // pull the sign off
                    char sign = all[all.Length - 1];
                    all = all.Substring(0, all.Length - 1);
                    bool negative = false;
                    if (_signed)
                    {
                        if (sign == 'D')
                        {
                            negative = true;
                        }
                        else if (sign == 'C')
                        {
                            // positive ... dont need to do anything
                        }
                        else
                        {
                            throw new ApplicationException("Unexpected sign character " + sign);
                        }
                    }

                    // now put it all together
                    if (negative)
                    {
                        convert = "-";
                    }

                    // add all the whole digits
                    for (int i = 0; i < all.Length - _fractiondigits; i++)
                    {
                        convert = convert + all[i];
                    }

                    // if we have fraction digits
                    if (_fractiondigits > 0)
                    {
                        // add the decimal point
                        convert = convert + ".";

                        // now add the fraction digits
                        for (int i = all.Length - _fractiondigits; i < all.Length; i++)
                        {
                            convert = convert + all[i];
                        }
                    }
                }
                else if (comptype == 100) // PIC S999999V99
                {
                    string all = ""; // holds the decoded digits

                    // unpack the number
                    char[] cc = s.ToCharArray();
                    foreach (char b in cc)
                    {
                        if ((b & 0xFF) != 0x40)
                        {
                            all = all + Hex((b & 0x0F));
                        }
                    }

                    // convert it to a number
                    double result = 0;
                    if (all.Length > 0)
                    {
                        result = Convert.ToDouble(all);
                    }

                    // work out the sign
                    double multiplier = 1;
                    if (_signed)
                    {
                        if ((((int)cc[cc.GetLength(0) - 1]) & 0x00F0) == 0x00D0)
                        {
                            multiplier = -1;
                        }
                        else if ((((int)cc[cc.GetLength(0) - 1]) & 0x00F0) == 0x00C0)
                        {
                        }
                        else if ((((int)cc[cc.GetLength(0) - 1]) & 0x00F0) == 0x00F0)
                        {
                            // missing sign but we can ignore this
                        }
                        else
                        {
                            throw new ApplicationException("Error reading sign in Zoned decimal: " + Hex(cc[cc.GetLength(0) - 1]));
                        }
                    }

                    // now work out what we need to divide it by to account for the decimal place
                    double divider = 1;
                    divider = divider * Math.Pow(10, (double)_fractiondigits);

                    // now finalise the number
                    result = result * multiplier / divider;

                    // now format it
                    convert = result.ToString(format);
                }
                else if (comptype == 0) // COMP
                {
                    string all = ""; // extract the digits

                    // extract the digits
                    char[] cc = s.ToCharArray();
                    foreach (char b in cc)
                    {
                        all = all + (char)b;
                    }

                    long result = 0; // our result
                    int multiplier = 1; // negative multiplier

                    // convert the binary number to a number field
                    for (int i = all.Length - 1; i >= 0; i--)
                    {
                        int bvalue = Convert.ToByte(all[i]);
                        result = result + bvalue * multiplier;
                        multiplier = multiplier * 256;
                    }

                    // extract the sign
                    if (Convert.ToByte(all[0]) >= 64)
                    {
                        if (!_signed)
                        {
                            throw new ApplicationException("Negative number found (COMP) but we were not expecting a negative number " + s);
                        }
                        result = ~result;
                        result++;

                        result = result * -1;
                    }
                    else
                    {
                        // positive number ... do nothing
                    }

                    // if we are a whole number
                    if (_fractiondigits == 0)
                    {
                        // format it
                        convert = result.ToString(format);
                    }
                    else
                    {
                        // work out the fractions
                        double d = result;

                        for (int i = 0; i < _fractiondigits; i++)
                        {
                            d = d / 10;
                        }

                        // format it
                        convert = d.ToString(format);
                    }
                }
                else if (comptype == 1) // COMP-1
                {
                    // check it is the right length
                    if (s.Length != 4)
                    {
                        throw new ApplicationException("COMP-1 fields must be 4 bytes long");
                    }

                    float result = 0; // the result

                    // extract the floating point components
                    int exponent = (byte)((int)s[0] * 2 + ((int)s[1] / 128)) - 127;
                    int fraction = s[3] + s[2] * 256 + s[1] * 256 * 256;

                    int divider = 1 << 23;
                    float mult = 1.0f;
                    for (int i = 0; i < 23; i++)
                    {
                        if ((fraction & divider) > 0)
                        {
                            result = result + mult;
                        }
                        divider = divider / 2;
                        mult = mult / 2;
                    }
                    result = result * (1 << exponent);

                    // work out the sign
                    if (s[0] > 128)
                    {
                        if (!_signed)
                        {
                            throw new ApplicationException("Unexpected Negative number");
                        }

                        result = result * -1;
                    }

                    // format the result
                    convert = result.ToString(format);
                }
                else if (comptype == 2) // COMP-2
                {
                    // check it is the right length
                    if (s.Length != 8)
                    {
                        throw new ApplicationException("COMP-2 fields must be 8 bytes long");
                    }

                    double result = 0; // the result

                    // extract the floating point components
                    int exponent = (((int)s[0] << 4) + (((int)s[1] & 0xF0) >> 4)) - 1023;
                    long fraction = s[7] + ((long)s[6] << 8) + ((long)s[5] << 16) + ((long)s[4] << 24) + ((long)s[3] << 32) + ((long)s[2] << 40) + (((long)s[1] & 0x0F) << 48) + ((long)1 << 52);

                    result = ((double)fraction) / ((long)1 << (-1 * (exponent - 52)));

                    // work out the sign
                    if (s[0] > 128)
                    {
                        if (!_signed)
                        {
                            throw new ApplicationException("Unexpected Negative number");
                        }

                        result = result * -1;
                    }

                    // format it
                    convert = result.ToString(format);
                }
                else
                {
                    throw new ApplicationException("Unsupported COMP type : " + comptype.ToString());
                }

                // return the result
                return convert;
            }
            #endregion

            #region Constructor

            /// <summary>
            /// Parse out the column rule from the XML file
            /// </summary>
            /// <param name="node">XmlNode to read</param>
            public ColumnParsingRule(XmlNode node, bool tracexml, string prefix)
            {
                if (node.Name.ToLower() == "column")
                {
                    foreach (XmlNode n in node.ChildNodes)
                    {
                        // start character of the column
                        if (n.Name.ToLower() == "start")
                        {
                            _start = Convert.ToInt16(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <start>" + _start.ToString() + "</start>");
                            }
                        }
                        // length of the column
                        else if (n.Name.ToLower() == "length")
                        {
                            _length = Convert.ToInt16(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <length>" + _length.ToString() + "</length>");
                            }
                        }
                        else if (n.Name.ToLower() == "name")
                        {
                            _name = n.InnerText.ToLower();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Name>" + _name + "</Name>");
                            }
                        }
                        // do ebcdic string conversion
                        else if (n.Name.ToLower() == "ebcdic")
                        {
                            _ebcdic = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <EBCDIC/>");
                            }
                        }
                        else if (n.Name.ToLower() == "zoned")
                        {
                            _comp = 100;
                            ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Zoned>" + n.InnerText + "</Zoned>");
                            }
                        }
                        // do ebcdic string conversion
                        else if (n.Name.ToLower() == "comp3" || n.Name.ToLower() == "comp-3")
                        {
                            _comp = 3;
                            ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp3>" + n.InnerText + "</Comp3>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp")
                        {
                            _comp = 0;
                            ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp>" + n.InnerText + "</Comp>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp1" || n.Name.ToLower() == "comp-1")
                        {
                            _comp = 1;
                            ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp1>" + n.InnerText + "</Comp1>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp2" || n.Name.ToLower() == "comp-2")
                        {
                            _comp = 2;
                            ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp2>" + n.InnerText + "</Comp2>");
                            }
                        }
                        // characters to stop parsing at
                        else if (n.Name.ToLower() == "stopchars")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <StopChars>");
                            }
                            foreach (XmlNode n2 in n.ChildNodes)
                            {
                                if (n2.Name.ToLower() == "char")
                                {
                                    // If the char is preceeded by a \ then it is special
                                    char c = n2.InnerText.Substring(0, 1)[0];
                                    if (c == '\\')
                                    {
                                        if (n2.InnerText.Substring(0, 2) == "\\t")
                                        {
                                            c = '\t';
                                        }
                                        else if (n2.InnerText.Substring(0, 2) == "\\n")
                                        {
                                            c = '\n';
                                        }
                                        else if (n2.InnerText.Substring(0, 2) == "\\r")
                                        {
                                            c = '\r';
                                        }
                                        else if (n2.InnerText.Substring(0, 2) == "\\0")
                                        {
                                            c = '\0';
                                        }
                                    }

                                    _stopchars.Add(c);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    <Char>" + c + "</Char>");
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </StopChars>");
                            }
                        }
                    }
                }

                // work out the format for comp fields
                if (_comp != -1)
                {
                    _format = ColumnParsingRule.GenerateCompFormat(_wholedigits, _fractiondigits);
                }
            }
            #endregion

            #region Accessors
            /// <summary>
            /// Get the name of the column
            /// </summary>
            public string Name
            {
                get
                {
                    return _name;
                }
            }
            #endregion

            #region Public Functions
            /// <summary>
            /// Apply the column definition to the line and extract the column data
            /// </summary>
            /// <param name="line">The line to apply the rule to</param>
            /// <returns>The column value</returns>
            public string Extract(string line)
            {
                string s; // The string we are going to return

                // if line is not long enough then return blank
                if (line.Length < _start)
                {
                    s = string.Empty;
                }
                // Grab the characters
                else
                {
                    if (_length == -1 || line.Length < _start + _length)
                    {
                        s = line.Substring(_start);
                    }
                    else
                    {
                        s = line.Substring(_start, _length);
                    }
                }

                int found = -1; // where we found the first stop character

                // check each stop character
                foreach (char c in _stopchars)
                {
                    // if we find the stop character
                    int f = s.IndexOf(c);

                    // if this is closer to the start of our column then previous stop characters then keep this
                    if (f != -1 && (found == -1 || f < found))
                    {
                        found = f;
                    }
                }

                // If we found something
                if (found != -1)
                {
                    // Cut it short
                    s = s.Substring(0, found);
                }

                // if this is an ebcdic column do that conversion
                if (_ebcdic)
                {
                    s = ColumnParsingRule.EBCDICtoASCII(s);
                }
                // else if this is a comp or zoned field
                else if (_comp != -1)
                {
                    try
                    {
                        s = ColumnParsingRule.COMPtoNumber(_comp, _wholedigits, _fractiondigits, _signed, s, _format);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error parsing compfield " + _comp.ToString() + " '" + ColumnParsingRule.HexString(s, "", "", "") + "'", ex);
                    }
                }

                // Return the new value
                return s;
            }
            #endregion
        }
        #endregion

        #region Member Variables
        List<ColumnParsingRule> _cols = new List<ColumnParsingRule>(); // The columns
        #endregion

        #region Constructors
        public fixedparser(CSVJob j, XmlNode node, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, node, tracexml, prefix, /*filename*/fi, new string[] { "column" })
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name.ToLower() == "column")
                {
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "<Column>");
                    }
                    // Save the column definition
                    _cols.Add(new ColumnParsingRule(n, tracexml, prefix + "  "));
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "</Column>");
                    }
                }
            }

            // set the names of the columns using the names from the column definitions.
            ColumnLine clNames = new ColumnLine();
            foreach (ColumnParsingRule cpr in _cols)
            {
                clNames.AddEnd(cpr.Name);
            }
            ColumnNumber.SetColNames(j, clNames, false);
        }
        #endregion

        #region Overriden Methods
        public override ColumnLine Parse(string line, long inputline)
        {
            // create the output columns
            ColumnLine cl = new ColumnLine(line, inputline, /*_filename*/_fi);

            // process each column and add the result
            foreach (ColumnParsingRule cpr in _cols)
            {
                cl.AddEnd(cpr.Extract(line));
            }

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// A parser for handling delimited files
    /// </summary>
    class delimitedparser : InputParser
    {
        #region Member Variables
        char _quote = delimitedparser.DefaultQuote; // the quote
        char _delimiter = delimitedparser.DefaultDelimiter; // the delimiter
        char _quoteescape = delimitedparser.DefaultQuoteEscape; // the quote escape character
        #endregion

        #region Accessors
        /// <summary>
        /// Get the delimiter
        /// </summary>
        public char Delimiter
        {
            get
            {
                return _delimiter;
            }
        }
        /// <summary>
        /// Get the quote
        /// </summary>
        public char Quote
        {
            get
            {
                return _quote;
            }
        }
        /// <summary>
        /// Get the quote escape character
        /// </summary>
        public char QuoteEscape
        {
            get
            {
                return _quoteescape;
            }
        }
        public static char DefaultDelimiter
        {
            get
            {
                return ',';
            }
        }
        public static char DefaultQuote
        {
            get
            {
                return '"';
            }
        }
        public static char DefaultQuoteEscape
        {
            get
            {
                return '\\';
            }
        }
        #endregion

        #region Constructors
        public delimitedparser(CSVJob j, XmlNode node, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, node, tracexml, prefix, /*filename*/fi, new string[] { "delimiter", "noquote", "quote", "quoteescape", "noinputquote" })
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name.ToLower() == "delimiter")
                {
                    // read in the input delimiter
                    try
                    {
                        _delimiter = n.InnerText.Substring(0, 1)[0];

                        if (_delimiter == '\\')
                        {
                            if (n.InnerText.Substring(0, 2).ToLower() == "\\t")
                            {
                                _delimiter = '\t';
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("DelimitedParser::Constructor Error extracting input delimiter from parameters : " + n.InnerText, ex);
                    }
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "  <Delimiter>" + _delimiter + "</Delimiter>");
                    }
                }
                else if (n.Name.ToLower() == "quote")
                {
                    // read in the input quotes
                    try
                    {
                        _quote = n.InnerText.Substring(0, 1)[0];
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("DelimitedParser::Constructor Error extracting quote from parameters : " + n.InnerText, ex);
                    }
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "  <Quote>" + _quote + "</Quote>");
                    }
                }
                else if (n.Name.ToLower() == "quoteescape")
                {
                    // read in the input quote escape
                    try
                    {
                        _quoteescape = n.InnerText.Substring(0, 1)[0];
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("DelimitedParser::Constructor Error extracting quote escape character from parameters : " + n.InnerText, ex);
                    }
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "  <QuoteEscape>" + _quoteescape + "</QuoteEscape>");
                    }
                }
                else if (n.Name.ToLower() == "noquote")
                {
                    // set no input quote
                    _quote = (char)128;
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "  <NoQuote/>");
                    }
                }
            }
        }
        #endregion

        #region Overriden Methods
        /// <summary>
        /// Parse the row using delimiters
        /// </summary>
        /// <param name="line"></param>
        /// <param name="inputline"></param>
        /// <returns></returns>
        public override ColumnLine Parse(string line, long inputline)
        {
            // create the output line
            CSVFileLine fl = new CSVFileLine(line, inputline, /*_filename*/_fi);

            // parse it
            fl.Parse(_delimiter, _quote, _quoteescape);

            return fl;
        }
        #endregion
    }

    #region Untested Parsers
    /// <summary>
    /// Parser for reading JSON strings
    /// </summary>
    class jsonparser : xmlparser
    {
        bool _storexmlasraw = false;

        #region Constructors
        public jsonparser(CSVJob j, XmlNode node, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, node, tracexml, prefix, /*filename*/fi)
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name.ToLower() == "storexmlasraw")
                {
                    _storexmlasraw = true;
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "  <StoreXMLAsRaw/>");
                    }
                }
            }
        }
        #endregion

        #region Overriden Methods
        public override ColumnLine Parse(string line, long inputline)
        {
            ColumnLine cl = new ColumnLine();
            XNode node = JsonConvert.DeserializeXNode(line, "Root");
            cl = base.Parse(node.ToString(), inputline);
            if (!_storexmlasraw)
            {
                cl[0] = line;
            }
            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Parser for XML strings
    /// </summary>
    class xmlparser : InputParser
    {
        public class XPathColumnParsingRule
        {
            #region Member Variables
            string _xPath;
            string _name;
            #endregion

            #region Constructor

            /// <summary>
            /// Parse out the column rule from the XML file
            /// </summary>
            /// <param name="node">XmlNode to read</param>
            public XPathColumnParsingRule(XmlNode node, bool tracexml, string prefix)
            {
                if (node.Name.ToLower() == "column")
                {
                    foreach (XmlNode n in node.ChildNodes)
                    {
                        // start character of the column
                        if (n.Name.ToLower() == "xpath")
                        {
                            _xPath = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <XPath>" + _xPath + "</XPath>");
                            }
                        }
                        else if (n.Name.ToLower() == "name")
                        {
                            _name = n.InnerText.ToLower();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Name>" + _name + "</Name>");
                            }
                        }
                    }
                }
            }
            #endregion

            #region Accessors
            /// <summary>
            /// Get the name of the column
            /// </summary>
            public string Name
            {
                get
                {
                    return _name;
                }
            }
            #endregion

            #region Public Functions
            /// <summary>
            /// Apply the column definition to the line and extract the column data
            /// </summary>
            /// <param name="line">The line to apply the rule to</param>
            /// <returns>The column value</returns>
            public string Extract(string line)
            {
                string s = string.Empty; // The string we are going to return

                XmlDocument n = new XmlDocument();
                n.Load(new StringReader(line));
                XPathNavigator xpn = n.CreateNavigator();

                XPathNodeIterator xpni = xpn.Select(_xPath);

                while (xpni.MoveNext())
                {
                    s = s + xpni.Current.Value;
                }

                // Return the new value
                return s;
            }
            #endregion
        }

        #region Member Variables
        List<XPathColumnParsingRule> _cols = new List<XPathColumnParsingRule>(); // The columns
        #endregion

        #region Constructors
        public xmlparser(CSVJob j, XmlNode node, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, node, tracexml, prefix, /*filename*/fi, new string[] { "column" })
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name.ToLower() == "column")
                {
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "<Column>");
                    }
                    // Save the column definition
                    _cols.Add(new XPathColumnParsingRule(n, tracexml, prefix + "  "));
                    if (tracexml)
                    {
                        CSVProcessor.DisplayError(prefix + "</Column>");
                    }
                }
            }

            // set the names of the columns using the names from the column definitions.
            ColumnLine clNames = new ColumnLine();
            foreach (XPathColumnParsingRule cpr in _cols)
            {
                clNames.AddEnd(cpr.Name);
            }
            ColumnNumber.SetColNames(j, clNames, false);
        }
        #endregion

        #region Overriden Methods
        public override ColumnLine Parse(string line, long inputline)
        {
            // create the output columns
            ColumnLine cl = new ColumnLine(line, inputline, /*_filename*/_fi);

            // process each column and add the result
            foreach (XPathColumnParsingRule cpr in _cols)
            {
                cl.AddEnd(cpr.Extract(line));
            }

            return cl;
        }
        #endregion
    }

    #endregion

    #region Unimplemented Parsers
    /// <summary>
    /// Parser for HTML tables
    /// </summary>
    class htmltableparser : InputParser
    {
        #region Constructors
        public htmltableparser(CSVJob j, XmlNode node, bool tracexml, string prefix, /*string filename*/CSVFileInfo fi)
            : base(j, node, tracexml, prefix, /*filename*/fi, new string[] { })
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Overriden Methods
        public override ColumnLine Parse(string line, long inputline)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    #endregion
}
