﻿namespace CSVProcessor
{
   partial class OverrideDisable
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          this.components = new System.ComponentModel.Container();
          this.button1 = new System.Windows.Forms.Button();
          this.treeView1 = new System.Windows.Forms.TreeView();
          this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
          this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.deselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.contextMenuStrip1.SuspendLayout();
          this.SuspendLayout();
          // 
          // button1
          // 
          this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.button1.Location = new System.Drawing.Point(187, 437);
          this.button1.Name = "button1";
          this.button1.Size = new System.Drawing.Size(84, 23);
          this.button1.TabIndex = 0;
          this.button1.Text = "Ok";
          this.button1.UseVisualStyleBackColor = true;
          this.button1.Click += new System.EventHandler(this.button1_Click);
          // 
          // treeView1
          // 
          this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.treeView1.CheckBoxes = true;
          this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
          this.treeView1.Location = new System.Drawing.Point(12, 12);
          this.treeView1.Name = "treeView1";
          this.treeView1.Size = new System.Drawing.Size(436, 419);
          this.treeView1.TabIndex = 1;
          this.treeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
          // 
          // contextMenuStrip1
          // 
          this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.deselectAllToolStripMenuItem});
          this.contextMenuStrip1.Name = "contextMenuStrip1";
          this.contextMenuStrip1.Size = new System.Drawing.Size(153, 70);
          // 
          // selectAllToolStripMenuItem
          // 
          this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
          this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
          this.selectAllToolStripMenuItem.Text = "&Select All";
          this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
          // 
          // deselectAllToolStripMenuItem
          // 
          this.deselectAllToolStripMenuItem.Name = "deselectAllToolStripMenuItem";
          this.deselectAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
          this.deselectAllToolStripMenuItem.Text = "&Deselect All";
          this.deselectAllToolStripMenuItem.Click += new System.EventHandler(this.deselectAllToolStripMenuItem_Click);
          // 
          // OverrideDisable
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(460, 472);
          this.ControlBox = false;
          this.Controls.Add(this.treeView1);
          this.Controls.Add(this.button1);
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "OverrideDisable";
          this.ShowInTaskbar = false;
          this.Text = "Override Disable";
          this.Load += new System.EventHandler(this.OverrideDisable_Load);
          this.contextMenuStrip1.ResumeLayout(false);
          this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.TreeView treeView1;
      private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
      private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem deselectAllToolStripMenuItem;
   }
}