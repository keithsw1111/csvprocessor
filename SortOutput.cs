// Code last tidied up September 4, 2010

using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSVProcessor
{
	/// <summary>
	/// This class is a line that can be sorted
	/// </summary>
	class SortableLine : CSVFileLine, IComparable
	{
		static int __sequence = 1;

		List<KeyColumn> _keycols = null; // the columns we are sorting on
		int _sequence = 0;

		#region IComparable
		/// <summary>
		/// Comparator used to compare 2 lines
		/// </summary>
		/// <param name="o">object to compare - must be a sortable line</param>
		/// <returns></returns>
		public int CompareTo(object o)
		{
			// cast the object as a sortable line
			SortableLine l2 = (SortableLine)o;

			// for each key column
			foreach (KeyColumn k in _keycols)
			{
				int r = 0; // return value - default is EQUAL

				// process according to key column type
				switch (k.Type)
				{
					case "integer":
						// convert to integer
						long i1 = 0;
						try
						{
							i1 = long.Parse(this[k.Number]);
						}
						catch
						{
							i1 = 0;
						}
						//convert to integer
						long i2 = 0;
						try
						{
							i2 = long.Parse(l2[k.Number]);
						}
						catch
						{
							i2 = 0;
						}
						// now compare
						if (i1 < i2)
						{
							r = -1;
						}
						else if (i1 > i2)
						{
							r = 1;
						}
						else
						{
							r = 0;
						}
						break;
					case "float":
						// convert to float
						double d1 = 0.0;
						try
						{
							d1 = double.Parse(this[k.Number]);
						}
						catch
						{
							d1 = 0.0;
						}

						// convert to float
						double d2 = 0.0;
						try
						{
							d2 = double.Parse(l2[k.Number]);
						}
						catch
						{
							d2 = 0.0;
						}

						// now compare
						if (d1 < d2)
						{
							r = -1;
						}
						else if (d1 > d2)
						{
							r = 1;
						}
						else
						{
							r = 0;
						}
						break;
					case "datetime":
						// convert to datetime
						DateTime dt1 = DateTime.MinValue;
						try
						{
							dt1 = DateTime.ParseExact(this[k.Number], k.InFormat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
						}
						catch
						{
							dt1 = DateTime.MinValue;
						}
						// convert to datetime
						DateTime dt2 = DateTime.MinValue;
						try
						{
							dt2 = DateTime.ParseExact(l2[k.Number], k.InFormat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
						}
						catch
						{
							dt2 = DateTime.MinValue;
						}

						// now compare
						r = DateTime.Compare(dt1, dt2);
						break;
					case "string":
						// compare the strings
						r = string.Compare(this[k.Number], l2[k.Number]);
						break;
				}

				// if r is 0 then we need to look at our next column
				if (r == 0)
				{
					// do nothing
				}
				else
				{
					// no zero so we will be exiting but with what value

					// if ascending return our value
					if (k.Ascending)
					{
						return r;
					}
					else
					{
						// if descending invert our value
						return r * -1;
					}
				}

			}

			// now compare
			if (this._sequence < l2._sequence)
			{
				return -1;
			}
			else if (this._sequence > l2._sequence)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Create a sortable line
		/// </summary>
		/// <param name="s">The line</param>
		/// <param name="keycols">The key columns</param>
		public SortableLine(string s, List<KeyColumn> keycols) : base(s, 0)
		{
			// save the key cols
			_keycols = keycols;
			_sequence = __sequence++;
		}

		public SortableLine(ColumnLine s, List<KeyColumn> keycols) : base(s, 0)
		{
			// save the key cols
			_keycols = keycols;
			_sequence = __sequence++;
		}
		#endregion
	}

	/// <summary>
	/// Describes a column which is being used in a sort
	/// </summary>
	class KeyColumn
	{
		#region Member Variables
		ColumnNumber _col; // the column number
		string _type; // the column data type
		bool _ascending; // the sort direction
		string _informat; // the input format for date parsing
		#endregion

		#region Accessors

		/// <summary>
		/// Input format for a date
		/// </summary>
		public string InFormat
		{
			get
			{
				return _informat;
			}
		}

		/// <summary>
		/// True if sorted in ascending order
		/// </summary>
		public bool Ascending
		{
			get
			{
				return _ascending;
			}
		}

		/// <summary>
		/// Type of the key column
		/// </summary>
		public string Type
		{
			get
			{
				return _type;
			}
		}

		/// <summary>
		/// Column number to sort on
		/// </summary>
		public int Number
		{
			get
			{
				return _col.Number;
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Create a key column
		/// </summary>
		/// <param name="col"></param>
		/// <param name="ascending"></param>
		/// <param name="type"></param>
		/// <param name="informat"></param>
		public KeyColumn(ColumnNumber col, bool ascending, string type, string informat)
		{
			// save the values
			_col = col;
			_ascending = ascending;
			_type = type;
			_informat = informat;
		}
		#endregion
	}

	/// <summary>
	/// Responsible for processing sorted output
	/// </summary>
	public class SortOutput
	{
		#region Member Variables
		List<Output> _outputs = null; // This is the file we are writing to
		SortedDictionary<SortableLine, int> _outputlist = new SortedDictionary<SortableLine, int>(); // these are the rows we will sort
		List<KeyColumn> _keyColumns = new List<KeyColumn>(); // these are the key columns on which we will sort
		List<ColumnNumber> _excludecolsindedup = new List<ColumnNumber>(); // these columns will be ignored when doing a dedup
		CSVProcess _csvProcess = null; // our parent
		string _id = string.Empty;
		bool _dedup = false; // if true then we will reject duplicate records
		int _unique = UniqueIdClass.Allocate();
		#endregion

		public string UniqueId
		{
			get
			{
				return "SO" + _unique.ToString();
			}
		}

		public string Id
		{
			get
			{
				if (_id != string.Empty)
				{
					return _id;
				}
				else
				{
					return _csvProcess.Id;
				}
			}
		}

		public void WriteDot(StreamWriter dotWriter, string prefix)
		{
			Debug.Fail("Not implemented yet.");
		}

		#region Public Functions
		/// <summary>
		/// Writes a record to the output
		/// </summary>
		/// <param name="s"></param>
		public void DisplayOutput(string s, bool data)
		{
			foreach (Output o in _outputs)
			{
				o.Write(s, data);
			}
		}

		/// <summary>
		/// Writes a record to the output
		/// </summary>
		/// <param name="l"></param>
		public void DisplayOutput(ColumnLine l)
		{
			foreach (Output o in _outputs)
			{
				o.Write(l);
			}
		}

		/// <summary>
		/// Adds a line to be sorted
		/// </summary>
		/// <param name="s">The line to sort</param>
		public void AddOutput(ColumnLine s)
		{
			// create a sortable line
			SortableLine l = new SortableLine(s, _keyColumns);

			// Parse the line
			l.Parse();

			// add it to our list
			_outputlist.Add(l, 1);
		}

		/// <summary>
		/// Tell the program to use an output file
		/// </summary>
		/// <param name="twOutput"></param>
		public void SetOutput(List<Output> outputs)
		{
			// save the output file writer
			_outputs = outputs;
		}

		/// <summary>
		/// Called once we have finished adding the lines ... this is where the real work gets done
		/// </summary>
		public void Finish()
		{
			// create a step timer
			Timer StepTimer = new Timer();
			StepTimer.Reset();

			// tell the user what is going on
			CSVProcessor.DisplayError(ANSIHelper.Information + "Writing sorted data." + ANSIHelper.White);

			// sort the data
			// _outputlist.Sort();

			// tell the user what is going on
			// CSVProcessor.DisplayError(ANSIHelper.Timing + "Output data sorted in " + StepTimer.DurationAsString + ANSIHelper.White);
			// StepTimer.Reset();

			long dups = 0; // count the duplicates
			SortableLine last = new SortableLine("", _keyColumns); // dummy last line

			// look at each line
			//foreach (SortableLine l in _outputlist)
			foreach (KeyValuePair<SortableLine, int> kvp in _outputlist)
			{
				SortableLine l = kvp.Key;
				// if we are looking for duplicates and this is a duplicate
				if (_dedup && IsDup(last, l))
				{
					// count it and skip it
					dups++;
				}
				else
				{
					// display it
					DisplayOutput(l);
				}

				// remember this line so we can compare duplicates
				last = l;
			}

			// tell the user what is going on
			CSVProcessor.DisplayError(ANSIHelper.Timing + "Output data written in " + StepTimer.DurationAsString + ANSIHelper.White);

			// tell the user how many lines we deleted
			if (dups > 0)
			{
				CSVProcessor.DisplayError(ANSIHelper.Information + "Duplicates eliminated " + dups.ToString() + ANSIHelper.White);
			}
		}

		#endregion

		#region Private Functions
		/// <summary>
		/// Checks if 2 lines are duplicates
		/// </summary>
		/// <param name="l1"></param>
		/// <param name="l2"></param>
		/// <returns></returns>
		bool IsDup(SortableLine l1, SortableLine l2)
		{
			// if line 1 has less columns than line 2 then reverse the parameters otherwise wrong answer may be calculated
			if (l1.Count < l2.Count)
			{
				return IsDup(l2, l1);
			}
			else
			{
				// check each column
				for (int i = 0; i < l1.Count; i++)
				{
					// if the columns are not equal
					if (l1[i] != l2[i])
					{
						bool found = false; // true if I am to ignore this column

						// check each ignore column
						foreach (ColumnNumber cn in _excludecolsindedup)
						{
							// if this is a column I was to ignore
							if (cn.Number == i)
							{
								// we can ignore this
								found = true;
								break;
							}
						}

						// if we did not find this column in the ignore list
						if (!found)
						{
							// lines are not the same
							return false;
						}
					}
				}
			}

			// lines were identical (after taking into account ignore list
			return true;
		}
		#endregion

		#region Constructor
		/// <summary>
		/// Create a SortOutput object
		/// </summary>
		/// <param name="forceSTDOUT">Force output to stdout</param>
		/// <param name="n">XML node to create it from</param>
		public SortOutput(XmlNode n, CSVProcess processor, bool tracexml, string prefix)
		{
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "<SortOutput>");
			}

			// save the owning processor
			_csvProcess = processor;

			// process each node
			foreach (XmlNode n1 in n.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "keycolumns")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <KeyColumns>");
						}
						// process each node
						foreach (XmlNode n2 in n1.ChildNodes)
						{
							if (n2.NodeType != XmlNodeType.Comment)
							{
								if (n2.Name.ToLower() == "key")
								{
									if (tracexml)
									{
										CSVProcessor.DisplayError(prefix + "  <Key>");
									}
									ColumnNumber col = null; // key column
									bool ascending = true; // ascending flag
									string t = null; // type
									string informat = string.Empty; // input format

									// process each node
									foreach (XmlNode n3 in n2.ChildNodes)
									{
										if (n3.NodeType != XmlNodeType.Comment)
										{
											if (n3.Name.ToLower() == "column")
											{
												// save the column number
												col = new ColumnNumber(_csvProcess.Job, n3.InnerText);
												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "    <Column>" + col.ToString() + "</Column>");
												}
											}
											else if (n3.Name.ToLower() == "ascending")
											{
												// ascending key
												ascending = true;
												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "    <Ascending/>");
												}
											}
											else if (n3.Name.ToLower() == "descending")
											{
												// descending key
												ascending = false;
												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "    <Descending/>");
												}
											}
											else if (n3.Name.ToLower() == "type")
											{
												// type
												t = n3.InnerText.ToLower();

												if (t != "string" &&
													t != "integer" &&
													t != "datetime" &&
													t != "float")
												{
													throw new ApplicationException("Sort data type '" + t + "' not recognised.");
												}

												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "    <Type>" + t + "</Type>");
												}
											}
											else if (n3.Name.ToLower() == "informat")
											{
												// date input format
												informat = n3.InnerText;
												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "    <InFormat>" + informat + "</InFormat>");
												}
											}
											else
											{
												CSVProcessor.DisplayError(ANSIHelper.Warning + "SortOutput::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
											}
										}
									}
									if (tracexml)
									{
										CSVProcessor.DisplayError(prefix + "  </Key>");
									}

									// add the key column
									_keyColumns.Add(new KeyColumn(col, ascending, t, informat));
								}
							}
						}
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </KeyColumns>");
						}
					}
					else if (n1.Name.ToLower() == "id")
					{
						_id = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
						}
					}
					else if (n1.Name.ToLower() == "deduplicate")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Deduplicate>");
						}
						// we need to deduplicate
						_dedup = true;

						// process each node
						foreach (XmlNode n2 in n1.ChildNodes)
						{
							if (n2.NodeType != XmlNodeType.Comment)
							{
								if (n2.Name.ToLower() == "excludecolumn")
								{
									ColumnNumber cn = new ColumnNumber(_csvProcess.Job, n2.InnerText);
									// exclude this column
									_excludecolsindedup.Add(cn);
									if (tracexml)
									{
										CSVProcessor.DisplayError(prefix + "    <ExcludeColumn>" + cn.ToString() + "</ExcludeColumn>");
									}
								}
							}
						}
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </Deduplicate>");
						}
					}
					else
					{
						CSVProcessor.DisplayError(ANSIHelper.Warning + "SortOutput::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
					}
				}
			}
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "</SortOutput>");
			}

			// if we have no sort columns
			if (_keyColumns.Count == 0)
			{
				// throw an error
				throw new ApplicationException("Sort must have at least one key column.");
			}
		}
		#endregion
	}
}
