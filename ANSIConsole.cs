// Code last tidied up September 4, 2010

using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CSVProcessor
{   /// <summary>
    /// Summary description for ANSIConsole.
    /// </summary>
    public class ANSIHelper
    {
        #region Constants
        // define constant strings for use by the user
        public const string Reset = "\x1b[0m";
        public const string BlackBackground = "\x1b[40m";
        public const string WhiteBackground = "\x1b[47m";
        public const string Black = "\x1b[30m";
        /// <summary>
        /// Red text
        /// </summary>
        public const string Red = "\x1b[31m" + BlackBackground;
        public const string Error = "\x1b[31m" + BlackBackground;
        /// <summary>
        /// Magenta text
        /// </summary>
        public const string Green = "\x1b[32m" + BlackBackground;
        /// <summary>
        /// Yellow text
        /// </summary>
        public const string Yellow = "\x1b[33m" + BlackBackground;
        public const string Warning = "\x1b[33m" + BlackBackground;
        /// <summary>
        /// Blue text
        /// </summary>
        public const string Blue = "\x1b[34m" + WhiteBackground;
        public const string Timing = "\x1b[34m" + WhiteBackground;
        /// <summary>
        /// Magenta text
        /// </summary>
        public const string Magenta = "\x1b[35m" + BlackBackground;
        public const string Trace = "\x1b[35m" + BlackBackground;
        /// <summary>
        /// Cyan text
        /// </summary>
        public const string Cyan = "\x1b[36m" + BlackBackground;
        public const string Information = "\x1b[36m" + BlackBackground;
        /// <summary>
        /// White text
        /// </summary>
        public const string White = "\x1b[37m" + BlackBackground;
        public const string Normal = "\x1b[37m" + BlackBackground;
        #endregion

        #region Enumerations
        // stream to write to
        public enum STREAM { STDOUT, STDERR };
        #endregion

        /// <summary>
        /// Class for displaying ANSI text to a console
        /// </summary>
        class ANSIAction
        {
            #region Member Variables
            List<int> numbers = new List<int>(); // list of integers affecting the action
            int _codeSize = 0; // length of the ANSI code
            Action action; // the action to take
            #endregion

            #region Enumerations
            /// <summary>
            /// ANSI Action to take
            /// </summary>
            enum Action
            {
                CURSORUP, CURSORDOWN, CURSORFORWARD, CURSORBACK,
                CURSORNEXTLINE, CURSORPREVIOUSLINE, CURSORHORIZONTALABSOLUTE, CURSORPOSITION,
                ERASEDATA, ERASEINLINE, SCROLLUP, SCROLLDOWN, HORIZONTALANDVERTICALPOSITION,
                SGR, DSR, SCP, RCP, DECTCEMHIDE, DECTCEMSHOW, NOTHING
            };
            #endregion

            #region Windows API Constants
            const int STD_OUTPUT_HANDLE = -11;
            const int STD_ERROR_HANDLE = -12;
            const int FOREGROUND_BLUE = 0x0001; // text color contains blue.
            const int FOREGROUND_GREEN = 0x0002; // text color contains green.
            const int FOREGROUND_RED = 0x0004; // text color contains red.
            const int FOREGROUND_INTENSITY = 0x0008; // text color is intensified.
            const int BACKGROUND_BLUE = 0x0010; // background color contains blue.
            const int BACKGROUND_GREEN = 0x0020; // background color contains green.
            const int BACKGROUND_RED = 0x0040; // background color contains red.
            const int BACKGROUND_INTENSITY = 0x0080; // background color is intensified.
            const int COMMON_LVB_REVERSE_VIDEO = 0x4000; // DBCS: Reverse fore/back ground attribute.
            const int COMMON_LVB_UNDERSCORE = 0x8000; // DBCS: Underscore.
            #endregion

            #region Windows API Definitions
            [DllImportAttribute("Kernel32.dll")]
            private static extern bool SetConsoleTextAttribute
                (
                IntPtr hConsoleOutput, // handle to screen buffer
                int wAttributes    // text and background colors
                );

            [DllImportAttribute("Kernel32.dll")]
            private static extern bool GetConsoleScreenBufferInfo
                (
                IntPtr hConsoleOutput, // handle to screen buffer
                out CONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo
                );

            [DllImportAttribute("Kernel32.dll")]
            private static extern bool SetConsoleCursorPosition
                (
                IntPtr hConsoleOutput, // handle to screen buffer
                COORD coord
                );

            [DllImportAttribute("Kernel32.dll")]
            private static extern IntPtr GetStdHandle
                (
                int nStdHandle // input, output, or error device
                );

            struct COORD
            {
                public short X;
                public short Y;
            }

            struct SMALL_RECT
            {
                public short Left;
                public short Top;
                public short Right;
                public short Bottom;
            }

            struct CONSOLE_SCREEN_BUFFER_INFO
            {
                public COORD dwSize;
                public COORD dwCursorPosition;
                public int wAttributes;
                public SMALL_RECT srWindow;
                public COORD dwMaximumWindowSize;
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Default ansi action
            /// </summary>
            public ANSIAction()
            {
                action = Action.NOTHING;
            }

            /// <summary>
            /// Create an ansi action
            /// </summary>
            /// <param name="s"></param>
            public ANSIAction(string s)
            {
                // if it is too short then it is a nothing action
                if (s.Length < 2)
                {
                    action = Action.NOTHING;
                    _codeSize = 0;
                    return;
                }

                _codeSize = 2;
                string s1 = s;
                Regex regexParameter = new Regex("(^|^;)(?'val'[^A-Za-z;]*)", RegexOptions.Singleline);
                string p = regexParameter.Match(s1).Groups["val"].Value;
                while (p != string.Empty)
                {
                    _codeSize += p.Length + 1;
                    numbers.Add(int.Parse(p));

                    s1 = s1.Substring(p.Length);
                    p = regexParameter.Match(s1).Groups["val"].Value;
                }

                Regex regexActionCode = new Regex("^[^A-Za-z]*(?'val'.)", RegexOptions.Singleline);

                switch (regexActionCode.Match(s).Groups["val"].Value)
                {
                    case "A":
                        action = Action.CURSORUP;
                        break;
                    case "B":
                        action = Action.CURSORDOWN;
                        break;
                    case "C":
                        action = Action.CURSORFORWARD;
                        break;
                    case "D":
                        action = Action.CURSORBACK;
                        break;
                    case "E":
                        action = Action.CURSORNEXTLINE;
                        break;
                    case "F":
                        action = Action.CURSORPREVIOUSLINE;
                        break;
                    case "G":
                        action = Action.CURSORHORIZONTALABSOLUTE;
                        break;
                    case "H":
                        action = Action.CURSORPOSITION;
                        break;
                    case "J":
                        action = Action.ERASEDATA;
                        break;
                    case "K":
                        action = Action.ERASEINLINE;
                        break;
                    case "S":
                        action = Action.SCROLLUP;
                        break;
                    case "T":
                        action = Action.SCROLLDOWN;
                        break;
                    case "f":
                        action = Action.HORIZONTALANDVERTICALPOSITION;
                        break;
                    case "m":
                        action = Action.SGR;
                        break;
                    case "n":
                        action = Action.DSR;
                        break;
                    case "s":
                        action = Action.SCP;
                        break;
                    case "u":
                        action = Action.RCP;
                        break;
                    case "I":
                        action = Action.DECTCEMHIDE;
                        break;
                    case "h":
                        action = Action.DECTCEMSHOW;
                        break;
                    default:
                        action = Action.NOTHING;
                        break;
                }
            }
            #endregion

            #region Accessors
            /// <summary>
            /// Get the code size
            /// </summary>
            public int CodeSize
            {
                get
                {
                    return _codeSize;
                }
            }
            #endregion

            #region Public Functions
            /// <summary>
            /// Execute the ansi action
            /// </summary>
            /// <param name="stream">stream to execute the action on</param>
            public void Execute(STREAM stream)
            {
                // chose action
                switch (action)
                {
                    case Action.CURSORUP:
                        break;
                    case Action.CURSORDOWN:
                        break;
                    case Action.CURSORFORWARD:
                        break;
                    case Action.CURSORBACK:
                        break;
                    case Action.CURSORNEXTLINE:
                        break;
                    case Action.CURSORPREVIOUSLINE:
                        break;
                    case Action.CURSORHORIZONTALABSOLUTE:
                        switch (stream)
                        {
                            case STREAM.STDERR:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_OUTPUT_HANDLE);

                                    CONSOLE_SCREEN_BUFFER_INFO sbi;
                                    GetConsoleScreenBufferInfo(nConsole, out sbi);

                                    COORD coord;
                                    coord.X = 0;
                                    coord.Y = (short)(sbi.dwCursorPosition.Y - 1);
                                    SetConsoleCursorPosition(nConsole, coord);
                                }
                                break;
                            case STREAM.STDOUT:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_ERROR_HANDLE);

                                    CONSOLE_SCREEN_BUFFER_INFO sbi;
                                    GetConsoleScreenBufferInfo(nConsole, out sbi);

                                    COORD coord;
                                    coord.X = 0;
                                    coord.Y = sbi.dwCursorPosition.Y;
                                    SetConsoleCursorPosition(nConsole, coord);
                                }
                                break;
                        }
                        break;
                    case Action.CURSORPOSITION:
                        break;
                    case Action.ERASEDATA:
                        break;
                    case Action.ERASEINLINE:
                        break;
                    case Action.SCROLLUP:
                        break;
                    case Action.SCROLLDOWN:
                        break;
                    case Action.HORIZONTALANDVERTICALPOSITION:
                        break;
                    case Action.SGR:
                        int colourMap = 0;
                        switch (stream)
                        {
                            case STREAM.STDERR:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_OUTPUT_HANDLE);
                                    colourMap = GetConsoleTextAttribute(nConsole);
                                }
                                break;
                            case STREAM.STDOUT:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_ERROR_HANDLE);
                                    colourMap = GetConsoleTextAttribute(nConsole);
                                }
                                break;
                        }
                        foreach (int i in numbers)
                        {
                            switch (i)
                            {
                                case 0:
                                    colourMap = 0;
                                    break;
                                case 1:
                                    colourMap |= FOREGROUND_INTENSITY;
                                    break;
                                case 4:
                                    colourMap |= COMMON_LVB_UNDERSCORE;
                                    break;
                                case 7:
                                    colourMap |= COMMON_LVB_REVERSE_VIDEO;
                                    break;
                                case 22:
                                    if ((colourMap & FOREGROUND_INTENSITY) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_INTENSITY;
                                    }
                                    break;
                                case 27:
                                    if ((colourMap & COMMON_LVB_REVERSE_VIDEO) != 0)
                                    {
                                        colourMap &= ~COMMON_LVB_REVERSE_VIDEO;
                                    }
                                    break;
                                case 30:
                                    if ((colourMap & FOREGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_BLUE;
                                    }
                                    if ((colourMap & FOREGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_GREEN;
                                    }
                                    if ((colourMap & FOREGROUND_RED) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_RED;
                                    }
                                    break;
                                case 31:
                                    if ((colourMap & FOREGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_BLUE;
                                    }
                                    if ((colourMap & FOREGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_GREEN;
                                    }
                                    colourMap |= FOREGROUND_RED;
                                    break;
                                case 32:
                                    if ((colourMap & FOREGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_BLUE;
                                    }
                                    colourMap |= FOREGROUND_GREEN;
                                    if ((colourMap & FOREGROUND_RED) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_RED;
                                    }
                                    break;
                                case 33:
                                    if ((colourMap & FOREGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_BLUE;
                                    }
                                    colourMap |= FOREGROUND_GREEN;
                                    colourMap |= FOREGROUND_RED;
                                    break;
                                case 34:
                                    colourMap |= FOREGROUND_BLUE;
                                    if ((colourMap & FOREGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_GREEN;
                                    }
                                    if ((colourMap & FOREGROUND_RED) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_RED;
                                    }
                                    break;
                                case 35:
                                    colourMap |= FOREGROUND_BLUE;
                                    if ((colourMap & FOREGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_GREEN;
                                    }
                                    colourMap |= FOREGROUND_RED;
                                    break;
                                case 36:
                                    colourMap |= FOREGROUND_BLUE;
                                    colourMap |= FOREGROUND_GREEN;
                                    if ((colourMap & FOREGROUND_RED) != 0)
                                    {
                                        colourMap &= ~FOREGROUND_RED;
                                    }
                                    break;
                                case 37:
                                    colourMap |= FOREGROUND_BLUE;
                                    colourMap |= FOREGROUND_GREEN;
                                    colourMap |= FOREGROUND_RED;
                                    break;
                                case 40:
                                    if ((colourMap & BACKGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_BLUE;
                                    }
                                    if ((colourMap & BACKGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_GREEN;
                                    }
                                    if ((colourMap & BACKGROUND_RED) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_RED;
                                    }
                                    break;
                                case 41:
                                    if ((colourMap & BACKGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_BLUE;
                                    }
                                    if ((colourMap & BACKGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_GREEN;
                                    }
                                    colourMap |= BACKGROUND_RED;
                                    break;
                                case 42:
                                    if ((colourMap & BACKGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_BLUE;
                                    }
                                    colourMap |= BACKGROUND_GREEN;
                                    if ((colourMap & BACKGROUND_RED) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_RED;
                                    }
                                    break;
                                case 43:
                                    if ((colourMap & BACKGROUND_BLUE) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_BLUE;
                                    }
                                    colourMap |= BACKGROUND_GREEN;
                                    colourMap |= BACKGROUND_RED;
                                    break;
                                case 44:
                                    colourMap |= BACKGROUND_BLUE;
                                    if ((colourMap & BACKGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_GREEN;
                                    }
                                    if ((colourMap & BACKGROUND_RED) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_RED;
                                    }
                                    break;
                                case 45:
                                    colourMap |= BACKGROUND_BLUE;
                                    if ((colourMap & BACKGROUND_GREEN) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_GREEN;
                                    }
                                    colourMap |= BACKGROUND_RED;
                                    break;
                                case 46:
                                    colourMap |= BACKGROUND_BLUE;
                                    colourMap |= BACKGROUND_GREEN;
                                    if ((colourMap & BACKGROUND_RED) != 0)
                                    {
                                        colourMap &= ~BACKGROUND_RED;
                                    }
                                    break;
                                case 47:
                                    colourMap |= BACKGROUND_BLUE;
                                    colourMap |= BACKGROUND_GREEN;
                                    colourMap |= BACKGROUND_RED;
                                    break;
                            }
                        }
                        switch (stream)
                        {
                            case STREAM.STDERR:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_OUTPUT_HANDLE);
                                    SetConsoleTextAttribute(nConsole, colourMap);
                                }
                                break;
                            case STREAM.STDOUT:
                                {
                                    IntPtr nConsole = GetStdHandle(STD_ERROR_HANDLE);
                                    SetConsoleTextAttribute(nConsole, colourMap);
                                }
                                break;
                        }
                        break;
                    case Action.DSR:
                        break;
                    case Action.SCP:
                        break;
                    case Action.RCP:
                        break;
                    case Action.DECTCEMHIDE:
                        break;
                    case Action.DECTCEMSHOW:
                        break;
                    case Action.NOTHING:
                        break;
                }
            }
            #endregion

            int GetConsoleTextAttribute(IntPtr nConsole)
            {
                CONSOLE_SCREEN_BUFFER_INFO sbi;
                GetConsoleScreenBufferInfo(nConsole, out sbi);
                return sbi.wAttributes;
            }
        }

        #region Public Static Functions
        /// <summary>
        /// Write out a string containing ANSI codes
        /// </summary>
        /// <param name="s">String to write out</param>
        /// <param name="stream">Which output stream to write to</param>
        /// <param name="newLine">trie if we are writing a new line</param>
        public static void WriteANSI(string s, STREAM stream, bool newLine)
        {
            List<string> al = new List<string>(); // list of characters and actions to write
            List<ANSIAction> alActions = new List<ANSIAction>(); // list of actions to take

            // parse the string
            string s1 = s;

            // while we find ANSI control strings
            while (s1.IndexOf("\x1b[") != -1)
            {
                ANSIAction aa; // an ANSI action

                // if it was at the start of the string
                if (s1.IndexOf("\x1b[") == 0)
                {
                    // decode the ANSI action
                    string s2 = s1.Substring(2);
                    aa = new ANSIAction(s2);

                    // add it to our actions
                    alActions.Add(aa);

                    // look for ANSI instruction
                    string s3;
                    if (s1.IndexOf("\x1b[", 1) == -1)
                    {
                        s3 = s1.Substring(aa.CodeSize);
                        s1 = string.Empty;
                    }
                    else
                    {
                        s3 = s1.Substring(aa.CodeSize, s1.IndexOf("\x1b[", 1) - aa.CodeSize);
                        s1 = s1.Substring(s1.IndexOf("\x1b[", 1));
                    }

                    // add the instruction
                    al.Add(s3);
                }
                else
                {
                    // get the non ANSI string
                    string s2 = s1.Substring(0, s1.IndexOf("\x1b["));

                    // add it to our actions
                    al.Add(s2);

                    // grab the ANSI action
                    if (al.Count == 1)
                    {
                        aa = new ANSIAction();
                    }
                    else
                    {
                        aa = new ANSIAction(s2);
                    }

                    // add it
                    alActions.Add(aa);

                    // move to the next part of the string
                    s1 = s1.Substring(s1.IndexOf("\x1b[") + aa.CodeSize);
                }
            }
            // add the rest
            al.Add(s1);
            if (al.Count == 1)
            {
                alActions.Add(new ANSIAction());
            }
            else
            {
                alActions.Add(new ANSIAction(s1));
            }

            // write out the characters and actions
            for (int i = 0; i < al.Count; i++)
            {
                ((ANSIAction)alActions[i]).Execute(stream);
                if (stream == STREAM.STDERR)
                {
                    Console.Error.Write(al[i]);
                }
                else
                {
                    Console.Out.Write(al[i]);
                }
            }

            // handle carriage return
            if (newLine)
            {
                if (stream == STREAM.STDERR)
                {
                    Console.Error.WriteLine();
                }
                else
                {
                    Console.Out.WriteLine();
                }
            }
        }
        #endregion

    }
}
