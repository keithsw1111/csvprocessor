// Code last tidied up September 4, 2010

using System;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace CSVProcessor
{
    /// <summary>
    /// This is the main program class.
    /// </summary>
    class CSVProcessor
    {
        #region Static Variables
        static TextWriter _twError = null; // stream to write errors to
        static bool _fForceSTDERR = false; // true if we force error messages to the screen
        static List<CSVJob> _jobs = new List<CSVJob>(); // list of jobs
        static Regex _removeANSI = new Regex("\\x1b\\[[0-9;]*.", RegexOptions.Singleline | RegexOptions.Compiled); // regex used to extract ANSI strings
        static string lockVar = ""; // multi threaded lock variable
        #endregion

        #region Static functions

        /// <summary>
        /// Display an error string
        /// </summary>
        /// <param name="s">Error string to display</param>
        public static void DisplayError(string s)
        {
            lock (lockVar)
            {
                // if we dont have a file or we are being forced write it to the screen
                if (_twError == null || _fForceSTDERR)
                {
                    ANSIHelper.WriteANSI(s, ANSIHelper.STREAM.STDERR, true);
                }

                string s1 = _removeANSI.Replace(s, "");

                // if we have a file write it to a file
                if (_twError != null)
                {
                    _twError.WriteLine(s1);
                }

                // Write it to the debugger
                Debug.WriteLine(s1);
            }
        }

        /// <summary>
        /// Displays the command line instructions
        /// </summary>
        static void DisplayUsage()
        {
            DisplayError(ANSIHelper.Information + "USAGE : " + ANSIHelper.Green + "CSVProcessor [-p:<paramater file>] [-e:<error file>]");
            DisplayError("        [-s] [-m] [-t] [-q] [-d] [-r] [-g] [-c] [-v<9>:<value>]");
            DisplayError("        [[input file 1] ... [input file n] |");
            DisplayError("        [directory 1] ... [directory n] | use stdin]" + ANSIHelper.Information);
            DisplayError("        " + ANSIHelper.Green + "-p" + ANSIHelper.Information + " - The parameter XML file to use. If not specified this defaults");
            DisplayError("             to CSVProcessor.XML in " + Environment.CurrentDirectory);
            DisplayError("        " + ANSIHelper.Green + "-e" + ANSIHelper.Information + " - The file to write error output to. If not specified this defaults");
            DisplayError("             to the screen (STDERR)");
            DisplayError("        " + ANSIHelper.Green + "-x" + ANSIHelper.Information + " - when specified with -e forces error output to the screen as well");
            DisplayError("             as the file.");
            DisplayError("        " + ANSIHelper.Green + "-v<9>" + ANSIHelper.Information + " - specifies a variable you can access via <SystemVariable>.");
            DisplayError("        " + ANSIHelper.Green + "-h" + ANSIHelper.Information + " - Display usage.");
            DisplayError("        " + ANSIHelper.Green + "-?" + ANSIHelper.Information + " - Display usage.");
            DisplayError("        " + ANSIHelper.Green + "-s" + ANSIHelper.Information + " - Dump supported OutputColumns, Filters and Totals.");
            DisplayError("        " + ANSIHelper.Green + "-m" + ANSIHelper.Information + " - Pauses before processing to allow performance monitoring to be setup.");
            DisplayError("        " + ANSIHelper.Green + "-r" + ANSIHelper.Information + " - Forces the re-registration of the performance counters.");
            DisplayError("        " + ANSIHelper.Green + "-q" + ANSIHelper.Information + " - Prompts to override <disable> on processes.");
            DisplayError("        " + ANSIHelper.Green + "-t" + ANSIHelper.Information + " - Traces the XML as it is parsed." + ANSIHelper.White);
            DisplayError("        " + ANSIHelper.Green + "-d" + ANSIHelper.Information + " - Suppresses the end of program debug halt when using the debug build." + ANSIHelper.White);
            DisplayError("        " + ANSIHelper.Green + "-g" + ANSIHelper.Information + " - Outputs a .dot file which can be visualised as a graph of the job being run." + ANSIHelper.White);
            DisplayError("        " + ANSIHelper.Green + "-c" + ANSIHelper.Information + " - Track performance using performance counters." + ANSIHelper.White);
        }

        /// <summary>
        /// Run the job
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Run(string[] args)
        {
            string parameterFile = "CSVProcessor.xml"; // name of the parameter file
            int rc = 0; // program return code
            bool tracexml = false; // trace xml flag
            List<string> files = new List<string>(); // files passed in on command line
            bool performancePause = false; // true if we are to pause before running so the user can set up performance monitoring
            bool reregisterPerformanceCounters = false; // true if we are to re-register performance counters
            bool promptOverrideDisable = false; // true if we are to prompt to override process enablement
            bool suppressdebugstop = false; // true if we are not to stop at the end of a debug run
            bool producedot = false; // dont produce a dot file by default
            bool usePerformanceCounters = false;

            Thread.CurrentThread.Name = "Main";

            // process command line arguments
            if (args.GetLength(0) > 0)
            {
                // for each file
                foreach (string parm in args)
                {
                    if (parm.ToLower() == "-x")
                    {
                        // force writing errors to the screen
                        _fForceSTDERR = true;
                    }
                    else if (parm.ToLower() == "-t")
                    {
                        // trace out the xml configuration
                        tracexml = true;
                    }
                    else if (parm.ToLower() == "-d")
                    {
                        suppressdebugstop = true;
                    }
                    else if (parm.ToLower() == "-g")
                    {
                        producedot = true;
                    }
                    else if (parm.Length >= 3 && parm.ToLower().Substring(0, 3) == "-p:")
                    {
                        // save the parameter file name
                        parameterFile = parm.Substring(3);
                    }
                    else if (parm.Length >= 3 && parm.ToLower().Substring(0, 3) == "-e:")
                    {
                        // file to write errors to
                        try
                        {
                            _twError = new StreamWriter(parm.Substring(3), false);
                        }
                        catch (Exception ex)
                        {
                            _twError = null;
                            DisplayError(ANSIHelper.Warning + "Error creating the error file: " + parm.Substring(3) + " : " + ex.Message + ANSIHelper.White);
                            DisplayError(ANSIHelper.Warning + "Continuing without creating file" + ANSIHelper.White);
                        }
                    }
                    else if (parm.ToLower() == "-h" || parm.ToLower() == "-?")
                    {
                        // Display the usage instructions
                        DisplayUsage();
                    }
                    else if (parm.ToLower() == "-s")
                    {
                        DisplayError(ANSIHelper.Information + "\n");
                        DisplayError("Dumping supported elements.");
                        DisplayError(ANSIHelper.Information + "\n");
                        foreach (string s in Column.SupportsCols())
                        {
                            DisplayError("Column::" + s);
                        }
                        DisplayError(ANSIHelper.Information + "\n");
                        foreach (string s in Condition.Supports())
                        {
                            DisplayError(s);
                        }
                        DisplayError(ANSIHelper.Information + "\n");
                        foreach (string s in Total.Supports())
                        {
                            DisplayError(s);
                        }
                        DisplayError(ANSIHelper.Normal + "\n");
                    }
                    else if (parm.ToLower() == "-m")
                    {
                        performancePause = true;
                    }
                    else if (parm.ToLower() == "-r")
                    {
                        reregisterPerformanceCounters = true;
                    }
                    else if (parm.ToLower() == "-q")
                    {
                        promptOverrideDisable = true;
                    }
                    else if (parm.ToLower() == "-c")
                    {
                        usePerformanceCounters = true;
                    }
                    else if (parm.Length >= 2 && parm.ToLower().Substring(0, 2) == "-v")
                    {
                        // save the variables
                        try
                        {
                            int var = Convert.ToInt32(parm.ToLower().Substring(2, 1));
                            systemvariable.SetVariable(var, parm.Substring(4));
                        }
                        catch
                        {
                            throw new ApplicationException("-v parameters must be in the form -v9:value");
                        }
                    }
                    else if (parm.Length >= 1 && parm.ToLower().Substring(0, 1) == "-")
                    {
                        // ignore unknown parameters
                    }
                    else
                    {
                        // must be a file or directory
                        files.Add(parm);
                    }
                }
            }

            if (usePerformanceCounters)
            {
                // Create the performance counters
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Job", "Queue Current Size", "", "Queue Current Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Job", "Queue Max Size", "", "Queue Maximum Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_NamedMessageQueue", "Queue Current Size", "", "Queue Current Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_NamedMessageQueue", "Queue Max Size", "", "Queue Maximum Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Process", "Queue Current Size", "", "Queue Current Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Process", "Queue Max Size", "", "Queue Maximum Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Output", "Queue Current Size", "", "Queue Current Size"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Output", "Queue Max Size", "", "Queue Maximum Size"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_Filter", "Filter Average Duration", "", "Average Time To Process A Filter"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_OutputColumns", "OutputColumns Average Duration", "", "Average Time To Process An Output Line"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_Totals", "Totals Average Duration", "", "Average Time To Process A Line for Totals"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_Column", "Column Average Duration", "", "Average Time To Process A Column"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_Condition", "Condition Average Duration", "", "Average Time To Process A Condition"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_TotalAccumulator", "Total Accumulator Average Duration", "", "Average Time To Process A Total Accumulator"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Filter", "Filtered Out %", "", "Proportion of rows filtered out"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Job", "Input Lines", "", "Lines read from input"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_OutputColumns", "Output Lines", "", "Lines sent to output"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Filter", "Filtered In Lines", "", "Lines filtered in"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Totals", "Total Lines", "", "Lines sent to totals processing");
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_Totals", "Total Groups", "", "Distinct row/columns we are accumulating totals for"); // done
                Performance.CreateCounterDefinitionNumberOfItems("CSVProcessor_TagValues", "Tagged Values", "", "Tagged values found"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_TagValues", "TagValue Store Average Duration", "", "Average TagValue Processing Time"); // done
                Performance.CreateCounterDefinitionAverageTimer("CSVProcessor_TagValues", "TagValue Access Average Duration", "", "Average TagValue Access Time"); // done

                // if we have a performance file then create our performance monitor
                Performance.DoCreateCounterDefinitions(reregisterPerformanceCounters);
            }
            else
            {
                Performance.SuppressPerformanceCounters();
            }

            // read in the configuration document
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(parameterFile);
            }
            catch (Exception ex)
            {
                DisplayError(ANSIHelper.Error + "Error loading the configuration file : " + parameterFile + " : " + ex.Message + ANSIHelper.White);
                DisplayError(ANSIHelper.Error + "Stack Trace : " + ex.StackTrace + ANSIHelper.White);
                DisplayUsage();
                Debug.Fail("Stopping in debug mode so you can check the output.");
                return 4;
            }

            // if we are to let the user override disabling of processors
            if (promptOverrideDisable)
            {
                OverrideDisable.Prompt(doc);
            }

            // Create a timer to measure overall runtime
            Timer OverallTimer = new Timer();
            OverallTimer.Reset();

            try
            {
                // trace the xml structure
                if (tracexml)
                {
                    DisplayError("<CSVProcessor>");
                }

                // extract the jobs from the configuration file
                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "csvprocessor")
                        {
                            foreach (XmlNode n2 in n.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "job")
                                    {
                                        _jobs.Add(new CSVJob(n2, files, tracexml, ""));
                                    }
                                    else if (n2.Name.ToLower() == "constants")
                                    {
                                        Constants.LoadConstants(n2, null, null, tracexml, "");
                                    }
                                }
                            }
                        }
                    }
                }

                if (tracexml)
                {
                    DisplayError("</CSVProcessor>");
                }
            }
            catch (Exception ex)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + "CSVProcessor::Main problem loading config : " + ex.Message + ANSIHelper.White);
                Exception e = ex.InnerException;
                while (e != null)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "*** " + e.Message + ANSIHelper.White + "\n");
                    e = e.InnerException;
                }
                DisplayError(ANSIHelper.Error + "Stack Trace : " + ex.StackTrace + ANSIHelper.White);

                // break the debugger so user can check the output
                Debug.Fail("Stopping in debug mode so you can check the output. rc = " + rc.ToString());

                return 3;
            }

            if (producedot && _jobs.Count != 0)
            {
                StreamWriter dotWriter = new StreamWriter(Path.GetFileNameWithoutExtension(parameterFile) + ".dot", false, System.Text.Encoding.ASCII);

                foreach (CSVJob j in _jobs)
                {
                    j.WriteDot(dotWriter, parameterFile);
                }
                dotWriter.Close();
                dotWriter = null;
            }

            // if we are to pause before before running so user can turn on performance monitoring
            if (performancePause)
            {
                DisplayError(ANSIHelper.Warning + "Set up your performance monitoring then press ENTER to continue." + ANSIHelper.White);
                Console.ReadLine();
            }

            if (_jobs.Count == 0)
            {
                DisplayError(ANSIHelper.Error + "No jobs found in " + parameterFile + ANSIHelper.White);
            }
            else
            {
                // start all the jobs
                foreach (CSVJob j in _jobs)
                {
                    j.Start();
                }

                // wait for all the jobs to finish
                foreach (CSVJob j in _jobs)
                {
                    j.Wait();
                    rc += j.RC;
                }

                // Close off the performance counters
                Performance.Close();
            }

            // display total runtime
            DisplayError(ANSIHelper.Timing + "CSVProcessor completed in " + OverallTimer.DurationAsString + ANSIHelper.White);

            // display the return result
            DisplayError(ANSIHelper.Information + "Return code " + rc.ToString() + ANSIHelper.White);

            // if we opened an error file
            if (_twError != null)
            {
                // close it
                _twError.Close();
                _twError = null;
            }

            if (!suppressdebugstop)
            {
                // break the debugger so user can check the output
                Debug.Fail("Stopping in debug mode so you can check the output. rc = " + rc.ToString());
            }

            return rc;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            return Run(args);
        }
        #endregion
    }
}
