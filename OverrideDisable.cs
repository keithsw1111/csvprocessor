﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;

namespace CSVProcessor
{
    public partial class OverrideDisable : Form
    {
        static Dictionary<string, bool> _table = null;

        public static int IsEnabled(string id)
        {
            if (_table != null && _table.ContainsKey(id.ToLower()))
            {
                if (_table[id.ToLower()])
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return -1;
            }
        }

        static public void Prompt(XmlDocument doc)
        {
            OverrideDisable od = new OverrideDisable(doc);
            od.ShowDialog();

            _table = od.Results;
        }

        public Dictionary<string, bool> Results
        {
            get
            {
                Dictionary<string, bool> r = new Dictionary<string, bool>();

                foreach (TreeNode tnj in treeView1.Nodes)
                {
                    foreach (TreeNode tnp in tnj.Nodes)
                    {
                        r.Add(tnp.Text.ToLower(), tnp.Checked);
                    }
                }

                return r;
            }
        }

        public OverrideDisable(XmlDocument doc)
        {
            InitializeComponent();

            foreach (XmlNode nCSVP in doc.ChildNodes)
            {
                if (nCSVP.Name.ToLower() == "csvprocessor")
                {
                    foreach (XmlNode n in nCSVP.ChildNodes)
                    {
                        if (n.Name.ToLower() == "job")
                        {
                            TreeNode tn = null;
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.Name.ToLower() == "id")
                                {
                                    tn = treeView1.Nodes.Add(n1.InnerText);
                                }
                            }

                            if (tn == null)
                            {
                                tn = treeView1.Nodes.Add("Job");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.Name.ToLower() == "process")
                                {
                                    TreeNode tnProcess = null;
                                    bool enabled = true;
                                    foreach (XmlNode n2 in n1.ChildNodes)
                                    {
                                        if (n2.Name.ToLower() == "id")
                                        {
                                            tnProcess = tn.Nodes.Add(n2.InnerText);
                                        }
                                        else if (n2.Name.ToLower() == "processparameters")
                                        {
                                            foreach (XmlNode n3 in n2.ChildNodes)
                                            {
                                                if (n3.Name.ToLower() == "disable" || n3.Name.ToLower() == "disabled")
                                                {
                                                    enabled = false;
                                                }
                                            }
                                        }
                                    }

                                    if (tnProcess == null)
                                    {
                                        tnProcess = tn.Nodes.Add("Process");
                                    }

                                    tn.ExpandAll();
                                    tnProcess.Checked = enabled;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void OverrideDisable_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        void SelectAll(TreeNode n, bool check)
        {
            foreach (TreeNode tn in n.Nodes)
            {
                tn.Checked = check;
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TreeNode tn in treeView1.Nodes)
            {
                SelectAll(tn, true);
            }
        }

        private void deselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (TreeNode tn in treeView1.Nodes)
            {
                SelectAll(tn, false);
            }
        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent == null)
            {
                SelectAll(e.Node, e.Node.Checked);
            }
        }
    }
}
