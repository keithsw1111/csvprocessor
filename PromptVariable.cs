using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CSVProcessor
{
	/// <summary>
	/// Summary description for PromptVariable.
	/// </summary>
	public class PromptVariable : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		Regex _regexValidate = null;

        public string DotName
        {
            get
            {
                return "PromptVariable_" + Name;
            }
        }
		public string EnteredValue
		{
			get
			{
				return textBox1.Text;
			}
		}

		public PromptVariable(string message, Regex regexValidate, string defaultValue, bool mask)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			label1.Text = message;
			_regexValidate = regexValidate;
			textBox1.Text = defaultValue;
			if (mask)
			{
				textBox1.PasswordChar = '*';
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(8, 72);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(440, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(440, 56);
			this.label1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(376, 96);
			this.button1.Name = "button1";
			this.button1.TabIndex = 2;
			this.button1.Text = "&Ok";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// PromptVariable
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(456, 125);
			this.ControlBox = false;
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.Name = "PromptVariable";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "Prompt Variable";
			this.Load += new System.EventHandler(this.PromptVariable_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
			ValidateWindow();
		}

		private void PromptVariable_Load(object sender, System.EventArgs e)
		{
			ValidateWindow();
		}

		void ValidateWindow()
		{
			if (_regexValidate == null)
			{
				button1.Enabled = true;
			}
			else
			{
				if (_regexValidate.IsMatch(textBox1.Text))
				{
					button1.Enabled = true;
				}
				else
				{
					button1.Enabled = false;
				}
			}
		}
	}
}
