//#define Framework35
#define Framework4

using System;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using Queues;
#if Framework4
using System.Windows.Forms.DataVisualization.Charting;
#endif
using System.Drawing.Imaging;

namespace CSVProcessor
{
    #region Base Class
    /// <summary>
    /// Base class for output writers.
    /// </summary>
    public abstract class Output
    {
        #region Member Variables
        protected long _lWriteCount = 0;          // count the records written
        protected BlockingQueue _mq = null;       // queue to use for multithreaded running
        int _queueLength = 0;                     // length of the queue
        Thread _thread = null;                    // thread to run the output writer on
        protected CSVProcess _p = null;           // the process that owns this output
        protected string _id = string.Empty;      // the id of the output
        protected OutputFormatter _outputFormatter = null;
        protected OutputValidator _outputValidator = null;
        int _unique = UniqueIdClass.Allocate();
        int _rc = 0;
        string _triggerFile = string.Empty;
        #endregion

        public int RC
        {
            get
            {
                return _rc;
            }
        }

        public string UniqueId
        {
            get
            {
                return "O" + _unique.ToString();
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        #region Accessors
        /// <summary>
        /// Get the number of records written so far
        /// </summary>
        public long WriteCount
        {
            get
            {
                return _lWriteCount;
            }
        }
        /// <summary>
        /// Get the id of the outpur
        /// </summary>
        public string Id
        {
            get
            {
                if (_id == string.Empty)
                {
                    return Name;
                }
                else
                {
                    return _id;
                }
            }
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Close the output
        /// </summary>
        protected virtual void Close()
        {
            if (FormatterRequired)
            {
                if (_outputFormatter.Postamble() != string.Empty)
                {
                    // have to write synchronously
                    WriteLine(_outputFormatter.Postamble(), false);
                }
            }

            if (_outputValidator != null)
            {
                _rc = _outputValidator.Process(this);
            }
        }

        /// <summary>
        /// Write a string to the output file
        /// </summary>
        /// <param name="s"></param>
        protected abstract void WriteLine(string s, bool data);

        /// <summary>
        /// Open the output
        /// </summary>
        protected virtual void Open()
        {
            if (FormatterRequired)
            {
                if (_outputFormatter.Preamble() != string.Empty)
                {
                    Write(_outputFormatter.Preamble(), false);
                }
            }
        }

        /// <summary>
        /// Get the name of the file
        /// </summary>
        public abstract string Name
        {
            get;
        }

        public static bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "id":
                    return true;
                default:
                    return false;
            }
        }

        // override to specify if the output should use a formatter.
        protected virtual bool FormatterRequired
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Messaging Functions
        /// <summary>
        /// Close the output file
        /// </summary>
        public void CloseOutput()
        {
            Debug.WriteLine("Output {" + Id + "} sending stop");

            // Create a message
            CSVMessage m = new CSVMessage(CSVMessage.STATUS.STOP);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);

            Wait();
        }

        /// <summary>
        /// Write a string to the output file
        /// </summary>
        /// <param name="s">String to write</param>
        public void Write(string s, bool data)
        {
            // Create a message
            CSVMessage m = new CSVMessage(s, data, CSVMessage.STATUS.LINE);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Wait for the processing thread to catch up to this point
        /// </summary>
        public void Synch()
        {
            // if this processor is disabled then there is nothing we need to do
            if (_p.ProcessParameters.Disabled)
            {
                return;
            }

            Debug.WriteLine("Output {" + Id + "} sending synch");

            // create a synch event
            ManualResetEvent mre = new ManualResetEvent(true);
            mre.Reset();

            // create a message to send
            CSVMessage m = new CSVMessage(mre);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);

            while (!_mq.Aborted && !mre.WaitOne(1000) && _thread != null && _thread.IsAlive)
            {
                // wait for the event or an abort
            }

            Debug.WriteLine("Output {" + Id + "} synch done");
        }
        #endregion

        #region Thread Functions
        /// <summary>
        /// Start the job thread
        /// </summary>
        public void Start()
        {
            // Create the thread
            _thread = new Thread(new ThreadStart(Run));

            // give the thread a name
            _thread.Name = "Output:" + Name;

            // start it
            _thread.Start();

            // Sleep to give it a chance to run
            Thread.Sleep(0);
        }

        /// <summary>
        /// Wait for a job to finish processing
        /// </summary>
        public void Wait()
        {
            Debug.WriteLine("Waiting for output {" + Id + "} to end.");

            // wait for the thread to finish
            if (_thread != null)
            {
                _thread.Join();
            }

            Debug.WriteLine("Output {" + Id + "} ended " + Name);
        }

        /// <summary>
        /// Job thread function
        /// </summary>
        void Run()
        {
            Debug.WriteLine("Ouput {" + Id + "} starting to run.");

            // Create a queue
            _mq = new BlockingQueue("CSVProcessor_Output", Id, _queueLength);

            bool fContinue = true; // continue flag

            Open();

            // loop processing messages
            do
            {
                // get the next message
                CSVMessage msg = (CSVMessage)_mq.Dequeue();

                switch (msg.Status)
                {
                    case CSVMessage.STATUS.LINE:

                        WriteLine(msg.String, msg.Bool);
                        break;

                    case CSVMessage.STATUS.STOP:
                        Debug.WriteLine("Output {" + Id + "} received stop ... closing.");

                        Close();

                        // stop processing messages
                        fContinue = false;
                        break;

                    case CSVMessage.STATUS.SYNCH:
                        Debug.WriteLine("Output {" + Id + "} received synch.");

                        // let the sending thread know we processed the synch message
                        msg.Synch.Set();
                        break;
                }
            } while (fContinue); // loop forever

            Debug.WriteLine("Ouput {" + Id + "} ending.");

            if (_triggerFile != string.Empty)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Output {" + Id + "} Creating trigger file: " + _triggerFile + ANSIHelper.White);

                FileStream fs = File.Create(_triggerFile);
                fs.Close();
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create an output
        /// </summary>
        /// <param name="n">XML node defining this output</param>
        /// <param name="tracexml">true if we are to trace out the configuration</param>
        /// <param name="prefix">indent for the tracexml</param>
        /// <param name="p">The owning CSVProcess</param>
        public Output(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
        {
            _p = p; // save the process
            _queueLength = QueueLength; // save the queue length

            // read out the configuration
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    bool found = false;

                    object[] o = { n3.Name.ToLower() };
                    // walk the type hierachy. from here up asking each class what values they support
                    Type t = this.GetType();
                    while (!found && t != typeof(Output))
                    {
                        MethodInfo[] mi = t.GetMethods();
                        foreach (MethodInfo m in mi)
                        {
                            if (m.Name == "Supports" && m.IsStatic)
                            {
                                try
                                {
                                    if ((bool)m.Invoke(null, o))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                catch (System.Exception ex)
                                {
                                    throw new ApplicationException("Error checking support for parameter " + n3.Name + " on object " + t.Name, ex);
                                }
                            }
                        }

                        t = t.BaseType;
                    }

                    if (n3.Name.ToLower() == "id")
                    {
                        _id = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n3.Name.ToLower() == "triggerfile")
                    {
                        _triggerFile = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TriggerFile>" + _triggerFile + "</TriggerFile>");
                        }
                    }
                    else if (!found)
                    {
                        if (FormatterRequired)
                        {
                            if (_outputFormatter != null)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Warning + "Output::Constructor only one formatter can be specified : " + n3.Name + ANSIHelper.White);
                            }
                            else
                            {
                                _outputFormatter = OutputFormatter.Create(j, p, n3, tracexml, prefix);
                            }
                        }
                        else
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Output::Constructor Ignoring xml node as output formatter is not suuported : " + n3.Name + ANSIHelper.White);
                        }
                    }
                }
            }

            if (FormatterRequired && _outputFormatter == null)
            {
                _outputFormatter = OutputFormatter.CreateDefault(j, p);
            }
        }
        #endregion

        #region Public Methods

        public void AddValidator(OutputValidator ov)
        {
            _outputValidator = ov;
        }
        /// <summary>
        /// Write a column line to the output file
        /// </summary>
        /// <param name="l">column line</param>
        public void Write(ColumnLine l)
        {
            if (FormatterRequired)
            {
                if (l.Data)
                {
                    // convert it to a string and write it
                    Write(_outputFormatter.FormatOutput(l), l.Data);
                }
                else
                {
                    Write(_outputFormatter.FormatHeader(l), l.Data);
                }
            }
            else
            {
                Write(l.ToString(), l.Data);
            }
        }
        #endregion

        #region Static Public Functions
        /// <summary>
        /// Create the outputs
        /// </summary>
        /// <param name="n">XmlNode containing the outputs</param>
        /// <param name="tracexml">true if we are to trace out the configuration</param>
        /// <param name="prefix">indent for the xml trace</param>
        /// <returns>A collection of outputs</returns>
        public static List<Output> Create(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Output>");
            }

            List<Output> outputs = new List<Output>(); // The output collection to return

            XmlNode ov = null;

            // look at each sub node
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "outputvalidate")
                    {
                        ov = n1;
                    }
                    else
                    {
                        // using the name of the node try to create a class name
                        string classname = "CSVProcessor." + n1.Name.ToLower();

                        // trace out the node name
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <" + n1.Name + ">");
                        }

                        // get the type we want to create. If the class does not exist in this assembly then this will fail
                        Type t = Type.GetType(classname);

                        if (t == null)
                        {
                            throw new ApplicationException("Output::Create - could not find type " + classname);
                        }

                        try
                        {
                            // now construct the object passing in our xml node
                            outputs.Add((Output)Activator.CreateInstance(t, new object[] { n1, j, tracexml, prefix + "  ", p, QueueLength }));
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Output::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </" + n1.Name + ">");
                        }
                    }
                }
            }

            if (ov != null && outputs.Count > 0)
            {
                outputs[0].AddValidator(new OutputValidator(ov, j, tracexml, prefix + "   ", outputs[0]));
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Output>");
            }

            // return our collection
            return outputs;
        }
        #endregion
    }
    #endregion

    #region Outputs
    /// <summary>
    /// This output writes to STDOUT
    /// </summary>
    public class stdoutput : Output
    {

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"STDOUTPUT\"];");
        }

        #region Constructors
        /// <summary>
        /// Create a STDOUT writer
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public stdoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                default:
                    return Output.Supports(s);
            }
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to STDOUT
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (data)
            {
                // increment our line count
                _lWriteCount++;
            }

            // write to the screen
            Console.Out.WriteLine(s);
        }

        /// <summary>
        /// Get the name of the stream
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return "STDOUT";
                }
            }
        }

        /// <summary>
        /// Close the stream
        /// </summary>
        protected override void Close()
        {
            base.Close();
        }

        protected override void Open()
        {
            base.Open();
        }
        #endregion
    }

    /// <summary>
    /// This output is used to write output to a queue read by another job
    /// </summary>
    public class queueoutput : Output
    {
        #region Member Variables
        string _name = string.Empty; // name of the queue
        NamedCSVMessageQueue _nmq = null; // the queue
        List<int> _threads = new List<int>(); // list of threads who are sending us data
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _name + "\"];");
        }

        #region Constructors
        /// <summary>
        /// Create a QueueOutput writer
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public queueoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "name")
                    {
                        _name = n3.InnerText.ToLower();
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Name>" + _name.ToString() + "</Name>");
                        }

                        _name = "Queue::" + _name;
                    }
                }
            }

            // we must have a name
            if (Name == string.Empty)
            {
                throw new ApplicationException("QueueOutput must have a name.");
            }

            // add us to the list of the available queues
            _nmq = NamedCSVMessageQueue.GetQueue(_name, QueueLength);
            Debug.WriteLine("Output queue found named " + _nmq.Name);
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "name":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to STDOUT
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (!_threads.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                _threads.Add(Thread.CurrentThread.ManagedThreadId);
            }

            if (data)
            {
                // increment our line count
                _lWriteCount++;
            }

            // Create a message
            CSVMessage m = new CSVMessage(s, CSVMessage.STATUS.LINE);

            // Enqueue the message
            _nmq.Queue.Enqueue(m);
            //Debug.WriteLine("Enqueued message on queue {"+Id+"} " + _nmq.Queue.Count.ToString() + " left.");
        }

        /// <summary>
        /// Get the name of the stream
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _name;
                }
            }
        }

        /// <summary>
        /// Close the stream
        /// </summary>
        protected override void Close()
        {
            Debug.WriteLine("OutputQueue {" + Id + "} closing.");

            base.Close();

            // give other threads that might want to write to this queue a chance to do so because if they dont write soon we are going to destroy the queue
            Thread.Sleep(100);

            _threads.Remove(Thread.CurrentThread.ManagedThreadId);

            if (_threads.Count == 0)
            {
                Debug.WriteLine("OutputQueue {" + Id + "} all outputters done so sending STOP.");

                // Create a message
                CSVMessage m = new CSVMessage(CSVMessage.STATUS.STOP);

                // Enqueue the message
                _nmq.Queue.Enqueue(m);
            }
        }

        protected override void Open()
        {
            base.Open();
        }
        #endregion
    }

    /// <summary>
    /// Write the results as events in the windows event log
    /// </summary>
    public class eventlogoutput : Output
    {
        #region Member Variables
        EventLog _eventLog = null; // the event log to write to
        string _logName = string.Empty; // the name of the event log
        EventLogEntryType _eventType = EventLogEntryType.Information; // the event type
        int _eventID = 0; // the event id
        short _category = 0; // the event category
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _logName + "\"];");
        }


        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        /// <param name="QueueLength"></param>
        public eventlogoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "log")
                    {
                        // save the flush frequency
                        _logName = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Log>" + _logName + "</Log>");
                        }
                    }
                    else if (n3.Name.ToLower() == "type")
                    {
                        // save the flush frequency
                        switch (n3.InnerText.ToLower())
                        {
                            case "error":
                                _eventType = EventLogEntryType.Error;
                                break;
                            case "failureaudit":
                                _eventType = EventLogEntryType.FailureAudit;
                                break;
                            case "information":
                                _eventType = EventLogEntryType.Information;
                                break;
                            case "successaudit":
                                _eventType = EventLogEntryType.SuccessAudit;
                                break;
                            case "warning":
                                _eventType = EventLogEntryType.Warning;
                                break;
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Type>" + _eventType.ToString() + "</Type>");
                        }
                    }
                    else if (n3.Name.ToLower() == "eventid")
                    {
                        // save the flush frequency
                        _eventID = Convert.ToInt32(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EventID>" + _eventID.ToString() + "</EventID>");
                        }
                    }
                    else if (n3.Name.ToLower() == "category")
                    {
                        // save the flush frequency
                        _category = Convert.ToInt16(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Category>" + _category.ToString() + "</Category>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "log":
                case "type":
                case "eventid":
                case "category":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Write the error
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (_eventLog != null)
            {
                _eventLog.WriteEntry(s, _eventType, _eventID, _category, null);
            }
        }

        /// <summary>
        /// Close the event log
        /// </summary>
        protected override void Close()
        {
            base.Close();

            if (_eventLog != null)
            {
                _eventLog.Close();
                _eventLog = null;
            }
        }

        /// <summary>
        /// Open the event log
        /// </summary>
        protected override void Open()
        {
            try
            {
                _eventLog = new EventLog(_logName);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error opening event log : " + Name, ex);
            }

            base.Open();
        }

        /// <summary>
        /// Get the name of the event log
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _logName;
                }
            }
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }
        #endregion
    }

    /// <summary>
    /// Write output to a file
    /// </summary>
    public class slicedfileoutput : Output
    {
        #region Member Variables
        protected string _outputfilePre = string.Empty; // The name of the output file
        protected string _outputfilePost = string.Empty; // The name of the output file
        int _flushfrequency = 0; // records between flushes
        protected Dictionary<string, TextWriter> _twFiles = new Dictionary<string, TextWriter>(); // the file
        List<ColumnNumber> _splitcolumns = new List<ColumnNumber>();
        List<string> _headers = new List<string>();
        bool _suppressSliceColumn = false;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfilePre + "*" + _outputfilePost + "\"];");
        }

        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public slicedfileoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "flushfrequency")
                    {
                        // save the flush frequency
                        _flushfrequency = Convert.ToInt32(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FlushFrequency>" + _flushfrequency.ToString() + "</FlushFrequency>");
                        }
                    }
                    else if (n3.Name.ToLower() == "suppresssplitcolumn")
                    {
                        // save the flush frequency
                        _suppressSliceColumn = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SuppressSplitColumn/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "splitfileon")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SplitFile>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "keycolumn")
                                {
                                    ColumnNumber col = new ColumnNumber(_p.Job, n3.InnerText);
                                    _splitcolumns.Add(col);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    <Column>" + col.ToString() + "</Column>");
                                    }
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </SplitFile>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutputFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        CSVFileInfo fi = new CSVFileInfo(p.Input.Name);
                        CSVFileLine csv = new CSVFileLine("", 1, fi);
                        string outputfile = sc.Value(csv);

                        if (Path.GetDirectoryName(outputfile) == "")
                        {
                            _outputfilePre = Path.GetFileNameWithoutExtension(outputfile);
                        }
                        else
                        {
                            _outputfilePre = Path.GetDirectoryName(outputfile) + "\\" + Path.GetFileNameWithoutExtension(outputfile);
                        }

                        _outputfilePost = Path.GetExtension(outputfile);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </OutputFile>");
                        }
                    }
                }
            }

            // if no file name was specified
            if (_outputfilePre == string.Empty)
            {
                throw new ApplicationException("No filename specified.");
            }
        }
        #endregion

        #region Private Functions
        protected TextWriter CreateSplitFile(string key)
        {
            string fn = _outputfilePre + "_" + key + _outputfilePost;

            CSVProcessor.DisplayError(ANSIHelper.Information + "SlicedFileOutput::Creating Split File " + fn + " for key '" + key + "'" + ANSIHelper.White);

            int tries = 0;
            while (!_twFiles.ContainsKey(key))
            {
                try
                {
                    _twFiles[key] = new StreamWriter(fn, false);
                    if (tries > 0)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Information + "SlicedFileOutput::OpenFile " + fn + " successfully opened." + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("used by another process"))
                    {
                        // display error every 5 seconds
                        if (tries % 50 == 0)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Error + "SlicedFileOutput::OpenFile Error opening " + fn + " because it is in use by another process. Close it to continue." + ANSIHelper.White);
                        }
                        tries++;
                        Thread.Sleep(100);
                    }
                    else
                    {
                        throw new ApplicationException("Error opening output file.", ex);
                    }
                }
            }

            foreach (string s in _headers)
            {
                // write the line
                _twFiles[key].WriteLine(s);
            }
            _twFiles[key].Flush();

            CSVProcessor.DisplayError(ANSIHelper.Information + "SlicedFileOutput::Split file created: " + fn + ANSIHelper.White);

            return _twFiles[key];
        }

        protected virtual void OpenFile()
        {
            // do nothing ... we open them when we need them
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "flushfrequency":
                case "outputfile":
                case "splitfileon":
                case "suppresssplitcolumn":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _outputfilePre + "_*" + _outputfilePost;
                }
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();

            foreach (TextWriter tw in _twFiles.Values)
            {
                // close it and forget it
                tw.Close();
            }
            _twFiles.Clear();
        }

        protected override void Open()
        {
            // nothing to do here
            base.Open();
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            string key = string.Empty;

            if (_splitcolumns.Count != 0)
            {
                CSVFileLine fl = new CSVFileLine(s, 0);
                fl.Parse(_outputFormatter.Delimiter, _outputFormatter.Quote, _outputFormatter.QuoteEscape);

                foreach (ColumnNumber cn in _splitcolumns)
                {
                    key += fl[cn];
                }
            }
            else
            {
                key = "ALL";
            }

            if (data)
            {
                TextWriter tw = null;
                try
                {
                    tw = _twFiles[key];
                }
                catch (Exception)
                {
                    tw = CreateSplitFile(key);
                }

                // increment our record count
                _lWriteCount++;

                if (_suppressSliceColumn)
                {
                    CSVFileLine fl = new CSVFileLine(s, 0);
                    fl.Parse(_outputFormatter.Delimiter, _outputFormatter.Quote, _outputFormatter.QuoteEscape);
                    var sc = _splitcolumns;
                    sc.Sort();
                    sc.Reverse();
                    foreach (ColumnNumber cn in sc)
                    {
                        fl.RemoveColumn(cn);
                    }
                    s = fl.ToString();
                }

                tw.WriteLine(s);

                // flush if required
                if (_flushfrequency > 0)
                {
                    if (_lWriteCount % _flushfrequency == 0)
                    {
                        tw.Flush();
                    }
                }
            }
            else
            {
                // save non data rows at start of the file as headers repeated in split files
                if (_lWriteCount == 0)
                {
                    _headers.Add(s);
                }
                else
                {
                    foreach (TextWriter t in _twFiles.Values)
                    {
                        t.WriteLine(s);
                        t.Flush();
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Write output to a file
    /// </summary>
    public class splitfileoutput : Output
    {
        #region Member Variables
        protected string _outputfilePre = string.Empty; // The name of the output file
        protected string _outputfilePost = string.Empty; // The name of the output file
        int _flushfrequency = 0; // records between flushes
        int _current = 0;
        int _splitCount = 10000;
        TextWriter _tw = null;
        List<string> _headers = new List<string>();
        StringOrColumn _lastLine = null;
        StringOrColumn _firstLine = null;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfilePre + "*" + _outputfilePost + "\"];");
        }

        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public splitfileoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "flushfrequency")
                    {
                        // save the flush frequency
                        _flushfrequency = Convert.ToInt32(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FlushFrequency>" + _flushfrequency.ToString() + "</FlushFrequency>");
                        }
                    }
                    else if (n3.Name.ToLower() == "splitcount")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SplitCount>");
                        }
                        _splitCount = Convert.ToInt32(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </SplitCount>");
                        }
                    }
                    else if (n3.Name.ToLower() == "givenlastline")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <GivenLastLine>");
                        }
                        _lastLine = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </GivenLastLine>");
                        }
                    }
                    else if (n3.Name.ToLower() == "givenfirstline")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <GivenFirstLine>");
                        }
                        _firstLine = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </GivenFirstLine>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutputFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        CSVFileInfo fi = new CSVFileInfo(p.Input.Name);
                        CSVFileLine csv = new CSVFileLine("", 1, fi);
                        string outputfile = sc.Value(csv);

                        if (Path.GetDirectoryName(outputfile) == "")
                        {
                            _outputfilePre = Path.GetFileNameWithoutExtension(outputfile);
                        }
                        else
                        {
                            _outputfilePre = Path.GetDirectoryName(outputfile) + "\\" + Path.GetFileNameWithoutExtension(outputfile);
                        }
                        _outputfilePost = Path.GetExtension(outputfile);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </OutputFile>");
                        }
                    }
                }
            }

            // if no file name was specified
            if (_outputfilePre == string.Empty)
            {
                throw new ApplicationException("No filename specified.");
            }
        }
        #endregion

        #region Private Functions
        protected TextWriter CreateSplitFile(int current, string line)
        {
            string fn = _outputfilePre + "_" + current.ToString("000") + _outputfilePost;
            StreamWriter sw = null;
            int tries = 0;
            while (sw == null)
            {
                try
                {
                    sw = new StreamWriter(fn, false);
                    if (tries > 0)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Information + "FileOutput::OpenFile " + fn + " successfully opened." + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("used by another process"))
                    {
                        // display error every 5 seconds
                        if (tries % 50 == 0)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Error + "FileOutput::OpenFile Error opening " + fn + " because it is in use by another process. Close it to continue." + ANSIHelper.White);
                        }
                        tries++;
                        Thread.Sleep(100);
                    }
                    else
                    {
                        throw new ApplicationException("Error opening output file.", ex);
                    }
                }
            }

            if (_firstLine != null)
            {
                ColumnLine csv = new ColumnLine();
                csv.AddEnd(line);
                sw.WriteLine(_firstLine.Value(csv));
            }

            foreach (string s in _headers)
            {
                // write the line
                sw.WriteLine(s);
            }
            sw.Flush();

            return sw;
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "flushfrequency":
                case "outputfile":
                case "splitcount":
                case "givenlastline":
                case "givenfirstline":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _outputfilePre + "_*" + _outputfilePost;
                }
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();

            if (_tw != null)
            {
                if (_lastLine != null)
                {
                    _tw.WriteLine(_lastLine.Value());
                }

                // close it and forget it
                _tw.Close();
                _tw = null;
            }
        }

        protected override void Open()
        {
            // nothing to do here
            base.Open();
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (data)
            {
                if (_tw == null)
                {
                    _tw = CreateSplitFile(_current, s);
                    _lWriteCount = 0;
                }

                if (_lWriteCount == _splitCount)
                {
                    Close();
                    _current++;
                    _tw = CreateSplitFile(_current, s);
                    _lWriteCount = 0;
                }

                // increment our record count
                _lWriteCount++;

                _tw.WriteLine(s);

                // flush if required
                if (_flushfrequency > 0)
                {
                    if (_lWriteCount % _flushfrequency == 0)
                    {
                        _tw.Flush();
                    }
                }
            }
            else
            {
                // save non data rows at start of the file as headers repeated in split files
                if (_lWriteCount == 0)
                {
                    _headers.Add(s);
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Write output to a file
    /// </summary>
    public class fileoutput : Output
    {
        #region Member Variables
        protected string _outputfile = string.Empty; // The name of the output file
        int _flushfrequency = 0; // records between flushes
        protected TextWriter _twFile = null; // the file
        bool _deleteemptyfile = false;
        bool _backupExistingFile = false; // true if we are to backup any existing file
        List<ColumnNumber> _splitcolumns = new List<ColumnNumber>();
        List<string> _headers = new List<string>();
        string _lastkey = null;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfile + "\"];");
        }


        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public fileoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "flushfrequency")
                    {
                        // save the flush frequency
                        _flushfrequency = Convert.ToInt32(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FlushFrequency>" + _flushfrequency.ToString() + "</FlushFrequency>");
                        }
                    }
                    else if (n3.Name.ToLower() == "backupexistingfile")
                    {
                        _backupExistingFile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <BackupExistingFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "deleteemptyoutputfile")
                    {
                        _deleteemptyfile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DeleteEmptyOutputFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "backupexistingfile")
                    {
                        _backupExistingFile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <BackupExistingFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "splitfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SplitFile>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "keycolumn")
                                {
                                    ColumnNumber col = new ColumnNumber(_p.Job, n3.InnerText);
                                    _splitcolumns.Add(col);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    <Column>" + col.ToString() + "</Column>");
                                    }
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </SplitFile>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutputFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        CSVFileInfo fi = new CSVFileInfo(p.Input.Name);
                        CSVFileLine csv = new CSVFileLine("", 1, fi);
                        _outputfile = sc.Value(csv);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </OutputFile>");
                        }
                    }
                }
            }

            if ((_deleteemptyfile || _backupExistingFile) && _splitcolumns.Count != 0)
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + "FileOutput::Constructor DeleteEmptyOutputFile and BackupExistingFile not supported with SplitFile. DeleteEmptyOutputFile ignored." + ANSIHelper.White);
                _deleteemptyfile = false;
            }

            // if no file name was specified
            if (_outputfile == string.Empty)
            {
                throw new ApplicationException("No filename specified.");
            }
            else
            {
                if (_backupExistingFile && File.Exists(_outputfile))
                {
                    string backupFile;
                    string backupAdornment = "_backup_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    int lastdot = _outputfile.LastIndexOf('.');

                    if (lastdot == -1)
                    {
                        backupFile = _outputfile + backupAdornment;
                    }
                    else
                    {
                        backupFile = _outputfile.Substring(0, lastdot) + backupAdornment + _outputfile.Substring(lastdot);
                    }

                    CSVProcessor.DisplayError(ANSIHelper.Information + "Backing up existing " + _outputfile + " to " + backupFile + ANSIHelper.White);

                    File.Copy(_outputfile, backupFile);
                }
            }
        }
        #endregion

        #region Private Functions
        protected void CycleFile(string key)
        {
            Close();
            OpenFile();

            foreach (string s in _headers)
            {
                // write the line
                _twFile.WriteLine(s);
            }
            _twFile.Flush();
        }

        protected virtual void OpenFile()
        {
            if (_twFile != null)
            {
                throw new ApplicationException("Opening a second output file without closing the first!");
            }

            int tries = 0;
            while (_twFile == null)
            {
                try
                {
                    // Create the output file
                    if (_splitcolumns.Count != 0)
                    {
                        if (_lastkey != null)
                        {
                            _twFile = new StreamWriter(_lastkey + "_" + _outputfile, false);
                        }
                    }
                    else
                    {
                        _twFile = new StreamWriter(_outputfile, false);
                    }
                    if (tries > 0)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Information + "FileOutput::OpenFile " + _outputfile + " successfully opened." + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("used by another process"))
                    {
                        // display error every 5 seconds
                        if (tries % 50 == 0)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Error + "FileOutput::OpenFile Error opening " + _outputfile + " because it is in use by another process. Close it to continue." + ANSIHelper.White);
                        }
                        tries++;
                        Thread.Sleep(100);
                    }
                    else
                    {
                        throw new ApplicationException("Error opening output file.", ex);
                    }
                }
            }
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "flushfrequency":
                case "outputfile":
                case "backupexistingfile":
                case "deleteemptyoutputfile":
                case "splitfile":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _outputfile;
                }
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();

            // if we have a file
            if (_twFile != null)
            {
                // close it and forget it
                _twFile.Close();
                _twFile = null;
            }

            if (_deleteemptyfile)
            {
                if (_lWriteCount == 0)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Information + "Deleting file " + _outputfile + " as no data was written to it." + ANSIHelper.White);
                    File.Delete(_outputfile);
                }
            }
        }

        protected override void Open()
        {
            try
            {
                OpenFile();
            }
            catch (IOException ex)
            {
                throw new ApplicationException("Error opening output file - check you dont have it open : " + _outputfile, ex);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error opening output file : " + _outputfile, ex);
            }

            base.Open();
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (data)
            {
                // increment our record count
                _lWriteCount++;

                if (_splitcolumns.Count != 0)
                {
                    string newkey = string.Empty;
                    CSVFileLine fl = new CSVFileLine(s, 0);
                    fl.Parse(_outputFormatter.Delimiter, _outputFormatter.Quote, _outputFormatter.QuoteEscape);

                    foreach (ColumnNumber cn in _splitcolumns)
                    {
                        newkey += fl[cn];
                    }

                    if (_lastkey == null || newkey != _lastkey)
                    {
                        _lastkey = newkey;
                        CycleFile(newkey);
                    }
                }
            }
            else
            {
                // save non data rows at start of the file as headers repeated in split files
                if (_lWriteCount == 0)
                {
                    _headers.Add(s);
                }
            }

            // make sure we have a file
            if (_twFile != null)
            {
                // write the line
                _twFile.WriteLine(s);

                // flush if required
                if (_flushfrequency > 0)
                {
                    if (_lWriteCount % _flushfrequency == 0)
                    {
                        _twFile.Flush();
                    }
                }
            }
            else
            {
                if (_splitcolumns.Count == 0 || _lWriteCount > 0)
                {
                    throw new ApplicationException("Error writing to FileOutput - no file is open");
                }
            }
        }
        #endregion
    }

    public class keyedoutputfile : Output
    {
        #region Member Variables
        protected string _outputfile = string.Empty; // The name of the output file
        bool _backupExistingFile = false; // true if we are to backup any existing file
        Dictionary<string, string> _lines = new Dictionary<string, string>(); // the lines we will write
        List<ColumnNumber> _keys = new List<ColumnNumber>(); // the columns that make up the keys
        bool _updateExistingFile = false; // true if we are to read and then update the existing file
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfile + "\"];");
        }

        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public keyedoutputfile(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "keys")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Keys>");
                        }
                        try
                        {
                            foreach (XmlNode n4 in n3.ChildNodes)
                            {
                                if (n4.NodeType != XmlNodeType.Comment)
                                {
                                    if (n4.Name.ToLower() == "key")
                                    {
                                        _keys.Add(new ColumnNumber(_p.Job, n4));
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("KeyedOutputFile::Constructor problem parsing key ", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Keys>");
                        }
                    }
                    else if (n3.Name.ToLower() == "backupexistingfile")
                    {
                        _backupExistingFile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <BackupExistingFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "updateexistingfile")
                    {
                        _updateExistingFile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <UpdateExistingFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutputFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        CSVFileInfo fi = new CSVFileInfo(p.Input.Name);
                        CSVFileLine csv = new CSVFileLine("", 1, fi);
                        _outputfile = sc.Value(csv);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </OutputFile>");
                        }
                    }
                }
            }

            // if no file name was specified
            if (_outputfile == string.Empty)
            {
                throw new ApplicationException("No filename specified.");
            }
            else
            {
                if (_backupExistingFile && File.Exists(_outputfile))
                {
                    string backupFile;
                    string backupAdornment = "_backup_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    int lastdot = _outputfile.LastIndexOf('.');

                    if (lastdot == -1)
                    {
                        backupFile = _outputfile + backupAdornment;
                    }
                    else
                    {
                        backupFile = _outputfile.Substring(0, lastdot) + backupAdornment + _outputfile.Substring(lastdot);
                    }

                    CSVProcessor.DisplayError(ANSIHelper.Information + "Backing up existing " + _outputfile + " to " + backupFile + ANSIHelper.White);

                    File.Copy(_outputfile, backupFile);
                }
            }
        }
        #endregion

        #region Private Functions
        void SaveLine(string l)
        {
            string key = string.Empty;
            CSVFileLine csv = new CSVFileLine(l, 0);
            csv.Parse(_outputFormatter.Delimiter, _outputFormatter.Quote, _outputFormatter.QuoteEscape);

            foreach (ColumnNumber cn in _keys)
            {
                key = key + CSVFileLine.CSVSafe(csv[cn], _outputFormatter.Quote, _outputFormatter.Delimiter, _outputFormatter.QuoteEscape) + _outputFormatter.Delimiter;
            }

            _lines[key] = l;
        }

        protected virtual void OpenFile()
        {
            if (_updateExistingFile)
            {
                // I need to read the file here and store it in _lines
                if (File.Exists(_outputfile))
                {
                    TextReader tr = new StreamReader(_outputfile);

                    string l = tr.ReadLine();

                    while (l != null)
                    {
                        // as each line is read I need to parse out the key
                        SaveLine(l);

                        l = tr.ReadLine();
                    }

                    tr.Close();
                }
            }
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "keys":
                case "outputfile":
                case "backupexistingfile":
                case "updateexistingfile":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _outputfile;
                }
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();

            // here i need to write all the records to the file
            TextWriter tw = new StreamWriter(_outputfile);

            foreach (KeyValuePair<string, string> kv in _lines)
            {
                tw.WriteLine(kv.Value);
            }

            tw.Close();
        }

        protected override void Open()
        {
            try
            {
                OpenFile();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error opening output file : " + _outputfile + " : " + ex.Message, ex);
            }

            base.Open();
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            SaveLine(s);
        }
        #endregion
    }

    public class emailoutput : Output
    {
        #region Member Variables
        string _smtpServer = string.Empty; // The server to send via
        string _toEmailAddress = string.Empty; // The person to send to
        string _fromEmailAddress = string.Empty; // The person to send to
        string _emailSubject = string.Empty; // the subject of the email
        TimeSpan _minimumElapsed = TimeSpan.MinValue; // minimum time between emails
        string _user = string.Empty; // the subject of the email
        string _password = string.Empty; // the subject of the email
        string _pending = string.Empty; // any pending content
        DateTime _lastSent = DateTime.MinValue; // when we last sent an email
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _toEmailAddress + "\"];");
        }

        #region Email Class
        // This code comes from CodeProject
        // http://www.codeproject.com/KB/IP/Email_Sending_in_C_.aspx
        // Florentin BADEA

        /// <summary>
        /// EnhancedMailMessage is a class that provides
        /// more features for email sending in .NET
        /// </summary>
        public class EnhancedMailMessage : MailMessage
        {
            #region Member Variables
            private string fromName;
            private string smtpServerName;
            private string smtpUserName;
            private string smtpUserPassword;
            private int smtpServerPort;
            private bool smtpSSL;
            #endregion

            #region Constructors
            public EnhancedMailMessage()
            {
                fromName = string.Empty;
                smtpServerName = string.Empty;
                smtpUserName = string.Empty;
                smtpUserPassword = string.Empty;
                smtpServerPort = 25;
                smtpSSL = false;
            }
            #endregion

            #region Accessors
            /// <summary>
            /// The display name that will appear
            /// in the recipient mail client
            /// </summary>
            public string FromName
            {
                set
                {
                    fromName = value;
                }
                get
                {
                    return fromName;
                }
            }

            /// <summary>
            /// SMTP server (name or IP address)
            /// </summary>
            public string SMTPServerName
            {
                set
                {
                    smtpServerName = value;
                }
                get
                {
                    return smtpServerName;
                }
            }

            /// <summary>
            /// Username needed for a SMTP server
            /// that requires authentication
            /// </summary>
            public string SMTPUserName
            {
                set
                {
                    smtpUserName = value;
                }
                get
                {
                    return smtpUserName;
                }
            }

            /// <summary>
            /// Password needed for a SMTP server
            /// that requires authentication
            /// </summary>
            public string SMTPUserPassword
            {
                set
                {
                    smtpUserPassword = value;
                }
                get
                {
                    return smtpUserPassword;
                }
            }

            /// <summary>
            /// SMTP server port (default 25)
            /// </summary>
            public int SMTPServerPort
            {
                set
                {
                    smtpServerPort = value;
                }
                get
                {
                    return smtpServerPort;
                }
            }

            /// <summary>
            /// If SMTP server requires SSL
            /// </summary>
            public bool SMTPSSL
            {
                set
                {
                    smtpSSL = value;
                }
                get
                {
                    return smtpSSL;
                }
            }
            #endregion

            #region Public Functions
            public void Send()
            {
                if (smtpServerName.Length == 0)
                {
                    throw new Exception("SMTP Server not specified");
                }

                if (fromName.Length > 0)
                {
                    this.Headers.Add("From",
                         string.Format("{0} <{1}>",
                         FromName, From));
                }

                SmtpClient sc = new SmtpClient(smtpServerName, smtpServerPort);
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;

                if (smtpUserName.Length > 0 && smtpUserPassword.Length > 0)
                {
                    sc.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpUserPassword);
                }

                // ssl if needed
                sc.EnableSsl = smtpSSL;

                sc.Send(this);
            }
            #endregion

            #region Static Functions
            public static void QuickSend(
                string SMTPServerName,
                string ToEmail,
                string FromEmail,
                string Subject,
                string Body,
                bool IsBodyHTML)
            {
                EnhancedMailMessage msg = new EnhancedMailMessage();

                msg.From = new MailAddress(FromEmail);
                msg.To.Add(new MailAddress(ToEmail));
                msg.Subject = Subject;
                msg.Body = Body;
                msg.IsBodyHtml = IsBodyHTML;

                msg.SMTPServerName = SMTPServerName;
                msg.Send();
            }
            #endregion
        }
        #endregion

        #region Private Functions
        void Send()
        {
            if (_pending != string.Empty)
            {
                EnhancedMailMessage msg = new EnhancedMailMessage();

                msg.From = new MailAddress(_fromEmailAddress);
                msg.FromName = "CSVProcessor";
                msg.To.Add(new MailAddress(_toEmailAddress));
                msg.Subject = _emailSubject;
                msg.Body = _pending;

                msg.SMTPServerName = _smtpServer;
                msg.SMTPUserName = _user;
                msg.SMTPUserPassword = _password;

                msg.Send();

                _lastSent = DateTime.Now;
                _pending = string.Empty;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public emailoutput(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "server")
                    {
                        // save the flush frequency
                        _smtpServer = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Server>" + _smtpServer + "</Server>");
                        }
                    }
                    if (n3.Name.ToLower() == "to")
                    {
                        // save the flush frequency
                        _toEmailAddress = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <To>" + _toEmailAddress + "</To>");
                        }
                    }
                    if (n3.Name.ToLower() == "from")
                    {
                        // save the flush frequency
                        _fromEmailAddress = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <From>" + _fromEmailAddress + "</From>");
                        }
                    }
                    if (n3.Name.ToLower() == "subject")
                    {
                        // save the flush frequency
                        _emailSubject = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Subject>" + _emailSubject + "</Subject>");
                        }
                    }
                    if (n3.Name.ToLower() == "user")
                    {
                        // save the flush frequency
                        _user = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <User>" + _user + "</User>");
                        }
                    }
                    else if (n3.Name.ToLower() == "interval")
                    {
                        _minimumElapsed = TimeSpan.Parse(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Intverval>" + _minimumElapsed.ToString() + "</Interval>");
                        }
                    }
                }
            }

            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "password")
                    {
                        // save the flush frequency
                        _password = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Password>" + _password + "</Password>");
                        }

                        if (_password == "*")
                        {
                            _password = string.Empty;

                            // write the prompt and then set text output to black on black
                            Console.Write("Enter password for server '" + _smtpServer + "' for user '" + _user + "' : ");
                            CSVProcessor.DisplayError(ANSIHelper.Black + ANSIHelper.BlackBackground);

                            int i = 0;

                            while (i != -1)
                            {
                                // read in the password
                                i = Console.Read();

                                if (i == 13)
                                {
                                    i = -1;
                                }

                                if (i != -1)
                                {
                                    _password += (char)i;
                                }
                            }

                            // set the screen back to normal
                            CSVProcessor.DisplayError(ANSIHelper.Reset + ANSIHelper.White + "Password accepted.");
                        }
                    }
                }
            }

            // if no file name was specified
            if (_smtpServer == string.Empty || _toEmailAddress == string.Empty)
            {
                throw new ApplicationException("SMTP server and email address must both be specified.");
            }
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "server":
                case "to":
                case "from":
                case "subject":
                case "interval":
                case "user":
                case "password":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                if (_id != "")
                {
                    return _id;
                }
                else
                {
                    return _toEmailAddress + ":" + _emailSubject;
                }
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();
            Send();
        }

        protected override void Open()
        {
            base.Open();
        }

        protected override bool FormatterRequired
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (_pending == string.Empty)
            {
                _pending = s;
            }
            else
            {
                _pending = _pending + "\n" + s;
            }

            if (_lastSent == DateTime.MinValue || _lastSent + _minimumElapsed < DateTime.Now)
            {
                // only send if our queue is empty
                if (_mq.Count == 0)
                {
                    Send();
                }
            }
            else
            {
                // set an alarm so I get called when the minimum time elapses

                // sleep up to the event
                Thread.Sleep(_lastSent + _minimumElapsed - DateTime.Now);

                // only send if our queue is empty
                if (_mq.Count == 0)
                {
                    Send();
                }
            }
        }
        #endregion
    }

    public class fileappend : fileoutput
    {

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfile + "\"];");
        }

        #region Constructors
        /// <summary>
        /// Create an output file
        /// </summary>
        /// <param name="n"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="p"></param>
        public fileappend(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength)
            : base(n, j, tracexml, prefix, p, QueueLength)
        {
        }
        #endregion

        #region Overrides
        protected override void Open()
        {
            // Create the output file
            _twFile = new StreamWriter(_outputfile, true);
        }
        #endregion
    }

#if Framework4
    /// <summary>
    /// This represents a chart output
    /// </summary>
    public class chart : Output
    {
        #region Member Variables
        string _outputfile = string.Empty; // the name of the image file
        bool _legend = false; // true if we are to show a legend
        Chart _chart = new Chart(); // the chart object
        string _dateformat = string.Empty; // date format for any parsed dates
        bool _first = true; // true if this is the first line
        bool _skipfirstline = false; // true if we are to skip the first line
        string _xAxisTitle = string.Empty;
        string _yAxisTitle = string.Empty;
        string _y2AxisTitle = string.Empty;
        double _miny = double.NaN;
        double _miny2 = double.NaN;
        double _maxy = double.NaN;
        double _maxy2 = double.NaN;
        #endregion

        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [shape=Mcircle];");
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _outputfile + "\"];");
        }

        #region Private Classes
        /// <summary>
        /// This represents a series we are to graph
        /// </summary>
        class SeriesData
        {
            #region Member Variables
            string _name = string.Empty; // name of the series
            ColumnNumber _xcol = null; // the column number where the X values come from
            ColumnNumber _ycol = null; // the column number where the Y values come from
            SeriesChartType _charttype = SeriesChartType.Line; // the type of chart to display
            AxisType _axistype = AxisType.Primary; // the axis to use for Y values
            string _xdatatype = string.Empty; // the X axis data type
            string _ydatatype = string.Empty; // the Y axis data type
            #endregion

            #region Constructors
            public SeriesData(string name, ColumnNumber xcol, ColumnNumber ycol, SeriesChartType charttype, AxisType axisType, string XDataType, string YDataType)
            {
                // save the values
                _name = name;
                _xcol = xcol;
                _ycol = ycol;
                _charttype = charttype;
                _axistype = axisType;
                _xdatatype = XDataType;
                _ydatatype = YDataType;
            }
            #endregion

            #region Accessors
            public string XDataType
            {
                get
                {
                    return _xdatatype;
                }
            }
            public string YDataType
            {
                get
                {
                    return _ydatatype;
                }
            }

            public ColumnNumber X
            {
                get
                {
                    return _xcol;
                }
            }
            public ColumnNumber Y
            {
                get
                {
                    return _ycol;
                }
            }
            #endregion
        }
        #endregion

        #region Constructors
        public chart(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p, int QueueLength) :
            base(n, j, tracexml, prefix, p, QueueLength)
        {
            // proces the configuration xml
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "title")
                    {
                        // add a title to the graph
                        _chart.Titles.Add(n3.InnerText);

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Title>" + n3.InnerText + "</Title>");
                        }
                    }
                    else if (n3.Name.ToLower() == "skipfirstline")
                    {
                        _skipfirstline = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SkipFirstLine/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "dateformat")
                    {
                        // save the flush frequency
                        _dateformat = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DateFormat>" + _dateformat + "</DateFormat>");
                        }
                    }
                    else if (n3.Name.ToLower() == "xaxistitle")
                    {
                        // save the flush frequency
                        _xAxisTitle = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <XAxisTitle>" + _xAxisTitle + "</XAxisTitle>");
                        }
                    }
                    else if (n3.Name.ToLower() == "yaxistitle")
                    {
                        // save the flush frequency
                        _yAxisTitle = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <YAxisTitle>" + _yAxisTitle + "</YAxisTitle>");
                        }
                    }
                    else if (n3.Name.ToLower() == "y2axistitle")
                    {
                        // save the flush frequency
                        _y2AxisTitle = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Y2AxisTitle>" + _xAxisTitle + "</Y2AxisTitle>");
                        }
                    }
                    else if (n3.Name.ToLower() == "minyvalue")
                    {
                        // save the flush frequency
                        _miny = Convert.ToDouble(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <MinYValue>" + _miny + "</MinYValue>");
                        }
                    }
                    else if (n3.Name.ToLower() == "miny2value")
                    {
                        // save the flush frequency
                        _miny2 = Convert.ToDouble(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <MinY2Value>" + _miny2 + "</MinY2Value>");
                        }
                    }
                    else if (n3.Name.ToLower() == "maxyvalue")
                    {
                        // save the flush frequency
                        _maxy = Convert.ToDouble(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <MaxYValue>" + _maxy + "</MaxYValue>");
                        }
                    }
                    else if (n3.Name.ToLower() == "maxy2value")
                    {
                        // save the flush frequency
                        _maxy2 = Convert.ToDouble(n3.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <MaxY2Value>" + _maxy2 + "</MaxY2Value>");
                        }
                    }
                    else if (n3.Name.ToLower() == "displaylegend")
                    {
                        _legend = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DisplayLegend/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "series")
                    {
                        string name = string.Empty; // the series name
                        ColumnNumber xcol = null; // the column with the X values
                        ColumnNumber ycol = null; // the column with the Y values
                        SeriesChartType charttype = SeriesChartType.Line; // the chart type
                        AxisType axisType = AxisType.Primary; // the Y axis
                        string xdatatype = string.Empty; // the X axis data type
                        string ydatatype = string.Empty; // the Y axis data type

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Series>");
                        }

                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "name")
                                {
                                    name = n4.InnerText;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Name>" + name + "</Name>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "secondaryaxis")
                                {
                                    axisType = AxisType.Secondary;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <SecondaryAxis/>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "xcol")
                                {
                                    xcol = new ColumnNumber(_p.Job, n4.InnerText);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <XCol>" + xcol.Number.ToString() + "</XCol>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "ycol")
                                {
                                    ycol = new ColumnNumber(_p.Job, n4.InnerText);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <YCol>" + ycol.Number.ToString() + "</YCol>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "xdatatype")
                                {
                                    switch (n4.InnerText.ToLower())
                                    {
                                        case "string":
                                            xdatatype = "string";
                                            break;
                                        case "int":
                                            xdatatype = "int";
                                            break;
                                        case "float":
                                            xdatatype = "float";
                                            break;
                                        case "datetime":
                                            xdatatype = "datetime";
                                            break;
                                        default:
                                            throw new ApplicationException("Chart::Constructor unknown x data type.");
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <XDataType>" + xdatatype + "</XDataType>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "ydatatype")
                                {
                                    switch (n4.InnerText.ToLower())
                                    {
                                        case "int":
                                            ydatatype = "int";
                                            break;
                                        case "float":
                                            ydatatype = "float";
                                            break;
                                        default:
                                            throw new ApplicationException("Chart::Constructor unknown y data type.");
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <YDataType>" + ydatatype + "</YDataType>");
                                    }
                                }
                                else if (n4.Name.ToLower() == "charttype")
                                {
                                    switch (n4.InnerText.ToLower())
                                    {
                                        case "line":
                                            charttype = SeriesChartType.Line;
                                            break;
                                        case "bar":
                                            charttype = SeriesChartType.Column;
                                            break;
                                        case "stackedbar":
                                            charttype = SeriesChartType.StackedColumn;
                                            break;
                                        case "stackedarea":
                                            charttype = SeriesChartType.StackedArea;
                                            break;
                                        case "stackedarea100":
                                            charttype = SeriesChartType.StackedArea100;
                                            break;
                                        case "stackedbar100":
                                            charttype = SeriesChartType.StackedColumn100;
                                            break;
                                        case "point":
                                            charttype = SeriesChartType.Point;
                                            break;
                                        default:
                                            throw new ApplicationException("Chart::Constructor unknown charttype.");
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <ChartType>" + charttype.ToString() + "</ChartType>");
                                    }
                                }
                            }
                        }

                        // create the series
                        Series s = new Series(name);

                        // add it to our chart
                        _chart.Series.Add(s);

                        // set the attributes
                        s.YAxisType = axisType;
                        s.ChartType = charttype;

                        // save the rest
                        s.Tag = new SeriesData(name, xcol, ycol, charttype, axisType, xdatatype, ydatatype);

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Series>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutputFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                        CSVFileInfo fi = new CSVFileInfo(p.Input.Name);
                        CSVFileLine csv = new CSVFileLine("", 1, fi);
                        _outputfile = sc.Value(csv);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </OutputFile>");
                        }
                    }
                }
            }

            // if no file name was specified
            if (_outputfile == string.Empty)
            {
                throw new ApplicationException("Chart::Constructor No filename specified.");
            }
            else
            {
            }
        }
        #endregion

        #region Overrides
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "outputfile":
                case "title":
                case "dateformat":
                case "displaylegend":
                case "series":
                case "skipfirstline":
                case "minyvalue":
                case "miny2value":
                case "maxyvalue":
                case "maxy2value":
                case "xaxistitle":
                case "yaxistitle":
                case "y2axistitle":
                    return true;
                default:
                    return Output.Supports(s);
            }
        }

        /// <summary>
        /// Get the filename
        /// </summary>
        public override string Name
        {
            get
            {
                return _outputfile;
            }
        }

        /// <summary>
        /// Close the file
        /// </summary>
        protected override void Close()
        {
            base.Close();

            // save the image according to the file name extension
            switch (Path.GetExtension(_outputfile.ToLower()))
            {
                case ".jpg":
                case ".jpeg":
                    _chart.SaveImage(_outputfile, ImageFormat.Jpeg);
                    break;
                case ".gif":
                    _chart.SaveImage(_outputfile, ImageFormat.Gif);
                    break;
                case ".png":
                    _chart.SaveImage(_outputfile, ImageFormat.Png);
                    break;
                case ".bmp":
                    _chart.SaveImage(_outputfile, ImageFormat.Bmp);
                    break;
                default:
                    throw new ApplicationException("Chart::Close file type not supported.");
            }
        }

        protected override void Open()
        {
            // set the graph size
            _chart.Size = new System.Drawing.Size(800, 600);

            ChartArea chartArea1 = new ChartArea();
            chartArea1.Name = "ChartArea1";
            _chart.ChartAreas.Add(chartArea1);

            // remove grid lines
            foreach (Axis x in chartArea1.Axes)
            {
                x.MajorGrid.Enabled = false;
            }
            chartArea1.AxisX.Title = _xAxisTitle;
            chartArea1.AxisY.Title = _yAxisTitle;
            chartArea1.AxisY2.Title = _y2AxisTitle;
            if (_miny != double.NaN)
            {
                chartArea1.AxisY.Minimum = _miny;
            }
            if (_miny2 != double.NaN)
            {
                chartArea1.AxisY2.Minimum = _miny2;
            }
            if (_maxy != double.NaN)
            {
                chartArea1.AxisY.Maximum = _maxy;
            }
            if (_maxy2 != double.NaN)
            {
                chartArea1.AxisY2.Maximum = _maxy2;
            }

            if (_legend)
            {
                Legend legend1 = new Legend();
                legend1.Name = "Legend1";
                _chart.Legends.Add(legend1);
            }

            base.Open();
        }


        /// <summary>
        /// Write a string to the file
        /// </summary>
        /// <param name="s"></param>
        protected override void WriteLine(string s, bool data)
        {
            if (_first)
            {
                _first = false;

                if (_skipfirstline)
                {
                    return;
                }
            }

            CSVFileLine cl = new CSVFileLine(s, 0);
            cl.Parse(',', '"', '\\');

            foreach (Series ss in _chart.Series)
            {
                object x = null;
                object y = null;

                string xs = cl[((SeriesData)ss.Tag).X];
                string ys = cl[((SeriesData)ss.Tag).Y];

                switch (((SeriesData)ss.Tag).XDataType)
                {
                    case "string":
                        x = xs;
                        break;
                    case "int":
                        x = Convert.ToInt32(xs);
                        break;
                    case "float":
                        x = Convert.ToDouble(xs);
                        break;
                    case "datetime":
                        x = DateTime.ParseExact(xs, _dateformat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                        break;
                }

                switch (((SeriesData)ss.Tag).YDataType)
                {
                    case "string":
                        y = ys;
                        break;
                    case "int":
                        y = Convert.ToInt32(ys);
                        break;
                    case "float":
                        y = Convert.ToDouble(ys);
                        break;
                    case "datetime":
                        y = DateTime.ParseExact(ys, _dateformat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                        break;
                }

                ss.Points.AddXY(x, Convert.ToDouble(y));
            }
        }
        #endregion

    }
#endif
    #endregion
}
