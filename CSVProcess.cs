// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Queues;

namespace CSVProcessor
{
	/// <summary>
	/// This class represents a process of a CSV file.
	/// </summary>
	public class CSVProcess
	{
		#region Member Variables
		JobParameters _jp = null; // holds the output file specific parameters
		CSVJob _j = null;
		ProcessParameters _fp = null; // holds the output file specific parameters
		OutputDefinition _od = null; // holds the transformation definition
		Compare _compare = null; // holds the compare definition
		FilterDefinition _fd = null; // holds the filter rules
		TagValues _tv = null; // holds the tag values
		Subroutines _subroutines = null; // holds any subroutine columns
		Totals _t = null; // holds the totals definition
		Thread _thread = null; // Thread this processor runs on
		BlockingQueue _mq = null; // queue to use for multithreaded running
		SortOutput _so = null; // hold the sort function
		Column _header = null; // header record
		Column _footer = null; // footer record
		List<ColumnNumber> _grouprows = null; // list of columns than constitute a group ... file should be sorted by these columns
		Column _pregroup = null; // output before a new group
		string _lastgroupkey = string.Empty;
		ForEach _foreach = null; // foreach processor
		int _failed = 0; // our result
		List<Output> _outputs = new List<Output>(); // outputs to send results to
		string _id = string.Empty; // Id of the process
		long _lastOutputCount = 0; // count of rows at last new file
		bool _skipfound = false; // found a reason to start
		bool _stopfound = false; // found a reason to start
		LogicFilterNode _startAfter = null; // a rule to check before starting
		bool _includeSkipStartRow = false;
		LogicFilterNode _stopAfter = null; // a rule to check before starting
		bool _includeStopAfterRow = false;
		int _unique = UniqueIdClass.Allocate();
		#endregion

		public string UniqueId
		{
			get
			{
				return "clusterP" + _unique.ToString();
			}
		}
		public string FirstUniqueId
		{
			get
			{
				if (_fd != null && _fd.SomethingToDo)
				{
					return _fd.FirstUniqueId;
				}
				else if (_od != null)
				{
					return _od.FirstUniqueId;
				}
				else if (_t != null)
				{
					return _t.FirstUniqueId;
				}
				else
				{
					Debug.Fail("Nothing to connect the process to for Dot output.");
					return string.Empty;
				}
			}
		}

		public void WriteDot(StreamWriter dotWriter, string prefix)
		{
			dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
			dotWriter.WriteLine(prefix + "   shape=rect;");
			dotWriter.WriteLine(prefix + "   color=blue;");
			dotWriter.WriteLine(prefix + "   style=filled;");
			dotWriter.WriteLine(prefix + "   label=\"Process::" + Id + "\";");

			if (_foreach != null)
			{
				_foreach.WriteDot(dotWriter, "   " + prefix);
			}
			if (_header != null)
			{
				_header.WriteDot(dotWriter, "   " + prefix);
			}
			if (_footer != null)
			{
				_footer.WriteDot(dotWriter, "   " + prefix);
			}
			if (_fd != null)
			{
				_fd.WriteDot(dotWriter, "   " + prefix);
			}
			if (_tv != null)
			{
				_tv.WriteDot(dotWriter, "   " + prefix);
			}
			if (_subroutines != null)
			{
				_subroutines.WriteDot(dotWriter, "   " + prefix);
			}
			if (_fp != null)
			{
				_fp.WriteDot(dotWriter, "   " + prefix);
			}
			if (_od != null)
			{
				_od.WriteDot(dotWriter, "   " + prefix);
			}
			if (_compare != null)
			{
				_compare.WriteDot(dotWriter, "   " + prefix);
			}
			if (_t != null)
			{
				_t.WriteDot(dotWriter, "   " + prefix);
			}
			if (_so != null)
			{
				_so.WriteDot(dotWriter, "   " + prefix);
			}
			foreach (Output o in _outputs)
			{
				o.WriteDot(dotWriter, "   " + prefix);
			}
			foreach (Output o in _outputs)
			{
				if (_so == null && _od != null)
				{
					dotWriter.WriteLine(prefix + _od.FirstUniqueId + " -> " + o.UniqueId + " [ltail=" + _od.UniqueId + "];");
				}
				else if (_so != null)
				{
					dotWriter.WriteLine(prefix + _so.UniqueId + " -> " + o.UniqueId + ";");
				}
				if (_t != null)
				{
					dotWriter.WriteLine(prefix + _t.FirstUniqueId + " -> " + o.UniqueId + " [ltail=" + _t.UniqueId + "];");
				}
			}
			if (_fd != null && _od != null && _fd.SomethingToDo)
			{
				dotWriter.WriteLine(prefix + _fd.FirstUniqueId + " -> " + _od.FirstUniqueId + "[ltail=" + _fd.UniqueId + ", lhead=" + _od.UniqueId + "];");
			}
			if (_fd != null && _fd.SomethingToDo && _t != null)
			{
				dotWriter.WriteLine(prefix + _fd.FirstUniqueId + " -> " + _t.FirstUniqueId + "[ltail=" + _fd.UniqueId + ", lhead=" + _t.UniqueId + "];");
			}
			if (_od != null && _so != null)
			{
				dotWriter.WriteLine(prefix + _od.FirstUniqueId + " -> " + _so.UniqueId + "[ltail=" + _od.UniqueId + "];");
			}
			dotWriter.WriteLine(prefix + "}");
		}

		#region Thread Functions
		/// <summary>
		/// Thread function to process this on a separate thread
		/// </summary>
		void Processor()
		{
			Debug.WriteLine("Process {" + Id + "} Starting.");

			int rc = 0; // the process result code

			// this should never be called for disabled processors
			Debug.Assert(_fp.Disabled == false);

			bool fContinue = true; // true to continue looping

			// loop processing messages
			do
			{
				// get the next message
				CSVMessage msg = (CSVMessage)_mq.Dequeue();

				switch (msg.Status)
				{
					case CSVMessage.STATUS.NEWFILE:
						NewFile();
						break;
					case CSVMessage.STATUS.SYNCH:
						// if it is a synch message then release the other thread
						Debug.WriteLine("Process {" + Id + "} received synch from job");
						msg.Synch.Set();
						break;
					case CSVMessage.STATUS.STOP:
						Debug.WriteLine("Process {" + Id + "} received stop from job");
						fContinue = false;
						break;
					case CSVMessage.STATUS.LINE:
						if (Failed == 0)
						{
							if (_startAfter != null && !_skipfound)
							{
								if (_includeSkipStartRow)
								{
									_skipfound = _startAfter.Evaluate(msg.Line);
								}
							}

							if (_stopAfter != null && !_stopfound)
							{
								if (_includeStopAfterRow)
								{
									_stopfound = _stopAfter.Evaluate(msg.Line);
								}
							}

							if ((_startAfter == null || _skipfound) && !_stopfound)
							{
								try
								{
									if (_foreach != null)
									{
										if (msg.LineNumber <= _fp.SkipNLines)
										{
											// we ignore this line
										}
										else
										{
											rc = _foreach.ProcessLine(msg.Line, msg.LineNumber, msg.FirstFile);
										}
									}
									else
									{
										// Process the line
										rc = ProcessLine(msg.Line, msg.LineNumber, msg.FirstFile);
									}
								}
								catch (Exception ex)
								{
									CSVProcessor.DisplayError(ANSIHelper.Red + "Problem generating output line in file : " + File + " : " + ex.Message + ANSIHelper.White);
								}
							}

							if (_startAfter != null && !_skipfound)
							{
								_skipfound = _startAfter.Evaluate(msg.Line);
							}

							if (_stopAfter != null && !_stopfound)
							{
								_stopfound = _stopAfter.Evaluate(msg.Line);
							}
						}

						// set the rsult
						if (rc > 0)
						{
							lock (this)
							{
								_failed = rc;
							}
						}
						break;
				}
			} while (fContinue); // loop forever

			if (Failed == 0)
			{
				// If we have a sort
				if (_so != null)
				{
					// do the sort
					_so.Finish();
				}
			}

			// Close the output file
			Close();

			if (_failed == 0 && _outputs.Count > 0)
			{
				_failed = _outputs[0].RC;
			}

			Debug.WriteLine("Process {" + Id + "} Ending.");
		}

		/// <summary>
		/// Start the thread processor
		/// </summary>
		public void StartProcessor()
		{
			// if this processor is disabled then there is nothing we need to do
			if (_fp.Disabled)
			{
				return;
			}

			// Create the thread
			_thread = new Thread(new ThreadStart(Processor));

			_thread.Name = "Process:" + Id;

			// start it
			_thread.Start();

			// Sleep to give it a chance to run
			Thread.Sleep(0);
		}

		/// <summary>
		/// Wait for the process to finish
		/// </summary>
		public void Wait()
		{
			Debug.WriteLine("Waiting for process {" + Id + "} to end");

			// wait for the thread to finish
			if (_thread != null)
			{
				_thread.Join();
			}
			Debug.WriteLine("Process {" + Id + "} ended");
		}
		#endregion

		#region Message Functions

		/// <summary>
		/// Send a new file thread to the job thread
		/// </summary>
		/// <param name="file"></param>
		public void NewFile(string file)
		{
			// if this processor is disabled then there is nothing we need to do
			if (_fp.Disabled)
			{
				return;
			}

			// Create a message
			CSVMessage m = new CSVMessage(file, CSVMessage.STATUS.NEWFILE);

			// If the queue has not been created
			if (_mq == null)
			{
				// Sleep for a second to give it plenty of time to start
				Thread.Sleep(1000);
			}

			// If the queue has still not been created
			if (_mq == null)
			{
				// This is a big problem
				throw new ApplicationException("Can't send to non existent queue.");
			}

			// Enqueue the message
			_mq.Enqueue(m);
		}

		/// <summary>
		/// Send a line for processing
		/// </summary>
		/// <param name="l"></param>
		/// <param name="line"></param>
		/// <param name="firstfile"></param>
		public void Send(ColumnLine l, long line, bool firstfile)
		{
			// if this processor is disabled then there is nothing we need to do
			if (_fp.Disabled)
			{
				return;
			}

			// create a message to send
			CSVMessage m = new CSVMessage(l, line, firstfile);

			// If the queue has not been created
			if (_mq == null)
			{
				// Sleep for a second to give it plenty of time to start
				Thread.Sleep(1000);
			}

			// If the queue has still not been created
			if (_mq == null)
			{
				// This is a big problem
				throw new ApplicationException("Can't send to non existent queue.");
			}

			// Enqueue the message
			_mq.Enqueue(m);

			// if there is space in our queue
			if (_mq.QueueFree < 1.0)
			{
				// if there is enough space to force a read
				if (_jp.FreePctBeforeMoreReads > 0)
				{
					// yield to let the read occur
					Thread.Sleep(0);
				}
			}
		}

		/// <summary>
		/// Wait for the processing thread to catch up to this point
		/// </summary>
		public void Synch()
		{
			// if this processor is disabled then there is nothing we need to do
			if (_fp.Disabled)
			{
				return;
			}

			Debug.WriteLine("Process {" + Id + "} sending synch");

			// create a synch event
			ManualResetEvent mre = new ManualResetEvent(true);
			mre.Reset();

			// create a message to send
			CSVMessage m = new CSVMessage(mre);

			// If the queue has not been created
			if (_mq == null)
			{
				// Sleep for a second to give it plenty of time to start
				Thread.Sleep(1000);
			}

			// If the queue has still not been created
			if (_mq == null)
			{
				// This is a big problem
				throw new ApplicationException("Can't send to non existent queue.");
			}

			// Enqueue the message
			_mq.Enqueue(m);

			while (!_mq.Aborted && !mre.WaitOne(1000) && _thread != null && _thread.IsAlive)
			{
				// wait for the event or an abort
			}

			Debug.WriteLine("Process {" + Id + "} synch done");
		}

		/// <summary>
		/// Tell the processing thread we are done
		/// </summary>
		public void Stop()
		{
			// if the processor is disabled the we dont need to do anything
			if (_fp.Disabled)
			{
				return;
			}

			Debug.WriteLine("Process {" + Id + "} sending stop");

			// Create a message
			CSVMessage m = new CSVMessage(CSVMessage.STATUS.STOP);

			// If the queue has not been created
			if (_mq == null)
			{
				// Sleep for a second to give it plenty of time to start
				Thread.Sleep(1000);
			}

			// If the queue has still not been created
			if (_mq == null)
			{
				// This is a big problem
				throw new ApplicationException("Can't send to non existent queue.");
			}

			// Enqueue the message
			_mq.Enqueue(m);
		}
		#endregion

		#region Accessors
		/// <summary>
		/// Get the id of this process
		/// </summary>
		public string Id
		{
			get
			{
				// if no id then use the file name
				if (_id == string.Empty)
				{
					return File;
				}
				else
				{
					return _id;
				}
			}
		}

		public Input Input
		{
			get
			{
				return _j.Input;
			}
		}

		/// <summary>
		/// Get the prcoess name
		/// </summary>
		public string Name
		{
			get
			{
				// if we have an id use that
				if (Id != "")
				{
					return Id;
				}
				else
				{
					// use the first output name
					return _outputs[0].Name;
				}
			}
		}

		/// <summary>
		/// Get the process result code
		/// </summary>
		public int Failed
		{
			get
			{
				int rc = 0;

				lock (this)
				{
					rc = _failed;
				}

				return rc;
			}
		}

		/// <summary>
		/// Get the number of records written
		/// </summary>
		public long WriteCount
		{
			get
			{
				if (_outputs.Count == 0) return 0;

				long rc = 0;

				// get output to synch to this point so our record count will be correct
				((Output)_outputs[0]).Synch();

				lock (this)
				{
					rc = ((Output)_outputs[0]).WriteCount - _lastOutputCount;
				}

				return rc;
			}
		}

		/// <summary>
		/// Gets the file ouput parameters
		/// </summary>
		public ProcessParameters ProcessParameters
		{
			get
			{
				return _fp;
			}
		}

		public CSVJob Job
		{
			get
			{
				return _j;
			}
		}

		public JobParameters JobParameters
		{
			get
			{
				return _jp;
			}
		}

		/// <summary>
		/// Gets the name of the output file
		/// </summary>
		public string File
		{
			get
			{
				string s = string.Empty;

				// concatenate all the file names
				foreach (Output o in _outputs)
				{
					s = s + o.Name + " ";
				}

				if (s == string.Empty)
				{
					s = "NO_FILE_NAME";
				}

				return s;
			}
		}

		/// <summary>
		/// Get the tag values
		/// </summary>
		public TagValues Tags
		{
			get
			{
				return _tv;
			}
		}

		public Subroutines Subroutines
		{
			get
			{
				return _subroutines;
			}
		}
		#endregion

		#region Private Functions
		/// <summary>
		/// Notify that we have a new file
		/// </summary>
		void NewFile()
		{
			// save where we are up to on output files
			_lastOutputCount = ((Output)_outputs[0]).WriteCount;
		}

		/// <summary>
		/// Process an input line
		/// </summary>
		/// <param name="l">Line to process</param>
		/// <param name="line">Line number</param>
		/// <param name="firstfile">First file flag</param>
		public int ProcessLine(ColumnLine l, long line, bool firstfile)
		{
			// If this processor is disabled then we dont need to do anything
			if (_fp.Disabled)
			{
				return 0;
			}

			// if it is first line of the first file and we are to pass it through then just write it out
			if (line == 1 && _fp.PassThroughFirstLineOfFirstFile)
			{
				if (firstfile)
				{
					if (_compare != null)
					{
						ColumnLine comp = new CSVFileLine("Compare", 0);
						comp.Parse();
						comp.Data = false;
						comp.AddEnd(l);
						DisplayOutput(comp);
					}
					else
					{
						// pass through the line
						l.Data = false;
						DisplayOutput(l);
					}

					// do tag processing
					_tv.Process(l, line, TagValues.State.FIRSTLINEOFFIRSTFILE);
				}
				else
				{
					// eat the first line in all other files
				}
			}
			// if it is first line and we are to pass it through then just write it out
			else if (line == 1 && _fp.PassThroughFirstLine)
			{
				if (_compare != null)
				{
					ColumnLine comp = new CSVFileLine("Compare", 0);
					comp.Parse();
					comp.Data = false;
					comp.AddEnd(l);
					DisplayOutput(comp);
				}
				else
				{
					// pass through the line
					l.Data = false;
					DisplayOutput(l);
				}

				// do tag processing
				_tv.Process(l, line, TagValues.State.FIRSTLINE);
			}
			// if it is a line less that the number of lines we are to skip then do nothing
			else if (line <= _fp.SkipNLines)
			{
			}
			//else if (line == 1 && _fp.SkipFirstLine)
			//{
			//}
			// if we are to skip blank lines and we read no columns then do nothing
			else if (_fp.SkipBlankLines && l.Count == 1 && l[0].Length == 0)
			{
			}
			// process the line
			else
			{
				try
				{
					// if the filter says we must include the line
					if (_fd.In(l))
					{
						if (_grouprows != null && _pregroup != null)
						{
							string newgroupkey = string.Empty;
							foreach (ColumnNumber cn in _grouprows)
							{
								newgroupkey += l[cn];
							}

							if (newgroupkey != _lastgroupkey)
							{
								CSVFileLine fl = new CSVFileLine();
								fl.AddEnd(_pregroup.Process(l));
								OutputData(fl);
								_lastgroupkey = newgroupkey;
							}
						}

						if (_od != null)
						{
							//process the line
							// string output = _od.Process(l);
							CSVFileLine output = _od.Process(l);

							// if the input line is not a data row then flag the output as not a data row
							if (!l.Data)
							{
								output.Data = l.Data;
							}

							// If we got output write it out - blank output lines are swallowed
							// if (output != string.Empty)
							if (output.Count != 0)
							{
								OutputData(output);
							}
						}

						if (_compare != null)
						{
							_compare.Process(l);
						}

						// If we have totals then pass the line to have the totals accumulated
						if (_t != null)
						{
							_t.Process(l);
						}

						// do tag value processing
						_tv.Process(l, line, TagValues.State.PROCESSED);
					}
					else
					{
						// do tag value processing on filtered out records
						_tv.Process(l, line, TagValues.State.FILTERED);

						if (_fp.FailOnFilter)
						{
							CSVProcessor.DisplayError(ANSIHelper.Red + "Aborting due to record being filtered!!!" + ANSIHelper.White);
							return 1;
						}
					}
				}
				catch (Exception ex)
				{
					CSVProcessor.DisplayError(ANSIHelper.Red + "Problem processing line # : " + line.ToString() + " : " + ex.Message + ANSIHelper.White);
					Exception e = ex.InnerException;

					while (e != null)
					{
						CSVProcessor.DisplayError(ANSIHelper.Red + "      " + e.Message + ANSIHelper.White);
						e = e.InnerException;
					}
					CSVProcessor.DisplayError(l.Raw);

					if (_fp.FailOnProcessingError)
					{
						CSVProcessor.DisplayError(ANSIHelper.Red + "Aborting!!!" + ANSIHelper.White);
						return 3;
					}
				}
			}

			// now clear any tag values that should be cleared in this line
			_tv.ClearIfAppropriate(l);

			return 0;
		}

		/// <summary>
		/// This function is called after all input records have been processed to close up the file
		/// </summary>
		void Close()
		{
			// If this processor is disabled then we have nothing to do
			if (_fp.Disabled)
			{
				return;
			}

			// if we did not fail
			if (Failed == 0)
			{
				if (_compare != null)
				{
					_compare.CloseUp();
				}

				// If we have totals
				if (_t != null)
				{
					// if we have to sort the totals
					if (_so != null)
					{
						bool first = true;
						foreach (ColumnLine s in _t.Result())
						{
							if (first)
							{
								// assumes there is 1 and only 1 title line ... this is not true so will cause problems
								first = false;
								DisplayOutput(s);
							}
							else
							{
								OutputData(s);
							}
						}

						_so.Finish();
					}
					else
					{
						// Get each total line and display it
						foreach (ColumnLine s in _t.Result())
						{
							DisplayOutput(s);
						}
					}
				}

				// if we have a given last line write it out
				if (_fp.GivenLastLine != string.Empty)
				{
					DisplayOutput(_fp.GivenLastLine, false);
				}

				if (_footer != null)
				{
					ColumnLine footer = _footer.Process(new ColumnLine());
					footer.Data = false;
					DisplayOutput(footer);
				}
			}

			// now close all the output files
			foreach (Output o in _outputs)
			{
				// wait for them to catchup
				o.Synch();
				o.CloseOutput();
				o.Wait();
			}
		}

		/// <summary>
		/// Write the output line out
		/// </summary>
		/// <param name="s"></param>
		void OutputData(ColumnLine s)
		{
			// if we are going to sort the output
			if (_so != null)
			{
				// add the line to the sort list
				_so.AddOutput(s);
			}
			else
			{
				// turn it into a string and display it
				DisplayOutput(s);
			}
		}

		/// <summary>
		/// Display an output line
		/// </summary>
		/// <param name="l"></param>
		public void DisplayOutput(ColumnLine l)
		{
			// send it to each of out outputs
			foreach (Output o in _outputs)
			{
				o.Write(l);
			}
		}

		/// <summary>
		/// Write to the ouput file
		/// </summary>
		/// <param name="s">String to write</param>
		void DisplayOutput(string s, bool data)
		{
			// Send it to each of out outputs
			foreach (Output o in _outputs)
			{
				o.Write(s, data);
			}
		}
		#endregion

		#region Static Functions
		/// <summary>
		/// Creates the processors from the XML definition
		/// </summary>
		/// <param name="forceSTDOUT">True if we are to force writing output to the screen</param>
		/// <param name="doc">XML document containing the processor definitions</param>
		/// <returns></returns>
		public static List<CSVProcess> Create(XmlNode node, bool tracexml, string prefix, JobParameters jobParameters, CSVJob j)
		{
			// Create an empty list of CSVProcess objects
			List<CSVProcess> _al = new List<CSVProcess>();

			try
			{
				foreach (XmlNode n1 in node.ChildNodes)
				{
					if (n1.NodeType != XmlNodeType.Comment)
					{
						if (n1.Name.ToLower() == "process")
						{
							// parse the XML document
							// create the processor
							CSVProcess p = new CSVProcess(n1, tracexml, prefix + "  ", jobParameters, j);

							// start the thread
							p.StartProcessor();

							// add the process to our list
							_al.Add(p);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// kill all threads
				foreach (CSVProcess p in _al)
				{
					p.Stop();
				}

				throw ex;
			}

			// sleep to let the threads start
			Thread.Sleep(0);

			// return the list of processes
			return _al;
		}
		#endregion

		void ParseRule(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
		{
			// process each child node
			foreach (XmlNode n2 in n.ChildNodes)
			{
				if (n2.NodeType != XmlNodeType.Comment)
				{
					// if it is an and
					if (n2.Name.ToLower() == "and")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<And>");
						}
						// create an and node
						LogicFilterNode newFn = new AndFilterNode();

						// add the and node
						fn.Add(newFn);

						// parse the and contents
						ParseRule(newFn, j, n2, tracexml, prefix + "  ");
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "</And>");
						}
					}
					// if it is an or
					else if (n2.Name.ToLower() == "or")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Or>");
						}
						// create an or node
						LogicFilterNode newFn = new OrFilterNode();

						// add the or node
						fn.Add(newFn);

						// parse the or contents
						ParseRule(newFn, j, n2, tracexml, prefix + "  ");
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "</Or>");
						}
					}
					// if it is a not
					else if (n2.Name.ToLower() == "not")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Not>");
						}
						// create a not node
						LogicFilterNode newFn = new NotFilterNode();

						// add the not node
						fn.Add(newFn);

						// parse the not node
						ParseRule(newFn, j, n2, tracexml, prefix + "  ");
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "</Not>");
						}
					}
					else
					{
						// it must be an evaluation node

						// create the evaluation node and add it
						fn.Add(new FilterNode(Condition.Create(n2, j, this, tracexml, prefix)));
					}
				}
			}
		}

		#region Constructor
		/// <summary>
		/// Create a CSVProcess object
		/// </summary>
		/// <param name="forceSTDOUT">Force output to stdout</param>
		/// <param name="n">XML node to create it from</param>
		public CSVProcess(XmlNode n, bool tracexml, string prefix, JobParameters jobParameters, CSVJob j)
		{
			// save the job parameters
			_jp = jobParameters;
			_j = j;

			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "<Process>");
			}

			// get the output name first so if we are disabled the output file name is displayed
			foreach (XmlNode n1 in n.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "output")
					{
						_outputs = Output.Create(n1, j, tracexml, prefix + "  ", this, _jp.QueueLength);
					}
				}
			}

			foreach (XmlNode n1 in n.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "processparameters")
					{
						// extract the line filter definition from the configuration file
						try
						{
							_fp = new ProcessParameters(n1, j, this, tracexml, prefix + "  ");

						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the file parameter definition from the configuration file", ex);
						}
					}
				}
			}

			// if we found no parameters create one
			if (_fp == null)
			{
				_fp = new ProcessParameters();
			}

			// process each node
			foreach (XmlNode n1 in n.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "processparameters")
					{
						// we already handled this
					}
					else if (n1.Name.ToLower() == "id")
					{
						_id = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
						}
					}
					else if (n1.Name.ToLower() == "foreach")
					{
						// extract the foreach definition from the configuration file
						try
						{
							_foreach = new ForEach(n1, j, this, tracexml, prefix + "  ", _fp);
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the ForEach definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "filter")
					{
						// extract the line filter definition from the configuration file
						try
						{
							_fd = new FilterDefinition(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the line filter definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "startrule")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <StartRule>");
						}

						// extract the rule to detect when rows should start being processed
						_startAfter = new AndFilterNode();
						ParseRule(_startAfter, j, n1, tracexml, prefix + "   ");

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </StartRule>");
						}
					}
					else if (n1.Name.ToLower() == "endrule")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <EndRule>");
						}

						// extract the rule to detect when rows should start being processed
						_stopAfter = new AndFilterNode();
						ParseRule(_stopAfter, j, n1, tracexml, prefix + "   ");

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </EndRule>");
						}
					}
					else if (n1.Name.ToLower() == "includeskipstartrow")
					{
						_includeSkipStartRow = true;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <IncludeSkipStartRow/>");
						}
					}
					else if (n1.Name.ToLower() == "includestopafterrow")
					{
						_includeStopAfterRow = true;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <IncludeStopAfterRow/>");
						}
					}
					else if (n1.Name.ToLower() == "header")
					{
						if (_header != null)
						{
							throw new ApplicationException("Processor can only have one header.");
						}

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Header>");
						}

						try
						{
							_header = Column.Create(n1.ChildNodes[0], j, this, null, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error creating column for header", ex);
						}
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </Header>");
						}

					}
					else if (n1.Name.ToLower() == "footer")
					{
						if (_footer != null)
						{
							throw new ApplicationException("Processor can only have one footer.");
						}

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Footer>");
						}

						try
						{
							_footer = Column.Create(n1.ChildNodes[0], j, this, null, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error creating column for footer", ex);
						}

						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </Footer>");
						}
					}
					else if (n1.Name.ToLower() == "groupcolumns")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <GroupColumns>");
						}
						// process each node
						foreach (XmlNode n2 in n1.ChildNodes)
						{
							if (n2.NodeType != XmlNodeType.Comment)
							{
								if (n2.Name.ToLower() == "keycolumns")
								{
									foreach (XmlNode n3 in n2.ChildNodes)
									{
										if (n3.NodeType != XmlNodeType.Comment)
										{
											if (n3.Name.ToLower() == "column")
											{
												ColumnNumber col = new ColumnNumber(Job, n3.InnerText);
												if (_grouprows == null)
												{
													_grouprows = new List<ColumnNumber>();
												}
												_grouprows.Add(col);
												if (tracexml)
												{
													CSVProcessor.DisplayError(prefix + "<Column>" + col.ToString() + "</Column>");
												}
											}
										}
									}
								}
								else if (n2.Name.ToLower() == "output")
								{
									if (_pregroup != null)
									{
										throw new ApplicationException("GroupColumns can only have one Output.");
									}

									if (tracexml)
									{
										CSVProcessor.DisplayError(prefix + "     <Output>");
									}

									try
									{
										_pregroup = Column.Create(n2.ChildNodes[0], j, this, null, tracexml, prefix + "  ");
									}
									catch (Exception ex)
									{
										throw new ApplicationException("Error creating column for GroupColumns Output", ex);
									}

									if (tracexml)
									{
										CSVProcessor.DisplayError(prefix + "  </Output>");
									}
								}
							}
						}
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </GroupColumns>");
						}
					}
					else if (n1.Name.ToLower() == "tagvalues")
					{
						// extract the line filter definition from the configuration file
						try
						{
							_tv = new TagValues(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the tag values definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "subroutines")
					{
						// extract the line filter definition from the configuration file
						try
						{
							_subroutines = new Subroutines(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the subroutines definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "totals")
					{
						// extract the totals definition from the configuration file
						try
						{
							_t = new Totals(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the totals definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "compare")
					{
						// extract the transformation definition from the configuration file
						try
						{
							_compare = new Compare(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the compare definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "outputcolumns")
					{
						// extract the transformation definition from the configuration file
						try
						{
							_od = new OutputDefinition(n1, j, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the output file definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "sortoutput")
					{
						try
						{
							_so = new SortOutput(n1, this, tracexml, prefix + "  ");
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error extracting the sort output definition from the configuration file", ex);
						}
					}
					else if (n1.Name.ToLower() == "output")
					{
					}
					else
					{
						CSVProcessor.DisplayError(ANSIHelper.Warning + "CSVProcess::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
					}
				}
			}

			if (_compare != null && (_od != null || _t != null))
			{
				throw new ApplicationException("<Compare> cannot be specified with a <totals> or an <outputcolumns> tag");
			}

			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "</Process>");
			}

			// if we have an id
			if (_id != string.Empty)
			{
				// check if it has been enabled
				if (OverrideDisable.IsEnabled(_id) == 1)
				{
					// user enabled us
					if (_fp.Disabled)
					{
						CSVProcessor.DisplayError(ANSIHelper.Information + "User enabled " + _id + ANSIHelper.White);
					}
					_fp.Disabled = false;
				}
				else if (OverrideDisable.IsEnabled(_id) == 0)
				{
					// user disabled us
					if (!_fp.Disabled)
					{
						CSVProcessor.DisplayError(ANSIHelper.Information + "User disabled " + _id + ANSIHelper.White);
					}
					_fp.Disabled = true;
				}
			}

			// if we are diabled
			if (_fp.Disabled)
			{
				// exit
				return;
			}
			else
			{
				// open the output files
				foreach (Output o in _outputs)
				{
					o.Start();
				}
			}

			// if we have no OutputDefinition then create a default one
			//		if (_od == null)
			//		{
			//			_od = new OutputDefinition(this);
			//		}

			// If we have no filter create a default one
			if (_fd == null)
			{
				_fd = new FilterDefinition(this);
			}

			if (_compare != null)
			{
				_compare.SetFilter(_fd);
			}

			// tell our header about our output definition
			if (_header != null)
			{
				_header.SetOutputDefinition(_od);
			}

			// tell our footer about our output definition
			if (_footer != null)
			{
				_footer.SetOutputDefinition(_od);
			}

			// if we dont have a tag value
			if (_tv == null)
			{
				// create an empty one
				_tv = new TagValues(this);
			}
			_tv.SetOutputDefinition(_od);

			if (_subroutines == null)
			{
				_subroutines = new Subroutines(this);
			}
			_subroutines.SetOutputDefinition(_od);

			// if we have a sort output
			if (_so != null)
			{
				// tell it where to write
				_so.SetOutput(_outputs);
			}

			// display our header if we have one
			if (_header != null)
			{
				ColumnLine header = _header.Process(new ColumnLine());
				header.Data = false;
				DisplayOutput(header);
			}

			// if we have a given first line write it out
			if (_fp.GivenFirstLine != string.Empty)
			{
				DisplayOutput(_fp.GivenFirstLine, false);
			}

			// if we are to use column titles ... display them now
			if (_fp.UseColumTitles && _od != null)
			{
				DisplayOutput(_od.Titles);
			}

			// Create a queue
			_mq = new BlockingQueue("CSVProcessor_Process", Id, _jp.QueueLength);
		}
		#endregion
	}
}
