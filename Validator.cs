// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Queues;
using System.Reflection;

namespace CSVProcessor
{
    public class Validation
    {
        protected Validator _v = null;
        protected string _id = string.Empty; // a id for the column
        protected bool _trace = false; // trace instruction
        string _name = string.Empty;
        protected enum LINESCOPE { UNKNOWN, FIRST, ALL, LAST, ALLBUTFIRST, ALLBUTFIRSTANDLAST };
        protected LogicFilterNode _condition = new AndFilterNode(); // holds the default AND filter
        string _message = string.Empty;
        protected bool _fail = false;
        bool _pause = false;

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        protected void Fail(string message)
        {
            CSVProcessor.DisplayError(ANSIHelper.Warning + message + ANSIHelper.White);
            if (_fail)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + _message + ANSIHelper.White);
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + _message + ANSIHelper.White);
            }

            if (_pause)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Press any key to continue ..." + ANSIHelper.White);
                Console.ReadKey(true);
            }
        }

        protected LINESCOPE ParseLineScope(string s)
        {
            switch (s.Trim().ToLower())
            {
                case "unknown":
                    return LINESCOPE.UNKNOWN;
                case "first":
                    return LINESCOPE.FIRST;
                case "all":
                    return LINESCOPE.ALL;
                case "last":
                    return LINESCOPE.LAST;
                case "allbutfirst":
                    return LINESCOPE.ALLBUTFIRST;
                case "allbutfirstandlast":
                    return LINESCOPE.ALLBUTFIRSTANDLAST;
            }
            return LINESCOPE.UNKNOWN;
        }

        protected void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, null, tracexml, prefix + "  ")));
                    }
                }
            }
        }

        protected Validation()
        {
        }

        public Validation(XmlNode n, CSVJob j, Validator v, bool tracexml, string prefix)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "message")
                    {
                        _message = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Message>" + _message + "</Message>");
                        }
                    }
                    else if (n1.Name.ToLower() == "condition")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Condition>");
                        }
                        // parse the if condition
                        ParseFilter(_condition, j, n1, tracexml, prefix + "    ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Condition>");
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "fail")
                    {
                        _fail = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Fail/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "pause")
                    {
                        _pause = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Pause/>");
                        }
                    }
                    else
                    {
                        bool found = false;

                        object[] o = { n1.Name.ToLower() };
                        // walk the type hierachy. from here up asking each class what values they support
                        Type t = this.GetType();
                        while (!found && t != typeof(Validation))
                        {
                            MethodInfo[] mi = t.GetMethods();
                            foreach (MethodInfo m in mi)
                            {
                                if (m.Name == "Supports" && m.IsStatic)
                                {
                                    try
                                    {
                                        if ((bool)m.Invoke(null, o))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                    }
                                }
                            }

                            t = t.BaseType;
                        }

                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Validation::Constructor Ignoring xml node : " + n1.Name + " when creating " + this.GetType().Name + ANSIHelper.White);
                        }
                    }
                }
            }

            // save the processor we belong to
            _v = v;
        }

        /// <summary>
        /// Get a list of supported Columns
        /// </summary>
        /// <returns></returns>
        public static List<string> SupportsCols()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if (t.BaseType.Name == "Validation")
                {
                    s.Add(t.Name);
                }
            }

            return s;
        }

        public static bool Supports(string s)
        {
            switch (s)
            {
                case "id":
                case "trace":
                    return true;
                default:
                    return false;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Id
        {
            get
            {
                return _id;
            }
        }

        public virtual bool Process(ColumnLine line, long linenumber, bool firstfile)
        {
            throw new ApplicationException("Derived classes from Validation must provide their own Process() implementation.");
        }

        public static Validation Create(XmlNode n, CSVJob j, Validator v, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                // now construct the object passing in our xml node
                Validation o = (Validation)Activator.CreateInstance(t, new object[] { n, j, v, tracexml, prefix });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("Validation::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
    }

    public class inputcolumncountvalidate : Validation
    {
        Validation.LINESCOPE _linescope = Validation.LINESCOPE.UNKNOWN;
        int _minvalue = int.MaxValue;
        int _maxvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "row":
                case "minvalue":
                case "maxvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public inputcolumncountvalidate(XmlNode node, CSVJob j, Validator v, bool tracexml, string prefix)
           : base(node, j, v, tracexml, prefix)
        {
            Name = "InputColumnCountValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "row")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <row>");
                            }
                            _linescope = ParseLineScope(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(_linescope.ToString() + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "minvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MinValue>");
                            }
                            try
                            {
                                _minvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("InputColumnCountValidate {" + Id + "} Error parsing minimum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MinValue>");
                            }
                        }
                        else if (n.Name.ToLower() == "maxvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MaxValue>");
                            }
                            try
                            {
                                _maxvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("InputColumnCountValidate {" + Id + "} Error parsing maximum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MaxValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating InputColumnCountValidate : {" + Id + "}", ex);
            }

            if (_linescope == Validation.LINESCOPE.UNKNOWN)
            {
                _linescope = Validation.LINESCOPE.ALL;
            }

            if (_maxvalue == int.MinValue && _minvalue != int.MaxValue)
            {
                _maxvalue = _minvalue;
            }

            if (_maxvalue != int.MinValue && _minvalue == int.MaxValue)
            {
                _minvalue = _maxvalue;
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(ColumnLine line, long linenumber, bool firstfile)
        {
            if (_condition != null && !_condition.Evaluate(line))
            {
                return true;
            }

            if ((_linescope == Validation.LINESCOPE.FIRST && linenumber == 1) ||
                (_linescope == Validation.LINESCOPE.ALLBUTFIRST && linenumber != 1) ||
                (_linescope == Validation.LINESCOPE.ALL))
            {
                if (_condition != null && !_condition.Evaluate(line))
                {
                    return true;
                }
                else if (line.Count < _minvalue || line.Count > _maxvalue)
                {
                    Fail("Validation:InputColumnCountValidate: {" + Id + "} failed cols (" + line.Count.ToString() + ") not between (" + _minvalue + " and " + _maxvalue + ") on line " + line.LineNumber + " in file " + line.FileName + " : " + line.Raw);
                    return !_fail;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class inputcolumnvaluesvalidate : Validation
    {
        Validation.LINESCOPE _linescope = Validation.LINESCOPE.UNKNOWN;
        List<string> _values = new List<string>();
        int _col = 0;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "row":
                case "values":
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public inputcolumnvaluesvalidate(XmlNode node, CSVJob j, Validator v, bool tracexml, string prefix)
           : base(node, j, v, tracexml, prefix)
        {
            Name = "InputColumnValuesValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "row")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Row>");
                            }
                            _linescope = ParseLineScope(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(_linescope.ToString() + "  </Row>");
                            }
                        }
                        else if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            try
                            {
                                _col = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("InputColumnValuesValidate {" + Id + "} Error parsing column number.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(_linescope.ToString() + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "values")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Values>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "value")
                                    {
                                        _values.Add(n1.InnerText.Trim().ToLower());
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Value>" + n1.InnerText.Trim().ToLower() + "</Value>");
                                        }
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Values>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating InputColumnValuesValidate : {" + Id + "}", ex);
            }

            if (_linescope == Validation.LINESCOPE.UNKNOWN)
            {
                _linescope = Validation.LINESCOPE.ALL;
            }

            if (_values.Count == 0)
            {
                _values.Add("");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(ColumnLine line, long linenumber, bool firstfile)
        {
            if ((_linescope == Validation.LINESCOPE.FIRST && linenumber == 1) ||
                (_linescope == Validation.LINESCOPE.ALLBUTFIRST && linenumber != 1) ||
                (_linescope == Validation.LINESCOPE.ALL))
            {
                if (_condition != null && !_condition.Evaluate(line))
                {
                    return true;
                }
                else if (_values.Contains(line[_col].Trim().ToLower()))
                {
                    return true;
                }
                else
                {
                    Fail("Validation:InputColumnValuesValidate: {" + Id + "} failed on line " + line.LineNumber + " in file " + line.FileName + " : " + line.Raw);
                    return !_fail;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class conditionvalidate : Validation
    {
        Validation.LINESCOPE _linescope = Validation.LINESCOPE.UNKNOWN;
        protected LogicFilterNode _conditionvalidate = new AndFilterNode(); // holds the default AND filter

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "row":
                case "conditionvalidate":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public conditionvalidate(XmlNode node, CSVJob j, Validator v, bool tracexml, string prefix)
           : base(node, j, v, tracexml, prefix)
        {
            Name = "ConditionValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "row")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Row>");
                            }
                            _linescope = ParseLineScope(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(_linescope.ToString() + "  </Row>");
                            }
                        }
                        else if (n.Name.ToLower() == "conditionvalidate")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ConditionValidate>");
                            }
                            try
                            {
                                ParseFilter(_conditionvalidate, j, n, tracexml, prefix + "    ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("InputColumnValuesValidate {" + Id + "} Error parsing column number.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(_linescope.ToString() + "  </ConditionValidate>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating ConditionValidate : {" + Id + "}", ex);
            }

            if (_linescope == Validation.LINESCOPE.UNKNOWN)
            {
                _linescope = Validation.LINESCOPE.ALL;
            }

            if (_conditionvalidate == null)
            {
                _conditionvalidate = new AndFilterNode();
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(ColumnLine line, long linenumber, bool firstfile)
        {
            if ((_linescope == Validation.LINESCOPE.FIRST && linenumber == 1) ||
                (_linescope == Validation.LINESCOPE.ALLBUTFIRST && linenumber != 1) ||
                (_linescope == Validation.LINESCOPE.ALL))
            {
                if (_condition != null && !_condition.Evaluate(line))
                {
                    return true;
                }
                else if (_conditionvalidate.Evaluate(line))
                {
                    return true;
                }
                else
                {
                    Fail("Validation:ConditionValidate: {" + Id + "} failed on line " + line.LineNumber + " in file " + line.FileName + " : " + line.Raw);
                    return !_fail;
                }
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    /// <summary>
    /// This class represents a process of a CSV file.
    /// </summary>
    public class Validator
    {
        #region Member Variables
        JobParameters _jp = null; // holds the output file specific parameters
        CSVJob _j = null;
        string _id = string.Empty; // Id of the validator
        List<Validation> _validations = new List<Validation>();
        int _failed = 0;
        #endregion

        #region Accessors
        /// <summary>
        /// Get the id of this process
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
        }

        public Input Input
        {
            get
            {
                return _j.Input;
            }
        }

        /// <summary>
        /// Get the prcoess name
        /// </summary>
        public string Name
        {
            get
            {
                return Id;
            }
        }

        /// <summary>
        /// Get the process result code
        /// </summary>
        public int Failed
        {
            get
            {
                int rc = 0;

                lock (this)
                {
                    rc = _failed;
                }

                return rc;
            }
        }

        public CSVJob Job
        {
            get
            {
                return _j;
            }
        }

        public JobParameters JobParameters
        {
            get
            {
                return _jp;
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Notify that we have a new file
        /// </summary>
        void NewFile()
        {
        }

        /// <summary>
        /// Process an input line
        /// </summary>
        /// <param name="l">Line to process</param>
        /// <param name="line">Line number</param>
        /// <param name="firstfile">First file flag</param>
        public int ProcessLine(ColumnLine l, long line, bool firstfile)
        {
            // TODO this is where we need tokenise do real work
            foreach (Validation v in _validations)
            {
                try
                {
                    if (!v.Process(l, line, firstfile))
                    {
                        return 9;
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException("Validator encountered error on Input line " + line.ToString() + " : " + l.Raw, e);
                }
            }

            return 0;
        }

        /// <summary>
        /// This function is called after all input records have been processed to close up the file
        /// </summary>
        void Close()
        {
            // if we did not fail
            if (Failed == 0)
            {
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a CSVProcess object
        /// </summary>
        /// <param name="forceSTDOUT">Force output to stdout</param>
        /// <param name="n">XML node to create it from</param>
        public Validator(XmlNode n, bool tracexml, string prefix, JobParameters jobParameters, CSVJob j)
        {
            // save the job parameters
            _jp = jobParameters;
            _j = j;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Validate>");
            }

            // process each node
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else
                    {
                        _validations.Add(Validation.Create(n1, j, this, tracexml, prefix + "  "));
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Validator>");
            }
        }
        #endregion

        public string DotName
        {
            get
            {
                return ("Validator_" + Id).Replace(" ", "").Replace(".", "").Replace("\\", "");
            }
        }
        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph validator_" + Id + " { color=lightgrey; label = \"" + Id + "\";");

            foreach (Validation v in _validations)
            {
                v.WriteDot(dotWriter, prefix + "   ");
            }

            dotWriter.WriteLine(prefix + "   }");
        }

    }
}
