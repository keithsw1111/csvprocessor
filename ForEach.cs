﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Diagnostics;

namespace CSVProcessor
{
    abstract class ForEachBase
    {
        protected CSVProcess _p = null;

        public ForEachBase(CSVJob j, CSVProcess p)
        {
            _p = p;
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        public abstract ColumnLine GenerateFirst(ColumnLine line);
        public abstract ColumnLine GenerateNext(ColumnLine line);

        public static ForEachBase Create(CSVJob j, CSVProcess p, XmlNode n, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                // now construct the object passing in our xml node
                ForEachBase o = (ForEachBase)Activator.CreateInstance(t, new object[] { j, p, n, tracexml, prefix });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("ForEachBase::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
    }

    class datebetween : ForEachBase
    {
        #region Member Variables
        DateTime _start = DateTime.MinValue;
        DateTime _end = DateTime.MinValue;
        string _informat = "dd/MM/yyyy";
        string _outformat = "dd/MM/yyyy";
        ColumnNumberOrColumn _startdatecol = null;
        ColumnNumberOrColumn _enddatecol = null;
        int _dayincrement = 1;
        isdatea _isdatea = null;
        string _id = string.Empty;
        DateTime _current = DateTime.MinValue;
        #endregion

        #region Constructor
        public datebetween(CSVJob j, CSVProcess p, XmlNode n, bool tracexml, string prefix) : base(j, p)
        {
            string start = string.Empty;
            string end = string.Empty;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<DateBetween>");
            }

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "start")
                    {
                        start = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Start>" + start + "</Start>");
                        }
                    }
                    else if (n1.Name.ToLower() == "end")
                    {
                        end = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <End>" + end + "</End>");
                        }
                    }
                    else if (n1.Name.ToLower() == "dayincrement")
                    {
                        _dayincrement = Int32.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DayIncrement>" + _dayincrement.ToString() + "</DayIncrement>");
                        }
                    }
                    else if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</InFormat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "outformat")
                    {
                        _outformat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outformat + "</OutFormat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "startdatecolumn")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StartDateColumn>");
                        }
                        _startdatecol = new ColumnNumberOrColumn(n1, j, _p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </StartDateColumn>");
                        }
                    }
                    else if (n1.Name.ToLower() == "enddatecolumn")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EndDateColumn>");
                        }
                        _enddatecol = new ColumnNumberOrColumn(n1, j, _p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </EndDateColumn>");
                        }
                    }
                    else if (n1.Name.ToLower() == "isdatea")
                    {
                        _isdatea = new isdatea(n1, j, _p, tracexml, prefix + "   ");
                    }
                    else
                    {
                        throw new ApplicationException("Unknown ForEach node " + n1.Name);
                    }
                }
            }

            if (_startdatecol == null)
            {
                _start = ParseDate(start);
            }

            if (_enddatecol == null)
            {
                _end = ParseDate(end);
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</DatesBetween>");
            }
        }
        #endregion

        DateTime ParseDate(string date)
        {
            try
            {
                if (_informat == "JJjjj")
                {
                    if (date.Length != 5)
                    {
                        throw new ApplicationException("Date in not in JJjjj format");
                    }
                    return new DateTime(Convert.ToInt32(date.Substring(0, 2)), 1, 1).AddDays(Convert.ToDouble(date.Substring(3, 3)));
                }
                else if (_informat == "JJJJjjj")
                {
                    if (date.Length != 5)
                    {
                        throw new ApplicationException("Date in not in JJjjj format");
                    }
                    return new DateTime(Convert.ToInt32(date.Substring(0, 4)), 1, 1).AddDays(Convert.ToDouble(date.Substring(4, 3)));
                }
                else
                {
                    // parse the date/time
                    return DateTime.ParseExact(date, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error parsing date for DatesBetween '" + date + "' '" + _informat + "'", ex);
            }
        }

        #region Overrides
        public override ColumnLine GenerateFirst(ColumnLine line)
        {
            if (_startdatecol != null)
            {
                _start = ParseDate(_startdatecol.Value(line));
            }
            if (_enddatecol != null)
            {
                _end = ParseDate(_enddatecol.Value(line));
            }

            _current = _start;
            ColumnLine cl = new CSVFileLine(_current.ToString(_outformat), 0);
            cl.Parse();

            if (_isdatea != null)
            {
                while (!_isdatea.True(cl) && _current <= _end)
                {
                    _current = _current.AddDays(_dayincrement);
                    cl = new CSVFileLine(_current.ToString(_outformat), 0);
                    cl.Parse();
                }
            }

            if (_current <= _end)
            {
                ColumnLine ncl = line;
                ncl.AddEnd(_current.ToString(_outformat));

                return ncl;
            }
            else
            {
                return null;
            }
        }

        public override ColumnLine GenerateNext(ColumnLine line)
        {
            _current = _current.AddDays(_dayincrement);
            ColumnLine cl = new CSVFileLine(_current.ToString(_outformat), 0);
            cl.Parse();

            if (_isdatea != null)
            {
                while (!_isdatea.True(cl) && _current <= _end)
                {
                    _current = _current.AddDays(_dayincrement);
                    cl = new CSVFileLine(_current.ToString(_outformat), 0);
                    cl.Parse();
                }
            }

            if (_current <= _end)
            {
                ColumnLine ncl = line;
                ncl[ncl.Count - 1] = _current.ToString(_outformat);

                return ncl;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }

    class numberbetween : ForEachBase
    {
        #region Member Variables
        double _start = double.MinValue;
        double _end = double.MinValue;
        ColumnNumberOrColumn _startcol = null;
        ColumnNumberOrColumn _endcol = null;
        double _increment = 1;
        string _id = string.Empty;
        double _current = double.MinValue;
        bool _float = false;
        #endregion

        #region Constructor
        public numberbetween(CSVJob j, CSVProcess p, XmlNode n, bool tracexml, string prefix) : base(j, p)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<NumberBetween>");
            }

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "start")
                    {
                        _start = Double.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Start>" + _start + "</Start>");
                        }
                    }
                    else if (n1.Name.ToLower() == "end")
                    {
                        _end = Double.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <End>" + _end + "</End>");
                        }
                    }
                    else if (n1.Name.ToLower() == "increment")
                    {
                        _increment = Double.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Increment>" + _increment.ToString() + "</Increment>");
                        }
                    }
                    else if (n1.Name.ToLower() == "float")
                    {
                        _float = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Float/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "startcolumn")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StartColumn>");
                        }
                        _startcol = new ColumnNumberOrColumn(n1, j, _p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </StartColumn>");
                        }
                    }
                    else if (n1.Name.ToLower() == "endcolumn")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <EndColumn>");
                        }
                        _endcol = new ColumnNumberOrColumn(n1, j, _p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </EndColumn>");
                        }
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</NumberBetween>");
            }
        }
        #endregion

        #region Overrides
        public override ColumnLine GenerateFirst(ColumnLine line)
        {
            if (_startcol != null)
            {
                _start = Double.Parse(_startcol.Value(line));
            }
            if (_endcol != null)
            {
                _end = Double.Parse(_endcol.Value(line));
            }

            _current = _start;

            if (_current <= _end)
            {
                ColumnLine ncl = line;
                if (_float)
                {
                    ncl.AddEnd(_current.ToString());
                }
                else
                {
                    ncl.AddEnd(((int)_current).ToString());
                }

                return ncl;
            }
            else
            {
                return null;
            }
        }

        public override ColumnLine GenerateNext(ColumnLine line)
        {
            _current = _current + _increment;

            if (_current <= _end)
            {
                ColumnLine ncl = line;
                if (_float)
                {
                    ncl[ncl.Count - 1] = _current.ToString();
                }
                else
                {
                    ncl[ncl.Count - 1] = ((int)_current).ToString();
                }

                return ncl;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }

    class iteminlist : ForEachBase
    {
        #region Member Variables
        List<string> _list = new List<string>();
        string _id = string.Empty;
        int _current = -1;
        #endregion

        #region Constructor
        public iteminlist(CSVJob j, CSVProcess p, XmlNode n, bool tracexml, string prefix) : base(j, p)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<ItemInList>");
            }

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "listfile")
                    {
                        StringOrColumn fc = new StringOrColumn(n1, j, p, null, tracexml, prefix + "   ");
                        string file = fc.Value();

                        try
                        {
                            StreamReader sr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

                            string l = null; // holds a line from the input file

                            // read until we get to the end of the file
                            while ((l = sr.ReadLine()) != null)
                            {
                                _list.Add(l);
                            }

                            // close the file
                            sr.Close();
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error loading listfile " + file + " for ItemInList", ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "list")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <List>");
                        }
                        foreach (XmlNode n2 in n1.ChildNodes)
                        {
                            if (n2.NodeType != XmlNodeType.Comment)
                            {
                                if (n2.Name.ToLower() == "item")
                                {
                                    _list.Add(n2.InnerText);
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "     <Item>" + n2.InnerText + "</Item>");
                                    }
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </List>");
                        }
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</ItemInList>");
            }
        }
        #endregion
        #region Overrides
        public override ColumnLine GenerateFirst(ColumnLine line)
        {
            _current = 0;

            if (_current < _list.Count)
            {
                ColumnLine ncl = line;
                ncl.AddEnd(_list[_current]);

                return ncl;
            }
            else
            {
                return null;
            }
        }

        public override ColumnLine GenerateNext(ColumnLine line)
        {
            _current++;

            if (_current < _list.Count)
            {
                ColumnLine ncl = line;
                ncl[ncl.Count - 1] = _list[_current];

                return ncl;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }

    class ForEach
    {
        #region Member Variables
        ForEachBase _foreach = null; // out foreach that will generate the new lines
        CSVProcess _p = null; // owning process
        string _id = string.Empty; // for each Id
        ProcessParameters _fp = null;
        #endregion

        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            _foreach.WriteDot(dotWriter, prefix);
        }

        #region Constructors
        public ForEach(XmlNode n, CSVJob j, CSVProcess p, bool tracexml, string prefix, ProcessParameters fp)
        {
            // save the owning processor
            _p = p;
            _fp = fp;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<ForEach>");
            }

            // get the output name first so if we are disabled the output file name is displayed
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else
                    {
                        if (_foreach == null)
                        {
                            _foreach = ForEachBase.Create(j, _p, n1, tracexml, prefix + "  ");
                        }
                        else
                        {
                            throw new ApplicationException("Only one foreach type is supported {" + _id + "}");
                        }
                    }
                }
            }

            // if we found no parameters create one
            if (_foreach == null)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + "ForEach {" + _id + "} has no child type." + ANSIHelper.White);
                throw new ApplicationException("Foreach with no child type.");
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</ForEach>");
            }
        }
        #endregion

        #region PublicMethods
        public int ProcessLine(ColumnLine Line, long LineNumber, bool FirstFile)
        {
            int rc = 0;

            if (LineNumber < _fp.SkipNLines)
            {
                // do nothing
            }
            else
            {
                if (_foreach != null)
                {
                    ColumnLine curr = _foreach.GenerateFirst(Line);

                    while (rc == 0 && curr != null)
                    {
                        rc = _p.ProcessLine(curr, LineNumber, FirstFile);

                        curr = _foreach.GenerateNext(Line);
                    }
                }
            }

            return rc;
        }
        #endregion
    }
}
