﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;

namespace CSVProcessor
{
    #region Base Class for Output Formatters
    // Base class for formatting an output to a file or the like
    public class OutputFormatter
    {
        #region Member Variables
        protected string _id = string.Empty;      // the id of the outputformatter

        // these should not really be here - they rightly belong in DelimitedFormatter - but the code assumes they always exist
        protected char _delimiter = ',';
        protected char _quote = '\"';
        protected char _quoteescape = '\\';
        #endregion

        #region Accessors
        public char Delimiter
        {
            get
            {
                return _delimiter;
            }
        }
        public char Quote
        {
            get
            {
                return _quote;
            }
        }
        public char QuoteEscape
        {
            get
            {
                return _quoteescape;
            }
        }
        /// <summary>
        /// Get the id of the outputformatter
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
        }
        #endregion

        #region Abstract Methods
        public virtual string FormatHeader(ColumnLine line) { throw new NotImplementedException(); }

        public virtual string FormatHeader(string s) { throw new NotImplementedException(); }

        public virtual string Preamble() { throw new NotImplementedException(); }

        public virtual string Postamble() { throw new NotImplementedException(); }

        /// <summary>
        /// Format a column line for output
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public virtual string FormatOutput(ColumnLine line) { throw new NotImplementedException(); }

        /// <summary>
        /// Format a string for output
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public virtual string FormatOutput(string s) { throw new NotImplementedException(); }
        #endregion

        #region Constructors
        public OutputFormatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
        {
            // read out the configuration
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    bool found = false;

                    object[] o = { n3.Name.ToLower() };
                    // walk the type hierachy. from here up asking each class what values they support
                    Type t = this.GetType();
                    while (!found && t != typeof(OutputFormatter))
                    {
                        MethodInfo[] mi = t.GetMethods();
                        foreach (MethodInfo m in mi)
                        {
                            if (m.Name == "Supports" && m.IsStatic)
                            {
                                try
                                {
                                    if ((bool)m.Invoke(null, o))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                catch (System.Exception ex)
                                {
                                    throw new ApplicationException("Error checking support for parameter " + n3.Name + " on object " + t.Name, ex);
                                }
                            }
                        }

                        t = t.BaseType;
                    }

                    if (n3.Name.ToLower() == "id")
                    {
                        _id = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Static Methods
        public static OutputFormatter CreateDefault(CSVJob j, CSVProcess p)
        {
            // Mock up the XML
            XmlDocument doc = new XmlDocument();
            XmlNode n = doc.CreateNode(XmlNodeType.Element, "delimitedformatter", "");

            // create the input parser
            OutputFormatter of = OutputFormatter.Create(j, p, n, false, "");

            // warn the user
            CSVProcessor.DisplayError(ANSIHelper.Warning + "Using default OutputFomatter : " + of.GetType().Name + "\n" + ANSIHelper.White);

            return of;
        }

        public static OutputFormatter Create(CSVJob j, CSVProcess p, XmlNode n, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                if (t == null)
                {
                    throw new ApplicationException("OutputFormatter::Create - " + n.Name + " is not a valid OutputFormatter type.");
                }

                // now construct the object passing in our xml node
                OutputFormatter ip = (OutputFormatter)Activator.CreateInstance(t, new object[] { n, j, tracexml, prefix, p });
                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return ip;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("InputParser::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }

        public static bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "id":
                    return true;
                default:
                    return false;
            }
        }
        #endregion
    }
    #endregion

    class delimitedformatter : OutputFormatter
    {
        #region Overriden Functions
        public override string FormatHeader(ColumnLine line)
        {
            return FormatOutput(line);
        }
        public override string FormatHeader(string s)
        {
            return FormatOutput(s);
        }
        public override string FormatOutput(ColumnLine line)
        {
            return line.ToString(_quote, _delimiter, _quoteescape);
        }
        public override string FormatOutput(string s)
        {
            return s;
        }
        public override string Preamble()
        {
            return string.Empty;
        }
        public override string Postamble()
        {
            return string.Empty;
        }
        #endregion

        #region Constructors
        public delimitedformatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
            : base(n, j, tracexml, prefix, p)
        {
            // read out the configuration
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "delimiter")
                    {
                        // save the output delimiter to use for this file
                        try
                        {
                            if (n3.InnerText.Length == 0)
                            {
                                _delimiter = char.MaxValue;
                            }
                            else
                            {
                                _delimiter = n3.InnerText.Substring(0, 1)[0];

                                // If it is a \ then it is a special character
                                if (_delimiter == '\\')
                                {
                                    if (n3.InnerText.Substring(0, 2).ToLower() == "\\t")
                                    {
                                        _delimiter = '\t';
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error extracting output delimiter from parameters : " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Delimiter>" + _delimiter + "</Delimiter>");
                        }

                    }
                    else if (n3.Name.ToLower() == "nodelimiter")
                    {
                        _delimiter = char.MaxValue;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NoDelimiter/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "noquote")
                    {
                        _quote = char.MaxValue;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NoQuote/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "noquoteescape")
                    {
                        _quoteescape = char.MaxValue;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <NoQuoteEscape/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "quote")
                    {
                        // Output quote character
                        try
                        {
                            _quote = n3.InnerText.Substring(0, 1)[0];
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error extracting output quote from parameters : " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Quote>" + _quote + "</Quote>");
                        }

                    }
                    else if (n3.Name.ToLower() == "quoteescape")
                    {
                        // Output quote character escape
                        try
                        {
                            _quoteescape = n3.InnerText.Substring(0, 1)[0];
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error extracting output quote escape from parameters : " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <QuoteEscape>" + _quoteescape + "</QuoteEscape>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Static Methods
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "delimiter":
                case "quote":
                case "quoteescape":
                    return true;
                default:
                    return OutputFormatter.Supports(s);
            }
        }
        #endregion
    }

    class excelcsvformatter : delimitedformatter
    {
        #region Constructors
        public excelcsvformatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
            : base(n, j, tracexml, prefix, p)
        {
            bool tab = false;
            // read out the configuration
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "tab")
                    {
                        tab = true;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Tab/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "comma")
                    {
                        tab = false;

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Comma/>");
                        }
                    }

                }
            }

            _quoteescape = '"';
            if (tab)
            {
                _delimiter = '\t';
            }
            else
            {
                _delimiter = ',';
            }
            _quote = '"';
        }
        #endregion

        #region Public Static Methods
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "tab":
                case "comma":
                    return true;
                default:
                    // skip my base class as i dont want to support any of the values it supports but i do want to reuse the functions
                    return OutputFormatter.Supports(s);
            }
        }
        #endregion
    }


    class xmlformatter : OutputFormatter
    {
        #region Member Variables
        ColumnLine _header = null; // header is kept so we know the field tags to use
        string _rootnode = "XMLOut"; // The root node
        string _rownode = "RowOut"; // the row node
        #endregion

        #region Overriden Functions
        public override string FormatHeader(string s)
        {
            return string.Empty;
        }
        public override string FormatHeader(ColumnLine line)
        {
            _header = line;
            return string.Empty;
        }
        public override string Preamble()
        {
            return "<" + _rootnode + ">";
        }
        public override string Postamble()
        {
            return "</" + _rootnode + ">";
        }
        public override string FormatOutput(string s)
        {
            return "<string>" + s + "</string>";
        }
        public override string FormatOutput(ColumnLine line)
        {
            if (_header == null)
            {
                throw new ApplicationException("Trying to write an XML fomrat with no column headers.");
            }
            if (_header.Count < line.Count)
            {
                throw new ApplicationException("Trying to write an XML fomrat with too few column headers.");
            }
            string s = "<" + _rownode + ">";

            for (int i = 0; i < line.Count; i++)
            {
                s += "<" + _header[i] + ">" + line[i] + "</" + _header[i] + ">";
            }
            s += "</" + _rownode + ">";

            return s;
        }
        #endregion

        #region Constructors
        public xmlformatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
            : base(n, j, tracexml, prefix, p)
        {
            foreach (XmlNode n3 in n.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "rootnode")
                    {
                        _rootnode = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RootNode>" + _rootnode + "</RootNode>");
                        }
                    }
                    else if (n3.Name.ToLower() == "rownode")
                    {
                        _rownode = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RowNode>" + _rownode + "</RowNode>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Static Methods
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "rownode":
                case "rootnode":
                    return true;
                default:
                    return OutputFormatter.Supports(s);
            }
        }
        #endregion
    }

    class htmltableformatter : OutputFormatter
    {
        #region Overriden Functions
        public override string FormatHeader(string s)
        {
            return "<th><td>" + s + "</td></th>";
        }
        public override string FormatHeader(ColumnLine line)
        {
            string s = "<th>";

            foreach (string c in line.Items)
            {
                s += "<td>" + c + "</td>";
            }
            s += "</th>";

            return s;
        }
        public override string FormatOutput(ColumnLine line)
        {
            string s = "<tr>";

            foreach (string c in line.Items)
            {
                s += "<td>" + c + "</td>";
            }
            s += "</tr>";

            return s;
        }
        public override string Preamble()
        {
            return "<table>";
        }
        public override string Postamble()
        {
            return "</table>";
        }
        public override string FormatOutput(string s)
        {
            return "<tr><td>" + s + "</td></tr>";
        }
        #endregion

        #region Constructors
        public htmltableformatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
            : base(n, j, tracexml, prefix, p)
        {
        }
        #endregion

        #region Public Static Methods
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                default:
                    return OutputFormatter.Supports(s);
            }
        }
        #endregion
    }

    class textformatter : OutputFormatter
    {
        #region Overriden Functions
        public override string FormatHeader(string s)
        {
            return FormatOutput(s);
        }

        public override string FormatHeader(ColumnLine line)
        {
            return FormatOutput(line);
        }

        public override string Preamble()
        {
            return string.Empty;
        }

        public override string Postamble()
        {
            return string.Empty;
        }

        public override string FormatOutput(ColumnLine line)
        {
            string s = string.Empty;
            foreach (string c in line.Items)
            {
                s += c;
            }

            return s;
        }

        public override string FormatOutput(string s)
        {
            // dont do anything
            return s;
        }
        #endregion

        #region Constructors
        public textformatter(XmlNode n, CSVJob j, bool tracexml, string prefix, CSVProcess p)
            : base(n, j, tracexml, prefix, p)
        {
        }
        #endregion

        #region Public Static Methods
        public static new bool Supports(string s)
        {
            switch (s.ToLower())
            {
                default:
                    return OutputFormatter.Supports(s);
            }
        }
        #endregion
    }

    #region Un-implemented Output Formatters
    //class JSONFormatter : OutputFormatter
    //{
    //}
    #endregion
}
