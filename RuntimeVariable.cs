﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CSVProcessor
{
    class RuntimeVariable
    {
        static Dictionary<string, string> __vars = new Dictionary<string, string>();
        string _name = string.Empty;
        StringOrColumn _col = null;
        CSVProcess _p = null;
        OutputDefinition _od = null;

        public static string GetValue(string name)
        {
            if (!__vars.ContainsKey(name))
            {
                throw new ApplicationException("Unknown variable " + name);
            }
            return __vars[name];
        }
        public RuntimeVariable(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            _p = p;
            _od = od;

            try
            {
                // parse the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "var" || n.Name.ToLower() == "variable")
                        {
                            // decode the formula
                            _name = n.InnerText.ToLower();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Variable>" + _name + "</Variable>");
                            }
                        }
                        else if (n.Name.ToLower() == "value")
                        {
                            if (_col != null)
                            {
                                throw new ApplicationException("RuntimeVariable can have only one child node under values.");
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Value>");
                            }
                            _col = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Value>");
                            }
                        }
                    }
                }

                if (_name == string.Empty)
                {
                    throw new ApplicationException("RuntimeVariable must have a name");
                }
                if (systemvariable.IsReservedName(_name.ToLower()))
                {
                    throw new ApplicationException("RuntimeVariable must not have a reserved SystemVariable name");
                }
                if (_col == null)
                {
                    throw new ApplicationException("RuntimeVariable must have a <value>");
                }

                if (__vars.ContainsKey(_name))
                {
                    // don't add it
                }
                else
                {
                    __vars[_name] = string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" RuntimeVariable::Constructor ", ex);
            }
        }

        public string Value
        {
            get
            {
                return __vars[_name];
            }
        }

        public void Process(ColumnLine line)
        {
            __vars[_name] = _col.Value(line);
        }
    }
}
