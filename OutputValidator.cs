// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Queues;
using System.Reflection;

namespace CSVProcessor
{
    public class OutputValidation
    {
        protected OutputValidator _v = null;
        protected string _id = string.Empty; // a id for the column
        protected bool _trace = false; // trace instruction
        string _name = string.Empty;
        string _message = string.Empty;
        protected bool _fail = false;
        bool _pause = false;

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            Debug.Fail("This should never be reached.");
        }

        protected void Fail(string message)
        {
            CSVProcessor.DisplayError(ANSIHelper.Warning + message + ANSIHelper.White);
            if (_fail)
            {
                CSVProcessor.DisplayError(ANSIHelper.Error + _message + ANSIHelper.White);
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + _message + ANSIHelper.White);
            }

            if (_pause)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Press any key to continue ..." + ANSIHelper.White);
                Console.ReadKey(true);
            }
        }

        protected OutputValidation()
        {
        }

        public OutputValidation(XmlNode n, OutputValidator v, bool tracexml, string prefix)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "message")
                    {
                        _message = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Message>" + _message + "</Message>");
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "fail")
                    {
                        _fail = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Fail/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "pause")
                    {
                        _pause = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Pause/>");
                        }
                    }
                    else
                    {
                        bool found = false;

                        object[] o = { n1.Name.ToLower() };
                        // walk the type hierachy. from here up asking each class what values they support
                        Type t = this.GetType();
                        while (!found && t != typeof(OutputValidation))
                        {
                            MethodInfo[] mi = t.GetMethods();
                            foreach (MethodInfo m in mi)
                            {
                                if (m.Name == "Supports" && m.IsStatic)
                                {
                                    try
                                    {
                                        if ((bool)m.Invoke(null, o))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                    }
                                }
                            }

                            t = t.BaseType;
                        }

                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "OutputValidation::Constructor Ignoring xml node : " + n1.Name + " when creating " + this.GetType().Name + ANSIHelper.White);
                        }
                    }
                }
            }

            // save the processor we belong to
            _v = v;
        }

        /// <summary>
        /// Get a list of supported Columns
        /// </summary>
        /// <returns></returns>
        public static List<string> SupportsCols()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if (t.BaseType.Name == "OutputValidation")
                {
                    s.Add(t.Name);
                }
            }

            return s;
        }

        public static bool Supports(string s)
        {
            switch (s)
            {
                case "id":
                case "trace":
                    return true;
                default:
                    return false;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Id
        {
            get
            {
                return _id;
            }
        }

        public virtual bool Process(Output o)
        {
            throw new ApplicationException("Derived classes from OutputValidation must provide their own Process() implementation.");
        }

        public static OutputValidation Create(XmlNode n, CSVJob j, OutputValidator v, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                // now construct the object passing in our xml node
                OutputValidation o = (OutputValidation)Activator.CreateInstance(t, new object[] { n, j, v, tracexml, prefix });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("OutputValidation::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
    }

    public class minoutputrowsvalidate : OutputValidation
    {
        int _minvalue = int.MaxValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "minvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public minoutputrowsvalidate(XmlNode node, OutputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MinOutputRowsValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "minvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MinValue>");
                            }
                            try
                            {
                                _minvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing minimum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MinValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(Output o)
        {
            if (o.WriteCount < _minvalue)
            {
                Fail("OutputValidation:" + Name + ": {" + Id + "} failed files (" + o.WriteCount.ToString() + ") not greater than or equal to " + _minvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class maxoutputrowsvalidate : OutputValidation
    {
        int _maxvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "maxvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public maxoutputrowsvalidate(XmlNode node, OutputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MaxOutputRowsValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "maxvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MaxValue>");
                            }
                            try
                            {
                                _maxvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing maximum value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </MaxValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(Output o)
        {
            if (o.WriteCount > _maxvalue)
            {
                Fail("OutputValidation:" + Name + ": {" + Id + "} failed files (" + o.WriteCount.ToString() + ") not less than or equal to " + _maxvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    public class exactoutputrowsvalidate : OutputValidation
    {
        int _exactvalue = int.MinValue;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "exactvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public exactoutputrowsvalidate(XmlNode node, OutputValidator v, bool tracexml, string prefix)
           : base(node, v, tracexml, prefix)
        {
            Name = "MaxOutputFilesValidate";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "exactvalue")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ExactValue>");
                            }
                            try
                            {
                                _exactvalue = Int32.Parse(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException(Name + " {" + Id + "} Error parsing exact value.", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </ExactValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating " + Name + " : {" + Id + "}", ex);
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override bool Process(Output o)
        {
            if (o.WriteCount != _exactvalue)
            {
                Fail("OutputValidation:" + Name + ": {" + Id + "} failed files (" + o.WriteCount.ToString() + ") not equal to " + _exactvalue);
                return !_fail;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }

    /// <summary>
    /// This class represents a process of a CSV file.
    /// </summary>
    public class OutputValidator
    {
        #region Member Variables
        Output _o = null;
        string _id = string.Empty; // Id of the validator
        List<OutputValidation> _validations = new List<OutputValidation>();
        int _failed = 0;
        #endregion

        #region Accessors
        /// <summary>
        /// Get the id of this process
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
        }

        /// <summary>
        /// Get the prcoess name
        /// </summary>
        public string Name
        {
            get
            {
                return Id;
            }
        }

        /// <summary>
        /// Get the process result code
        /// </summary>
        public int Failed
        {
            get
            {
                int rc = 0;

                lock (this)
                {
                    rc = _failed;
                }

                return rc;
            }
        }

        public Output Output
        {
            get
            {
                return _o;
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Notify that we have a new file
        /// </summary>
        void NewFile()
        {
        }

        /// <summary>
        /// Process an input line
        /// </summary>
        /// <param name="l">Line to process</param>
        /// <param name="line">Line number</param>
        /// <param name="firstfile">First file flag</param>
        public int Process(Output o)
        {
            // TODO this is where we need tokenise do real work
            foreach (OutputValidation v in _validations)
            {
                try
                {
                    if (!v.Process(o))
                    {
                        return 9;
                    }
                }
                catch (Exception e)
                {
                    throw new ApplicationException("OutputValidator encountered error.", e);
                }
            }

            return 0;
        }

        /// <summary>
        /// This function is called after all input records have been processed to close up the file
        /// </summary>
        void Close()
        {
            // if we did not fail
            if (Failed == 0)
            {
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a CSVProcess object
        /// </summary>
        /// <param name="forceSTDOUT">Force output to stdout</param>
        /// <param name="n">XML node to create it from</param>
        public OutputValidator(XmlNode n, CSVJob j, bool tracexml, string prefix, Output o)
        {
            // save the job parameters
            _o = o;

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<OutputValidate>");
            }

            // process each node
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }
                    }
                    else
                    {
                        _validations.Add(OutputValidation.Create(n1, j, this, tracexml, prefix + "  "));
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</OutputValidate>");
            }
        }
        #endregion

        public string DotName
        {
            get
            {
                return ("OutputValidate_" + Id).Replace(" ", "").Replace(".", "").Replace("\\", "");
            }
        }
        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph outputvalidate_" + Id + " { color=lightgrey; label = \"" + Id + "\";");

            foreach (OutputValidation v in _validations)
            {
                v.WriteDot(dotWriter, prefix + "   ");
            }

            dotWriter.WriteLine(prefix + "   }");
        }
    }
}
