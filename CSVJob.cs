// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Queues;

namespace CSVProcessor
{
    /// <summary>
    /// This class represents a process of a CSV file.
    /// </summary>
    public class CSVJob
    {
        #region Member Variables
        TextWriter _twReject = null; // stream to write rejects to
        JobParameters _p = null; // holds the job parameter settings
        FilterVars _fv = null; // holds the filter variables that get processed before the line is processed by individual processors
        List<CSVProcess> _processors = new List<CSVProcess>(); // list of processors processing the input records
        Validator _validator = null;
        Input _input = null; // the input to read from
        Thread _thread = null; // Thread this job runs on
        BlockingQueue _mq = null; // queue to use for multithreaded running
        string _currentfile = string.Empty; // current input file
        int _fileCount = 0;
        int _inputLineCount = 0;
        string _id = string.Empty; // id of the job
        bool _noprocesses = false; // true if there are no processes to run
        int _rc = 0;
        int _unique = UniqueIdClass.Allocate();
        DateTime _startTime = DateTime.Now;
        #endregion

        public DateTime StartTime
        {
            get
            {
                return _startTime;
            }
        }

        public string UniqueId
        {
            get
            {
                return "clusterJ" + _unique.ToString();
            }
        }

        public void WriteDot(StreamWriter dotWriter, string parameterFile)
        {
            dotWriter.WriteLine("digraph CSVProcessor {");
            dotWriter.WriteLine("   compound=true;");

            dotWriter.WriteLine("   subgraph " + UniqueId + " {");
            dotWriter.WriteLine("   shape=rect;");
            dotWriter.WriteLine("   label=\"Job::" + Id + "\";");
            dotWriter.WriteLine("   color=lightgrey;");
            dotWriter.WriteLine("   style=filled;");

            dotWriter.WriteLine("   node [shape=Msquare];");
            string lastkey = string.Empty;
            foreach (string s in _p.PromptVariables.Keys)
            {
                dotWriter.WriteLine("   " + s + " [label=\"PromptVariable::" + s + "\"];");
                if (lastkey != string.Empty)
                {
                    dotWriter.WriteLine(lastkey + " -> " + s + ";");
                }
                lastkey = s;
            }

            if (_p.RejectFile != string.Empty)
            {
                dotWriter.WriteLine("   node [shape=cds];");
                dotWriter.WriteLine("   \"" + _p.RejectFile + "\" [label=\"RejectFile::" + _p.RejectFile + "\"];");
                dotWriter.WriteLine(_input.UniqueId + " -> \"" + _p.RejectFile + "\";");
            }

            if (_input != null)
            {
                dotWriter.WriteLine("   node [shape=Mdiamond];");
                _input.WriteDot(dotWriter, "   ");
            }

            if (_validator != null)
            {
                dotWriter.WriteLine("   node [shape=rect];");
                _validator.WriteDot(dotWriter, "   ");
                dotWriter.WriteLine(_input.UniqueId + " -> " + _validator.DotName + ";");
            }
            if (_fv != null)
            {
                dotWriter.WriteLine("   node [shape=rect];");
                _fv.WriteDot(dotWriter, "   ");
            }

            dotWriter.WriteLine("   node [shape=rect];");
            foreach (CSVProcess pp in _processors)
            {
                pp.WriteDot(dotWriter, "   ");
                dotWriter.WriteLine(_input.UniqueId + " -> " + pp.FirstUniqueId + "[lhead=" + pp.UniqueId + "];");
            }

            dotWriter.WriteLine("   }");
            dotWriter.WriteLine("}");
        }

        #region Constructor
        /// <summary>
        /// Create a job
        /// </summary>
        /// <param name="n">XML node with configuration</param>
        /// <param name="files">files to process</param>
        /// <param name="tracexml">true if we are to trace the config</param>
        public CSVJob(XmlNode n, List<string> files, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  <Job>");
            }

            // must process the parameters first
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "jobparameters")
                    {
                        _p = new JobParameters(this, n1, tracexml, prefix + "    ");
                    }
                }
            }

            // create default job parameters
            if (_p == null)
            {
                _p = new JobParameters();
            }

            // now process the rest of the configuration
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "filtercolumns")
                    {
                        _fv = new FilterVars(this, n1, tracexml, prefix + "    ");
                    }
                    else if (n1.Name.ToLower() == "input")
                    {
                        _input = Input.Create(this, n1, tracexml, prefix + "    ", files);
                        _input.PostCreate(n1, tracexml, prefix + "      ");
                    }
                    else if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "jobparameters")
                    {
                        // already processed this
                    }
                    else if (n1.Name.ToLower() == "validate")
                    {
                        _validator = new Validator(n1, tracexml, prefix + "    ", _p, this);
                    }
                    else if (n1.Name.ToLower() == "process")
                    {
                        // will process this separately
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "CSVJob::Constructor Ignoring xml node : " + n1.Name + "\n" + ANSIHelper.White);
                    }
                }
            }

            if (_input == null)
            {
                throw new ApplicationException("Job has no input.");
            }

            // now create the processors
            _processors = CSVProcess.Create(n, tracexml, "  ", _p, this);

            // check that at least one process is enabled
            bool ok = false;
            foreach (CSVProcess pp in _processors)
            {
                if (pp.ProcessParameters.Disabled == false)
                {
                    ok = true;
                }
            }
            if (!ok)
            {
                _noprocesses = true;
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  </Job>");
            }

            // If a reject file is specified in the parameters and one was not opened using the command line open it.
            if (_p.RejectFile != string.Empty && _twReject == null)
            {
                _twReject = new StreamWriter(_p.RejectFile, false);
            }

            // Create a queue
            _mq = new BlockingQueue("CSVProcessor_Job", Id, _p.QueueLength);

            // create performance counters
            Performance.CreateCounterInstance("CSVProcessor_Job", "Input Lines", Id);
        }
        #endregion

        #region Accessors

        public int InputLineCount
        {
            get
            {
                return _inputLineCount;
            }
        }
        public int FileCount
        {
            get
            {
                return _fileCount;
            }
        }
        public int RC
        {
            get
            {
                return _rc;
            }
        }

        public Input Input
        {
            get
            {
                return _input;
            }
        }

        public JobParameters JobParameters
        {
            get
            {
                return _p;
            }
        }

        /// <summary>
        /// Get the id of the job
        /// </summary>
        public string Id
        {
            get
            {
                if (_id == string.Empty)
                {
                    if (_input == null)
                    {
                        Debug.Fail("Input was null when trying to get the job id. This is a coding error.");
                    }
                    return _input.Name;
                }
                else
                {
                    return _id;
                }
            }
        }

        /// <summary>
        /// Get the job name
        /// </summary>
        public string Name
        {
            get
            {
                return Id;
            }
        }
        #endregion

        #region Thread Functions
        /// <summary>
        /// Start the job thread
        /// </summary>
        public void Start()
        {
            // dont start unless we have some processes
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create the thread
            _thread = new Thread(new ThreadStart(Run));

            // give it a name
            _thread.Name = "Job:" + Id;

            // start it
            _thread.Start();

            // Sleep to give it a chance to run
            Thread.Sleep(0);
        }

        /// <summary>
        /// Wait for a job to finish processing
        /// </summary>
        public void Wait()
        {
            Debug.WriteLine("Waiting for job {" + Id + "}to end");

            // wait for the thread to finish
            if (_thread != null)
            {
                _thread.Join();
            }

            Debug.WriteLine("Job {" + Id + "}ended");
        }

        /// <summary>
        /// Job thread function
        /// </summary>
        void Run()
        {
            Debug.WriteLine("Job {" + Id + "} Starting.");

            Timer overalltimer = new Timer();
            overalltimer.Reset();
            Timer FileTimer = new Timer(); // time file processing
            FileTimer.Reset(); // zero the time
            int fileReject = 0; // file rejected records
            int overallReject = 0; // overall reject count

            // display the disabled processors
            foreach (CSVProcess p in _processors)
            {
                if (p.ProcessParameters.Disabled)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Information + p.File + " Disabled.\n" + ANSIHelper.White);
                }
            }

            bool fContinue = true; // continue flag

            // start the input thread
            if (_input != null)
            {
                _input.Start();
                Thread.Sleep(0);
            }
            else
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + "No input found.\n" + ANSIHelper.White);
                fContinue = false;
            }

            // loop processing messages
            while (fContinue)
            {
                // get the next message
                CSVMessage msg = (CSVMessage)_mq.Dequeue();

                switch (msg.Status)
                {
                    case CSVMessage.STATUS.NEWFILE:
                        // wait for processes to finish with this file 
                        foreach (CSVProcess p in _processors)
                        {
                            p.Synch();
                        }

                        // if this is not the first file
                        if (_fileCount > 0)
                        {
                            // now write the process stats
                            foreach (CSVProcess p in _processors)
                            {
                                if (!p.ProcessParameters.Disabled)
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Green + "Processor wrote " + p.WriteCount.ToString() + " records out of " + _inputLineCount.ToString() + " to file " + p.File + " Result = " + p.Failed.ToString() + "\n" + ANSIHelper.White);
                                    p.NewFile(msg.String);
                                }
                            }

                            // display processed stats
                            CSVProcessor.DisplayError(ANSIHelper.Timing + "File " + _currentfile + " processed in " + FileTimer.DurationAsString + "\n" + ANSIHelper.White);

                            // Display rejected records count
                            if (fileReject > 0)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Information + "Rejected records for file : " + fileReject.ToString() + "\n" + ANSIHelper.White);
                            }
                        }

                        // wait for all the new file messages
                        foreach (CSVProcess p in _processors)
                        {
                            p.Synch();
                        }

                        // reste out file specific counters
                        fileReject = 0;
                        _inputLineCount = 0;
                        _currentfile = msg.String;
                        _fileCount++;
                        FileTimer.Reset();
                        break;

                    case CSVMessage.STATUS.LINE:

                        // add to our line count
                        _inputLineCount++;

                        Performance.SetCounter("Input Lines", Id, _inputLineCount);

                        // if we are on the first line of the first file
                        // I changed the following line because it was not working
                        //if (fileCount == 0 && msg.LineNumber == 1 && _p.GivenTitles == null)
                        //if (fileCount == 1 && msg.LineNumber == 1 && _p.GivenTitles == null)

                        if (_p.RefreshColumnNamesForEachInputFile && msg.LineNumber == 1 && _p.GivenTitles == null)
                        {
                            ColumnNumber.SetColNames(this, msg.Line, true);
                        }
                        else if (_fileCount <= 1 && msg.LineNumber == 1 && _p.GivenTitles == null)
                        {
                            // save away the values as column headings so the user can refer to columns by name
                            ColumnNumber.SetColNames(this, msg.Line, false);
                        }

                        // copy the line
                        ColumnLine csv = msg.Line;

                        // apply any filter variables
                        if (_fv != null)
                        {
                            _fv.Process(csv);
                        }

                        // send the line to each process
                        if (_validator == null || _validator.ProcessLine(csv, msg.LineNumber, _fileCount == 1) == 0)
                        {
                            foreach (CSVProcess p in _processors)
                            {
                                p.Send(csv, msg.LineNumber, _fileCount == 1);
                            }
                        }
                        else
                        {
                            _rc++;

                            CSVProcessor.DisplayError(ANSIHelper.Error + "Stopping due to file validation failure.\n" + ANSIHelper.White);
                            fContinue = false;
                            _mq.Abort();
                        }
                        break;

                    case CSVMessage.STATUS.STOP:
                        Debug.WriteLine("Job {" + Id + "} received stop from input");

                        // stop processing messages
                        fContinue = false;
                        break;

                    case CSVMessage.STATUS.SYNCH:
                        Debug.WriteLine("Job {" + Id + "} received synch from input");

                        // let the sending thread know we processed the synch message
                        msg.Synch.Set();
                        break;

                    case CSVMessage.STATUS.MESSAGE:
                        CSVProcessor.DisplayError(msg.String);
                        break;

                    case CSVMessage.STATUS.PROGRESS:
                        CSVProcessor.DisplayError(PadString("\x1b[0GProgress : " + RightString(msg.String, 40) + " : " + msg.Long.ToString() + " : " + overalltimer.DurationAsString + "   ", 78));
                        break;

                    case CSVMessage.STATUS.PROGRESSPCT:
                        CSVProcessor.DisplayError(PadString("\x1b[0GProgress : " + RightString(msg.String, 40) + " : " + msg.Float.ToString("##0.0%") + " : " + overalltimer.DurationAsString + "   ", 78));
                        break;

                    case CSVMessage.STATUS.REJECT:

                        // write the reject
                        WriteReject(msg.String);

                        // increment our counters
                        fileReject++;
                        overallReject++;

                        // if we are to fail on reject
                        if (_p.FailOnReject)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Error + "Stopping due to rejected record.\n" + ANSIHelper.White);
                            fContinue = false;
                            _mq.Abort();
                            _rc++;
                        }
                        break;
                }
            }

            // wait for them to finish
            foreach (CSVProcess p in _processors)
            {
                p.Synch();
            }

            // Tell each of our processors we are done.
            foreach (CSVProcess p in _processors)
            {
                p.Stop();
            }

            // wait for them to finish
            foreach (CSVProcess p in _processors)
            {
                if (_rc == 0)
                {
                    _rc = p.Failed;
                }

                if (!p.ProcessParameters.Disabled)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Green + "Processor wrote " + p.WriteCount.ToString() + " records out of " + _inputLineCount.ToString() + " to file " + p.File + " Result = " + p.Failed.ToString() + "\n" + ANSIHelper.White);
                }

                p.Wait();
            }

            Debug.WriteLine("Job {" + Id + "} All processes are dead.");

            // display total rejects
            if (overallReject > 0)
            {
                CSVProcessor.DisplayError(ANSIHelper.Warning + "Total rejected records " + overallReject.ToString() + "\n" + ANSIHelper.White);
            }

            if (_rc == 0)
            {
                _rc = _input.RC;
            }

            Debug.WriteLine("Job {" + Id + "} Ending.");
        }
        #endregion

        #region Message Functions
        /// <summary>
        /// Send a stop message to the job thread
        /// </summary>
        public void Stop()
        {
            Debug.WriteLine("Job {" + Id + "} sending stop");

            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(CSVMessage.STATUS.STOP);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Send a new file thread to the job thread
        /// </summary>
        /// <param name="file"></param>
        public void NewFile(string file)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(file, CSVMessage.STATUS.NEWFILE);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Send a file line to the job thread
        /// </summary>
        /// <param name="l"></param>
        public void Send(ColumnLine l)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(l, l.LineNumber, false);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Send a rejected line to the job thread
        /// </summary>
        /// <param name="l"></param>
        public void Reject(string l)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(l, CSVMessage.STATUS.REJECT);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Report progress to the job thread
        /// </summary>
        /// <param name="l"></param>
        public void ReportProgress(string name, long l)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(l, CSVMessage.STATUS.PROGRESS);
            m.String = name;

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Report progress to the job thread
        /// </summary>
        /// <param name="f"></param>
        public void ReportProgressPct(string name, float f)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(f, CSVMessage.STATUS.PROGRESSPCT);
            m.String = name;

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Report a message to the job thread
        /// </summary>
        /// <param name="error"></param>
        public void ReportError(string error)
        {
            if (_noprocesses && _validator == null)
            {
                return;
            }

            // Create a message
            CSVMessage m = new CSVMessage(error, CSVMessage.STATUS.MESSAGE);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);
        }

        /// <summary>
        /// Synchronise processing with the job thread
        /// </summary>
        public void Synch()
        {
            if (_noprocesses && _validator == null || _mq.Aborted)
            {
                return;
            }

            Debug.WriteLine("Job {" + Id + "} sending synch");

            // create a synch event
            ManualResetEvent mre = new ManualResetEvent(true);
            mre.Reset();

            // create a message to send
            CSVMessage m = new CSVMessage(mre);

            // If the queue has not been created
            if (_mq == null)
            {
                // Sleep for a second to give it plenty of time to start
                Thread.Sleep(1000);
            }

            // If the queue has still not been created
            if (_mq == null)
            {
                // This is a big problem
                throw new ApplicationException("Can't send to non existent queue.");
            }

            // Enqueue the message
            _mq.Enqueue(m);

            // wait for it to be processed
            while (!_mq.Aborted && !mre.WaitOne(1000) && _thread != null && _thread.IsAlive)
            {
                // wait for the event or an abort
            }
            Debug.WriteLine("Job {" + Id + "} synch done");
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Write a reject record if required
        /// </summary>
        /// <param name="s">reject record</param>
        void WriteReject(string l)
        {
            // if we have a file write it to a file
            if (_twReject != null)
            {
                _twReject.WriteLine(l);
            }
        }

        /// <summary>
        /// Pad out a string to the specified number of characters
        /// </summary>
        /// <param name="s"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        string PadString(string s, int len)
        {
            string rc = s;

            while (rc.Length < len)
            {
                rc += " ";
            }

            return rc;
        }

        /// <summary>
        /// If string is larger than specified length take up to that many characters from the right
        /// </summary>
        /// <param name="s"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        string RightString(string s, int len)
        {
            if (s.Length > len)
            {
                return s.Substring(s.Length - len);
            }
            else
            {
                return s;
            }
        }
        #endregion
    }
}
