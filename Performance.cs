// Code last tidied up September 4, 2010

using System;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;

namespace CSVProcessor
{
    /// <summary>
    /// Performance monitoring and logging class.
    /// </summary>
    public class Performance
    {
        /// <summary>
        /// Get a high resolution time
        /// </summary>
        /// <param name="ticks"></param>
        [DllImport("Kernel32.dll")]
        public static extern void QueryPerformanceCounter(ref long ticks);

        #region Static Variables
        static Dictionary<string, CounterCreationDataCollection> __counters = new Dictionary<string, CounterCreationDataCollection>(); // counters to create
        static Dictionary<string, PerformanceCounter> __pcs = new Dictionary<string, PerformanceCounter>(); // our performance counters
        static bool _performance = true; // true if we are to update performance counters
        #endregion

        #region Static Functions

        public static void SuppressPerformanceCounters()
        {
            _performance = false;
        }

        /// <summary>
        /// Create a number of items counter
        /// </summary>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        /// <param name="help"></param>
        public static void CreateCounterDefinitionNumberOfItems(string category, string name, string instance, string help)
        {
            if (!_performance) return;

            // add our category if it does not exist
            if (!__counters.ContainsKey(category))
            {
                __counters.Add(category, new CounterCreationDataCollection());
            }

            // define the counter
            CounterCreationData counter = new CounterCreationData();
            counter.CounterName = name;
            counter.CounterHelp = help;
            counter.CounterType = PerformanceCounterType.NumberOfItems32;

            // add it to the list
            __counters[category].Add(counter);
        }

        /// <summary>
        /// Create an average timer counter
        /// </summary>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        /// <param name="help"></param>
        public static void CreateCounterDefinitionAverageTimer(string category, string name, string instance, string help)
        {
            if (!_performance) return;

            // if we dont have the category create it
            if (!__counters.ContainsKey(category))
            {
                __counters.Add(category, new CounterCreationDataCollection());
            }

            // define the counter
            CounterCreationData counter = new CounterCreationData();
            counter.CounterName = name;
            counter.CounterHelp = help;
            counter.CounterType = PerformanceCounterType.AverageTimer32;
            __counters[category].Add(counter);

            // and its base
            counter = new CounterCreationData();
            counter.CounterName = name + " Base";
            counter.CounterHelp = help + " Base";
            counter.CounterType = PerformanceCounterType.AverageBase;
            __counters[category].Add(counter);
        }

        /// <summary>
        /// Execute the performance counter definition creation
        /// </summary>
        /// <param name="delete"></param>
        public static void DoCreateCounterDefinitions(bool delete)
        {
            if (_performance)
            {
                // process each counter
                foreach (KeyValuePair<string, CounterCreationDataCollection> c in __counters)
                {
                    if (_performance)
                    {
                        try
                        {
                            // if we are to delete and recreate and it exists
                            if (delete && PerformanceCounterCategory.Exists(c.Key))
                            {
                                // delete the counter
                                try
                                {
                                    PerformanceCounterCategory.Delete(c.Key);
                                }
                                catch (SecurityException)
                                {
                                    // silently ignore security exception  
                                }
                            }

                            // create the counter if it does not exist
                            if (!PerformanceCounterCategory.Exists(c.Key))
                            {
                                try
                                {
                                    PerformanceCounterCategory.Create(c.Key, "CSV Processor counters", PerformanceCounterCategoryType.MultiInstance, c.Value);
                                }
                                catch (SecurityException ex)
                                {
                                    // assume all counters will fail so turn performance monitoring off
                                    _performance = false;
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Performance::Performance Counters Disabled : " + ex.Message + ANSIHelper.White);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            // any exception here can be ignored as it wont stop execution
                            _performance = false;
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Performance::Performance Counters Disabled : " + ex.Message + ANSIHelper.White);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create the counter instances
        /// </summary>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        public static void CreateCounterInstance(string category, string name, string instance)
        {
            if (_performance)
            {
                Debug.Assert(category != string.Empty);
                Debug.Assert(name != string.Empty);
                Debug.Assert(instance != string.Empty);

                PerformanceCounter counterinstance = null;

                try
                {
                    // create the counter
                    counterinstance = new PerformanceCounter();
                    counterinstance.CategoryName = category;
                    counterinstance.InstanceName = instance.Replace('\\', '-');
                    counterinstance.CounterName = name;
                    counterinstance.MachineName = ".";
                    counterinstance.InstanceLifetime = PerformanceCounterInstanceLifetime.Process;
                    counterinstance.ReadOnly = false;
                    counterinstance.RawValue = 0;
                }
                catch (Exception ex)
                {
                    // silently ignore security exception  
                    counterinstance = null;
                    _performance = false;
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "Performance::Performance Counters Disabled : " + category + ":" + name + " : " + ex.Message + ANSIHelper.White);
                }

                // create the key to store the counter under
                string key = name.ToLower() + ":" + instance.ToLower();
                Debug.WriteLine("Adding counter - " + key);

                // if it does not already exist create it ... some times counters dont have unique names
                if (!__pcs.ContainsKey(key))
                {
                    __pcs.Add(key, counterinstance);
                }
            }
        }

        /// <summary>
        /// Set a counter to a specific value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        /// <param name="value"></param>
        public static void SetCounter(string name, string instance, int value)
        {
            // find the counter and set the value
            if (_performance)
            {
                Debug.Assert(name != string.Empty);
                Debug.Assert(instance != string.Empty);
                Debug.Assert(__pcs.ContainsKey(name.ToLower() + ":" + instance.ToLower()));

                if (__pcs[name.ToLower() + ":" + instance.ToLower()] != null)
                {
                    __pcs[name.ToLower() + ":" + instance.ToLower()].RawValue = value;
                }
            }
        }

        /// <summary>
        /// Increment a counter by 1
        /// </summary>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        public static void Increment(string name, string instance)
        {
            // find the counter and increment it
            if (_performance)
            {
                Debug.Assert(name != string.Empty);
                Debug.Assert(instance != string.Empty);
                Debug.Assert(__pcs.ContainsKey(name.ToLower() + ":" + instance.ToLower()));

                if (__pcs[name.ToLower() + ":" + instance.ToLower()] != null)
                {
                    __pcs[name.ToLower() + ":" + instance.ToLower()].Increment();
                }
            }
        }

        /// <summary>
        /// Increment a counter by the specified value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        /// <param name="value"></param>
        public static void IncrementBy(string name, string instance, long value)
        {
            Debug.Assert(name != string.Empty);
            Debug.Assert(instance != string.Empty);
            Debug.Assert(__pcs.ContainsKey(name.ToLower() + ":" + instance.ToLower()));

            // find the counter and increment it
            if (_performance)
            {
                if (__pcs[name.ToLower() + ":" + instance.ToLower()] != null)
                {
                    __pcs[name.ToLower() + ":" + instance.ToLower()].IncrementBy(value);
                }
            }
        }

        /// <summary>
        /// Decrement a counter by 1
        /// </summary>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        public static void Decrement(string name, string instance)
        {
            // find the counter and decrement it
            if (_performance)
            {
                Debug.Assert(name != string.Empty);
                Debug.Assert(instance != string.Empty);
                Debug.Assert(__pcs.ContainsKey(name.ToLower() + ":" + instance.ToLower()));

                if (__pcs[name.ToLower() + ":" + instance.ToLower()] != null)
                {
                    __pcs[name.ToLower() + ":" + instance.ToLower()].Decrement();
                }
            }
        }

        /// <summary>
        /// Decrement a counter by the given value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="instance"></param>
        /// <param name="value"></param>
        public static void DecrementBy(string name, string instance, long value)
        {
            // find the counter and decrement it
            if (_performance)
            {
                Debug.Assert(name != string.Empty);
                Debug.Assert(instance != string.Empty);
                Debug.Assert(__pcs.ContainsKey(name.ToLower() + ":" + instance.ToLower()));

                if (__pcs[name.ToLower() + ":" + instance.ToLower()] != null)
                {
                    __pcs[name.ToLower() + ":" + instance.ToLower()].IncrementBy(value * -1);
                }
            }
        }

        /// <summary>
        /// Close the performance counters
        /// </summary>
        public static void Close()
        {
            // look at each counter
            foreach (KeyValuePair<string, PerformanceCounter> pc in __pcs)
            {
                // if there is a valid counter there
                if (pc.Value != null)
                {
                    // close it
                    pc.Value.Close();
                }
            }

            // clear the list
            __pcs.Clear();
        }

        #endregion
    }
}
