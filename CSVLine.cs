// Code last tidied up September 4, 2010

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;

namespace CSVProcessor
{
	#region Base Class
	/// <summary>
	/// This class generically represents a line from a file
	/// </summary>
	public class ColumnLine
	{
		#region Member Variables
		protected List<string> _items = new List<string>(); // holds the columns once parsed
		long _lineNumber = 0; // stores the lines line number from the input file
		protected string _raw = string.Empty; // stores the original raw line
		string _filename = string.Empty; // name of the file this line came from
		CSVFileInfo _fi = null;
		bool _data = true;
		#endregion

		public void OverrideLineNumber(long lineNumber)
		{
			_lineNumber = lineNumber;
		}

		public bool Data
		{
			get
			{
				return _data;
			}
			set
			{
				_data = value;
			}
		}

		#region Private Functions

		static int CountChar(string s, char c)
		{
			int i = 0;
			foreach (char cc in s)
			{
				if (cc == c)
				{
					i++;
				}
			}

			return i;
		}

		/// <summary>
		/// Utility function to wrap a column in quotes it it contains a delimiter
		/// </summary>
		/// <param name="s">Column value</param>
		/// <param name="outputquote">Quote character</param>
		/// <param name="outputdelimiter">Delimiter character</param>
		/// <returns></returns>
		static public string CSVSafe(string s, char outputquote, char outputdelimiter, char quoteescape)
		{
			string safe = s;

			// If we have an output delimiter
			if (outputdelimiter != char.MaxValue || outputquote != char.MaxValue)
			{
				// count number of quotes
				int quotecount = CountChar(safe, outputquote);

				// check if quote at start
				bool containsstartquote = false;
				if (safe.Length > 0 && safe[0] == outputdelimiter)
				{
					containsstartquote = true;
				}

				// check if quote at end
				bool containsendquote = false;
				if (safe.Length > 0 && safe[safe.Length - 1] == outputdelimiter && safe.Length != 1)
				{
					containsendquote = true;
				}

				// check if it contains a delimiter
				bool containsdelimiter = false;
				if (safe.IndexOf(outputdelimiter) != -1)
				{
					containsdelimiter = true;
				}

				if (quotecount == 0 && !containsdelimiter)
				{
					// does not need quoting
					return safe;
				}
				else if (quotecount == 2 && containsstartquote && containsendquote)
				{
					// already quoted
					return safe;
				}
				else if (quotecount == 0 && containsdelimiter)
				{
					// simple quoting only
					return outputquote + safe + outputquote;
				}
				else if (quotecount > 2 && containsstartquote && containsendquote)
				{
					// strip start and end quote then escape quotes then make it safe
					safe = safe.Substring(1, safe.Length - 2);
					safe = safe.Replace(outputquote.ToString(), quoteescape.ToString() + outputquote.ToString());
					return outputquote + safe + outputquote;
				}
				else if (quotecount > 0)
				{
					// escape the quotes then make it safe
					safe = safe.Replace(outputquote.ToString(), quoteescape.ToString() + outputquote.ToString());
					return outputquote + safe + outputquote;
				}
				else
				{
					throw new ApplicationException("ColumnLine.Safe ... unexpected combination of string attributes: " + safe);
				}

				// if the string already starts and ends with a quote
				//if (safe[0] == outputdelimiter && safe[safe.Length - 1] == outputdelimiter && safe.Length != 1)
				//{
				//   Debug.WriteLine("Column already surrounded by quotes ... no more added");
				//   // dont add more quotes
				//   return safe;
				//}

				// if the string contains a delimiter
				//if (safe.IndexOf(outputdelimiter) != -1 && outputquote != char.MaxValue)
				//{
				//	safe = safe.Replace(outputquote.ToString(), quoteescape.ToString() + outputquote.ToString());
				//	// quote the entire string
				//	return outputquote + safe + outputquote;
				//}

				// if the string contains a quote
				//if (safe.IndexOf(outputquote, 1) != -1 && outputquote != char.MaxValue)
				//{
				//    safe.Replace(outputquote.ToString(), quoteescape.ToString() + outputquote.ToString());
				//    // quote the entire string
				//    return outputquote + safe + outputquote;
				//}
			}

			// return the string
			return safe;
		}
		#endregion

		#region Public Functions

		/// <summary>
		/// return the columnline as a string
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return ToString('"', ',', '\\');
		}

		public string ToString(bool delimit)
		{
			if (delimit)
			{
				return ToString();
			}
			else
			{
				// Start with an empty string
				string res = string.Empty;

				// process each column
				foreach (string s in _items)
				{
					// add the column value
					res = res + s;
				}

				// return the string
				return res;
			}
		}
		/// <summary>
		/// Output the line as a string
		/// </summary>
		/// <param name="outputquote"></param>
		/// <param name="outputdelimiter"></param>
		/// <returns></returns>
		public string ToString(char outputquote, char outputdelimiter, char outputquoteescape)
		{
			// Start with an empty string
			string res = string.Empty;

			// start with the first column
			bool first = true;

			// process each column
			foreach (string s in _items)
			{
				// if not the first column add a delimiter
				if (!first && outputdelimiter != char.MaxValue)
				{
					res = res + outputdelimiter;
				}

				// add the column value
				res = res + CSVSafe(s, outputquote, outputdelimiter, outputquoteescape);

				// no longer the first
				first = false;
			}

			// return the string
			return res;
		}

		/// <summary>
		/// Insert a line at the beginning of this line
		/// </summary>
		/// <param name="line"></param>
		public void AddBeginning(ColumnLine line)
		{
			// create a new list of columns
			List<string> newItems = new List<string>();

			// Add the new items at the start
			foreach (string s in line.Cols)
			{
				newItems.Add(s);
			}

			// Add the old items at the end
			foreach (string s in _items)
			{
				newItems.Add(s);
			}

			// This is now the new items
			_items = newItems;
		}

		/// <summary>
		/// Add a column to the end
		/// </summary>
		/// <param name="val">the column value</param>
		public void AddEnd(string val)
		{
			// Add it
			_items.Add(val);
		}

		/// <summary>
		/// Add a column line to the end of this line
		/// </summary>
		/// <param name="cl"></param>
		public void AddEnd(ColumnLine cl)
		{
			// add the items to the end
			_items.AddRange(cl.Items);
		}

		/// <summary>
		/// Function that is overrriden to parse out columns
		/// 
		/// This is kept separate from the constructor so that the object gets created even if the
		/// parsing throws an exception
		/// </summary>
		public virtual void Parse()
		{
		}

		/// <summary>
		/// Function that is overrriden to parse out columns
		/// 
		/// This is kept separate from the constructor so that the object gets created even if the
		/// parsing throws an exception
		/// </summary>
		/// <param name="delim"></param>
		/// <param name="quote"></param>
		/// <param name="quoteescape"></param>
		public virtual void Parse(char delim, char quote, char quoteescape)
		{
		}

		#endregion

		#region Constructors
		/// <summary>
		/// Create an empty column line
		/// </summary>
		/// <param name="n">Number of the line</param>
		public ColumnLine(string l, long n, CSVFileInfo fi/*string filename*/)
		{
			// save the raw line
			_raw = l;

			// save the filename
			_fi = fi;

			// save the line
			_lineNumber = n;
		}

		public ColumnLine(long n, CSVFileInfo fi/*string filename*/)
		{
			// save the filename
			_fi = fi;

			// save the line
			_lineNumber = n;
		}

		public ColumnLine(long n, string filename)
		{
			// save the line
			_lineNumber = n;

			_filename = filename;
		}

		public ColumnLine(string l, long n, string filename)
		{
			// save the raw line
			_raw = l;

			// save the line
			_lineNumber = n;

			_filename = filename;
		}

		/// <summary>
		/// Create an empty column line
		/// </summary>
		/// <param name="n">Number of the line</param>
		public ColumnLine(string l)
		{
			// save the raw line
			_raw = l;
		}

		/// <summary>
		/// Create an empty column line
		/// </summary>
		public ColumnLine()
		{
			// create an empty line
		}

		/// <summary>
		/// Copy constructor
		/// </summary>
		/// <param name="l"></param>
		/// <param name="n"></param>
		public ColumnLine(ColumnLine l, long n)
		{
			// save the raw line
			_raw = l.ToString();

			// save the line number
			_lineNumber = n;

			// save the items
			_items.AddRange(l.Items);

			// save the filename
			_filename = l.FileName;
			_fi = l.FileInfo;
		}

		/// <summary>
		/// Copy constructor
		/// </summary>
		/// <param name="l"></param>
		public ColumnLine(ColumnLine l)
		{
			// save the raw line
			_raw = l.ToString();

			// save the line number
			_lineNumber = 0;

			// save the items
			_items = new List<string>(l.Items);

			// save the filename
			_filename = l.FileName;
			_fi = l.FileInfo;
		}

		#endregion

		#region Accessors
		/// <summary>
		/// Get this lines line number in the input file
		/// </summary>
		public long LineNumber
		{
			get
			{
				return _lineNumber;
			}
		}

		/// <summary>
		/// The name of the file this line comes from
		/// </summary>
		public string FileName
		{
			get
			{
				if (_fi != null)
				{
					return _fi.FullFileName;
				}
				else
				{
					return _filename;
				}
			}
		}

		public CSVFileInfo FileInfo
		{
			get
			{
				return _fi;
			}
		}

		/// <summary>
		/// Get the items
		/// </summary>
		public List<string> Items
		{
			get
			{
				return _items;
			}
		}

		/// <summary>
		/// Gets the original raw line
		/// </summary>
		public string Raw
		{
			get
			{
				return _raw;
			}
		}

		/// <summary>
		/// Returns a count of the number of columns found on the line
		/// </summary>
		public int Count
		{
			get
			{
				// return a count of parsed columns
				return _items.Count;
			}
		}

		/// <summary>
		/// Converts the columns to a simple string array
		/// </summary>
		public string[] Cols
		{
			get
			{
				// convert it
				return _items.ToArray();
			}
		}

		/// <summary>
		/// Provides access to a nominated column. Columns start at column 0
		/// </summary>
		public string this[int index]
		{
			get
			{
				// if we have no columns then probably raw ... so if they ask for zero give them raw
				if (_items.Count == 0 && index == 0)
				{
					return Raw;
				}
				else if (index == int.MaxValue - 1)
				{
					return Raw;
				}
				// if the requested column is greater than the number of columns we have the return a blank column
				else if (index == int.MaxValue || _items.Count <= index)
				{
					return "";
				}
				// otherwise return the column value
				else
				{
					return (string)_items[index];
				}
			}
			set
			{
				if (_items.Count <= index)
				{
					throw new ApplicationException("Can't set a column value larger than number of columns.");
				}
				// otherwise return the column value
				else
				{
					_items[index] = value;
				}
			}
		}

		/// <summary>
		/// Get a column
		/// </summary>
		public string this[ColumnNumber cn]
		{
			get
			{
				return this[cn.Number];
			}
			set
			{
				this[cn.Number] = value;
			}
		}
		#endregion
	}
	#endregion

	/// <summary>
	/// This class represents a line from a delimited text file.
	/// </summary>
	public class CSVFileLine : ColumnLine
	{
		#region Member Variables
		char id = ',';
		char iqe = '\\';
		char iq = '"';
		#endregion

		#region Code from CodeProject for parsing a line
		/// <summary>
		/// This function courtesy CodeProject.com
		/// Mandar Ranjit Date
		/// http://www.codeproject.com/KB/recipes/Basic_CSV_Parser_Function.aspx
		/// Some of the comments are mine
		/// </summary>
		/// <param name="strInputString">Line to parse</param>
		/// <returns>Array list of columns</returns>
		List<string> CSVParser(string strInputString)
		{
			int intCounter = 0;
			int intLength;
			StringBuilder strElem = new StringBuilder(1024);
			List<string> alParsedCsv = new List<string>();
			intLength = strInputString.Length;
			strElem = strElem.Append("");
			int intCurrState = 0;
			int[][] aActionDecider = new int[9][];

			//Build the state array
			// 0 =
			// 1 = add the char
			// 2 = 
			// 3 = add the char
			// 4 = add the char
			// 5 = end of line
			// 6 = error state
			// 7 =
			// 8 =
			aActionDecider[0] = new int[4] { 2, 0, 1, 5 };
			aActionDecider[1] = new int[4] { 6, 0, 1, 5 };
			aActionDecider[2] = new int[4] { 4, 3, 3, 6 };
			aActionDecider[3] = new int[4] { 4, 3, 3, 6 };
			aActionDecider[4] = new int[4] { 2, 8, 6, 7 };
			aActionDecider[5] = new int[4] { 5, 5, 5, 5 };
			aActionDecider[6] = new int[4] { 6, 6, 6, 6 };
			aActionDecider[7] = new int[4] { 5, 5, 5, 5 };
			aActionDecider[8] = new int[4] { 0, 0, 0, 0 };

			for (intCounter = 0; intCounter < intLength; intCounter++)
			{
				int inputid = GetInputID(strInputString[intCounter]);

				// Not debugged
				if (inputid == 99)
				{
					// if this is the last char in the line
					if (intCounter == intLength - 1)
					{
						// treat it like a normal char
						inputid = 2;
					}
					// if the next char is a quote
					else if (strInputString[intCounter + 1] == iq)
					{
						// move to next char and trest it as a normal char
						intCounter++;
						inputid = 2;
					}
					else
					{
						// just treat this char like a normal char
						inputid = 2;
					}
				}

				intCurrState = aActionDecider[intCurrState][inputid];

				//take the necessary action depending upon the state returned
				PerformAction(ref intCurrState, strInputString[intCounter], ref strElem, ref alParsedCsv);
			}
			intCurrState = aActionDecider[intCurrState][3];
			PerformAction(ref intCurrState, '\0', ref strElem, ref alParsedCsv);
			return alParsedCsv;
		}

		/// <summary>
		/// This function courtesy CodeProject.com
		/// Mandar Ranjit Date
		/// http://www.codeproject.com/KB/recipes/Basic_CSV_Parser_Function.aspx
		/// </summary>
		/// <param name="chrInput"></param>
		/// <returns></returns>
		private /* static */ int GetInputID(char chrInput)
		{
			if (chrInput == iq)
			{
				return 0;
			}
			else if (chrInput == id)
			{
				return 1;
			}
			else if (chrInput == iqe)
			{
				return 99;
			}
			else
			{
				return 2;
			}
		}

		/// <summary>
		/// This function courtesy CodeProject.com
		/// Mandar Ranjit Date
		/// http://www.codeproject.com/KB/recipes/Basic_CSV_Parser_Function.aspx
		/// </summary>
		/// <param name="intCurrState"></param>
		/// <param name="chrInputChar"></param>
		/// <param name="strElem"></param>
		/// <param name="alParsedCsv"></param>
		private static void PerformAction(ref int intCurrState, char chrInputChar, ref StringBuilder strElem, ref List<string> alParsedCsv)
		{
			string strTemp = null;
			switch (intCurrState)
			{
				case 0:
					//Seperate out value to array list
					strTemp = strElem.ToString();
					alParsedCsv.Add(strTemp);
					strElem = new StringBuilder(1024);
					break;
				case 1:
				case 3:
				case 4:
					//accumulate the character
					strElem.Append(chrInputChar);
					break;
				case 5:
					//End of line reached. Seperate out value to array list
					strTemp = strElem.ToString();
					alParsedCsv.Add(strTemp);
					break;
				case 6:
					//Erroneous input. Reject line.
					alParsedCsv.Clear();
					throw new ApplicationException("Error parsing line at character " + chrInputChar + " : Current column : " + strElem.ToString());
				case 7:
					//wipe ending " and Seperate out value to array list
					strElem.Remove(strElem.Length - 1, 1);
					strTemp = strElem.ToString();
					alParsedCsv.Add(strTemp);
					strElem = new StringBuilder(1024);
					intCurrState = 5;
					break;
				case 8:
					//wipe ending " and Seperate out value to array list
					strElem.Remove(strElem.Length - 1, 1);
					strTemp = strElem.ToString();
					alParsedCsv.Add(strTemp);
					strElem = new StringBuilder(1024);
					//goto state 0
					intCurrState = 0;
					break;
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Constructor. Parses the text line and saves the column values
		/// </summary>
		/// <param name="s">A line from the text file</param>
		/// <param name="n">The number of the line in the input file</param>
		public CSVFileLine(string s, long n, /*string filename*/CSVFileInfo fi) : base(s, n, /*filename*/fi)
		{
		}

		public CSVFileLine(string s, long n) : base(s, n, new CSVFileInfo())
		{
		}

		public CSVFileLine(ColumnLine s, long n) : base(s, n)
		{
		}

		public CSVFileLine(ColumnLine s) : base(s)
		{
		}

		public CSVFileLine() : base()
		{
		}
		#endregion

		#region Public Functions

		/// <summary>
		/// Parse out the columns
		/// </summary>
		public override void Parse(char delim, char quote, char quoteescape)
		{
			base.Parse(delim, quote, quoteescape);

			id = delim;
			iqe = quoteescape;
			iq = quote;

			// parse the string and save the columns
			_items = CSVParser(_raw);
		}

		public void RemoveColumn(ColumnNumber cn)
		{
			_items.RemoveAt(cn.Number);
		}

		public override void Parse()
		{
			base.Parse();

			if (_raw != null && _raw != string.Empty)
			{
				// parse the string and save the columns
				_items = CSVParser(_raw);
			}
		}
		#endregion
	}

}
