// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace CSVProcessor
{
    /// <summary>
    /// Overall parameters.
    /// </summary>
    public class JobParameters
    {
        #region Member Variables
        string _rejectfile = string.Empty; // the file to write rejected records to
        int _queuelength = 10000; // maximum number of lines in the queues
        Dictionary<string, string> _promptVars = new Dictionary<string, string>(); // prompt variables
        int _FreePctBeforeMoreReads = 80; // amount of free space in queue before we should read more
        bool _failonreject = false; // are we to fail if a record is rejected
        ColumnLine _givenColumnTitles = null;
        bool _rejectonunknowncolumn = false;
        bool _refreshcolumnnamesforeachinputfile = false;
        #endregion

        #region Accessors

        public Dictionary<string, string> PromptVariables
        {
            get
            {
                return _promptVars;
            }
        }

        public bool RejectOnUnknownColumn
        {
            get
            {
                return _rejectonunknowncolumn;
            }
        }

        public bool RefreshColumnNamesForEachInputFile
        {
            get
            {
                return _refreshcolumnnamesforeachinputfile;
            }
        }

        public ColumnLine GivenTitles
        {
            get
            {
                return _givenColumnTitles;
            }
        }

        /// <summary>
        /// Amount of free space in queue before we should read more to refill it
        /// </summary>
        public int FreePctBeforeMoreReads
        {
            get
            {
                return _FreePctBeforeMoreReads;
            }
        }

        /// <summary>
        /// Should we fail if we reject a record
        /// </summary>
        public bool FailOnReject
        {
            get
            {
                return _failonreject;
            }
        }

        /// <summary>
        /// File to write rejected records to
        /// </summary>
        public string RejectFile
        {
            get
            {
                return _rejectfile;
            }
        }

        /// <summary>
        /// Queue length to use when passing records to a independent thread
        /// </summary>
        public int QueueLength
        {
            get
            {
                return _queuelength;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Get a prompt variable.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string PromptVariable(string name)
        {
            // if we have it return the value
            if (_promptVars.ContainsKey(name.ToLower()))
            {
                return _promptVars[name.ToLower()];
            }
            else
            {
                // we did not have the prompt variable
                throw new ApplicationException("Prompt Variable " + name + " not defined.");
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Use default parameters
        /// </summary>
        public JobParameters()
        {
        }

        /// <summary>
        /// Create the parameters from the XML file
        /// </summary>
        /// <param name="doc">XML document to read</param>
        public JobParameters(CSVJob j, XmlNode node, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<JobParameters>");
            }

            // process the prompt variables first so they can be used inside parameters
            foreach (XmlNode n3 in node.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "promptvariables")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PromptVariables>");
                        }

                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                // prompt user for variable
                                if (n4.Name.ToLower() == "promptvariable")
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    <PromptVariable>");
                                    }

                                    string name = string.Empty;
                                    string message = string.Empty;
                                    string regex = string.Empty;
                                    string defaultValue = string.Empty;
                                    bool mask = false;

                                    foreach (XmlNode n5 in n4.ChildNodes)
                                    {
                                        if (n5.NodeType != XmlNodeType.Comment)
                                        {
                                            // read in the number of bytes to skip
                                            if (n5.Name.ToLower() == "name")
                                            {
                                                name = n5.InnerText.ToLower();
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Name>" + name + "</Name>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "message")
                                            {
                                                message = n5.InnerText;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Message>" + message + "</Message>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "mask")
                                            {
                                                mask = true;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Mask/>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "validationregex")
                                            {
                                                regex = n5.InnerText;
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <ValidationRegex>" + regex + "</ValidationRegex>");
                                                }
                                            }
                                            else if (n5.Name.ToLower() == "default")
                                            {
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      <Default>");
                                                }
                                                StringOrColumn sc = new StringOrColumn(n5, j, null, null, tracexml, prefix + "   ");
                                                defaultValue = sc.Value();
                                                if (tracexml)
                                                {
                                                    CSVProcessor.DisplayError(prefix + "      </Default>");
                                                }
                                            }
                                        }
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "    </PromptVariable>");
                                    }
                                    Regex r = null;
                                    try
                                    {
                                        r = new Regex(regex, RegexOptions.Singleline);
                                    }
                                    catch (Exception e)
                                    {
                                        throw new ApplicationException("JobParameters error parsing PromptVariable regex : " + regex, e);
                                    }
                                    PromptVariable pv = new PromptVariable(message, r, defaultValue, mask);

                                    CSVProcessor.DisplayError(ANSIHelper.Information + "Prompting for variable: " + name + " : " + message + "." + ANSIHelper.White);

                                    pv.ShowDialog(null);

                                    if (systemvariable.IsReservedName(name))
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + name + " cannot be used by PromptVariable. Internal value will be used." + ANSIHelper.White);
                                    }

                                    _promptVars[name] = pv.EnteredValue;
                                    if (!mask)
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Information + prefix + "   PromptVariable " + name + " = " + pv.EnteredValue + ANSIHelper.White);
                                    }
                                    else
                                    {
                                        string pwd = string.Empty;
                                        for (int i = 0; i < pv.EnteredValue.Length; i++)
                                        {
                                            pwd += "*";
                                        }
                                        CSVProcessor.DisplayError(ANSIHelper.Information + prefix + "   PromptVariable " + name + " = " + pwd + ANSIHelper.White);
                                    }
                                }
                                else
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "JobParameters::Constructor Ignoring xml node : " + n4.Name + ANSIHelper.White);
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </PromptVariables>");
                        }
                    }
                }
            }

            foreach (XmlNode n3 in node.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "freepctbeforemorereads")
                    {
                        try
                        {
                            _FreePctBeforeMoreReads = Convert.ToInt32(n3.InnerText);
                            if (_FreePctBeforeMoreReads < 0)
                            {
                                _FreePctBeforeMoreReads = 0;
                            }
                            else if (_FreePctBeforeMoreReads > 100)
                            {
                                _FreePctBeforeMoreReads = 100;
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FreePctBeforeMoreReads></FreePctBeforeMoreReads>");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing FreePctBeforeMoreReads. Value must be 0 - 100.", ex);
                        }
                    }
                    else if (n3.Name.ToLower() == "queuelength")
                    {
                        // read in the queue length
                        try
                        {
                            _queuelength = Convert.ToInt32(n3.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error reading QueueLength parameter : " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <QueueLength>" + _queuelength.ToString() + "</QueueLength>");
                        }
                    }
                    else if (n3.Name.ToLower() == "givencolumntitles")
                    {
                        try
                        {
                            _givenColumnTitles = new CSVFileLine(n3.InnerText, 0);
                            _givenColumnTitles.Parse();
                            ColumnNumber.SetColNames(j, _givenColumnTitles, false);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error reading GivenColumnTitles parameter : " + n3.InnerText, ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <GivenColumnTitles>" + n3.InnerText + "</GivenColumnTitles>");
                        }
                    }
                    else if (n3.Name.ToLower() == "priority")
                    {
                        switch (n3.InnerText.ToLower())
                        {
                            case "normal":
                                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Normal;
                                break;
                            case "abovenormal":
                                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.AboveNormal;
                                break;
                            case "belownormal":
                                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal;
                                break;
                            case "low":
                                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Idle;
                                break;
                            case "high":
                                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
                                break;
                            default:
                                throw new ApplicationException("Unknown process priority : " + n3.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "      <Priority>" + n3.InnerText + "</Priority>");
                        }
                        CSVProcessor.DisplayError(ANSIHelper.Information + "Process priority changed to " + Process.GetCurrentProcess().PriorityClass.ToString() + ANSIHelper.Normal);
                    }
                    else if (n3.Name.ToLower() == "rejectfile")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RejectFile>");
                        }
                        StringOrColumn sc = new StringOrColumn(n3, j, null, null, tracexml, prefix + "   ");
                        _rejectfile = sc.Value();
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </RejectFile>");
                        }
                    }
                    else if (n3.Name.ToLower() == "failonreject")
                    {
                        _failonreject = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <FailOnReject/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "rejectonunknowncolumn")
                    {
                        _rejectonunknowncolumn = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RejectUnknownColumn/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "refreshcolumnnamesforeachinputfile")
                    {
                        _refreshcolumnnamesforeachinputfile = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RefreshColumnNamesForEachInputFile/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "promptvariables")
                    {
                        // already processed
                    }
                    else if (n3.Name.ToLower() == "runtimevariable")
                    {
                        RuntimeVariable _var = new RuntimeVariable(n3, j, null, null, tracexml, prefix);
                        _var.Process(null);
                        // already processed
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "JobParameters::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                    }
                }
            }

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</JobParameters>");
            }
        }
        #endregion
    }
}
