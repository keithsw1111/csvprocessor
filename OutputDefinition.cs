//#define Framework35

using System;
using System.Xml;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Web;
using System.ComponentModel;
using Fesersoft.Hashing;
using System.Net.NetworkInformation;
using System.Globalization;

namespace CSVProcessor
{
    #region Exceptions
    class UseMultItemProcess : Exception
    {
    }
    #endregion

    #region OutputColumnProcess Class
    public class OutputColumnProcess
    {
        protected OutputDefinition _od = null; // the output definition which we need to access
        protected CSVProcess _p = null; // collection of owning processors
        protected string _id = string.Empty; // a id for the column
        protected bool _trace = false; // trace instruction
        bool _timePerformance = false;
        protected long starttime = 0;
        string _name = string.Empty;
        int _unique = UniqueIdClass.Allocate();

        public string UniqueId
        {
            get
            {
                return "C" + _unique.ToString();
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + Name + "::" + Id + "\"];");
        }

        /// <summary>
        /// Get a list of supported Columns
        /// </summary>
        /// <returns></returns>
        public static List<string> SupportsCols()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if (t.BaseType != null && (t.BaseType.Name == "OuputColumnProcess" || t.BaseType.Name == "Column"))
                {
                    if (t.Name != "Column")
                    {
                        s.Add(t.Name);
                    }
                }
            }

            return s;
        }

        public static bool Supports(string s)
        {
            switch (s)
            {
                case "id":
                case "trace":
                case "timeperformance":
                    return true;
                default:
                    return false;
            }
        }

        protected OutputColumnProcess()
        {
        }

        public OutputColumnProcess(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "id")
                    {
                        _id = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else if (n1.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<TimePerformance/>");
                        }
                    }
                    else
                    {
                        bool found = false;

                        object[] o = { n1.Name.ToLower() };
                        // walk the type hierachy. from here up asking each class what values they support
                        Type t = this.GetType();
                        while (!found && t != typeof(OutputColumnProcess))
                        {
                            MethodInfo[] mi = t.GetMethods();
                            foreach (MethodInfo m in mi)
                            {
                                if (m.Name == "Supports" && m.IsStatic)
                                {
                                    try
                                    {
                                        if ((bool)m.Invoke(null, o))
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("Error checking support for parameter " + n1.Name + " on object " + t.Name, ex);
                                    }
                                }
                            }

                            t = t.BaseType;
                        }

                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "OutputColumnProcess::Constructor Ignoring xml node : " + n1.Name + " when creating " + this.GetType().Name + ANSIHelper.White);
                        }
                    }
                }
            }

            if (_timePerformance && _id == "")
            {
                throw new ApplicationException("Column::Constructor <TimePerformance> requires <Id> to also be specified.");
            }

            // save the processor we belong to
            _p = p;

            // save the output definition that owns us
            _od = od;

            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_Column", "Column Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_Column", "Column Average Duration Base", Id);
            }
        }

        protected void PreProcess()
        {
            Performance.QueryPerformanceCounter(ref starttime);
        }

        protected void PostProcess()
        {
            if (_timePerformance)
            {
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                Performance.IncrementBy("Column Average Duration", Id, endtime - starttime);
                Performance.Increment("Column Average Duration Base", Id);
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Id
        {
            get
            {
                return _id;
            }
        }

        public void SetOutputDefinition(OutputDefinition od)
        {
            _od = od;
        }

        public virtual ColumnLine Process(ColumnLine line)
        {
            throw new ApplicationException("Derived classes from OutputColumnProcess must provide their own Process() implementation.");
        }

        /// <summary>
        /// This is the coolest function. It extracts from an xml node the name of a class derived from Column
        /// and creates an instance of it. If no node mentioning a class is found in the xml file then no
        /// instance of the class will be created.
        /// </summary>
        /// <param name="n">The node containing the name of the class to create</param>
        /// <returns></returns>
        public static OutputColumnProcess Create(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                if (t == null)
                {
                    throw new ApplicationException("OutputColumnProcess::Create - " + n.Name + " is not a valid outputcolumn type.");
                }


                // now construct the object passing in our xml node
                OutputColumnProcess o = (OutputColumnProcess)Activator.CreateInstance(t, new object[] { n, j, p, od, tracexml, prefix });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("OutputColumnProcess::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }

    }
    #endregion

    #region Column Classes

    #region Base Column Class
    /// <summary>
    /// This is the base class for an output column
    /// </summary>
    public class Column : OutputColumnProcess
    {
        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (Title == string.Empty)
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + Name + "::" + Id + "\"];");
            }
            else
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + Name + "::" + Title + "\"];");
            }
        }

        protected bool _safe = false; // when true the column contents do not need to be further wrapped in quotes

        protected string _title = string.Empty;

        public static Type Type
        {
            get
            {
                Column n = new Column();
                return n.GetType();
            }
        }

        #region Accessors
        /// <summary>
        /// Accesses flag indicating that column contents have already been made safe and quotes are not needed
        /// </summary>
        public bool Safe
        {
            get
            {
                return _safe;
            }
        }

        public virtual string Title
        {
            get
            {
                return _title;
            }
        }

        public override string Id
        {
            get
            {
                if (base.Id != string.Empty)
                {
                    return base.Id;
                }
                else
                {
                    return Title;
                }
            }
        }


        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "title":
                case "safe":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        // void constructor ... nothing to do
        public Column(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(n, j, p, od, tracexml, prefix)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "title")
                    {
                        _title = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Title>" + _title + "</Title>");
                        }
                    }
                    else if (n1.Name.ToLower() == "safe")
                    {
                        _safe = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Safe/>");
                        }
                    }
                }
            }
        }

        protected Column()
           : base()
        {
        }
        #endregion

        #region Public Functions

        public virtual ColumnLine ProcessMulti(ColumnLine line)
        {
            ColumnLine cl = new ColumnLine();
            return cl;
        }

        #endregion

        #region Static Functions

        public new static Column Create(XmlNode n, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
        {
            return (Column)OutputColumnProcess.Create(n, j, p, od, tracexml, prefix);
        }
        #endregion
    }

    #endregion

    #region Utility Classes

    public class DecodeTableEntry
    {
        ColumnLine _csv = null;
        string _s = string.Empty;
        bool _referenced = false;

        public DecodeTableEntry(ColumnLine csv)
        {
            _csv = csv;
        }
        public DecodeTableEntry(string s)
        {
            _s = s;
        }
        public object Value
        {
            get
            {
                if (_csv != null)
                {
                    return _csv;
                }
                else
                {
                    return _s;
                }
            }
        }
        public bool Referenced
        {
            get
            {
                return _referenced;
            }
            set
            {
                _referenced = value;
            }
        }
    }

    /// <summary>
    /// This class represents a decode table used by the decode column and lookup classes
    /// </summary>
    public class DecodeTable
    {
        static Dictionary<string, Dictionary<string, DecodeTableEntry>> __tables = new Dictionary<string, Dictionary<string, DecodeTableEntry>>(); // a hashtable of loaded tables. This allows us to reuse the same table if it is referenced multiple times.
        Dictionary<string, DecodeTableEntry> _table = new Dictionary<string, DecodeTableEntry>(); // Hashtable to code the codes
        static Dictionary<string, ColumnLine> __tableColumnNames = new Dictionary<string, ColumnLine>();
        string _default = string.Empty; // The default value for when a code is not found
        bool _useoriginal = false; // indicator to control if the default value is returned or the original value
        ColumnLine _columnNames = null;
        List<string> _keycols = null;

        #region Public Functions

        /// <summary>
        /// Check if the decode table contains this key
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Contains(string s)
        {
            return _table.ContainsKey(s.ToLower());
        }

        /// <summary>
        /// Finds the object associated with a code
        /// </summary>
        /// <param name="s">Code to lookup</param>
        /// <returns>Looked up value</returns>
        public object Decode(string s, bool fastfailblank)
        {
            if (s.Trim() == string.Empty && fastfailblank)
            {
                // if we are to return the original value
                if (_useoriginal)
                {
                    // return original value
                    return s;
                }
                else
                {
                    // return the default value
                    return _default;
                }
            }

            // If the code is in our hashtable
            if (_table.ContainsKey(s.ToLower().Trim()))
            {
                _table[s.ToLower()].Referenced = true;
                // return the decode value
                return _table[s.ToLower()].Value;
            }
            else
            {
                // if we are to return the original value
                if (_useoriginal)
                {
                    // return original value
                    return s;
                }
                else
                {
                    // return the default value
                    return _default;
                }
            }
        }

        public List<DecodeTableEntry> UnaccessedCodes
        {
            get
            {
                List<DecodeTableEntry> rc = new List<DecodeTableEntry>();

                foreach (KeyValuePair<string, DecodeTableEntry> kvp in _table)
                {
                    if (kvp.Value.Referenced == false)
                    {
                        rc.Add(kvp.Value);
                    }
                }

                return rc;
            }
        }
        #endregion

        #region Accessors
        public List<String> KeyCols
        {
            get
            {
                return _keycols;
            }
        }

        public string ColumnIndex(string col)
        {
            if (col.Trim() == "*") return "*";

            if (Regex.IsMatch(col.Trim(), "^[\\-0-9]+$"))
            {
                int index = Int32.Parse(col.Trim());
                if (index < 0)
                {
                    index = _columnNames.Count + index;
                    if (index < 0)
                    {
                        throw new ApplicationException("DecodeTable:ColumnIndex '" + col + "' is not valid for lookup file.");
                    }
                    else
                    {
                        return index.ToString();
                    }
                }
                else if (index > _columnNames.Count)
                {
                    throw new ApplicationException("DecodeTable:ColumnIndex '" + col + "' is not valid for lookup file.");
                }
                else
                {
                    return index.ToString();
                }
            }

            if (_columnNames == null)
            {
                throw new ApplicationException("DecodeTable:ColumnIndex Attempt to decode decode table column when no column names have been loaded.");
            }

            if (col.Trim().ToLower().StartsWith("colname:"))
            {
                string k = col.Trim().ToLower().Substring(col.Trim().IndexOf(":") + 1);
                for (int j = 0; j < _columnNames.Count; j++)
                {
                    if (_columnNames[j].ToLower() == k)
                    {
                        return j.ToString();
                    }
                }
                if (col.Trim().ToLower().StartsWith("colname:"))
                {
                    throw new ApplicationException("DecodeTable:ColumnIndex Lookup file key '" + col + "' is not valid for lookup file.");
                }
            }
            else
            {
                throw new ApplicationException("Lookup file key '" + col + "' is not valid for lookup file.");
            }

            Debug.Assert(false, "we should never get here.");
            return "0";
        }

        /// <summary>
        /// Flag to indicate if original value should be returned
        /// </summary>
        public bool UseOriginal
        {
            set
            {
                _useoriginal = value;
            }
            get
            {
                return _useoriginal;
            }
        }

        public string[] Keys
        {
            get
            {
                string[] keys = new string[_table.Keys.Count];
                _table.Keys.CopyTo(keys, 0);
                return keys;
            }
        }

        /// <summary>
        /// Sets the default value to return if the code is not found
        /// </summary>
        public string Default
        {
            set
            {
                _default = value;
            }
            get
            {
                return _default;
            }
        }
        #endregion

        #region Constructor
        public DecodeTable(string file, List<string> keys, bool dumpLookup, bool caseSensitive, char delimiter, char quote, char quoteescape)
        {
            // if we cant find our code we will return the key
            _useoriginal = true;

            _keycols = keys;

            // name the table using the file name and the key columns. This allows us to load it once
            string tablename = file.ToLower();
            foreach (string n in keys)
            {
                tablename += n.ToString();
            }

            // If we have already loaded this lookup file
            if (__tables.ContainsKey(tablename))
            {
                // copy it
                _table = __tables[tablename];
                _columnNames = __tableColumnNames[tablename];
            }
            else
            {
                // we have to read the file
                CSVProcessor.DisplayError(ANSIHelper.Information + "Reading lookup file " + file + ANSIHelper.White);

                Timer LoadTimer = new Timer();
                LoadTimer.Reset();

                // open it
                StreamReader sr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

                string l = null; // holds a line from the input file
                CSVFileInfo fi = new CSVFileInfo(new FileInfo(file));
                bool first = true;

                // read until we get to the end of the file
                while ((l = sr.ReadLine()) != null)
                {
                    try
                    {
                        // parse the line
                        ColumnLine csv = new CSVFileLine(l, 0, /*file*/fi);
                        csv.Parse(delimiter, quote, quoteescape);

                        if (first)
                        {
                            first = false;
                            _columnNames = csv;
                            CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " column names: " + _columnNames.ToString() + ANSIHelper.White);

                            for (int i = 0; i < keys.Count; i++)
                            {
                                if (Regex.IsMatch(keys[i].Trim(), "^[\\-0-9]+$"))
                                {
                                    int index = Int32.Parse(keys[i].Trim());
                                    if (index < 0)
                                    {
                                        index = _columnNames.Count + index;
                                        if (index < 0)
                                        {
                                            throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                        else
                                        {
                                            keys[i] = index.ToString();
                                        }
                                    }
                                    else if (index > _columnNames.Count)
                                    {
                                        throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                    }
                                }
                                else if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                {
                                    string k = keys[i].Trim().ToLower().Substring(keys[i].Trim().IndexOf(":") + 1);
                                    for (int j = 0; j < _columnNames.Count; j++)
                                    {
                                        if (_columnNames[j].ToLower() == k)
                                        {
                                            keys[i] = j.ToString();
                                            break;
                                        }

                                        if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                        {
                                            throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                }
                            }
                        }

                        // extract the line key
                        string key = string.Empty;
                        foreach (string cn in keys)
                        {
                            int index = Int32.Parse(cn.Trim());
                            if (index >= 0 && index < csv.Count)
                            {
                                key += csv[index].Trim();
                            }
                            if (!caseSensitive)
                            {
                                key = key.ToLower();
                            }
                        }

                        // save the row under the key
                        _table[key] = new DecodeTableEntry(csv);
                    }
                    catch
                    {
                        // error processing line ... ignore it
                    }
                }

                // close the file
                sr.Close();

                CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " read in " + LoadTimer.DurationAsString + "." + ANSIHelper.White);

                if (dumpLookup)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Lookup file dump:" + ANSIHelper.White);
                    int count = 0;
                    foreach ( var it in _table)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Key : '" + it.Key + "' Value : '" + it.Value + "'." + ANSIHelper.White);
                        count++;
                        if (count > 20) break;
                    }
                }

                // save it in our list of tables
                __tableColumnNames[tablename] = _columnNames;
                __tables[tablename] = _table;
            }
        }

        /// <summary>
        /// Creates a decode table from another CSV file
        /// </summary>
        /// <param name="file">file to read</param>
        /// <param name="keys">List of Column numbers for the key</param>
        public DecodeTable(string file, List<string> keys, bool dumpLookup, bool caseSensitive)
        {
            // if we cant find our code we will return the key
            _useoriginal = true;
            _keycols = keys;

            // name the table using the file name and the key columns. This allows us to load it once
            string tablename = file.ToLower();
            foreach (string n in keys)
            {
                tablename += n.ToString();
            }

            // If we have already loaded this lookup file
            if (__tables.ContainsKey(tablename))
            {
                // copy it
                _table = __tables[tablename];
                _columnNames = __tableColumnNames[tablename];
            }
            else
            {
                // we have to read the file
                CSVProcessor.DisplayError(ANSIHelper.Information + "Reading lookup file " + file + ANSIHelper.White);

                Timer LoadTimer = new Timer();
                LoadTimer.Reset();

                // open it
                StreamReader sr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

                string l = null; // holds a line from the input file
                CSVFileInfo fi = new CSVFileInfo(new FileInfo(file));
                bool first = true;

                // read until we get to the end of the file
                while ((l = sr.ReadLine()) != null)
                {
                    try
                    {
                        // parse the line
                        ColumnLine csv = new CSVFileLine(l, 0, /*file*/fi);
                        csv.Parse();

                        if (first)
                        {
                            first = false;
                            _columnNames = csv;
                            CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " column names: " + _columnNames.ToString() + ANSIHelper.White);

                            for (int i = 0; i < keys.Count; i++)
                            {
                                if (Regex.IsMatch(keys[i].Trim(), "^[\\-0-9]+$"))
                                {
                                    int index = Int32.Parse(keys[i].Trim());
                                    if (index < 0)
                                    {
                                        index = _columnNames.Count + index;
                                        if (index < 0)
                                        {
                                            throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                        else
                                        {
                                            keys[i] = index.ToString();
                                        }
                                    }
                                    else if (index > _columnNames.Count)
                                    {
                                        throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                    }
                                }
                                else if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                {
                                    string k = keys[i].Trim().ToLower().Substring(keys[i].Trim().IndexOf(":") + 1);
                                    for (int j = 0; j < _columnNames.Count; j++)
                                    {
                                        if (_columnNames[j].ToLower() == k)
                                        {
                                            keys[i] = j.ToString();
                                            break;
                                        }

                                        if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                        {
                                            throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("Lookup file key '" + keys[i] + "' is not valid for lookup file " + file);
                                }
                            }
                        }

                        // extract the line key
                        string key = string.Empty;
                        foreach (String cn in keys)
                        {
                            int index = Int32.Parse(cn.Trim());
                            if (index >= 0 && index < csv.Count)
                            {
                                key += csv[index].Trim();
                            }
                            if (!caseSensitive)
                            {
                                key = key.ToLower();
                            }
                        }

                        // save the row under the key
                        _table[key] = new DecodeTableEntry(csv);
                    }
                    catch
                    {
                        // error processing line ... ignore it
                    }
                }

                // close the file
                sr.Close();

                CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " read in " + LoadTimer.DurationAsString + "." + ANSIHelper.White);

                if (dumpLookup)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Lookup file dump:" + ANSIHelper.White);
                    int count = 0;
                    foreach (var it in _table)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Key : '" + it.Key + "' Value : '" + it.Value.Value + "'." + ANSIHelper.White);
                        count++;
                        if (count > 20) break;
                    }
                }

                // save it in our list of tables
                __tableColumnNames[tablename] = _columnNames;
                __tables[tablename] = _table;
            }
        }

        void LoadDecodeTable(XmlNode n, bool tracexml, string prefix, bool caseSensitive)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  <DecodeTable>");
            }
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    if (n2.Name.ToLower() == "default")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "     <Default>");
                        }

                        foreach (XmlNode n3 in n2.ChildNodes)
                        {
                            if (n3.NodeType != XmlNodeType.Comment)
                            {
                                if (n3.Name.ToLower() == "useoriginal")
                                {
                                    // if this node is present then the default is the uncoded value
                                    _useoriginal = true;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "        <UseOriginal/>");
                                    }
                                }
                                else if (n3.Name.ToLower() == "value")
                                {
                                    // this is the default value if a decode does not exist
                                    _default = n3.InnerText;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "        <Value>" + _default + "</Value>");
                                    }
                                }
                                else
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Warning + "DecodeTable::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "     </Default>");
                        }
                    }
                    else if (n2.Name.ToLower() == "code")
                    {
                        string code = string.Empty; // the coded value
                        string val = string.Empty; // the decoded value

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "     <Code>");
                        }

                        foreach (XmlNode n3 in n2.ChildNodes)
                        {
                            if (n3.NodeType != XmlNodeType.Comment)
                            {

                                if (n3.Name.ToLower() == "value")
                                {
                                    // this is the code
                                    if (caseSensitive)
                                    {
                                        code = n3.InnerText.Trim();
                                    }
                                    else
                                    {
                                        code = n3.InnerText.ToLower().Trim();
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "        <Value>" + code + "</Value>");
                                    }

                                }
                                else if (n3.Name.ToLower() == "decode")
                                {
                                    // this is the decode
                                    val = n3.InnerText;
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "        <Decode>" + val + "</Decode>");
                                    }
                                }
                            }
                        }
                        // add the entry to our codes table
                        try
                        {
                            if (_table.ContainsKey(code))
                            {
                                throw new ApplicationException("Code : " + code + " : already exists.");
                            }

                            _table.Add(code, new DecodeTableEntry(val));
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error adding code " + code + " to lookup table", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "     </Code>");
                        }
                    }
                    else
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Warning + "DecodeTable::Constructor Ignoring xml node : " + n2.Name + ANSIHelper.White);
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "  </DecodeTable>");
            }
        }

        public DecodeTable(XmlNode n, bool tracexml, string prefix, bool caseSensitive)
        {
            LoadDecodeTable(n, tracexml, prefix, caseSensitive);
        }

        /// <summary>
        /// Create a decode table from the provided XML file. The file should be in the format ...
        /// 
        /// <DecodeTable>
        ///		<Default>
        ///			<UseOriginal /> or <Value>Default Value</Value>
        ///		</Default>
        ///		<code>
        ///			<Value>code</Value><Decode>decode</Decode>
        ///		</code>
        ///		...
        /// </DecodeTable>
        /// 
        /// </summary>
        /// <param name="file">Name of the XML file containing the decode table</param>
        public DecodeTable(string file, bool caseSensitive)
        {
            // if we have loaded the file before
            if (__tables.ContainsKey(file.ToLower()))
            {
                // copy it
                _table = __tables[file];
                _columnNames = __tableColumnNames[file];
            }
            else
            {
                XmlDocument doc = new XmlDocument(); // xml document

                // Load the xml file
                doc.Load(file);

                // iterate through the nodes
                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "decodetable")
                        {
                            LoadDecodeTable(n, false, string.Empty, caseSensitive);
                        }
                        else
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "DecodeTable::Constructor Ignoring xml node : " + n.Name + ANSIHelper.White);
                        }
                    }
                }

                // save it for future reuse
                __tableColumnNames[file.ToLower()] = _columnNames;
                __tables[file.ToLower()] = _table;
            }
        }
        #endregion
    }
    public class MultiDecodeTable
    {
        static Dictionary<string, Dictionary<string, List<DecodeTableEntry>>> __multitables = new Dictionary<string, Dictionary<string, List<DecodeTableEntry>>>(); // a hashtable of loaded tables. This allows us to reuse the same table if it is referenced multiple times.
        Dictionary<string, List<DecodeTableEntry>> _table = new Dictionary<string, List<DecodeTableEntry>>(); // Hashtable to code the codes
        static Dictionary<string, ColumnLine> __tableColumnNames = new Dictionary<string, ColumnLine>();
        string _default = string.Empty; // The default value for when a code is not found
        bool _useoriginal = false; // indicator to control if the default value is returned or the original value
        List<string> _keycols = null;
        ColumnLine _columnNames = null;

        public class DecodeTableEntry
        {
            ColumnLine _csv = null;
            string _s = string.Empty;
            bool _referenced = false;

            public DecodeTableEntry(ColumnLine csv)
            {
                _csv = csv;
            }
            public DecodeTableEntry(string s)
            {
                _s = s;
            }
            public object Value
            {
                get
                {
                    if (_csv != null)
                    {
                        return _csv;
                    }
                    else
                    {
                        return _s;
                    }
                }
            }
            public bool Referenced
            {
                get
                {
                    return _referenced;
                }
                set
                {
                    _referenced = value;
                }
            }
        }

        #region Public Functions

        /// <summary>
        /// Check if the decode table contains this key
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Contains(string s)
        {
            return _table.ContainsKey(s.ToLower());
        }

        /// <summary>
        /// Finds the object associated with a code
        /// </summary>
        /// <param name="s">Code to lookup</param>
        /// <returns>Looked up value</returns>
        public List<object> Decode(string s, LogicFilterNode f, bool fastfailblank)
        {
            List<object> res = new List<object>();

            if (s.Trim() == string.Empty && fastfailblank)
            {
                // if we are to return the original value
                if (_useoriginal)
                {
                    // return original value
                    res.Add(s);
                    return res;
                }
                else
                {
                    // return the default value
                    res.Add(_default);
                    return res;
                }
            }

            // If the code is in our hashtable
            if (_table.ContainsKey(s.ToLower().Trim()))
            {
                _table[s.ToLower()][0].Referenced = true;
                foreach (DecodeTableEntry de in _table[s.ToLower()])
                {
                    if (f.Evaluate((ColumnLine)de.Value))
                    {
                        res.Add(de.Value);
                    }
                }
                if (res.Count == 0)
                {
                    // if we are to return the original value
                    if (_useoriginal)
                    {
                        // return original value
                        res.Add(s);
                    }
                    else
                    {
                        // return the default value
                        res.Add(_default);
                    }
                }
                return res;
            }
            else
            {
                // if we are to return the original value
                if (_useoriginal)
                {
                    // return original value
                    res.Add(s);
                    return res;
                }
                else
                {
                    // return the default value
                    res.Add(_default);
                    return res;
                }
            }
        }

        public List<DecodeTableEntry> UnaccessedCodes
        {
            get
            {
                List<DecodeTableEntry> rc = new List<DecodeTableEntry>();

                foreach (KeyValuePair<string, List<DecodeTableEntry>> kvp in _table)
                {
                    if (kvp.Value[0].Referenced == false)
                    {
                        rc.Add(kvp.Value[0]);
                    }
                }

                return rc;
            }
        }
        #endregion

        #region Accessors
        public List<string> KeyCols
        {
            get
            {
                return _keycols;
            }
        }

        public string ColumnIndex(string col)
        {
            if (col.Trim() == "*") return "*";

            if (Regex.IsMatch(col.Trim(), "^[\\-0-9]+$"))
            {
                int index = Int32.Parse(col.Trim());
                if (index < 0)
                {
                    index = _columnNames.Count + index;
                    if (index < 0)
                    {
                        throw new ApplicationException("DecodeTable:ColumnIndex '" + col + "' is not valid for lookup file.");
                    }
                    else
                    {
                        return index.ToString();
                    }
                }
                else if (index > _columnNames.Count)
                {
                    throw new ApplicationException("DecodeTable:ColumnIndex '" + col + "' is not valid for lookup file.");
                }
                else
                {
                    return index.ToString();
                }
            }

            if (_columnNames == null)
            {
                throw new ApplicationException("DecodeTable:ColumnIndex Attempt to decode decode table column when no column names have been loaded.");
            }

            if (col.Trim().ToLower().StartsWith("colname:"))
            {
                string k = col.Trim().ToLower().Substring(col.Trim().IndexOf(":") + 1);
                for (int j = 0; j < _columnNames.Count; j++)
                {
                    if (_columnNames[j].ToLower() == k)
                    {
                        return j.ToString();
                    }
                }
                if (col.Trim().ToLower().StartsWith("colname:"))
                {
                    throw new ApplicationException("DecodeTable:ColumnIndex Lookup file key '" + col + "' is not valid for lookup file.");
                }
            }
            else
            {
                throw new ApplicationException("Lookup file key '" + col + "' is not valid for lookup file.");
            }

            Debug.Assert(false, "we should never get here.");
            return "0";
        }

        /// <summary>
        /// Flag to indicate if original value should be returned
        /// </summary>
        public bool UseOriginal
        {
            set
            {
                _useoriginal = value;
            }
            get
            {
                return _useoriginal;
            }
        }

        public string[] Keys
        {
            get
            {
                string[] keys = new string[_table.Keys.Count];
                _table.Keys.CopyTo(keys, 0);
                return keys;
            }
        }

        /// <summary>
        /// Sets the default value to return if the code is not found
        /// </summary>
        public string Default
        {
            set
            {
                _default = value;
            }
            get
            {
                return _default;
            }
        }
        #endregion

        #region Constructor
        public MultiDecodeTable(string file, List<string> keys, bool dumpLookup, bool caseSensitive, char delimiter, char quote, char quoteescape)
        {
            // if we cant find our code we will return the key
            _useoriginal = true;

            _keycols = keys;

            // name the table using the file name and the key columns. This allows us to load it once
            string tablename = file.ToLower();
            foreach (string n in keys)
            {
                tablename += n;
            }

            // If we have already loaded this lookup file
            if (__multitables.ContainsKey(tablename))
            {
                // copy it
                _columnNames = __tableColumnNames[tablename];
                _table = __multitables[tablename];
            }
            else
            {
                // we have to read the file
                CSVProcessor.DisplayError(ANSIHelper.Information + "Reading multilookup file " + file + ANSIHelper.White);

                Timer LoadTimer = new Timer();
                LoadTimer.Reset();

                // open it
                StreamReader sr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

                string l = null; // holds a line from the input file
                CSVFileInfo fi = new CSVFileInfo(new FileInfo(file));
                bool first = true;

                // read until we get to the end of the file
                while ((l = sr.ReadLine()) != null)
                {
                    try
                    {
                        // parse the line
                        ColumnLine csv = new CSVFileLine(l, 0, /*file*/fi);
                        csv.Parse(delimiter, quote, quoteescape);

                        if (first)
                        {
                            first = false;
                            _columnNames = csv;
                            CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " column names: " + _columnNames.ToString() + ANSIHelper.White);

                            for (int i = 0; i < keys.Count; i++)
                            {
                                if (Regex.IsMatch(keys[i].Trim(), "^[\\-0-9]+$"))
                                {
                                    int index = Int32.Parse(keys[i].Trim());
                                    if (index < 0)
                                    {
                                        index = _columnNames.Count + index;
                                        if (index < 0)
                                        {
                                            throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                        else
                                        {
                                            keys[i] = index.ToString();
                                        }
                                    }
                                    else if (index > _columnNames.Count)
                                    {
                                        throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                    }
                                }
                                else if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                {
                                    string k = keys[i].Trim().ToLower().Substring(keys[i].Trim().IndexOf(":") + 1);
                                    for (int j = 0; j < _columnNames.Count; j++)
                                    {
                                        if (_columnNames[j].ToLower() == k)
                                        {
                                            keys[i] = j.ToString();
                                            break;
                                        }

                                        if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                        {
                                            throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                }
                            }
                        }

                        // extract the line key
                        string key = string.Empty;
                        foreach (string cn in keys)
                        {
                            int index = Int32.Parse(cn.Trim());
                            if (index >= 0 && index < csv.Count)
                            {
                                key += csv[index].Trim();
                            }
                            if (!caseSensitive)
                            {
                                key = key.ToLower();
                            }
                        }

                        // save the row under the key
                        if (!_table.ContainsKey(key))
                        {
                            _table[key] = new List<DecodeTableEntry>();
                        }
                        _table[key].Add(new DecodeTableEntry(csv));
                    }
                    catch
                    {
                        // error processing line ... ignore it
                    }
                }

                // close the file
                sr.Close();

                CSVProcessor.DisplayError(ANSIHelper.Information + "MultiLookup file " + file + " read in " + LoadTimer.DurationAsString + "." + ANSIHelper.White);

                if (dumpLookup)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Lookup file dump:" + ANSIHelper.White);
                    int count = 0;
                    foreach (var it in _table)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Key : '" + it.Key + "' Value : '" + it.Value + "'." + ANSIHelper.White);
                        count++;
                        if (count > 20) break;
                    }
                }

                // save it in our list of tables
                __tableColumnNames[tablename] = _columnNames;
                __multitables[tablename] = _table;
            }
        }

        /// <summary>
        /// Creates a decode table from another CSV file
        /// </summary>
        /// <param name="file">file to read</param>
        /// <param name="keys">List of Column numbers for the key</param>
        public MultiDecodeTable(string file, List<string> keys, bool dumpLookup, bool caseSensitive)
        {
            // if we cant find our code we will return the key
            _useoriginal = true;
            _keycols = keys;

            // name the table using the file name and the key columns. This allows us to load it once
            string tablename = file.ToLower();
            foreach (string n in keys)
            {
                tablename += n;
            }

            // If we have already loaded this lookup file
            if (__multitables.ContainsKey(tablename))
            {
                // copy it
                _table = __multitables[tablename];
                _columnNames = __tableColumnNames[tablename];
            }
            else
            {
                // we have to read the file
                CSVProcessor.DisplayError(ANSIHelper.Information + "Reading multilookup file " + file + ANSIHelper.White);

                Timer LoadTimer = new Timer();
                LoadTimer.Reset();

                // open it
                StreamReader sr = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));

                string l = null; // holds a line from the input file
                CSVFileInfo fi = new CSVFileInfo(new FileInfo(file));
                bool first = true;

                // read until we get to the end of the file
                while ((l = sr.ReadLine()) != null)
                {
                    try
                    {
                        // parse the line
                        ColumnLine csv = new CSVFileLine(l, 0, /*file*/fi);
                        csv.Parse();

                        if (first)
                        {
                            first = false;
                            _columnNames = csv;
                            CSVProcessor.DisplayError(ANSIHelper.Information + "Lookup file " + file + " column names: " + _columnNames.ToString() + ANSIHelper.White);

                            for (int i = 0; i < keys.Count; i++)
                            {
                                if (Regex.IsMatch(keys[i].Trim(), "^[\\-0-9]+$"))
                                {
                                    int index = Int32.Parse(keys[i].Trim());
                                    if (index < 0)
                                    {
                                        index = _columnNames.Count + index;
                                        if (index < 0)
                                        {
                                            throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                        else
                                        {
                                            keys[i] = index.ToString();
                                        }
                                    }
                                    else if (index > _columnNames.Count)
                                    {
                                        throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                    }
                                }
                                else if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                {
                                    string k = keys[i].Trim().ToLower().Substring(keys[i].Trim().IndexOf(":") + 1);
                                    for (int j = 0; j < _columnNames.Count; j++)
                                    {
                                        if (_columnNames[j].ToLower() == k)
                                        {
                                            keys[i] = j.ToString();
                                            break;
                                        }

                                        if (keys[i].Trim().ToLower().StartsWith("colname:"))
                                        {
                                            throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("MultiDecodeTable file key '" + keys[i] + "' is not valid for lookup file " + file);
                                }
                            }
                        }

                        // extract the line key
                        string key = string.Empty;
                        foreach (string cn in keys)
                        {
                            int index = Int32.Parse(cn.Trim());
                            if (index >= 0 && index < csv.Count)
                            {
                                key += csv[index].Trim();
                            }
                            if (!caseSensitive)
                            {
                                key = key.ToLower();
                            }
                        }

                        // save the row under the key
                        if (!_table.ContainsKey(key))
                        {
                            _table[key] = new List<DecodeTableEntry>();
                        }
                        _table[key].Add(new DecodeTableEntry(csv));
                    }
                    catch
                    {
                        // error processing line ... ignore it
                    }
                }

                // close the file
                sr.Close();

                CSVProcessor.DisplayError(ANSIHelper.Information + "MultiLookup file " + file + " read in " + LoadTimer.DurationAsString + "." + ANSIHelper.White);

                if (dumpLookup)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Lookup file dump:" + ANSIHelper.White);
                    int count = 0;
                    foreach (var it in _table)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Key : '" + it.Key + "' Value : '" + it.Value + "'." + ANSIHelper.White);
                        count++;
                        if (count > 20) break;
                    }
                }

                // save it in our list of tables
                __tableColumnNames[tablename] = _columnNames;
                __multitables[tablename] = _table;
            }
        }
        #endregion
    }
    #endregion

    #region Child Classes

    public class dotnetresourcecolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool fT = false;
        bool fD = false;
        string _outputformat = string.Empty; // the output date format

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "param":
                case "outformat":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public dotnetresourcecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "DotNETResourceColumn";
            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "param")
                        {
                            if (n.InnerText.ToLower() == "t")
                            {
                                fT = true;
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <Param>T</Param>");
                                }
                            }
                            else if (n.InnerText.ToLower() == "d")
                            {
                                fD = true;
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <Param>D</Param>");
                                }
                            }
                        }
                        else if (n.Name.ToLower() == "outformat")
                        {
                            // get the output format
                            _outputformat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outputformat + "</OutFormat>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating DotNetResourceColumn : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating DotNetResourceColumn - Missing column number: {" + Id + "}");
            }

            if (!fT && !fD)
            {
                throw new ApplicationException("DotNetResourceColumn does not specify parameter : {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                if (fD)
                {
                    try
                    {
                        MethodInfo methodInfo = typeof(System.Web.UI.Page).GetMethod("DecryptString", BindingFlags.NonPublic | BindingFlags.Static);
                        if (methodInfo != null)
                        {
                            s = (string)methodInfo.Invoke(null, new object[] { cv });
                        }
                        else
                        {
                            s = "Could not find DecryptString method.";
                        }
                    }
                    catch (TargetInvocationException ex)
                    {
                        if (ex.InnerException != null && ex.InnerException is System.Security.Cryptography.CryptographicException)
                        {
                            s = "Failed to decrypt string. Check that the WebResource or ScriptResource is valid.";
                        }
                        else
                        {
                            s = ex.Message;
                        }
                    }
                }
                else if (fT)
                {
                    DateTime dt = new DateTime((long)Convert.ToDouble(cv));
                    s = dt.ToString(_outputformat);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "DotNetResourceColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Check for a value in a lookup file
    /// </summary>
    class matchinlookup : Column
    {
        #region Member Variables
        //List<ColumnNumberOrColumn> _keySrc = new List<ColumnNumberOrColumn>(); // source file key columns
        //Column _key = null; // output column to generate the key
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        DecodeTable _dt = null; // the lookup file to use
        bool _useinputrules = false;
        string _file = string.Empty;
        enum InLookupMode { EQUALS, CONTAINS, STARTSWITH, ENDSWITH };
        InLookupMode _mode = InLookupMode.EQUALS;
        bool _casesensitive = false;
        bool _usekey = false;
        string _defaultvalue = "";
        bool _fastfailblank = false;
        bool _dumpLookup = false;
        StringOrColumn _targetColumn; // target column to extract
        int _targetColumnNum = -1;
        #endregion

        string GetTargetColumn(string key)
        {
            if (_targetColumn == null) return "";

            string res = "";

            object row = _dt.Decode(key, _fastfailblank);

            if (row.GetType() == typeof(System.String))
            {
                // that must be our result
                res = (string)row;
            }
            else
            {
                // look up the code and return it
                try
                {
                    res = (((ColumnLine)row)[_targetColumnNum]);
                }
                catch
                {
                    // throw new ApplicationException("Problem looking up file : " + ex.Message);
                    res = key;
                }
            }
            return res;
        }

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "targetkeys":
                case "useinputparsingrules":
                case "lookupfile":
                case "casesensitive":
                case "equals":
                case "contains":
                case "beginswith":
                case "startswith":
                case "endswith":
                case "fastfailifblank":
                case "targetcolumn":
                case "dumplookup":
                case "default":
                    return true;
                default:
                    return false;
            }
        }


        #region Constructor
        public matchinlookup(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
            : base(node, j, p, od, tracexml, prefix)
        {
            Name = "MatchInLookup";

            List<string> keyTgt = null; // list of key columns in  the lookup file

            try
            {
                // iterate through the nodes
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetkeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <TargetKeys>");
                            }
                            // create a target key list
                            keyTgt = new List<string>();

                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the target key columns
                                        try
                                        {
                                            StringOrColumn cn = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                                            keyTgt.Add(cn.Value());
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading Matchinlookup target column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "MatchInLookup::Constructor Ignoring xml node : " + n3.Name + "\n" + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </TargetKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "useinputparsingrules")
                        {
                            _useinputrules = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <UseInputParsingRules/>");
                            }
                        }
                        else if (n.Name.ToLower() == "lookupfile")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LookupFile>");
                            }

                            StringOrColumn sc = new StringOrColumn(n, j, p, null, tracexml, prefix + "   ");

                            _file = sc.Value();

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    " + _file);
                                CSVProcessor.DisplayError(prefix + "  </LookupFile>");
                            }
                        }
                        else if (n.Name.ToLower() == "casesensitive")
                        {
                            _casesensitive = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                            }
                        }
                        else if (n.Name.ToLower() == "fastfailifblank")
                        {
                            _fastfailblank = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<FastFailIfBlank/>");
                            }
                        }
                        else if (n.Name.ToLower() == "dumplookup")
                        {
                            _dumpLookup = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<DumpLookup/>");
                            }
                        }
                        else if (n.Name.ToLower() == "equals")
                        {
                            _mode = InLookupMode.EQUALS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Equals/>");
                            }
                        }
                        else if (n.Name.ToLower() == "contains")
                        {
                            _mode = InLookupMode.CONTAINS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Contains/>");
                            }
                        }
                        else if (n.Name.ToLower() == "endswith")
                        {
                            _mode = InLookupMode.ENDSWITH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<EndsWith/>");
                            }
                        }
                        else if (n.Name.ToLower() == "startswith" || n.Name.ToLower() == "beginswith")
                        {
                            _mode = InLookupMode.STARTSWITH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<StartsWith/>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetcolumn")
                        {
                            // save the target column to extract
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<TargetColumn>");
                            }
                            try
                            {
                                _targetColumn = new StringOrColumn(n, j, p, od, tracexml, "   " + prefix);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading lookup target column", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "</TargetColumn>");
                            }
                        }
                        else if (n.Name.ToLower() == "default")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Default>");
                            }
                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "usekey")
                                    {
                                        // we should return the key if the key is not found
                                        _usekey = true;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <UseKey/>");
                                        }
                                    }
                                    else if (n3.Name.ToLower() == "value")
                                    {
                                        // we should return the given value if the key is not found
                                        _usekey = false;
                                        _defaultvalue = n3.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Value>" + _defaultvalue + "</Value>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "MatchInLookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Default>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("MatchInLookup does not have a source column.");
                }

                // if we got a file name and a target key then load the lookup file
                if (_file != string.Empty && keyTgt != null)
                {
                    try
                    {
                        if (_useinputrules)
                        {
                            // load the lookup file
                            _dt = new DecodeTable(_file, keyTgt, _dumpLookup, false, p.Input.Delimiter, p.Input.Quote, p.Input.QuoteEscape);
                        }
                        else
                        {
                            // load the lookup file
                            _dt = new DecodeTable(_file, keyTgt, _dumpLookup, false);
                        }
                        if (_targetColumn != null)
                        {
                            _targetColumn.SetStringValue(_dt.ColumnIndex(_targetColumn.Value()));
                            _targetColumnNum = Int32.Parse(_targetColumn.Value());
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Problem loading lookup file : " + _file, ex);
                    }
                }
                else
                {
                    throw new ApplicationException("Lookup file incompletely specified.");
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating MatchInLookup: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string src = "";

            try
            {
                string key = _srccolumn.Value(line);

                if (!_casesensitive)
                {
                    key = key.ToLower();
                }

                // check if the key exists
                bool b = false;

                if (_fastfailblank && key == string.Empty)
                {
                    // dont look
                }
                else
                {
                    switch (_mode)
                    {
                        case InLookupMode.EQUALS:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    if (s == key)
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }
                                else
                                {
                                    if (s.ToLower() == key)
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "MatchInLoolup:Equals" + Id + " : '" + key + "' --> " + src + ANSIHelper.White);
                            }

                            break;
                        case InLookupMode.CONTAINS:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    if (key.IndexOf(s) != -1)
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }
                                else
                                {
                                    if (key.IndexOf(s.ToLower()) != -1)
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "MatchInLookup:Contains" + Id + " : '" + key + "' --> " + src + ANSIHelper.White);
                            }
                            break;
                        case InLookupMode.STARTSWITH:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    if (key.StartsWith(s))
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }
                                else
                                {
                                    if (key.StartsWith(s.ToLower()))
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "MatchInLookup:StartsWith" + Id + " : '" + key + "' --> " + src + ANSIHelper.White);
                            }
                            break;
                        case InLookupMode.ENDSWITH:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    if (key.EndsWith(s))
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }
                                        b = true;
                                    }
                                }
                                else
                                {
                                    if (key.EndsWith(s.ToLower()))
                                    {
                                        if (_targetColumn == null)
                                        {
                                            src = s;
                                        }
                                        else
                                        {
                                            src = GetTargetColumn(s);
                                        }

                                        b = true;
                                    }
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "MatchInLookup:EndsWith" + Id + " : '" + key + "' --> " + src + ANSIHelper.White);
                            }
                            break;
                    }
                }

                if (!b)
                {
                    if (_usekey)
                    {
                        src = _srccolumn.Value(line);
                    }
                    else
                    {
                        src = _defaultvalue;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Processing MatchInLookup: {" + Id + "}", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(src);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class cobolcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool _ebcdic = false; // true if we are to do EBCDIC -> ASCII character mapping
        int _comp = -1; // -1 if no processing required else the type of COBOL comp unpacking to do
        int _wholedigits = -1; // for comp unpacking digits before the decimal point
        int _fractiondigits = -1; // for comp unpacking digits after the decimal point
        bool _signed = false; // for comp unpacking indicator of whether there is a sign
        string _format = string.Empty; // the format to use for COMP fields
        string _pic = string.Empty;

        static Regex repeat = new Regex("\\((?'val'[0-9]*?)\\)", RegexOptions.Singleline);

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "comp":
                case "comp1":
                case "comp2":
                case "comp3":
                case "zoned":
                case "ebcdic":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public cobolcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "COBOLColumn";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "ebcdic")
                        {
                            _ebcdic = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <EBCDIC/>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp")
                        {
                            // get the output format
                            _comp = 0;
                            _pic = n.InnerText;
                            fixedparser.ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <PIC>" + _pic + "</PIC>");
                            }
                        }
                        else if (n.Name.ToLower() == "zoned")
                        {
                            _comp = 100;
                            _pic = n.InnerText;
                            fixedparser.ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Zoned>" + n.InnerText + "</Zoned>");
                            }
                        }
                        // do ebcdic string conversion
                        else if (n.Name.ToLower() == "comp3" || n.Name.ToLower() == "comp-3")
                        {
                            _comp = 3;
                            _pic = n.InnerText;
                            fixedparser.ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp3>" + n.InnerText + "</Comp3>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp1" || n.Name.ToLower() == "comp-1")
                        {
                            _comp = 1;
                            _pic = n.InnerText;
                            fixedparser.ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp1>" + n.InnerText + "</Comp1>");
                            }
                        }
                        else if (n.Name.ToLower() == "comp2" || n.Name.ToLower() == "comp-2")
                        {
                            _comp = 2;
                            _pic = n.InnerText;
                            fixedparser.ColumnParsingRule.ParseComp(n.InnerText, out _wholedigits, out _fractiondigits, out _signed);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Comp2>" + n.InnerText + "</Comp2>");
                            }
                        }
                    }
                }

                if (_comp != -1)
                {
                    _format = fixedparser.ColumnParsingRule.GenerateCompFormat(_wholedigits, _fractiondigits);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating COBOLColumn : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating COBOLColumn - Missing column number: {" + Id + "}");
            }

            if (_ebcdic && _comp != -1)
            {
                throw new ApplicationException("COBOLColumn cannot be both EBCDIC and a number format : {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            string s = string.Empty;

            PreProcess();

            // get the column value
            string cv = string.Empty;

            try
            {
                cv = _srccolumn.Value(line);

                if (_ebcdic)
                {
                    s = fixedparser.ColumnParsingRule.EBCDICtoASCII(cv);
                }
                else if (_comp != -1)
                {
                    s = fixedparser.ColumnParsingRule.COMPtoNumber(_comp, _wholedigits, _fractiondigits, _signed, cv, _format);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "COBOLColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class base64column : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool fCode = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "code":
                case "decode":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public base64column(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Base64Column";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "code")
                        {
                            fCode = true;
                        }
                        else if (n.Name.ToLower() == "decode")
                        {
                            fCode = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating Base64Column : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating Base64Column - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                if (fCode)
                {
                    char[] ca = cv.ToCharArray();
                    byte[] ba = new byte[ca.GetLength(0)];
                    for (int i = 0; i < ca.GetLength(0); i++)
                    {
                        ba[i] = (byte)ca[i];
                    }
                    s = Convert.ToBase64String(ba);
                }
                else
                {
                    // decode it
                    byte[] ba = Convert.FromBase64String(cv);

                    // convert it to a char array
                    Encoding ascii = Encoding.ASCII;
                    char[] ca = new char[ascii.GetCharCount(ba, 0, ba.Length)];
                    ascii.GetChars(ba, 0, ba.Length, ca, 0);

                    // convert it to a string
                    s = new string(ca);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Base64Column: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class ipping : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        int _timeout = 2000; // default timeout

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "timeout":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public ipping(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "IPPing";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "timeout")
                        {
                            _timeout = Convert.ToInt32(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Timeout>" + _timeout.ToString() + "</Timeout>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating IPPing : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating IPPing - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                Ping pingSender = new Ping();

                PingOptions po = new PingOptions();
                po.DontFragment = true;

                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                try
                {
                    PingReply reply = pingSender.Send(cv, _timeout, buffer, po);

                    if (reply.Status == IPStatus.Success)
                    {
                        s = "Success";
                    }
                    else
                    {
                        s = "Failed";
                    }
                }
                catch (Exception ex)
                {
                    s = "Failed - " + ex.Message;
                }
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "IPPing: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class stringformatter : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        string _stripchars = string.Empty;
        string _formatstring = string.Empty;
        bool _trim = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "stripchars":
                case "format":
                case "trim":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public stringformatter(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "StringFormatter";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "stripchars")
                        {
                            _stripchars = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <StripChars>" + _stripchars.ToString() + "</StripChars>");
                            }
                        }
                        else if (n.Name.ToLower() == "format")
                        {
                            _formatstring = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Format>" + _formatstring.ToString() + "</Format>");
                            }
                        }
                        else if (n.Name.ToLower() == "trim")
                        {
                            _trim = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Trim/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating StringFormatter : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating StringFormatter - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                foreach (char c in _stripchars)
                {
                    cv = cv.Replace(c.ToString(), "");
                }

                if (_trim)
                {
                    cv = cv.Trim();
                }

                int state = 0;
                int i = 0;
                foreach (char c in _formatstring)
                {
                    switch (c)
                    {
                        case '\\':
                            if (state == 0)
                            {
                                state = 1;
                            }
                            else
                            {
                                s = s + '\\';
                                state = 0;
                            }
                            break;
                        case 'X':
                            if (state == 0)
                            {
                                if (i < cv.Length)
                                {
                                    s = s + cv[i++];
                                }
                            }
                            else
                            {
                                s = s + 'X';
                                state = 0;
                            }
                            break;
                        default:
                            s = s + c;
                            break;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "StringFormatter: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class numberformatter : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        string _formatstring = string.Empty;
        bool _float = false;
        bool _percentage = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "float":
                case "format":
                case "percentage":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public numberformatter(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "NumberFormatter";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "format")
                        {
                            _formatstring = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Format>" + _formatstring.ToString() + "</Format>");
                            }
                        }
                        else if (n.Name.ToLower() == "float")
                        {
                            _float = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Float/>");
                            }
                        }
                        else if (n.Name.ToLower() == "percentage")
                        {
                            _percentage = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Percentage/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating NumberFormatter : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating NumberFormatter - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line).Trim();

                if (cv == string.Empty)
                {
                    s = string.Empty;
                }
                else
                {
                    try
                    {
                        if (_float)
                        {
                            double d = Convert.ToDouble(cv);

                            if (_percentage)
                            {
                                d = d / 100;
                            }

                            s = d.ToString(_formatstring);
                        }
                        else
                        {
                            Int32 i = Convert.ToInt32(cv);

                            if (_percentage)
                            {
                                double d = ((double)i) / 100.0;
                                s = d.ToString(_formatstring);
                            }
                            else
                            {
                                s = i.ToString(_formatstring);
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        throw new ApplicationException("Error parsing number from '" + cv + "'", ex1);
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "NumberFormatter: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class urlping : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool _followredirects = false;
        bool _reportHTTPResult = false;
        bool _reportAnsweringURL = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "reporthttpresult":
                case "followredirects":
                case "reportansweringurl":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public urlping(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "URLPing";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "followredirects")
                        {
                            _followredirects = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FollowRedirects/>");
                            }
                        }
                        else if (n.Name.ToLower() == "reporthttpresult")
                        {
                            _reportHTTPResult = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ReportHTTPResult/>");
                            }
                        }
                        else if (n.Name.ToLower() == "reportanswrringurl")
                        {
                            _reportAnsweringURL = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ReportAnsweringURL/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating URLPing : {" + Id + "}", ex);
            }

            if (_reportAnsweringURL && _reportHTTPResult)
            {
                throw new ApplicationException("Error Creating URLPing - Can't specify <ReportAnsweringURL/> and <ReportHTTPResult/>: {" + Id + "}");
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating URLPing - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(cv);

                if (_followredirects)
                {
                    req.AllowAutoRedirect = true;
                }
                else
                {
                    req.AllowAutoRedirect = false;
                }

                HttpWebResponse r = (HttpWebResponse)req.GetResponse();

                if (_reportHTTPResult)
                {
                    s = r.StatusCode.ToString();
                }
                else if (_reportAnsweringURL)
                {
                    s = r.ResponseUri.AbsolutePath.ToString();
                }
                else
                {
                    if (Convert.ToInt32(r.StatusCode.ToString()) >= 200 && Convert.ToInt32(r.StatusCode.ToString()) < 400)
                    {
                        s = "Success";
                    }
                    else
                    {
                        s = "Failed";
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "URLPing: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class urlcomponent : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        enum WHATTOGET { SCHEME, QUERY, PORT, FILE, FILETYPE, ABSOLUTEPATH, HOST, NOTHING };
        WHATTOGET _whattoget = WHATTOGET.NOTHING;
        static Regex __fileTypeRegex = new Regex("\\.(?'val'.*?)$", RegexOptions.Compiled & RegexOptions.Singleline);
        static Regex __fileRegex = new Regex("(?'val'[^\\/]*?)$", RegexOptions.Compiled & RegexOptions.Singleline);

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "scheme":
                case "query":
                case "port":
                case "file":
                case "filetype":
                case "absolutepath":
                case "host":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        public urlcomponent(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "URLComponent";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "scheme")
                        {
                            _whattoget = WHATTOGET.SCHEME;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Scheme/>");
                            }
                        }
                        else if (n.Name.ToLower() == "query")
                        {
                            _whattoget = WHATTOGET.QUERY;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Query/>");
                            }
                        }
                        else if (n.Name.ToLower() == "port")
                        {
                            _whattoget = WHATTOGET.PORT;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Port/>");
                            }
                        }
                        else if (n.Name.ToLower() == "file")
                        {
                            _whattoget = WHATTOGET.FILE;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <File/>");
                            }
                        }
                        else if (n.Name.ToLower() == "filetype")
                        {
                            _whattoget = WHATTOGET.FILETYPE;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FileType/>");
                            }
                        }
                        else if (n.Name.ToLower() == "host")
                        {
                            _whattoget = WHATTOGET.HOST;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Host/>");
                            }
                        }
                        else if (n.Name.ToLower() == "absolutepath")
                        {
                            _whattoget = WHATTOGET.ABSOLUTEPATH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AbsolutePath/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating URLComponent : {" + Id + "}", ex);
            }

            if (_whattoget == WHATTOGET.NOTHING)
            {
                throw new ApplicationException("Error Creating URLComponent - You didnt ask for anything : {" + Id + "}");
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating URLComponent - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                try
                {
                    Uri url = new Uri(cv);

                    switch (_whattoget)
                    {
                        case WHATTOGET.FILE:
                            {
                                Match m = __fileRegex.Match(url.AbsolutePath);
                                if (m != null)
                                {
                                    s = m.Groups["val"].Value;
                                }
                            }
                            break;
                        case WHATTOGET.FILETYPE:
                            {
                                Match m = __fileTypeRegex.Match(url.AbsolutePath);
                                if (m != null)
                                {
                                    s = m.Groups["val"].Value;
                                }
                            }
                            break;
                        case WHATTOGET.SCHEME:
                            s = url.Scheme;
                            break;
                        case WHATTOGET.QUERY:
                            s = url.Query;
                            break;
                        case WHATTOGET.PORT:
                            s = url.Port.ToString();
                            break;
                        case WHATTOGET.ABSOLUTEPATH:
                            s = url.AbsolutePath; // no dns name ... no query
                            break;
                        case WHATTOGET.HOST:
                            s = url.Host; // the dns name
                            break;
                    }
                }
                catch
                {
                    s = "Error decoding URL " + cv;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "URLComponent: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class urlspecialcharcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool fCode = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "code":
                case "decode":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public urlspecialcharcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "URLSpecialCharColumn";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "code")
                        {
                            fCode = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Code/>");
                            }
                        }
                        else if (n.Name.ToLower() == "decode")
                        {
                            fCode = false;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Decode/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating URLSpecialCharColumn : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating URLSpecialCharColumn - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                if (fCode)
                {
                    s = HttpUtility.UrlEncode(cv);
                }
                else
                {
                    s = HttpUtility.UrlDecode(cv);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "URLSepecialCharColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class md5column : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool file = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "file":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public md5column(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "MD5Column";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "file")
                        {
                            file = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <File/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating MD5Column : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating MD5Column - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                if (file)
                {
                    if (File.Exists(cv))
                    {
                        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
                        StreamReader br = new StreamReader(cv);
                        byte[] bs = x.ComputeHash(br.BaseStream);
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        foreach (byte b in bs)
                        {
                            sb.Append(b.ToString("x2").ToLower());
                        }
                        s = sb.ToString();
                    }
                }
                else
                {
                    System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    byte[] bs = System.Text.Encoding.UTF8.GetBytes(cv);
                    bs = x.ComputeHash(bs);
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (byte b in bs)
                    {
                        sb.Append(b.ToString("x2").ToLower());
                    }
                    s = sb.ToString();
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "MD5Column: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class crccolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        bool file = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "file":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public crccolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "CRCColumn";
            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "file")
                        {
                            file = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <File/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating CRCColumn : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating CRCColumn - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                if (file)
                {
                    if (File.Exists(cv))
                    {
                        crc32 crc = new crc32();
                        s = crc.CRC(new StreamReader(cv).BaseStream).ToString();
                    }
                }
                else
                {
                    crc32 crc = new crc32();
                    s = crc.CRC(cv).ToString();
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "CRCColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class pathcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        enum action { EXT, FILENAME, BASEFILENAME, PATH, DRIVE, DIRECTORY, EXISTS, NONE };
        action _action = action.NONE;
        int _directory = 0;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "directoryindex":
                case "ext":
                case "filename":
                case "basefilename":
                case "path":
                case "drive":
                case "directory":
                case "exists":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public pathcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "PathColumn";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "directoryindex")
                        {
                            try
                            {
                                _directory = Convert.ToInt32(n.InnerText);
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <DirectoryIndex>" + _directory.ToString() + "</DirectoryIndex>");
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("PathColumn::Constructor error reading directoryindex", ex);
                            }
                        }
                        else if (n.Name.ToLower() == "ext")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.EXT;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Ext/>");
                            }
                        }
                        else if (n.Name.ToLower() == "filename")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.FILENAME;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Filename/>");
                            }
                        }
                        else if (n.Name.ToLower() == "exists")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.EXISTS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Exists/>");
                            }
                        }
                        else if (n.Name.ToLower() == "basefilename")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.BASEFILENAME;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <BaseFilename/>");
                            }
                        }
                        else if (n.Name.ToLower() == "path")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.PATH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Path/>");
                            }
                        }
                        else if (n.Name.ToLower() == "drive")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.DRIVE;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Drive/>");
                            }
                        }
                        else if (n.Name.ToLower() == "directory")
                        {
                            if (_action != action.NONE)
                            {
                                throw new ApplicationException("PathColumn::Constructor You can only specify one action.");
                            }
                            _action = action.DIRECTORY;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Directory/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating PathColumn : {" + Id + "}", ex);
            }
            if (_action == action.NONE)
            {
                throw new ApplicationException("PathColumn::Constructor You must specify an action.");
            }
            if (_action == action.DIRECTORY && _directory == 0)
            {
                throw new ApplicationException("PathColumn::Constructor If you specify <Directory/> then you must specify a non zero directory index.");
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating PathColumn - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                switch (_action)
                {
                    case action.BASEFILENAME:
                        s = Path.GetFileNameWithoutExtension(cv);
                        break;
                    case action.EXISTS:
                        if (File.Exists(cv))
                        {
                            s = "EXISTS";
                        }
                        else
                        {
                            s = "DOES NOT EXIST";
                        }
                        break;
                    case action.DIRECTORY:
                        string dirsonly = Path.GetFullPath(cv).Replace(Path.GetFileName(cv), "").Replace(Path.GetPathRoot(cv), "");
                        string[] parts = dirsonly.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
                        if (_directory < 0)
                        {
                            if (parts.GetLength(0) < Math.Abs(_directory))
                            {
                                s = "Invalid directory index " + _directory.ToString() + " " + cv;
                            }
                            else
                            {
                                s = parts[parts.GetLength(0) + _directory];
                            }
                        }
                        else
                        {
                            if (parts.GetLength(0) < _directory)
                            {
                                s = "Invalid directory index " + _directory.ToString() + " " + cv;
                            }
                            else
                            {
                                s = parts[_directory - 1];
                            }
                        }
                        break;
                    case action.DRIVE:
                        s = Path.GetPathRoot(cv);
                        break;
                    case action.EXT:
                        s = Path.GetExtension(cv);
                        break;
                    case action.FILENAME:
                        s = Path.GetFileName(cv);
                        break;
                    case action.PATH:
                        s = Path.GetDirectoryName(cv);
                        //s = Path.GetFullPath(cv).Replace(Path.GetFileName(cv),"");
                        break;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "PathColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class httpresult : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public httpresult(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "HTTPResult";
            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating HTTPResult : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating HTTPResult - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "";

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                try
                {
                    int res = Convert.ToInt32(cv);

                    switch (res)
                    {
                        case 100: s = "Continue"; break;
                        case 101: s = "Switching Protocols"; break;
                        case 200: s = "OK"; break;
                        case 201: s = "Created"; break;
                        case 202: s = "Accepted"; break;
                        case 203: s = "Non-Authoritative Information"; break;
                        case 204: s = "No Content"; break;
                        case 205: s = "Reset Content"; break;
                        case 206: s = "Partial Content"; break;
                        case 300: s = "Multiple Choices"; break;
                        case 301: s = "Moved Permanently"; break;
                        case 302: s = "Found"; break;
                        case 303: s = "See Other"; break;
                        case 304: s = "Not Modified"; break;
                        case 305: s = "Use Proxy"; break;
                        case 306: s = "(Unused)"; break;
                        case 307: s = "Temporary Redirect"; break;
                        case 400: s = "Bad Request"; break;
                        case 401: s = "Unauthorized"; break;
                        case 402: s = "Payment Required"; break;
                        case 403: s = "Forbidden"; break;
                        case 404: s = "Not Found"; break;
                        case 405: s = "Method Not Allowed"; break;
                        case 406: s = "Not Acceptable"; break;
                        case 407: s = "Proxy Authentication Required"; break;
                        case 408: s = "Request Timeout"; break;
                        case 409: s = "Conflict"; break;
                        case 410: s = "Gone"; break;
                        case 411: s = "Length Required"; break;
                        case 412: s = "Precondition Failed"; break;
                        case 413: s = "Request Entity Too Large"; break;
                        case 414: s = "Request-URI Too Long"; break;
                        case 415: s = "Unsupported Media Type"; break;
                        case 416: s = "Requested Range Not Satisfiable"; break;
                        case 417: s = "Expectation Failed"; break;
                        case 500: s = "Internal Server Error"; break;
                        case 501: s = "Not Implemented"; break;
                        case 502: s = "Bad Gateway"; break;
                        case 503: s = "Service Unavailable"; break;
                        case 504: s = "Gateway Timeout"; break;
                        case 505: s = "HTTP Version Not Supported"; break;
                    }
                }
                catch (Exception ex)
                {
                    s = "Error decoding HTTPError Code : " + cv + " : " + ex.Message;
                }


                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "HTTPResult: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class windowsresultcode : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public windowsresultcode(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "WindowsResultCode";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating WindowsResultCode : {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating WindowsResultCode - Missing column number: {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();


            string s = "";
            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                try
                {
                    int res = Convert.ToInt32(cv);

                    s = new Win32Exception(res).Message;
                }
                catch (Exception ex)
                {
                    s = "Error decoding WindowsResultCode : " + cv + " : " + ex.Message;
                }


                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "WindowsResultCode: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    public class coallesce : Column
    {
        List<ColumnNumberOrColumn> _srccolumns = new List<ColumnNumberOrColumn>(); // the source column we will be trimming
        string _blankvalue = string.Empty;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "blankvalue":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        public coallesce(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Coallesce";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            ColumnNumberOrColumn cnc = null;

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (cnc != null)
                                    {
                                        throw new ApplicationException("Coallesce column had more than one value.");
                                    }

                                    cnc = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                                }
                            }

                            if (cnc == null || !cnc.IsValid)
                            {
                                throw new ApplicationException("Coallesce column had no value.");
                            }

                            _srccolumns.Add(cnc);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "blankvalue")
                        {
                            _blankvalue = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <BlankValue>" + _blankvalue.ToString() + "</BlankValue>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating Coallesce : {" + Id + "}", ex);
            }

            if (_srccolumns.Count == 0)
            {
                throw new ApplicationException("Error Creating Coallesce - No columns specified : {" + Id + "}");
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                foreach (ColumnNumberOrColumn c in _srccolumns)
                {
                    string cv = c.Value(line);

                    if (cv.Trim() != _blankvalue)
                    {
                        s = cv.Trim();
                        break;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Coallesce: {" + Id + "} --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

    }

    /// <summary>
    /// This column class is used to classify a number column into groups
    /// </summary>
    public class numberclassify : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be classifying
        double _minimum = double.NegativeInfinity;
        double _maximum = double.NegativeInfinity;
        double _intervals = double.NegativeInfinity;
        double _intervalsize = double.NegativeInfinity;
        const double _nan = double.NegativeInfinity;
        string _numberformat = "#";
        bool _float = false;
        enum RANGEDISPLAY { LOWER, UPPER, BOTH, MIDDLE };
        RANGEDISPLAY _rangeDisplay = RANGEDISPLAY.BOTH;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "minimum":
                case "maximum":
                case "intervalsize":
                case "intervals":
                case "float":
                case "format":
                case "rangedisplay":
                    return true;
                default:
                    return false;
            }
        }


        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public numberclassify(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "NumberClassify";

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "rangedisplay")
                        {
                            // get the output format
                            switch (n.InnerText.ToLower())
                            {
                                case "lower":
                                    _rangeDisplay = RANGEDISPLAY.LOWER;
                                    break;
                                case "upper":
                                    _rangeDisplay = RANGEDISPLAY.UPPER;
                                    break;
                                case "both":
                                    _rangeDisplay = RANGEDISPLAY.BOTH;
                                    break;
                                case "middle":
                                    _rangeDisplay = RANGEDISPLAY.MIDDLE;
                                    break;
                                default:
                                    throw new ApplicationException("Unknown range display NumberClassify : " + n.InnerText);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <RangeDisplay>" + n.InnerText + "</RangeDisplay>");
                            }
                        }
                        else if (n.Name.ToLower() == "minimum")
                        {
                            _minimum = Convert.ToDouble(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Minimum>" + _minimum.ToString() + "</Minimum>");
                            }
                        }
                        else if (n.Name.ToLower() == "format")
                        {
                            _numberformat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Format>" + _numberformat + "</Format>");
                            }
                        }
                        else if (n.Name.ToLower() == "maximum")
                        {
                            _maximum = Convert.ToDouble(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Maximum>" + _maximum.ToString() + "</Maximum>");
                            }
                        }
                        else if (n.Name.ToLower() == "intervals")
                        {
                            _intervals = Convert.ToDouble(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Invervals>" + _intervals.ToString() + "</Intervals>");
                            }
                        }
                        else if (n.Name.ToLower() == "intervalsize")
                        {
                            _intervalsize = Convert.ToDouble(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InvervalSize>" + _intervalsize.ToString() + "</IntervalSize>");
                            }
                        }
                        else if (n.Name.ToLower() == "flaot")
                        {
                            _float = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Float/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error reading parameters for numberclassify", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating NumberClassify - Missing column number: {" + Id + "}");
            }

            if (_minimum != _nan && _maximum != _nan && _intervals != _nan && _intervalsize == _nan)
            {
                _intervalsize = (_maximum - _minimum) / _intervals;
            }
            else if (_minimum != _nan && _maximum != _nan && _intervals == _nan && _intervalsize != _nan)
            {
                _intervals = (_maximum - _minimum) / _intervalsize;
            }
            else if (_minimum != _nan && _maximum == _nan && _intervals != _nan && _intervalsize != _nan)
            {
                _maximum = _minimum + _intervals * _intervalsize;
            }
            else if (_minimum == _nan && _maximum != _nan && _intervals != _nan && _intervalsize != _nan)
            {
                _minimum = _maximum - _intervals * _intervalsize;
            }
            else if (_minimum != _nan && _maximum == _nan && _intervals == _nan && _intervalsize != _nan)
            {
                // we cant work out maximum but no big deal
            }
            else if (_minimum == _nan && _maximum != _nan && _intervals == _nan && _intervalsize != _nan)
            {
                // we cant work out minimum but no big deal
            }
            else
            {
                throw new ApplicationException("Error Creating NumberClassify - Not enough information about minimum, maximum, intervals and intervalsize to classify: {" + Id + "}");
            }

            if (_minimum != _nan && _maximum != _nan)
            {
                if (_minimum > _maximum)
                {
                    throw new ApplicationException("Error Creating NumberClassify - Maximum smaller than Minimum: {" + Id + "}");
                }
            }
        }
        #endregion

        string FormatD(double d)
        {
            return string.Format("{0:" + _numberformat + "}", d);
        }

        string FormatI(int i)
        {
            return string.Format("{0:" + _numberformat + "}", i);
        }

        string GetClassification(double d)
        {
            if (_minimum == _nan)
            {
                int multiple = (int)((_maximum - d) / _intervalsize);
                if (_float)
                {
                    if (d > _maximum)
                    {
                        return ">" + FormatD(_maximum);
                    }
                    else
                    {
                        switch (_rangeDisplay)
                        {
                            case RANGEDISPLAY.BOTH:
                                return FormatD(_maximum - (multiple + 1) * _intervalsize) + "-" + FormatD(_maximum - multiple * _intervalsize);
                            case RANGEDISPLAY.LOWER:
                                return FormatD(_maximum - (multiple + 1) * _intervalsize);
                            case RANGEDISPLAY.UPPER:
                                return FormatD(_maximum - multiple * _intervalsize);
                            case RANGEDISPLAY.MIDDLE:
                                return FormatD(_maximum - (multiple + 0.5) * _intervalsize);
                        }
                    }
                }
                else
                {
                    if (d > _maximum)
                    {
                        return ">" + FormatI((int)_maximum);
                    }
                    else
                    {
                        switch (_rangeDisplay)
                        {
                            case RANGEDISPLAY.BOTH:
                                return FormatI((int)(_maximum - (multiple + 1) * _intervalsize)) + "-" + FormatI((int)(_maximum - multiple * _intervalsize));
                            case RANGEDISPLAY.LOWER:
                                return FormatI((int)(_maximum - (multiple + 1) * _intervalsize));
                            case RANGEDISPLAY.UPPER:
                                return FormatI((int)(_maximum - multiple * _intervalsize));
                            case RANGEDISPLAY.MIDDLE:
                                return FormatI((int)(_maximum - (multiple + 0.5) * _intervalsize));
                        }
                    }
                }

            }
            else
            {
                int multiple = ((int)((d - _minimum) / _intervalsize));
                if (_float)
                {
                    if (d < _minimum)
                    {
                        return "<" + FormatD(_minimum);
                    }
                    else if (_maximum != _nan && d > _maximum)
                    {
                        return ">" + FormatD(_maximum);
                    }
                    else
                    {
                        switch (_rangeDisplay)
                        {
                            case RANGEDISPLAY.BOTH:
                                return FormatD(_minimum + multiple * _intervalsize) + "-" + FormatD(_minimum + (multiple + 1) * _intervalsize);
                            case RANGEDISPLAY.LOWER:
                                return FormatD(_minimum + multiple * _intervalsize);
                            case RANGEDISPLAY.UPPER:
                                return FormatD(_minimum + (multiple + 1) * _intervalsize);
                            case RANGEDISPLAY.MIDDLE:
                                return FormatD(_minimum + (multiple + 0.5) * _intervalsize);
                        }
                    }
                }
                else
                {
                    if (d < _minimum)
                    {
                        return "<" + FormatI((int)_minimum);
                    }
                    else if (_maximum != _nan && d > _maximum)
                    {
                        return ">" + FormatI((int)_maximum);
                    }
                    else
                    {
                        switch (_rangeDisplay)
                        {
                            case RANGEDISPLAY.BOTH:
                                return FormatI((int)(_minimum + multiple * _intervalsize)) + "-" + FormatI((int)(_minimum + (multiple + 1) * _intervalsize));
                            case RANGEDISPLAY.LOWER:
                                return FormatI((int)(_minimum + multiple * _intervalsize));
                            case RANGEDISPLAY.UPPER:
                                return FormatI((int)(_minimum + (multiple + 1) * _intervalsize));
                            case RANGEDISPLAY.MIDDLE:
                                return FormatI((int)(_minimum + (multiple + 0.5) * _intervalsize));
                        }
                    }
                }
            }

            return string.Empty;
        }

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                s = GetClassification(Convert.ToDouble(cv));

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "NumberClassify: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to classify a date column into groups
    /// </summary>
    public class datetimeclassify : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be classifying
        DateTime _minimum = DateTime.MinValue;
        DateTime _maximum = DateTime.MinValue;
        long _intervals = long.MinValue;
        TimeSpan _intervalsize = TimeSpan.MinValue;
        List<string> _inputformat = new List<string>(); // input date format
        string _outputformat = string.Empty; // output date format
        TimeSpan _adjustBy = TimeSpan.MinValue;
        ColumnNumberOrColumn _adjustByColumnTimeSpan = null;
        ColumnNumberOrColumn _adjustByColumnSeconds = null;
        bool _adjustByFixed = true;
        enum RANGEDISPLAY { LOWER, UPPER, BOTH, MIDDLE };
        RANGEDISPLAY _rangeDisplay = RANGEDISPLAY.BOTH;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "minimum":
                case "maximum":
                case "intervalsize":
                case "intervals":
                case "informat":
                case "outformat":
                case "adjustby":
                case "adjustbycolumntimespan":
                case "adjustbycolumnseconds":
                case "rangedisplay":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public datetimeclassify(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "DateTimeClassify";

            // get the informat first as we need it
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "informat")
                    {
                        // get the input format
                        _inputformat.Add(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + n.InnerText + "</InFormat>");
                        }
                    }
                }
            }

            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "outformat")
                        {
                            // get the output format
                            _outputformat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outputformat + "</OutFormat>");
                            }
                        }
                        else if (n.Name.ToLower() == "rangedisplay")
                        {
                            // get the output format
                            switch (n.InnerText.ToLower())
                            {
                                case "lower":
                                    _rangeDisplay = RANGEDISPLAY.LOWER;
                                    break;
                                case "upper":
                                    _rangeDisplay = RANGEDISPLAY.UPPER;
                                    break;
                                case "both":
                                    _rangeDisplay = RANGEDISPLAY.BOTH;
                                    break;
                                case "middle":
                                    _rangeDisplay = RANGEDISPLAY.MIDDLE;
                                    break;
                                default:
                                    throw new ApplicationException("Unknown range display DateTimeClassify : " + n.InnerText);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <RangeDisplay>" + n.InnerText + "</RangeDisplay>");
                            }
                        }
                        else if (n.Name.ToLower() == "adjustby")
                        {
                            // parse the adjustby timespan
                            try
                            {
                                _adjustBy = TimeSpan.Parse(n.InnerText);
                                _adjustByFixed = true;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error parsing DateTimeClassify adjustment", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AdjustBy>" + _adjustBy.ToString() + "</AdjustBy>");
                            }
                        }
                        else if (n.Name.ToLower() == "adjustbycolumntimespan")
                        {
                            // read the column containing the timespan to adjust by
                            try
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <AdjustByColumnTimespan>");
                                }
                                _adjustByColumnTimeSpan = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </AdjustByColumnTimespan>");
                                }
                                _adjustByFixed = false;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading adjust by column for datetimeclassify.\n" + n.InnerText, ex);
                            }
                        }
                        else if (n.Name.ToLower() == "adjustbycolumnseconds")
                        {
                            // read the column containing the seconds to adjust by
                            try
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <AdjustByColumnSeconds>");
                                }
                                _adjustByColumnSeconds = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </AdjustByColumnSeconds>");
                                }
                                _adjustByFixed = false;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading adjust by column for datetimeclassify.\n" + n.InnerText, ex);
                            }
                        }
                        else if (n.Name.ToLower() == "minimum")
                        {
                            _minimum = DateTime.ParseExact(n.InnerText, _inputformat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Minimum>" + _minimum.ToString() + "</Minimum>");
                            }
                        }
                        else if (n.Name.ToLower() == "maximum")
                        {
                            _maximum = DateTime.ParseExact(n.InnerText, _inputformat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Maximum>" + _maximum.ToString() + "</Maximum>");
                            }
                        }
                        else if (n.Name.ToLower() == "intervals")
                        {
                            _intervals = Convert.ToInt64(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Invervals>" + _intervals.ToString() + "</Intervals>");
                            }
                        }
                        else if (n.Name.ToLower() == "intervalsize")
                        {
                            _intervalsize = TimeSpan.Parse(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <IntervalSize>" + _intervalsize.ToString() + "</IntervalSize>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error reading parameters for DateTimeClassify", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating DateTimeClassify - Missing column number: {" + Id + "}");
            }

            if (_minimum != DateTime.MinValue && _maximum != DateTime.MinValue && _intervals != long.MinValue && _intervalsize == TimeSpan.MinValue)
            {
                _intervalsize = new TimeSpan(((TimeSpan)(_maximum - _minimum)).Ticks / _intervals);
            }
            else if (_minimum != DateTime.MinValue && _maximum != DateTime.MinValue && _intervals == long.MinValue && _intervalsize != TimeSpan.MinValue)
            {
                _intervals = ((TimeSpan)(_maximum - _minimum)).Ticks / _intervalsize.Ticks;
            }
            else if (_minimum != DateTime.MinValue && _maximum == DateTime.MinValue && _intervals != long.MinValue && _intervalsize != TimeSpan.MinValue)
            {
                _maximum = _minimum + new TimeSpan(_intervals * _intervalsize.Ticks);
            }
            else if (_minimum == DateTime.MinValue && _maximum != DateTime.MinValue && _intervals != long.MinValue && _intervalsize != TimeSpan.MinValue)
            {
                _minimum = _maximum - new TimeSpan(_intervals * _intervalsize.Ticks);
            }
            else if (_minimum != DateTime.MinValue && _maximum == DateTime.MinValue && _intervals == long.MinValue && _intervalsize != TimeSpan.MinValue)
            {
                // we cant work out maximum but no big deal
            }
            else if (_minimum == DateTime.MinValue && _maximum != DateTime.MinValue && _intervals == long.MinValue && _intervalsize != TimeSpan.MinValue)
            {
                // we cant work out minimum but no big deal
            }
            else
            {
                throw new ApplicationException("Error Creating DateTimeClassify - Not enough information about minimum, maximum, intervals and intervalsize to classify: {" + Id + "}");
            }

            if (_minimum > _maximum)
            {
                throw new ApplicationException("Error Creating DateTimeClassify - Maximum smaller than Minimum: {" + Id + "}");
            }

        }
        #endregion

        string GetClassification(DateTime d)
        {
            if (_minimum == DateTime.MinValue)
            {
                if (d > _maximum)
                {
                    return ">" + _maximum.ToString(_outputformat);
                }
                else
                {
                    int multiple = (int)(((TimeSpan)(_maximum - d)).Ticks / _intervalsize.Ticks);
                    switch (_rangeDisplay)
                    {
                        case RANGEDISPLAY.BOTH:
                            return (_maximum - new TimeSpan((multiple + 1) * _intervalsize.Ticks)).ToString(_outputformat) + "-" + (_maximum - new TimeSpan(multiple * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.LOWER:
                            return (_maximum - new TimeSpan((multiple + 1) * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.UPPER:
                            return (_maximum - new TimeSpan(multiple * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.MIDDLE:
                            return (_maximum - new TimeSpan((long)(((double)multiple + 0.5) * (double)_intervalsize.Ticks))).ToString(_outputformat);
                    }
                }
            }
            else
            {
                if (d < _minimum)
                {
                    return "<" + _minimum.ToString(_outputformat);
                }
                else if (_maximum != DateTime.MinValue && d > _maximum)
                {
                    return ">" + _maximum.ToString(_outputformat);
                }
                else
                {
                    int multiple = ((int)(((TimeSpan)(d - _minimum)).Ticks / _intervalsize.Ticks));
                    switch (_rangeDisplay)
                    {
                        case RANGEDISPLAY.BOTH:
                            return (_minimum + new TimeSpan(multiple * _intervalsize.Ticks)).ToString(_outputformat) + "-" + (_minimum + new TimeSpan((multiple + 1) * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.LOWER:
                            return (_minimum + new TimeSpan(multiple * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.UPPER:
                            return (_minimum + new TimeSpan((multiple + 1) * _intervalsize.Ticks)).ToString(_outputformat);
                        case RANGEDISPLAY.MIDDLE:
                            return (_minimum + new TimeSpan((long)(((double)multiple + 0.5) * (double)_intervalsize.Ticks))).ToString(_outputformat);
                    }
                }
            }

            return string.Empty;
        }

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string ss = string.Empty;

            try
            {
                // get the column value
                string cvdt = _srccolumn.Value(line);

                DateTime dt = DateTime.ParseExact(cvdt, _inputformat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);

                // if we are to adjust it by a fixed amount
                if (_adjustByFixed)
                {
                    // adjust it
                    dt = dt + _adjustBy;
                }
                else
                {
                    // if we are to use a column containing a timespan
                    if (_adjustByColumnTimeSpan != null)
                    {
                        // get the column value
                        string cv = _adjustByColumnTimeSpan.Value(line);

                        // parse the timespan
                        TimeSpan adjustBy = TimeSpan.Parse(cv);

                        // adjust the date
                        dt = dt + adjustBy;
                    }
                    // if we are to adjust by a number of seconds
                    else if (_adjustByColumnSeconds != null)
                    {
                        // get the column value
                        string cv = _adjustByColumnSeconds.Value(line);

                        // read the seconds and convert it to a timespan
                        double secs = Convert.ToDouble(cv);
                        int h = (int)(secs / 3600);
                        secs = secs - h * 3600;
                        int m = (int)(secs / 60);
                        secs = secs - m * 60;
                        int s = (int)secs;
                        secs = secs - s;
                        TimeSpan adjustBy = new TimeSpan(0, h, m, s, (int)(secs * 1000));

                        // adjust the time
                        dt = dt + adjustBy;
                    }
                }

                ss = GetClassification(dt);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "DateTimeClassify: {" + Id + "} '" + cvdt + "' --> '" + ss + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(ss);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Get the value of a system or command line variable
    /// </summary>
    public class callsubroutine : Column
    {
        #region Member Variables
        List<Column> _parms = new List<Column>(); // the source columns
        string _subroutine = string.Empty; // name of the variable
        #endregion

        #region Constructors
        /// <summary>
        /// Create a system variable column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public callsubroutine(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "CallSubroutine";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "subroutine")
                        {
                            // get the name of the variable
                            _subroutine = n.InnerText.Trim().ToLower();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Subroutine>" + _subroutine + "</Subroutine>");
                            }
                        }
                        else if (n.Name.ToLower() == "parameters")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Parameters>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    // store the source column processor
                                    _parms.Add(Column.Create(n1, j, p, od, tracexml, prefix + "    "));
                                }
                            }

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Parameters>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating CallSubroutine: {" + Id + "}", ex);
            }

            if (_subroutine == string.Empty)
            {
                throw new ApplicationException("Error Creating CallSubroutine - Missing subroutine: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "parameters":
                case "subroutine":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                ColumnLine parms = new ColumnLine();
                for (int i = 0; i < _parms.Count; i++)
                {
                    parms.AddEnd(((Column)_parms[i]).Process(line));
                }

                ColumnLine cl = _p.Subroutines.Call(_subroutine, parms);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "CallSubroutine: {" + Id + "}  '" + _subroutine + "'(" + parms.ToString() + ") --> '" + cl.ToString() + "'" + ANSIHelper.White);
                }

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    /// <summary>
    /// Trim spaces off a column
    /// </summary>
    public class trimcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public trimcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "TrimColumn";
            // extract the source column
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error reading source column for trimcolumn", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating TrimColumn - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public override ColumnLine ProcessMulti(ColumnLine line)
        {
            if (_srccolumn.ColumnNumber != null && _srccolumn.ColumnNumber.All)
            {
                ColumnLine lineout = new ColumnLine();

                foreach (string s in line.Cols)
                {
                    lineout.AddEnd(s.Trim());
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "TrimColumn: {" + Id + "}  '" + lineout.ToString() + "'" + ANSIHelper.White);
                }

                return lineout;
            }
            else
            {
                throw new ApplicationException("TrimColumn:ProcessMulti Should not get here!");
            }
        }

        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            if (_srccolumn.ColumnNumber != null && _srccolumn.ColumnNumber.All)
            {
                throw new UseMultItemProcess();
            }
            else
            {
                try
                {
                    // get the column value
                    string cv = _srccolumn.Value(line);

                    s = cv.Trim();

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "TrimColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
                }
            }
            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to read hexadecimal data
    /// </summary>
    public class hexread : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be dumping
        enum CONVERTTO { STRING, INT, FLOAT, DOUBLE };
        CONVERTTO _convertto = CONVERTTO.STRING;
        #endregion

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public hexread(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "HexRead";

            // read the XML
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "converto")
                    {
                        switch (n.InnerText.Trim().ToLower())
                        {
                            case "string":
                            case "char":
                                _convertto = CONVERTTO.STRING;
                                break;
                            case "int":
                            case "integer":
                                _convertto = CONVERTTO.INT;
                                break;
                            default:
                                throw new ApplicationException("HexRead Constructor: Unknown ConvertTo " + n.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ConvertTo>" + n.InnerText + "</ConvertTo>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating HexRead - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "convertto":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                switch (_convertto)
                {
                    case CONVERTTO.STRING:
                        for (int i = 0; i < cv.Length; i++)
                        {
                            string c = "0x" + cv.Substring(i, 2);
                            c = Convert.ToChar(Int16.Parse(c)).ToString();
                            s += c;
                        }
                        break;
                    case CONVERTTO.INT:
                        cv = "0x" + cv;
                        s = Int32.Parse(cv).ToString();
                        break;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "HexRead: {" + Id + "} '" + cv + "' as " + _convertto.ToString() + " --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to trim spaces off a column
    /// </summary>
    public class zscorecolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        double _average = 0.0; // the average
        double _stddev = 0.0; // the standard deviation

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "average":
                case "stddev":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public zscorecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "ZScoreColumn";
            // read the XML
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "average")
                    {

                        try
                        {
                            _average = Convert.ToDouble(n.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error reading average", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Average>" + _average.ToString() + "</Average>");
                        }
                    }
                    else if (n.Name.ToLower() == "stddev")
                    {

                        try
                        {
                            _stddev = Convert.ToDouble(n.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error reading standard deviation", ex);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <StdDev>" + _stddev.ToString() + "</StdDev>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating ZScoreColumn - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                try
                {
                    double d = Convert.ToDouble(cv);

                    double zscore = (d - _average) / _stddev;

                    s = zscore.ToString();

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "ZScoreColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    s = "Error calculating ZScore : " + ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to round a number
    /// </summary>
    public class roundcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be trimming
        enum action { UP, DOWN, NORMAL }; // the average
        action _action = action.NORMAL;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "up":
                case "down":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public roundcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RoundColumn";

            // read the XML
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "up")
                    {
                        if (_action == action.DOWN)
                        {
                            throw new ApplicationException("RoundColumn::Constructor - you cant specify <Up/> and <Down/>");
                        }

                        _action = action.UP;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Up/>");
                        }
                    }
                    else if (n.Name.ToLower() == "down")
                    {
                        if (_action == action.UP)
                        {
                            throw new ApplicationException("RoundColumn::Constructor - you cant specify <Up/> and <Down/>");
                        }

                        _action = action.DOWN;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Down/>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating RoundColumn - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                try
                {
                    double d = Convert.ToDouble(cv);
                    long l = 0;
                    switch (_action)
                    {
                        case action.UP:
                            l = (long)(d + 1);
                            break;
                        case action.DOWN:
                            l = (long)(d);
                            break;
                        case action.NORMAL:
                            l = (long)Math.Round(d, MidpointRounding.AwayFromZero);
                            break;
                    }

                    s = l.ToString();

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "RoundColumn: {" + Id + "} '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                    }
                }
                catch (Exception ex)
                {
                    s = "Error calculating RoundColumn {" + Id + "} : " + ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to dump data in Hexadecimal
    /// </summary>
    public class hexdumpcolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be dumping
        string _break1display = "";
        string _break4display = "";
        string _break16display = "";
        bool _displayascii = false;

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "byte1break":
                case "byte4break":
                case "byte16break":
                case "displayascii":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public hexdumpcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "HexDumpColumn";

            // read the XML
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "byte1break")
                    {
                        _break1display = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Byte1Break>" + _break1display + "</Byte1Break>");
                        }
                    }
                    else if (n.Name.ToLower() == "byte4break")
                    {
                        _break4display = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Byte4Break>" + _break4display + "</Byte4Break>");
                        }
                    }
                    else if (n.Name.ToLower() == "byte16break")
                    {
                        _break16display = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Byte16Break>" + _break16display + "</Byte16Break>");
                        }
                    }
                    else if (n.Name.ToLower() == "displayascii")
                    {
                        _displayascii = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <DisplayASCII/>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating HexDumpColumn - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                s = fixedparser.ColumnParsingRule.HexString(cv, _break1display, _break4display, _break16display);

                if (_displayascii)
                {
                    s = s + "  | ";
                    foreach (char c in cv)
                    {
                        if ((int)c < 32 || (int)c > 127)
                        {
                            s = s + ".";
                        }
                        else
                        {
                            s = s + c;
                        }
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "HexDumpColumn: {" + Id + "} --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// This column class is used to change the case of a column
    /// </summary>
    public class casecolumn : Column
    {
        ColumnNumberOrColumn _srccolumn = null; // the source column we will be changing case of
        enum ChangeCase { LOWER, UPPER, PROPER }; // available cases
        ChangeCase _case = ChangeCase.LOWER; // case to change to
        enum State { WORD, NONWORD };

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "case":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public casecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "CaseColumn";
            // iterate through the nodes
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "case")
                    {
                        switch (n.InnerText.ToLower().Trim())
                        {
                            case "lower":
                                _case = ChangeCase.LOWER;
                                break;
                            case "upper":
                                _case = ChangeCase.UPPER;
                                break;
                            case "proper":
                                _case = ChangeCase.PROPER;
                                break;
                            default:
                                throw new ApplicationException("Unknown case in CaseColumn : {" + Id + "} : " + n.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Case>" + n.InnerText + "</Case>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating CaseColumn - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                switch (_case)
                {
                    case ChangeCase.UPPER:
                        s = cv.ToUpper();
                        break;
                    case ChangeCase.LOWER:
                        s = cv.ToLower();
                        break;
                    case ChangeCase.PROPER:
                        s = ProperCase(cv);
                        break;
                    default:
                        break;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "CaseColumn: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            // return result
            return cl;
        }
        #endregion

        #region Private Functions

        /// <summary>
        /// Convert string to proper case
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        string ProperCase(string s)
        {
            string rc = string.Empty;
            State state = State.NONWORD;

            foreach (char c in s)
            {
                switch (state)
                {
                    case State.NONWORD:
                        if (char.IsLetter(c))
                        {
                            rc = rc + char.ToUpper(c);
                            state = State.WORD;
                        }
                        else
                        {
                            rc = rc + c;
                        }
                        break;
                    case State.WORD:
                        if (char.IsLetter(c))
                        {
                            rc = rc + char.ToLower(c);
                        }
                        else
                        {
                            rc = rc + c;
                            state = State.NONWORD;
                        }
                        break;
                    default:
                        break;
                }
            }

            return rc;
        }
        #endregion

    }

    /// <summary>
    /// A column which decodes a given value from another CSV file
    /// </summary>
    public class lookup : Column
    {
        List<ColumnNumberOrColumn> _keySrc = new List<ColumnNumberOrColumn>(); // source file key columns
        Column _key = null;
        DecodeTable _dt = null; // the lookup file to use
        StringOrColumn _targetColumn; // target column to extract
        int _targetColumnNum = -1;
        bool _useinputrules = false;
        bool _includeallbutkeys = false;
        bool _fastfailblank = false;
        bool _all = false;
        bool _dumpLookup = false;

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string res = string.Empty; // the result

            try
            {
                string key = string.Empty;

                if (_key != null)
                {
                    key = _key.Process(line).ToString(false).Trim();
                }
                else
                {
                    // construct the key
                    foreach (ColumnNumberOrColumn n in _keySrc)
                    {
                        key += n.Value(line).Trim();
                    }
                }

                // lookup the row
                object row = _dt.Decode(key, _fastfailblank);

                // If we got back a string
                if (row.GetType() == typeof(System.String))
                {
                    // that must be our result
                    res = (string)row;
                }
                else
                {
                    // if we are extracting all columns
                    if (_includeallbutkeys)
                    {
                        res = string.Empty;
                        for (int i = 0; i < ((ColumnLine)row).Count; i++)
                        {
                            bool iskeycol = false;
                            foreach (string c in _dt.KeyCols)
                            {
                                if (Int32.Parse(c) == i)
                                {
                                    iskeycol = true;
                                    break;
                                }
                            }

                            if (!iskeycol)
                            {
                                if (res != string.Empty)
                                {
                                    res += ",";
                                }
                                res += ((ColumnLine)row)[i];
                            }
                        }
                    }
                    else if (_all)
                    {
                        // just get the whole lookup line
                        res = ((ColumnLine)row).Raw;
                    }
                    else
                    {
                        // look up the code and return it
                        try
                        {
                            res = (((ColumnLine)row)[_targetColumnNum]);
                        }
                        catch
                        {
                            // throw new ApplicationException("Problem looking up file : " + ex.Message);
                            res = key;
                        }
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Lookup: {" + Id + "}  '" + key + "' --> '" + res + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            if (_safe && (_includeallbutkeys || _all))
            {
                ColumnLine cl2 = new CSVFileLine(res, 0);
                cl2.Parse();
                cl.AddEnd(cl2);
            }
            else
            {
                cl.AddEnd(res);
            }

            PostProcess();

            // return the result
            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "sourcekeys":
                case "targetkeys":
                case "lookupfile":
                case "targetcolumn":
                case "includeallbutkeys":
                case "useinputparsingrules":
                case "fastfailifblank":
                case "default":
                case "dumplookup":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public lookup(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Lookup";
            string file = string.Empty; // name of the file
            List<string> keyTgt = null; // list of key columns in  the lookup file
            string defaultvalue = string.Empty; // the default value if a code is not found
            bool usekey = true; // flag to indicate if the default should be the code

            try
            {
                // iterate through the nodes
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "sourcekeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <SourceKeys>");
                            }
                            foreach (XmlNode n2 in n.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the source key columns
                                        try
                                        {
                                            ColumnNumberOrColumn cn = new ColumnNumberOrColumn(n2, j, p, od, tracexml, prefix + "   ");
                                            _keySrc.Add(cn);
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading lookup source column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        _key = Column.Create(n2, j, p, od, tracexml, prefix + "  ");
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </SourceKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetkeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <TargetKeys>");
                            }
                            // create a target key list
                            keyTgt = new List<string>();

                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the target key columns
                                        try
                                        {
                                            StringOrColumn cn = new StringOrColumn(n3, j, p, od, tracexml, prefix + "   ");
                                            keyTgt.Add(cn.Value());
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading lookup target column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Lookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </TargetKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "useinputparsingrules")
                        {
                            _useinputrules = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <UseInputParsingRules/>");
                            }
                        }
                        else if (n.Name.ToLower() == "fastfailifblank")
                        {
                            _fastfailblank = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FastFailIfBlank/>");
                            }
                        }
                        else if (n.Name.ToLower() == "dumplookup")
                        {
                            _dumpLookup = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <DumpLookup/>");
                            }
                        }
                        else if (n.Name.ToLower() == "includeallbutkeys")
                        {
                            _includeallbutkeys = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <IncludeAllButKeys/>");
                            }
                        }
                        else if (n.Name.ToLower() == "lookupfile")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LookupFile>");
                            }
                            StringOrColumn sc = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            file = sc.Value();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    " + file);
                                CSVProcessor.DisplayError(prefix + "  </LookupFile>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetcolumn")
                        {
                            // save the target column to extract
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<TargetColumn>");
                            }
                            try
                            {
                                _targetColumn = new StringOrColumn(n, j, p, od, tracexml, "   " + prefix);
                                if (_targetColumn.Value().Trim() == "*")
                                {
                                    _safe = true;
                                    _all = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading lookup target column", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "</TargetColumn>");
                            }
                        }
                        else if (n.Name.ToLower() == "default")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Default>");
                            }
                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "usekey")
                                    {
                                        // we should return the key if the key is not found
                                        usekey = true;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <UseKey/>");
                                        }
                                    }
                                    else if (n3.Name.ToLower() == "value")
                                    {
                                        // we should return the given value if the key is not found
                                        usekey = false;
                                        defaultvalue = n3.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Value>" + defaultvalue + "</Value>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Lookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Default>");
                            }
                        }
                    }
                }

                if (_targetColumn == null && !_includeallbutkeys)
                {
                    throw new ApplicationException("Lookup does not have a target column or request to include all.");
                }

                if (_targetColumn != null && _includeallbutkeys)
                {
                    throw new ApplicationException("Lookup can't have both a target column and request to include all.");
                }

                if (_keySrc.Count == 0 && _key == null)
                {
                    throw new ApplicationException("Lookup does not have a source key column.");
                }
                else if (_keySrc.Count > 0 && _key != null)
                {
                    throw new ApplicationException("Lookup can't have source keys and a source output column.");
                }

                // if we got a file name and a target key then load the lookup file
                if (file != string.Empty && keyTgt != null)
                {
                    try
                    {
                        if (_useinputrules)
                        {
                            // load the lookup file
                            _dt = new DecodeTable(file, keyTgt, _dumpLookup, false, p.Input.Delimiter, p.Input.Quote, p.Input.QuoteEscape);
                        }
                        else
                        {
                            // load the lookup file
                            _dt = new DecodeTable(file, keyTgt, _dumpLookup, false);
                        }
                        _targetColumn.SetStringValue(_dt.ColumnIndex(_targetColumn.Value()));
                        if (_targetColumn.Value() != "*")
                        {
                            _targetColumnNum = Int32.Parse(_targetColumn.Value());
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Problem loading lookup file : " + file, ex);
                    }
                }
                else
                {
                    throw new ApplicationException("Lookup file incompletely specified.");
                }

                // Tell the table whether or not to return the key
                _dt.UseOriginal = usekey;

                // if we are not returning the key then set the default value
                if (!usekey)
                {
                    _dt.Default = defaultvalue;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Lookup: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    public class multilookup : Column
    {
        List<ColumnNumberOrColumn> _keySrc = new List<ColumnNumberOrColumn>(); // source file key columns
        Column _key = null;
        LogicFilterNode _f = new AndFilterNode(); // holds the default AND filter
        MultiDecodeTable _dt = null; // the lookup file to use
        StringOrColumn _targetColumn = null; // target column to extract
        int _targetColumnNum = -1;
        bool _useinputrules = false;
        bool _includeallbutkeys = false;
        bool _fastfailblank = false;
        bool _all = false;
        string _formula;
        public enum Totals { First, Last, FirstNonBlank, LastNonBlank, SumInt, SumFloat, Average, Appended, Count, UniqueAppended, Formula };
        Totals _totals = Totals.First;
        string _appendSeparator = "|";
        arithmatic.round _round = arithmatic.round.round_none;

        #region Private Functions

        /// <summary>
        /// Parse the if condition
        /// </summary>
        /// <param name="fn"></param>
        /// <param name="n"></param>
        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, _p, tracexml, prefix + "  ")));
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string res = string.Empty; // the result

            try
            {
                string key = string.Empty;

                if (_key != null)
                {
                    key = _key.Process(line).ToString(false).Trim();
                }
                else
                {
                    // construct the key
                    foreach (ColumnNumberOrColumn n in _keySrc)
                    {
                        key += n.Value(line).Trim();
                    }
                }

                // lookup the row
                List<object> row = _dt.Decode(key, _f, _fastfailblank);

                string value = "";
                int sumi = 0;
                float sumf = 0;
                double sumd = 0;
                int count = 0;
                bool first = true;

                foreach (var r in row)
                {
                    // If we got back a string
                    if (r.GetType() == typeof(System.String))
                    {
                        // that must be our result
                        res = (string)r;
                    }
                    else
                    {
                        // if we are extracting all columns
                        if (_includeallbutkeys)
                        {
                            res = string.Empty;
                            for (int i = 0; i < ((ColumnLine)r).Count; i++)
                            {
                                bool iskeycol = false;
                                foreach (string c in _dt.KeyCols)
                                {
                                    if (Int32.Parse(c) == i)
                                    {
                                        iskeycol = true;
                                        break;
                                    }
                                }

                                if (!iskeycol)
                                {
                                    if (res != string.Empty)
                                    {
                                        res += ",";
                                    }
                                    res += ((ColumnLine)r)[i];
                                }
                            }
                        }
                        else if (_all)
                        {
                            // just get the whole lookup line
                            res = ((ColumnLine)r).Raw;
                        }
                        else
                        {
                            // look up the code and return it
                            try
                            {
                                res = (((ColumnLine)r)[_targetColumnNum]);
                            }
                            catch
                            {
                                // throw new ApplicationException("Problem looking up file : " + ex.Message);
                                res = key;
                            }
                        }
                    }

                    switch (_totals)
                    {
                        case Totals.UniqueAppended:
                            {
                                Regex rua = new Regex("(^|\\|)" + res + "(\\||$)");
                                if (rua.IsMatch(value))
                                {
                                    // already there so ignore it
                                }
                                else
                                {
                                    if (value != "") value += _appendSeparator;
                                    value += res;
                                }
                            }
                            break;
                        case Totals.Appended:
                            if (value != "") value += _appendSeparator;
                            value += res;
                            break;
                        case Totals.Average:
                            try
                            {
                                sumf += float.Parse(res);
                                count++;
                            }
                            catch { }
                            break;
                        case Totals.Formula:
                            string s = _formula;
                            s = s.Replace("[-1]", sumd.ToString());
                            s = s.Replace("[0]", res);

                            try
                            {
                                sumd = arithmatic.CalcFloat(s, _round);
                                if (_trace)
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Trace + "MultiLookup: {" + Id + "}       '" + s + "' --> '" + sumd.ToString() + "'" + ANSIHelper.White);
                                }
                            }
                            catch (Exception ex)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Error + "MultiLookup: {" + Id + "}       '" + s + "' threw exception when being evaluated." + ANSIHelper.White);
                                throw new ApplicationException("Error evaluating formula '" + s + "'", ex);
                            }
                            break;
                        case Totals.Count:
                            count++;
                            break;
                        case Totals.First:
                            if (first)
                            {
                                first = false;
                                value = res;
                            }
                            break;
                        case Totals.FirstNonBlank:
                            if (value == "") value = res;
                            break;
                        case Totals.Last:
                            value = res;
                            break;
                        case Totals.LastNonBlank:
                            if (res != "") value = res;
                            break;
                        case Totals.SumFloat:
                            try
                            {
                                sumf += float.Parse(res);
                            }
                            catch { }
                            break;
                        case Totals.SumInt:
                            try
                            {
                                sumi += int.Parse(res);
                            }
                            catch { }
                            break;
                    }
                }

                switch (_totals)
                {
                    case Totals.UniqueAppended:
                        res = value;
                        break;
                    case Totals.Appended:
                        res = value;
                        break;
                    case Totals.Average:
                        if (count == 0)
                        {
                            res = "0";
                        }
                        else
                        {
                            res = (sumf / count).ToString();
                        }
                        break;
                    case Totals.Count:
                        res = count.ToString();
                        break;
                    case Totals.First:
                        res = value;
                        break;
                    case Totals.FirstNonBlank:
                        res = value;
                        break;
                    case Totals.Last:
                        res = value;
                        break;
                    case Totals.LastNonBlank:
                        res = value;
                        break;
                    case Totals.Formula:
                        res = sumd.ToString();
                        break;
                    case Totals.SumFloat:
                        res = sumf.ToString();
                        break;
                    case Totals.SumInt:
                        res = sumi.ToString();
                        break;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "MultiLookup: {" + Id + "}  '" + key + "' --> '" + res + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            if (_safe && (_includeallbutkeys || _all))
            {
                ColumnLine cl2 = new CSVFileLine(res, 0);
                cl2.Parse();
                cl.AddEnd(cl2);
            }
            else
            {
                cl.AddEnd(res);
            }

            PostProcess();

            // return the result
            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "sourcekeys":
                case "targetkeys":
                case "lookupfile":
                case "targetcolumn":
                case "condition":
                case "includeallbutkeys":
                case "useinputparsingrules":
                case "fastfailifblank":
                case "default":
                case "totals":
                case "appendseparator":
                case "dumplookup":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public multilookup(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "MultiLookup";
            string file = string.Empty; // name of the file
            List<string> keyTgt = null; // list of key columns in  the lookup file
            string defaultvalue = string.Empty; // the default value if a code is not found
            bool usekey = true; // flag to indicate if the default should be the code
            bool _dumpLookup = false;

            try
            {
                // iterate through the nodes
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "sourcekeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <SourceKeys>");
                            }
                            foreach (XmlNode n2 in n.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the source key columns
                                        try
                                        {
                                            ColumnNumberOrColumn cn = new ColumnNumberOrColumn(n2, j, p, od, tracexml, prefix + "   ");
                                            _keySrc.Add(cn);
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading lookup source column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        _key = Column.Create(n2, j, p, od, tracexml, prefix + "  ");
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </SourceKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetkeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <TargetKeys>");
                            }
                            // create a target key list
                            keyTgt = new List<string>();

                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the target key columns
                                        try
                                        {
                                            StringOrColumn cn = new StringOrColumn(n3, j, p, od, tracexml, prefix + "   ");
                                            keyTgt.Add(cn.Value());
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading lookup target column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Lookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </TargetKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "totals")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Totals>");
                            }

                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "first")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <First/>");
                                        }
                                        _totals = Totals.First;
                                    }
                                    else if (n3.Name.ToLower() == "firstnonblank")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <FirstNonBlank/>");
                                        }
                                        _totals = Totals.FirstNonBlank;
                                    }
                                    else if (n3.Name.ToLower() == "last")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Last/>");
                                        }
                                        _totals = Totals.Last;
                                    }
                                    else if (n3.Name.ToLower() == "lastnonblank")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <LastNonBlank/>");
                                        }
                                        _totals = Totals.LastNonBlank;
                                    }
                                    else if (n3.Name.ToLower() == "count")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Count/>");
                                        }
                                        _totals = Totals.Count;
                                    }
                                    else if (n3.Name.ToLower() == "sumint")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <SumInt/>");
                                        }
                                        _totals = Totals.SumInt;
                                    }
                                    else if (n3.Name.ToLower() == "sumfloat")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <SumFloat/>");
                                        }
                                        _totals = Totals.SumFloat;
                                    }
                                    else if (n3.Name.ToLower() == "average")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Average/>");
                                        }
                                        _totals = Totals.Average;
                                    }
                                    else if (n3.Name.ToLower() == "append")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Append/>");
                                        }
                                        _totals = Totals.Appended;
                                    }
                                    else if (n3.Name.ToLower() == "uniqueappend")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <UniqueAppend/>");
                                        }
                                        _totals = Totals.UniqueAppended;
                                    }
                                    else if (n3.Name.ToLower() == "formula")
                                    {
                                        _formula = n3.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Formula>"+_formula+"</Formula>");
                                        }
                                        _totals = Totals.Formula;
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Lookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Totals>");
                            }
                        }
                        else if (n.Name.ToLower() == "useinputparsingrules")
                        {
                            _useinputrules = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <UseInputParsingRules/>");
                            }
                        }
                        else if (n.Name.ToLower() == "fastfailifblank")
                        {
                            _fastfailblank = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FastFailIfBlank/>");
                            }
                        }
                        else if (n.Name.ToLower() == "appendseparator")
                        {
                            _appendSeparator = n.InnerText;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AppendSeparator>" + _appendSeparator + "</AppendSeparator>");
                            }
                        }
                        else if (n.Name.ToLower() == "dumplookup")
                        {
                            _dumpLookup = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <DumpLookup/>");
                            }
                        }
                        else if (n.Name.ToLower() == "includeallbutkeys")
                        {
                            _includeallbutkeys = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <IncludeAllButKeys/>");
                            }
                        }
                        else if (n.Name.ToLower() == "lookupfile")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LookupFile>");
                            }
                            StringOrColumn sc = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            file = sc.Value();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    " + file);
                                CSVProcessor.DisplayError(prefix + "  </LookupFile>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetcolumn")
                        {
                            // save the target column to extract
                            try
                            {
                                _targetColumn = new StringOrColumn(n, j, _p, od, tracexml, prefix + "   ");
                                // if this is an all column extract then flag this column as not needing quotes as this will be handled by this class
                                if (_targetColumn.Value().Trim() == "*")
                                {
                                    _safe = true;
                                    _all = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading lookup target column", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <TargetColumn>" + _targetColumn.ToString() + "</TargetColumn>");
                            }
                        }
                        else if (n.Name.ToLower() == "condition")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Condition>");
                            }
                            // parse the if condition
                            ParseFilter(_f, j, n, tracexml, prefix + "    ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Condition>");
                            }
                        }
                        else if (n.Name.ToLower() == "default")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Default>");
                            }
                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "usekey")
                                    {
                                        // we should return the key if the key is not found
                                        usekey = true;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <UseKey/>");
                                        }
                                    }
                                    else if (n3.Name.ToLower() == "value")
                                    {
                                        // we should return the given value if the key is not found
                                        usekey = false;
                                        defaultvalue = n3.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Value>" + defaultvalue + "</Value>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "Lookup::Constructor Ignoring xml node : " + n3.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Default>");
                            }
                        }
                    }
                }

                if (_targetColumn == null && !_includeallbutkeys)
                {
                    throw new ApplicationException("Lookup does not have a target column or request to include all.");
                }

                if (_targetColumn != null && _includeallbutkeys)
                {
                    throw new ApplicationException("Lookup can't have both a target column and request to include all.");
                }

                // if we got a file name and a target key then load the lookup file
                if (file != string.Empty && keyTgt != null)
                {
                    try
                    {
                        if (_useinputrules)
                        {
                            // load the lookup file
                            _dt = new MultiDecodeTable(file, keyTgt, _dumpLookup, false, p.Input.Delimiter, p.Input.Quote, p.Input.QuoteEscape);
                        }
                        else
                        {
                            // load the lookup file
                            _dt = new MultiDecodeTable(file, keyTgt, _dumpLookup, false);
                        }
                        _targetColumn.SetStringValue(_dt.ColumnIndex(_targetColumn.Value()));
                        if (_targetColumn.Value() != "*")
                        {
                            _targetColumnNum = Int32.Parse(_targetColumn.Value());
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Problem loading multilookup file : " + file, ex);
                    }
                }
                else
                {
                    throw new ApplicationException("MultiLookup file incompletely specified.");
                }

                // Tell the table whether or not to return the key
                _dt.UseOriginal = usekey;

                // if we are not returning the key then set the default value
                if (!usekey)
                {
                    _dt.Default = defaultvalue;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating MutliLookup: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Used for a column which includes mathematical calculations
    /// </summary>
    public class arithmatic : Column
    {
        public enum round { round_default, round_up, round_down, round_none };
        string _format = string.Empty; // format string for the result
        bool _float = false; // flag to indicate we should process this as a floating point math
        bool _stripCurrencyPct = false; // flag to indicate if $,% should be stripped from columns before they are processed
        System.Collections.ArrayList _formulaDecoded = null; // the decoded formula to process
        round _round = round.round_none;

        #region Private Functions
        /// <summary>
        /// Calculate the result using integer math
        /// </summary>
        /// <param name="s">the formula</param>
        /// <returns>the result</returns>
        public static Int64 CalcInt(string s, round rnd)
        {
            // we use a data table to do the calcs
            DataTable dt = new DataTable("tbl");

            // create a column using our formula
            dt.Columns.Add(new DataColumn("calc", Type.GetType("System.Double"), s));

            // create a row
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);

            double r = Convert.ToDouble(dt.Rows[0]["calc"]);

            Int64 res = 0;
            if (rnd == round.round_down)
            {
                res = Convert.ToInt64(Math.Floor(r));
            }
            else if (rnd == round.round_up)
            {
                res = Convert.ToInt64(Math.Ceiling(r));
            }
            else
            {
                res = Convert.ToInt64(Math.Round(r));
            }

            // extract the result
            return res;
        }

        /// <summary>
        /// Calculate the result using floating point math
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double CalcFloat(string s, round rnd)
        {
            // we use a data table to do the calcs
            DataTable dt = new DataTable("tbl");

            // create a column using our formula
            dt.Columns.Add(new DataColumn("calc", Type.GetType("System.Double"), s));

            // create a row
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);

            // extract the result
            double r = Convert.ToDouble(dt.Rows[0]["calc"]);
            double res = 0;

            if (rnd == round.round_down)
            {
                res = Math.Floor(r);
            }
            else if (rnd == round.round_up)
            {
                res = Math.Ceiling(r);
            }
            else if (rnd == round.round_none)
            {
                res = r;
            }
            else
            {
                res = Math.Round(r);
            }
            return res;
        }

        /// <summary>
        /// Decode the formula into columns and literals
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static System.Collections.ArrayList DecodeFormula(string f, CSVJob j, CSVProcess p)
        {
            // collection to hold the decode formula
            System.Collections.ArrayList result = new System.Collections.ArrayList();

            int i = 0; // character iterator
            int state = 0; // parse state
            string s = string.Empty; // fragment

            // iterate through each character in the formula
            for (i = 0; i < f.Length; i++)
            {
                // extract the current character
                string c = f.Substring(i, 1);

                // if we are in literal state
                if (state == 0)
                {
                    // if we found a column definition start
                    if (c == "[")
                    {
                        // if we have a previous value
                        if (s != string.Empty)
                        {
                            // save it
                            result.Add(s);
                        }

                        // move to column name state
                        state = 1;

                        // clear the current fragment
                        s = string.Empty;
                    }
                    // if we found a column definition end
                    else if (c == "]")
                    {
                        // invalid so error
                        throw new ApplicationException("Invalid formula in column : " + f);
                    }
                    else
                    {
                        // just add the latest character to our literal fragment
                        s = s + c;
                    }
                }
                // if in column name state
                else if (state == 1)
                {
                    // if we find another column definition start
                    if (c == "[")
                    {
                        // invalid so error
                        throw new ApplicationException("Invalid formula in column : " + f);
                    }
                    // if we find a column definition end
                    else if (c == "]")
                    {
                        // if we have a column name
                        if (s != string.Empty)
                        {
                            // parse the column number and add it to our formula
                            if (p == null)
                            {
                                result.Add(new ColumnNumber(j, s));
                            }
                            else
                            {
                                result.Add(new ColumnNumber(p.Job, s));
                            }
                        }

                        // we go back to literal state
                        state = 0;

                        // clear out our fragment
                        s = string.Empty;
                    }
                    else
                    {
                        // add the latest character to our column name
                        s = s + c;
                    }
                }
            }

            // if we are in literal state
            if (state == 0)
            {
                // if we have an unsaved fragment
                if (s != string.Empty)
                {
                    // add it to our formula
                    result.Add(s);
                }
            }
            else
            {
                // column names must be terminated so this should not happen

                // invalid so error
                throw new ApplicationException("Invalid formula in column : " + f);
            }

            return result;
        }

        public static string Substitute(System.Collections.ArrayList formulaDecoded, ColumnLine line, CSVProcess p, bool stripCurrencyPct)
        {
            string formula = string.Empty; // our formula

            // iterate through the decoded formula fragments
            foreach (object o in formulaDecoded)
            {
                // if it is a literal
                if (o.GetType() == typeof(System.String))
                {
                    // add it as is to the formula
                    string s = (string)o;
                    formula = formula + s;
                }
                else
                {
                    // get the column number
                    ColumnNumber cn = (ColumnNumber)o;
                    string cv = string.Empty;
                    if (cn.TagValue)
                    {
                        cv = p.Tags.GetTagValue(cn.Name, line);
                    }
                    else if (cn.Variable)
                    {
                        cv = cn.GetVariableValue(p);
                    }
                    else
                    {
                        cv = line[cn.Number];
                    }

                    // if the column number is blank
                    if (cv.Trim() == string.Empty)
                    {
                        // put a zero in the formula
                        formula = formula + "0";
                    }
                    else
                    {
                        // lookup the column in our current line and add it to the formula
                        if (stripCurrencyPct)
                        {
                            cv = cv.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
                            cv = cv.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol, "");
                        }

                        formula = formula + cv;
                    }
                }
            }

            return formula;
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line">The input columns</param>
        /// <returns>the column result</returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                string formula = Substitute(_formulaDecoded, line, _p, _stripCurrencyPct); // our formula

                // if this is to be floating point math
                if (_float)
                {
                    // calculate float result
                    string d = "0.0";

                    try
                    {
                        double dd = CalcFloat(formula, _round);

                        if (double.IsNaN(dd))
                        {
                            d = string.Empty;
                        }
                        else
                        {
                            d = dd.ToString(_format);
                        }
                    }
                    catch
                    {
                        d = "0.0";
                    }

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Arithmatic: {" + Id + "}  '" + formula + "' --> '" + d + "'" + ANSIHelper.White);
                    }

                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(d);

                    PostProcess();

                    return cl;
                }
                else
                {
                    // calculate int result
                    string i = "0";
                    try
                    {
                        i = CalcInt(formula, _round).ToString(_format);
                    }
                    catch
                    {
                        i = "0";
                    }

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Arithmatic: {" + Id + "}  '" + formula + "' --> '" + i + "'" + ANSIHelper.White);
                    }

                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(i);

                    PostProcess();

                    return cl;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "formula":
                case "format":
                case "float":
                case "stripcurrencypct":
                case "stripcurrencypercent":
                case "round":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor

        /// <summary>
        /// Create an arithmatic column
        /// </summary>
        /// <param name="node">XML node to parse</param>
        public arithmatic(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Arithmatic";
            try
            {
                // parse the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "formula")
                        {
                            // decode the formula
                            _formulaDecoded = DecodeFormula(n.InnerText, j, p);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Formula>" + n.InnerText + "</Formula>");
                            }
                        }
                        else if (n.Name.ToLower() == "format")
                        {
                            // save the format
                            _format = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Format>" + _format + "</Format>");
                            }
                        }
                        else if (n.Name.ToLower() == "round")
                        {
                            string r = "None";
                            if (n.InnerText.ToLower() == "up")
                            {
                                _round = round.round_up;
                                r = "Up";
                            }
                            else if (n.InnerText.ToLower() == "down")
                            {
                                _round = round.round_down;
                                r = "Down";
                            }
                            else if (n.InnerText.ToLower() == "default")
                            {
                                _round = round.round_default;
                                r = "Default";
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Round>" + r + "</Round>");
                            }
                        }
                        else if (n.Name.ToLower() == "float")
                        {
                            // save the float flag
                            _float = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Float/>");
                            }
                        }
                        else if (n.Name.ToLower() == "stripcurrencypct" || n.Name.ToLower() == "stripcurrencypercent")
                        {
                            _stripCurrencyPct = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <StripCurrencyPct/>");
                            }
                        }
                    }
                }

                if (_formulaDecoded.Count == 0)
                {
                    throw new ApplicationException("No formula.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Arithmatic: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Writes out the output line number
    /// </summary>
    public class outputlinenumber : Column
    {
        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = "0";

            try
            {
                if (_od != null)
                {
                    // return the current output line number
                    s = (_od.OutputLineNumber.ToString());
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "OutputLineNumber: {" + Id + "}  --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            return false;
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public outputlinenumber(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            // No parameters
            Name = "OutputLineNumber";
        }
        #endregion
    }

    /// <summary>
    /// Get the number of columns on the input line
    /// </summary>
    public class inputlinecolumns : Column
    {
        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            string s = string.Empty;
            try
            {
                s = line.Count.ToString();
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputLineColumns: {" + Id + "}  --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            return false;
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public inputlinecolumns(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            // No parameters
            Name = "InputLineColumns";
        }
        #endregion
    }

    /// <summary>
    /// Writes out the input line number
    /// </summary>
    public class inputlinenumber : Column
    {
        #region Public Functions

        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            // look up the code and return it

            string s = string.Empty;

            try
            {
                s = (line.LineNumber.ToString());

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputLineNumber: {" + Id + "}  --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            return false;
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public inputlinenumber(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            // No parameters
            Name = "InputLineNumber";
        }
        #endregion
    }

    /// <summary>
    /// A column which decodes a given value
    /// </summary>
    public class decode : Column
    {
        ColumnNumberOrColumn _srccolumn; // column to find the value to decode
        DecodeTable _dt = null; // the decode table to use
        bool _fastfailblank = false;

        #region Public Functions
        /// <summary>
        /// Process the column to produce an output value
        /// </summary>
        /// <param name="line">CSV line to get the source column from</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            try
            {
                string key = _srccolumn.Value(line);

                // look up the code and return it
                string s = ((string)_dt.Decode(key, _fastfailblank));

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Decode: {" + Id + "}  '" + key + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "decodefile":
                case "decodetable":
                case "fastfailifblank":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public decode(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Decode";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {

                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "fastfailifblank")
                        {
                            _fastfailblank = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FastFailIfBlank/>");
                            }
                        }
                        else if (n.Name.ToLower() == "decodetable")
                        {
                            if (_dt != null)
                            {
                                throw new ApplicationException("Decode can only have one DecodeTable");
                            }
                            try
                            {
                                _dt = new DecodeTable(n, tracexml, prefix + "   ", false);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Problem loading codes table", ex);
                            }
                        }
                        else if (n.Name.ToLower() == "decodefile")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <DecodeFile>");
                            }

                            StringOrColumn sc = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            string file = sc.Value();

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </DecodeFile>");
                            }

                            if (_dt != null)
                            {
                                throw new ApplicationException("Decode can not have both a DecodeTable and a DecodeFile");
                            }

                            try
                            {
                                // load the codes table from the file
                                _dt = new DecodeTable(file, false);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Problem loading codes table file : " + file, ex);
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column for decode.");
                }

                if (_dt == null)
                {
                    throw new ApplicationException("No decode file/table.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Decode: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// This column class runs a c# function specified in the configuration file
    /// </summary>
    class customcodecolumn : Column
    {
        MethodInfo _method = null; // pointer to the function once it is compiled
        string _class = string.Empty; // name of the class
        string _code = string.Empty; // the code

        #region Private Functions
        /// <summary>
        /// Calls a code function in form ...
        /// 
        /// namespace CSVProcessor
        /// {
        ///		class xyx
        ///		{
        ///			public static ColumnLine Process(string[] cols)
        ///			{
        ///				return "Test";
        ///			}
        ///		}	
        ///	}
        ///	
        ///	This example would only include rows where the first column was "A"
        ///	
        /// </summary>
        /// <param name="code">The code to compile</param>
        /// <returns>A method to call</returns>
        MethodInfo BuildAssembly(string code)
        {
            // setup the compiler
            System.CodeDom.Compiler.CodeDomProvider provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp");
            CompilerParameters compilerparams = new CompilerParameters();
            compilerparams.GenerateExecutable = false;
            compilerparams.GenerateInMemory = true;

            // compile the code
            CompilerResults results = provider.CompileAssemblyFromSource(compilerparams, code);

            // if there were compile errors
            if (results.Errors.HasErrors)
            {
                // copy the errors in a string
                StringBuilder errors = new StringBuilder("Compiler Errors :\r\n");
                foreach (CompilerError error in results.Errors)
                {
                    errors.AppendFormat("Line {0},{1}\t: {2}\n", error.Line, error.Column, error.ErrorText);
                }

                // throw an exception with the errors
                throw new Exception(errors.ToString());
            }
            else
            {
                // get the compiled type
                Type type = null;
                type = results.CompiledAssembly.GetType("CSVProcessor." + _class);

                // find the process method
                MethodInfo method = type.GetMethod("Process");

                // if we didnt find it that is a problem
                if (method == null)
                {
                    throw new ApplicationException("Process() method not found in CSVProcessor." + _class);
                }

                // return the function pointer
                return method;
            }
        }

        /// <summary>
        /// Runs the function in the xml file
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        string RunFunction(ColumnLine line)
        {
            // convert the columns to a base type we can handle
            string[] cols = line.Cols;

            // call the function
            string res = null;
            try
            {
                res = (string)_method.Invoke(null, new object[] { cols });
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Custom code column threw exception", ex);
            }

            // return the result
            return res;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "class":
                case "code":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public customcodecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "CustomCodeColumn";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {

                        if (n.Name.ToLower() == "class")
                        {
                            // save the class name
                            _class = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Class>" + _class + "</Class>");
                            }
                        }
                        else if (n.Name.ToLower() == "code")
                        {
                            // save the code
                            _code = n.InnerText;

                            // compile it
                            _method = BuildAssembly(_code);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Code>" + _code + "</Code>");
                            }
                        }
                    }
                }

                if (_class == string.Empty)
                {
                    throw new ApplicationException("No class defined");
                }

                if (_method == null)
                {
                    throw new ApplicationException("No function to call");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating CustomCodeColumn: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Run the function
        /// </summary>
        /// <param name="line">the input line</param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            try
            {
                // Run it
                string s = RunFunction(line);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "CustomCodeColumn: {" + Id + "}  --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();
                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Strip a set of characters from a column and replace them with a given value
    /// </summary>
    public class stripchars : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn; // source column to use
        bool _stripLT32 = false;
        bool _stripGT126 = false;
        string _stripchars = string.Empty;
        StringOrColumn _replacecol = null; // the string to replace it with
        #endregion

        #region Private Functions
        string DoStrip(string cv, string replace)
        {
            string s = string.Empty;

            foreach (char c in cv)
            {
                if (_stripLT32 && c < 32)
                {
                    s = s + replace;
                }
                else if (_stripGT126 && c > 126)
                {
                    s = s + replace;
                }
                else if (_stripchars.Contains(c.ToString()))
                {
                    s = s + replace;
                }
                else
                {
                    s = s + c;
                }
            }

            return s;
        }
        #endregion

        #region Public Functions
        public override ColumnLine ProcessMulti(ColumnLine line)
        {
            if (_srccolumn.ColumnNumber.All)
            {
                ColumnLine cl = new ColumnLine(line);

                for (int i = 0; i < line.Count; i++)
                {
                    line[i] = DoStrip(line[i], _replacecol.Value(line));
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "StripChars: {" + Id + "}  '" + cl.ToString() + "'" + ANSIHelper.White);
                }

                return line;
            }
            else
            {
                throw new ApplicationException("StripChars:ProcessMulti Should not get here!");
            }
        }

        // this is a great idea but it wont work as it is called before the first line is read and the column names are allocated ... damn
        //public override string Title
        //{
        //   get
        //   {
        //      if (base.Title == string.Empty)
        //      {
        //         if (_srccolumn.ColumnNumber != null && _srccolumn.ColumnNumber.All)
        //         {
        //            return ColumnNumber.GetColNames(_p.Job).ToString();
        //         }
        //         else
        //         {
        //            return ColumnNumber.GetColNames(_p.Job)[_srccolumn.ColumnNumber].ToString();
        //         }
        //      }
        //      else
        //      {
        //         return base.Title;
        //      }
        //   }
        //}

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            // if all
            if (_srccolumn.ColumnNumber.All)
            {
                throw new UseMultItemProcess();
            }
            else
            {
                try
                {
                    string s = string.Empty;
                    string cv = _srccolumn.Value(line);

                    s = DoStrip(cv, _replacecol.Value(line));

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "StripChars: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                    }

                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(s);

                    PostProcess();

                    return cl;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
                }
            }
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "lessthan32":
                case "greaterthan126":
                case "replace":
                case "chars":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public stripchars(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "StripChars";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "lessthan32")
                        {
                            _stripLT32 = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LessThan32/>");
                            }
                        }
                        else if (n.Name.ToLower() == "greaterthan126")
                        {
                            _stripGT126 = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <GreaterThan126/>");
                            }
                        }
                        else if (n.Name.ToLower() == "chars")
                        {
                            // read the chars to strip
                            _stripchars = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Chars>" + _stripchars + "</Chars>");
                            }
                        }
                        else if (n.Name.ToLower() == "replace")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Replace>");
                            }
                            _replacecol = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Replace>");
                            }
                        }
                    }
                }

                if (_replacecol == null)
                {
                    _replacecol = new StringOrColumn("");
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating StripChars: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    public class inputcolumnname : Column
    {
        #region Member Variables
        ColumnNumber _srccolumn; // source column to use
        #endregion

        #region Public Functions

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                string s = ColumnNumber.GetColNames(_p.Job)[_srccolumn];

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputColumnName: {" + Id + "}  '" + _srccolumn.Number.ToString() + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public inputcolumnname(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "InputColumnName";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumber(p.Job, n);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating InputColumnName: {" + Id + "}", ex);
            }
        }
        #endregion
    }


    /// <summary>
    /// Replace a string matched by a regular expression
    /// </summary>
    public class regexreplace : Column
    {
        ColumnNumberOrColumn _srccolumn; // the column to search
        Regex _regex = null; // the regular expression to apply
        StringOrColumn _replacecol = null; // the string to replace it with
        bool _one = false; // if true only replace the first occurrence
        bool _repeat = false; // if true keep processing until none are processed

        #region Public functions
        /// <summary>
        /// Process this column
        /// </summary>
        /// <param name="line">Input columns</param>
        /// <returns>Output column</returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            // get the column value
            string cv = string.Empty;
            try
            {
                cv = _srccolumn.Value(line);

                string orig = cv;
                string s = string.Empty;

                string replace = _replacecol.Value(line);

                if (_one)
                {
                    // replace only the first occurrence
                    s = _regex.Replace(cv, replace, 1);
                }
                else if (_repeat)
                {
                    string sout = cv; // string to store iteration result

                    // clear the column value so we go through the loop at least once
                    cv = string.Empty;

                    // while the regex keeps changing something
                    while (sout != cv)
                    {
                        // start with the last iteration
                        cv = sout;

                        // replace all occurrences
                        sout = _regex.Replace(cv, replace);

                    }

                    // return the value
                    s = sout;
                }
                else
                {
                    // replace all regex matches of the string with our new value
                    s = _regex.Replace(cv, replace);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RegexReplace: {" + Id + "}  '" + orig + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "regex":
                case "replace":
                case "firstonly":
                case "repeat":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public regexreplace(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RegexReplace";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "regex")
                        {
                            try
                            {
                                _regex = new Regex(n.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                            }
                            catch (Exception e)
                            {
                                throw new ApplicationException("RegexReplace experience error parsing regular expression : " + n.InnerText, e);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Regex>" + _regex.ToString() + "</Regex>");
                            }
                        }
                        else if (n.Name.ToLower() == "replace")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Replace>");
                            }
                            _replacecol = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Replace>");
                            }
                        }
                        else if (n.Name.ToLower() == "firstonly")
                        {
                            _one = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FirstOnly/>");
                            }
                        }
                        else if (n.Name.ToLower() == "repeat")
                        {
                            _repeat = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Repeat/>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }

                if (_regex == null)
                {
                    throw new ApplicationException("No regular expression.");
                }

                if (_replacecol == null)
                {
                    _replacecol = new StringOrColumn(string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating RegexReplace: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Output the number of input columns
    /// </summary>
    public class inputcolumncount : Column
    {
        #region Public Functions
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                s = line.Count.ToString();
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputColumnCount: {" + Id + "}  '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            return false;
        }

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public inputcolumncount(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "InputColumnCount";
        }
        #endregion

    }
    /// <summary>
    /// Extract a substring from a column
    /// </summary>
    public class substring : Column
    {
        ColumnNumberOrColumn _srccolumn; //column to process
        int _start = 0; // character to start from
        int _length = -1; // number of characters to grab

        #region Public Functions
        /// <summary>
        /// Process this column
        /// </summary>
        /// <param name="line">Input columns</param>
        /// <returns>Output column</returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            try
            {
                // default result is a blank sting
                string res = string.Empty;
                string cv = _srccolumn.Value(line);

                if (cv.Length < _start)
                {
                    // dont do anything
                }
                else
                {
                    try
                    {
                        if (_length == -1)
                        {
                            // grab the substring
                            res = cv.Substring(_start);
                        }
                        else
                        {
                            if (cv.Length - _start < _length)
                            {
                                res = cv.Substring(_start);
                            }
                            else
                            {
                                // grab the substring
                                res = cv.Substring(_start, _length);
                            }
                        }
                    }
                    catch
                    {
                        // if an error occurs just return a blank string
                        res = string.Empty;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "SubString: {" + Id + "}  '" + cv + "' --> '" + res + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(res);

                PostProcess();

                // return the substring
                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "start":
                case "length":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public substring(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Substring";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "start")
                        {
                            try
                            {
                                _start = Convert.ToInt32(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading substring start", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Start>" + _start.ToString() + "</Start>");
                            }
                        }
                        else if (n.Name.ToLower() == "length")
                        {
                            try
                            {
                                _length = Convert.ToInt32(n.InnerText);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading substring length", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Length>" + _length.ToString() + "</Length>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Substring: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Grab characters from the right hand side of an input column
    /// </summary>
    public class rightstring : Column
    {
        ColumnNumberOrColumn _srccolumn; // column to process
        int _length = 0; // number of characters to grab

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            try
            {
                string cv = _srccolumn.Value(line);

                // get the string length
                int strlen = cv.Length;

                string s = string.Empty;

                // if the string is not long enough
                if (_length > strlen)
                {
                    // return the entire column
                    s = cv;
                }
                else
                {
                    // return the number of characters requested
                    s = cv.Substring(strlen - _length);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RightString: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "length":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Create a right string column processor
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public rightstring(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RightString";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "length")
                        {
                            // get the number of characters to grab
                            _length = Convert.ToInt32(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Length>" + _length.ToString() + "</Length>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating RightString: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Extract part of the column value using a regular expression
    /// </summary>
    public class regexsubstring : Column
    {
        ColumnNumberOrColumn _srccolumn; // source column to use
        Regex _regex = null; // regular expression to use
        string _regexString = "";
        StringOrColumn _regexSource = null;
        string _name = null; // name of the substring to extract
        bool _first = true;
        bool _last = false;
        int _nth = -1;

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "regex":
                case "name":
                case "first":
                case "last":
                case "nth":
                case "index":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // extract the column value
                string cv = _srccolumn.Value(line);

                string s = string.Empty;

                string newregex = _regexSource.Value(line);
                if (newregex != _regexString)
                {
                    try
                    {
                        _regex = new Regex(newregex, RegexOptions.Singleline | RegexOptions.Compiled);
                        _regexString = newregex;

                    }
                    catch (Exception ex)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Error + "StringSubstring:" + Id + " : ' tried to update regex but failed '" + newregex + "' " + ex.Message + ANSIHelper.White);
                    }
                }

                // if we dont have a name
                if (_name == null)
                {
                    // extract what matches
                    s = _regex.Match(cv).Value;
                }
                else
                {
                    // extract the named value within the match
                    //s = _regex.Match(cv).Groups[_name].Value;
                    if (_first)
                    {
                        s = _regex.Match(cv).Groups[_name].Value;
                    }
                    else if (_last)
                    {
                        MatchCollection mc = _regex.Matches(cv);
                        if (mc.Count > 0)
                        {
                            s = mc[mc.Count - 1].Groups[_name].Value;
                        }
                    }
                    else
                    {
                        MatchCollection mc = _regex.Matches(cv);
                        if (_nth >= 0 && _nth < mc.Count)
                        {
                            s = mc[_nth].Groups[_name].Value;
                        }
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RegexSubstring: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public regexsubstring(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RegexSubstring";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "regex")
                        {
                            // read the regular expression
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Regex>");
                            }
                            _regexSource = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            try
                            {
                                _regex = new Regex(_regexSource.Value(), RegexOptions.Singleline | RegexOptions.Compiled);
                            }
                            catch (Exception e)
                            {
                                throw new ApplicationException("RegexSubstring experienced error interpreting regular expression.", e);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Regex>");
                            }
                        }
                        else if (n.Name.ToLower() == "first")
                        {
                            _first = true;
                            _last = false;
                            _nth = -1;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <First/>");
                            }
                        }
                        else if (n.Name.ToLower() == "last")
                        {
                            _first = false;
                            _last = true;
                            _nth = -1;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Last/>");
                            }
                        }
                        else if (n.Name.ToLower() == "nth")
                        {
                            _first = false;
                            _last = false;
                            _nth = Int16.Parse(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Nth>" + _nth.ToString() + "</Nth>");
                            }
                        }
                        else if (n.Name.ToLower() == "index")
                        {
                            _first = false;
                            _last = false;
                            _nth = Int16.Parse(n.InnerText) + 1;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Index>" + (_nth - 1).ToString() + "</Index>");
                            }
                        }
                        else if (n.Name.ToLower() == "name")
                        {
                            // read the substring name
                            _name = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Name>" + _name + "</Name>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }

                if (_regex == null)
                {
                    throw new ApplicationException("No regular expression.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating RegexSubstring: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Tokenise a column and extract a value from the resulting array
    /// </summary>
    public class tokenise : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn; // source column to use
        int _index;
        List<string> _token = new List<string>();
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "index":
                case "tokens":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // extract the column value
                string cv = _srccolumn.Value(line);

                string s = string.Empty;

                string[] tokenised = cv.Split(_token.ToArray(), StringSplitOptions.None);
                if (_index >= tokenised.Length || _index * -1 > tokenised.Length)
                {
                    // index beyond end so leave s as empty
                }
                else
                {
                    if (_index < 0)
                    {
                        s = tokenised[tokenised.Length + _index];
                    }
                    else
                    {
                        s = tokenised[_index];
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Tokenise: {" + Id + "}  '" + cv + "'[" + _index.ToString() + "] --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public tokenise(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Tokenise";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "tokens")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Tokens>");
                            }
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "token")
                                    {
                                        _token.Add(n1.InnerText);
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Token>" + n1.InnerText + "</Token>");
                                        }
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Tokens>");
                            }
                        }
                        else if (n.Name.ToLower() == "index")
                        {
                            try
                            {
                                // read the index
                                _index = Convert.ToInt32(n.InnerText);
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <Index>" + _index.ToString() + "</Index>");
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Tokenise constructor: Error trying to read index '" + n.InnerText + "'", ex);
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Tokenise: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Categorise a column value using a regular expression
    /// </summary>
    public class regexcategorise : Column
    {
        class CATEGORY
        {
            public Regex _regex = null;
            public StringOrColumn _category = null;
        }

        ColumnNumberOrColumn _srccolumn; // source column to use
        StringOrColumn _default; // source column to use
        List<CATEGORY> _categories = new List<CATEGORY>();

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "category":
                case "default":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // extract the column value
                string cv = _srccolumn.Value(line);

                string s = string.Empty;
                bool found = false;

                foreach (CATEGORY c in _categories)
                {
                    if (c._regex.IsMatch(cv))
                    {
                        found = true;
                        s = c._category.Value(line);
                        break;
                    }
                }

                if (!found)
                {
                    s = _default.Value(line);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RegexCategorise: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public regexcategorise(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RegexCategorise";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "category")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Category>");
                            }

                            CATEGORY c = new CATEGORY();

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "regex")
                                    {
                                        // read the regular expression
                                        try
                                        {
                                            c._regex = new Regex(n1.InnerText, RegexOptions.Singleline | RegexOptions.Compiled);
                                        }
                                        catch (Exception e)
                                        {
                                            throw new ApplicationException("RegexCategorise experience an error parsing regex : " + n1.InnerText, e);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Regex>" + c._regex.ToString() + "</Regex>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "value")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Value>");
                                        }
                                        c._category = new StringOrColumn(n1, j, p, od, tracexml, prefix + "   ");
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     </Value>");
                                        }
                                    }
                                }
                            }

                            if (c._category == null || c._regex == null)
                            {
                                throw new ApplicationException("Category Name and Regular Expression must be specified.");
                            }

                            // add it to the list
                            _categories.Add(c);

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Category>");
                            }
                        }
                        else if (n.Name.ToLower() == "default")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Default>");
                            }
                            _default = new StringOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Default>");
                            }
                        }
                    }
                }

                if (_categories.Count == 0)
                {
                    throw new ApplicationException("No categories specified.");
                }

                if (_default == null)
                {
                    _default = new StringOrColumn(string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating RegexCategorise: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Output the name of the current input file
    /// </summary>
    public class inputfile : Column
    {
        #region Member Variables
        string _outputformat = string.Empty; // the output date format
        enum WHAT { NAME, CREATED, UPDATED, SIZE, ATTRIBS };
        enum NAMEBIT { EXT, FILENAME, FILEBASENAME, DIRECTORY, FULL, PATH };
        WHAT _what = WHAT.NAME;
        NAMEBIT _namebit = NAMEBIT.FULL;
        #endregion

        #region Public Functions
        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty;

            try
            {
                switch (_what)
                {
                    case WHAT.NAME:
                        switch (_namebit)
                        {
                            case NAMEBIT.DIRECTORY:
                                s = Path.GetFileName(Path.GetDirectoryName(line.FileName));
                                break;
                            case NAMEBIT.EXT:
                                s = Path.GetExtension(line.FileName);
                                if (s.Length > 0 && s[0] == '.')
                                {
                                    s = s.Substring(1);
                                }
                                break;
                            case NAMEBIT.FILEBASENAME:
                                s = Path.GetFileNameWithoutExtension(line.FileName);
                                break;
                            case NAMEBIT.FILENAME:
                                s = Path.GetFileName(line.FileName);
                                break;
                            case NAMEBIT.FULL:
                                s = line.FileName;
                                break;
                            case NAMEBIT.PATH:
                                s = Path.GetDirectoryName(line.FileName);
                                break;
                        }
                        break;
                    case WHAT.CREATED:
                        if (line.FileInfo == null)
                        {
                            s = "UNKNOWN";
                        }
                        else
                        {
                            if (_outputformat == string.Empty)
                            {
                                s = line.FileInfo.Created.ToString();
                            }
                            else
                            {
                                s = line.FileInfo.Created.ToString(_outputformat);
                            }
                        }
                        break;
                    case WHAT.UPDATED:
                        if (line.FileInfo == null)
                        {
                            s = "UNKNOWN";
                        }
                        else
                        {
                            if (_outputformat == string.Empty)
                            {
                                s = line.FileInfo.Updated.ToString();
                            }
                            else
                            {
                                s = line.FileInfo.Updated.ToString(_outputformat);
                            }
                        }
                        break;
                    case WHAT.SIZE:
                        if (line.FileInfo == null)
                        {
                            s = "UNKNOWN";
                        }
                        else
                        {
                            s = line.FileInfo.Size.ToString();
                        }
                        break;
                    case WHAT.ATTRIBS:
                        if (line.FileInfo == null)
                        {
                            s = "UNKNOWN";
                        }
                        else
                        {
                            s = line.FileInfo.Attributes;
                        }
                        break;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputFile: {" + Id + "}  --> '" + s + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s.ToLower())
            {
                case "name":
                case "created":
                case "updated":
                case "size":
                case "attribs":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Create a input file name column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public inputfile(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            // Nothing to do
            Name = "InputFile";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "name")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Name>");
                            }
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "ext")
                                    {
                                        _namebit = NAMEBIT.EXT;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Ext/>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "filename")
                                    {
                                        _namebit = NAMEBIT.FILENAME;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <FileName/>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "basefilename")
                                    {
                                        _namebit = NAMEBIT.FILEBASENAME;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <BaseFileName/>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "path")
                                    {
                                        _namebit = NAMEBIT.PATH;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Path/>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "full")
                                    {
                                        _namebit = NAMEBIT.FULL;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Full/>");
                                        }
                                    }
                                    else if (n1.Name.ToLower() == "directory")
                                    {
                                        _namebit = NAMEBIT.DIRECTORY;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <Directory/>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "InputFile: {" + Id + "} Unknown file name component " + n1.Name + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Name>");
                            }
                        }
                        else if (n.Name.ToLower() == "created")
                        {
                            _what = WHAT.CREATED;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Created>");
                            }
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "outformat")
                                    {
                                        _outputformat = n1.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <OutFormat>" + _outputformat + "</OutFormat>");
                                        }
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Created>");
                            }
                        }
                        else if (n.Name.ToLower() == "updated")
                        {
                            _what = WHAT.UPDATED;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Updated>");
                            }
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (n1.Name.ToLower() == "outformat")
                                    {
                                        _outputformat = n1.InnerText;
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "     <OutFormat>" + _outputformat + "</OutFormat>");
                                        }
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Updated>");
                            }
                        }
                        else if (n.Name.ToLower() == "size")
                        {
                            _what = WHAT.SIZE;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Size/>");
                            }
                        }
                        else if (n.Name.ToLower() == "attribs" || n.Name.ToLower() == "attributes")
                        {
                            _what = WHAT.ATTRIBS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Attribs/>");
                            }
                        }

                        if (n.Name.ToLower() == "outformat")
                        {
                            // get the output format
                            _outputformat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outputformat + "</OutFormat>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating InputFile Column: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Calculate the time between 2 times
    /// </summary>
    public class elapsedtimecolumn : Column
    {
        #region Member Variables
        bool _adddayifnegative = false;
        bool _zeroifnegative = false;
        ColumnNumberOrColumn _col1 = null;
        ColumnNumberOrColumn _col2 = null;
        List<string> _inputformat1 = new List<string>();
        List<string> _inputformat2 = new List<string>();
        StringOrColumn _adjustBySeconds = null; // column processor which generates the column
        bool _subtractAdjustment = false;

        bool _asseconds = false;
        string outputFormat = null;
        #endregion

        #region Public Functions
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string output = string.Empty;

            try
            {
                DateTime dt = DateTime.MinValue; // the output date

                // get the column value
                string cv1 = _col1.Value(line);

                string cv2 = _col2.Value(line);

                // if it is blank the output should be blank
                if (cv1 == string.Empty || cv2 == string.Empty)
                {
                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(string.Empty);

                    return cl;
                }

                DateTime dt1;
                DateTime dt2;
                DateTime now = DateTime.Now;

                try
                {
                    if (_inputformat1.Contains("mmm") & cv1 == "0")
                    {
                        dt1 = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                    }
                    else if (_inputformat1.Contains("mmm"))
                    {
                        int mins = Int32.Parse(cv1);
                        dt1 = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                        dt1 = dt1.AddMinutes(mins);
                    }
                    else
                    {
                        // parse the date/time
                        dt1 = DateTime.ParseExact(cv1, _inputformat1.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                    }
                }
                catch (Exception ex)
                {
                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd("Date 1 invalid : '" + _inputformat1.ToString() + "' : '" + cv1 + "' : " + ex.Message);

                    return cl;
                }

                try
                {
                    if (_inputformat2.Contains("mmm") & cv2 == "0")
                    {
                        dt2 = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                    }
                    else if (_inputformat2.Contains("mmm"))
                    {
                        int mins = Int32.Parse(cv2);
                        dt2 = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                        dt2 = dt2.AddMinutes(mins);
                    }
                    else
                    {
                        // parse the date/time
                        dt2 = DateTime.ParseExact(cv2, _inputformat2.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                    }
                }
                catch (Exception ex)
                {
                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd("Date 2 invalid : '" + _inputformat2.ToString() + "' : '" + cv2 + "' : " + ex.Message);

                    return cl;
                }

                TimeSpan ts = TimeSpan.Zero;

                if (dt2 < dt1)
                {
                    if (_zeroifnegative)
                    {
                        ts = new TimeSpan(0, 0, 0, 0);
                    }
                    else
                    {
                        if (_adddayifnegative)
                        {
                            dt1 = dt1 - new TimeSpan(1, 0, 0, 0);
                        }
                        ts = dt2 - dt1;
                    }
                }
                else
                {
                    ts = dt2 - dt1;
                }

                if (_adjustBySeconds != null)
                {
                    int adj = Int32.Parse(_adjustBySeconds.Value(line));
                    long ticks = 10000000 * adj;
                    if (_subtractAdjustment)
                    {
                        ticks *= -1;
                    }
                    ts = ts.Add(new TimeSpan(ticks));
                }

                if (_asseconds)
                {
                    output = ts.TotalSeconds.ToString();
                }
                else
                {
                    if (outputFormat != null)
                    {
                        output = Timer.FormatTimeSpan(ts, outputFormat);
                    }
                    else if (outputFormat == "hhh:mm")
                    {
                        output = ((int)ts.TotalMinutes / 60).ToString() + ":" + ((int)ts.TotalMinutes % 60).ToString("0#");
                    }
                    else
                    {
                        output = Timer.FormatTimeSpan(ts);
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "ElapsedTimeColumn: {" + Id + "} " + dt2.ToString() + " - " + dt1.ToString() + " --> " + output + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl2 = new ColumnLine();
            cl2.AddEnd(output);

            PostProcess();

            // format the date and return it
            return cl2;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column1":
                case "column2":
                case "informat1":
                case "informat2":
                case "adddayifnegative":
                case "zeroifnegative":
                case "asseconds":
                case "outformat":
                case "addseconds":
                case "subtractseconds":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Create a date column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public elapsedtimecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "ElapsedTimeColumn";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column1")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column1>");
                            }
                            // extract the source column
                            try
                            {
                                _col1 = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source column1.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column1>");
                            }
                        }
                        else if (n.Name.ToLower() == "column2")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column2>");
                            }
                            // extract the source column
                            try
                            {
                                _col2 = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source column2.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column2>");
                            }
                        }
                        else if (n.Name.ToLower() == "addseconds")
                        {
                            if (_adjustBySeconds != null)
                            {
                                throw new ApplicationException("ElapsedTimeColumn: only one AddSeconds or SubtractSecnnds is permitted.");
                            }

                            // get the input format
                            _adjustBySeconds = new StringOrColumn(n.InnerText);
                            _subtractAdjustment = false;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AddSeconds>" + _adjustBySeconds.ToString() + "</AddSeconds>");
                            }
                        }
                        else if (n.Name.ToLower() == "subtractseconds")
                        {
                            // get the input format
                            if (_adjustBySeconds != null)
                            {
                                throw new ApplicationException("ElapsedTimeColumn: only one AddSeconds or SubtractSecnnds is permitted.");
                            }

                            // get the input format
                            _adjustBySeconds = new StringOrColumn(n.InnerText);
                            _subtractAdjustment = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <SubtractSeconds>" + _adjustBySeconds.ToString() + "</SubtractSeconds>");
                            }
                        }
                        else if (n.Name.ToLower() == "informat1")
                        {
                            // get the input format
                            _inputformat1.Add(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InFormat1>" + n.InnerText + "</InFormat1>");
                            }
                        }
                        else if (n.Name.ToLower() == "informat2")
                        {
                            // get the input format
                            _inputformat2.Add(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InFormat2>" + n.InnerText + "</InFormat2>");
                            }
                        }
                        else if (n.Name.ToLower() == "outformat")
                        {
                            // get the input format
                            outputFormat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + outputFormat + "</OutFormat>");
                            }
                        }
                        else if (n.Name.ToLower() == "adddayifnegative")
                        {
                            _adddayifnegative = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AddDayIfNegative />");
                            }
                        }
                        else if (n.Name.ToLower() == "zeroifnegative")
                        {
                            _zeroifnegative = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <ZeroIfNegative />");
                            }
                        }
                        else if (n.Name.ToLower() == "asseconds")
                        {
                            _asseconds = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AsSeconds />");
                            }
                        }
                    }
                }

                if (_zeroifnegative && _adddayifnegative)
                {
                    throw new ApplicationException("ElapsedTimeColumn cant be both ZeroIfNegative and AddDayIfNegative.");
                }
                if (_inputformat1.Count == 0 || _inputformat2.Count == 0 || _col1 == null || _col2 == null)
                {
                    throw new ApplicationException("Missing parameters for ElapsedTimeColumn.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating ElapsedTimeColumn: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Choose between two dates/times
    /// </summary>
    public class minmaxcolumn : Column
    {
        #region Member Variables
        int _type = 0; // 0 is minimum. 1 is maximum
        string _coltype = "string";
        ColumnNumberOrColumn _col1 = null;
        ColumnNumberOrColumn _col2 = null;
        List<string> _inputformat1 = new List<string>();
        List<string> _inputformat2 = new List<string>();
        string outputFormat = null;
        #endregion

        #region Public Functions
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string output = string.Empty;

            try
            {
                // get the column value
                string cv1 = _col1.Value(line);
                string cv2 = _col2.Value(line);

                if (_coltype == "string")
                {
                    if (_type == 0)
                    {
                        // min
                        if (cv1.CompareTo(cv2) < 0)
                        {
                            output = cv1;
                        }
                        else
                        {
                            output = cv2;
                        }
                    }
                    else
                    {
                        // max
                        if (cv1.CompareTo(cv2) > 0)
                        {
                            output = cv1;
                        }
                        else
                        {
                            output = cv2;
                        }
                    }
                }
                else if (_coltype == "integer")
                {
                    int i = 0;
                    int i1 = Int32.Parse(cv1);
                    int i2 = Int32.Parse(cv2);

                    if (_type == 0)
                    {
                        // min
                        if (i1 < i2)
                        {
                            i = i1;
                        }
                        else
                        {
                            i = i2;
                        }
                    }
                    else
                    {
                        // max
                        if (i1 > i2)
                        {
                            i = i1;
                        }
                        else
                        {
                            i = i2;
                        }
                    }
                    if (outputFormat != null)
                    {
                        output = i.ToString(outputFormat);
                    }
                    else
                    {
                        output = i.ToString();
                    }
                }
                else if (_coltype == "float")
                {
                    float f = 0;
                    float f1 = float.Parse(cv1);
                    float f2 = float.Parse(cv2);

                    if (_type == 0)
                    {
                        // min
                        if (f1 < f2)
                        {
                            f = f1;
                        }
                        else
                        {
                            f = f2;
                        }
                    }
                    else
                    {
                        // max
                        if (f1 > f2)
                        {
                            f = f1;
                        }
                        else
                        {
                            f = f2;
                        }
                    }
                    if (outputFormat != null)
                    {
                        output = f.ToString(outputFormat);
                    }
                    else
                    {
                        output = f.ToString();
                    }
                }
                else
                {
                    // if it is blank the output should be blank
                    if (cv1 == string.Empty || cv2 == string.Empty)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd(string.Empty);

                        return cl;
                    }

                    DateTime dt1;
                    DateTime dt2;

                    try
                    {
                        // parse the date/time
                        dt1 = DateTime.ParseExact(cv1, _inputformat1.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                    }
                    catch (Exception ex)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd("Date1 invalid : '" + _inputformat1.ToString() + "' : '" + cv1 + "' : " + ex.Message);

                        return cl;
                    }

                    try
                    {
                        // parse the date/time
                        dt2 = DateTime.ParseExact(cv2, _inputformat2.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                    }
                    catch (Exception ex)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd("Date2 invalid : '" + _inputformat2.ToString() + "' : '" + cv2 + "' : " + ex.Message);

                        return cl;
                    }

                    DateTime dt = DateTime.MinValue;

                    if (_type == 0)
                    {
                        // min
                        if (dt1 < dt2)
                        {
                            dt = dt1;
                        }
                        else
                        {
                            dt = dt2;
                        }
                    }
                    else
                    {
                        // max
                        if (dt1 > dt2)
                        {
                            dt = dt1;
                        }
                        else
                        {
                            dt = dt2;
                        }
                    }

                    if (outputFormat != null)
                    {
                        output = dt.ToString(outputFormat);
                    }
                    else
                    {
                        output = dt.ToString();
                    }
                }
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "MinMaxColumn: {" + Id + "} " + cv1 + " or " + cv2 + " --> " + output + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl2 = new ColumnLine();
            cl2.AddEnd(output);

            PostProcess();

            // format the date and return it
            return cl2;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column1":
                case "column2":
                case "informat1":
                case "informat2":
                case "min":
                case "max":
                case "integer":
                case "float":
                case "string":
                case "datetime":
                case "outformat":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Create a date column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public minmaxcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "MinMaxColumn";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column1")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column1>");
                            }
                            // extract the source column
                            try
                            {
                                _col1 = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source column1.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column1>");
                            }
                        }
                        else if (n.Name.ToLower() == "column2")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column2>");
                            }
                            // extract the source column
                            try
                            {
                                _col2 = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source column2.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column2>");
                            }
                        }
                        else if (n.Name.ToLower() == "informat1")
                        {
                            // get the input format
                            _inputformat1.Add(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InFormat1>" + n.InnerText + "</InFormat1>");
                            }
                        }
                        else if (n.Name.ToLower() == "informat2")
                        {
                            // get the input format
                            _inputformat2.Add(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InFormat2>" + n.InnerText + "</InFormat2>");
                            }
                        }
                        else if (n.Name.ToLower() == "outformat")
                        {
                            // get the input format
                            outputFormat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + outputFormat + "</OutFormat>");
                            }
                        }
                        else if (n.Name.ToLower() == "min")
                        {
                            _type = 0;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Min />");
                            }
                        }
                        else if (n.Name.ToLower() == "max")
                        {
                            _type = 1;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Max />");
                            }
                        }
                        else if (n.Name.ToLower() == "string")
                        {
                            _coltype = "string";
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <String />");
                            }
                        }
                        else if (n.Name.ToLower() == "float")
                        {
                            _coltype = "float";
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Float />");
                            }
                        }
                        else if (n.Name.ToLower() == "integer")
                        {
                            _coltype = "integer";
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Integer />");
                            }
                        }
                        else if (n.Name.ToLower() == "datetime")
                        {
                            _coltype = "datetime";
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <DateTime />");
                            }
                        }
                    }
                }

                if ((_coltype == "datetime" && (_inputformat1.Count == 0 || _inputformat2.Count == 0)) || _col1 == null || _col2 == null)
                {
                    throw new ApplicationException("Missing parameters for MinMaxColumn.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating MinMaxColumn: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// An XSLT transformation column
    /// </summary>
    public class xsltcolumn : Column
    {
        #region Member Variables
        XslCompiledTransform _xslt = new XslCompiledTransform(); // the transformation to apply
        ColumnNumberOrColumn _col = null; // the column to apply it to
        #endregion

        #region Private Functions
        void Transform(XmlDocument doc, XsltArgumentList al, XmlWriter sw)
        {
#if Framework4
            _xslt.Transform(doc, al, sw, null);
#endif
#if Framework35
         XmlReader xr = new XmlNodeReader(doc.ChildNodes[0]);
         _xslt.Transform(xr, al, sw, null);
         xr.Close();
#endif
        }
        #endregion

        #region Public Functions
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            StringBuilder sb = new StringBuilder(string.Empty);
            string cv = string.Empty;

            try
            {
                TextWriter output = new StringWriter(sb); // our result

                // get the column value
                cv = _col.Value(line);

                // load the input column into an XML document
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(new StringReader(cv));
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Problem loading XML fragment for XSL transformation", ex);
                }

                // create a writer to write the result
                XmlWriter sw = new XmlTextWriter(output);

                // apply the transformation
                try
                {
                    XsltArgumentList al = new XsltArgumentList();
                    Transform(doc, al, sw);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Problem applying XSL transformation", ex);
                }

                // close the writer
                sw.Close();
                output.Close();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            string s = sb.ToString();

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "XSLTColumn: {" + Id + "} " + cv + " --> " + s + ANSIHelper.White);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            // format the date and return it
            return cl;
        }
        #endregion

        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "xsl":
                    return true;
                default:
                    return false;
            }
        }

        #region Constructors
        /// <summary>
        /// Create a date column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public xsltcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "XSLTColumn";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            // extract the source column
                            try
                            {
                                _col = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source column.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>" + _col.ToString() + "</Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "xsl")
                        {
                            // extract the XSLT
                            try
                            {
                                XmlDocument doc = new XmlDocument();
                                doc.Load(new StringReader(n.InnerText));
                                _xslt.Load(doc, null, null);
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading source xsl.\n" + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Xsl>" + n.InnerText + "</Xsl>");
                            }
                        }
                    }
                }

                if (_xslt == null || _col == null)
                {
                    throw new ApplicationException("Missing parameters for XsltColumn.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating XsltTimeColumn: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    public class padcolumn : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn; // the column to source the date from
        char _padchar = ' ';
        bool _padleft = false;
        int _length = 0;
        #endregion

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public padcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "PadColumn";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "char")
                        {
                            if (n.InnerText.Length != 1)
                            {
                                throw new ApplicationException("PadColumn <char> must be 1 character only.");
                            }
                            _padchar = n.InnerText[0];

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Char>" + _padchar + "</Char>");
                            }
                        }
                        else if (n.Name.ToLower() == "left")
                        {
                            _padleft = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Left/>");
                            }
                        }
                        else if (n.Name.ToLower() == "right")
                        {
                            _padleft = false;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Right/>");
                            }
                        }
                        else if (n.Name.ToLower() == "length")
                        {
                            // read the substring name
                            _length = Convert.ToInt32(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Length>" + _length.ToString() + "</Length>");
                            }
                        }
                    }
                }

                if (_srccolumn == null)
                {
                    throw new ApplicationException("No source column.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating PadColumn: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "char":
                case "length":
                case "left":
                case "right":
                    return true;
                default:
                    return false;
            }
        }

        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            // get the column value
            string cv = string.Empty;

            try
            {
                cv = _srccolumn.Value(line);

                string s = cv;

                if (_padleft)
                {
                    while (cv.Length < _length)
                    {
                        cv = _padchar + cv;
                    }
                }
                else
                {
                    while (cv.Length < _length)
                    {
                        cv = cv + _padchar;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "PadColumn: {" + Id + "}  '" + s + "' --> '" + cv + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(cv);

            PostProcess();

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// Date processing column
    /// </summary>
    public class datecolumn : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn = null; // the column to source the date from
        List<string> _inputformat = new List<string>(); // the input date format
        string _outputformat = string.Empty; // the output date format
        bool _trim = false; // indicates if we should trim spaces first
        bool _now = false; // indicates if we should output the date and time now
        TimeSpan _adjustBy = TimeSpan.Zero; // amount to adjust the date by
        ColumnNumberOrColumn _adjustByColumnTimeSpan = null; // column containing a timespan we should adjust the date by
        ColumnNumberOrColumn _adjustByColumnSeconds = null; // column containing the number of seconda we should adjust the date by
        bool _adjustByFixed = true; // true if the adjustment amount is fixed at XML load time
        string _invaliddatevalue = "";
        bool _useinvaliddatevalue = false;
        bool _preservevaluewheninvalid = false;
        bool _UTCToLocal = false;
        bool _LocalToUTC = false;
        int _weekcommencing = -1;
        int _weekending = -1;
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "now":
                case "trim":
                case "informat":
                case "outformat":
                case "localtoutc":
                case "utctolocal":
                case "adjustby":
                case "adjustbycolumntimespan":
                case "adjustbycolumnseconds":
                case "invaliddatevalue":
                case "weekcommencing":
                case "weekending":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string orig = string.Empty;
            bool negative = false;

            try
            {
                DateTime dt = DateTime.MinValue; // the output date

                // if we are to use the time now
                if (_now)
                {
                    orig = "Now()";
                    // get the time now
                    dt = DateTime.Now;
                }
                else
                {
                    // get the column value
                    string cv = _srccolumn.Value(line);

                    string indate = cv; // a working copy of the input date
                    orig = cv;

                    // if trim then trim off any blanks
                    if (_trim)
                    {
                        indate = indate.Trim();
                    }

                    // if it is blank the output should be blank
                    if (indate == string.Empty)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd(string.Empty);

                        return cl;
                    }

                    try
                    {
                        if (_inputformat.Contains("JJjjj"))
                        {
                            if (indate.Length == 5)
                            {
                                dt = new DateTime(Convert.ToInt32(indate.Substring(0, 2)), 1, 1).AddDays(Convert.ToDouble(indate.Substring(2, 3)));
                                if (dt.Year < 50)
                                {
                                    dt = dt.AddYears(2000);
                                }
                                else
                                {
                                    dt = dt.AddYears(1900);
                                }
                            }
                        }
                        else if (_inputformat.Contains("JJJJjjj"))
                        {
                            if (indate.Length == 7)
                            {
                                dt = new DateTime(Convert.ToInt32(indate.Substring(0, 4)), 1, 1).AddDays(Convert.ToDouble(indate.Substring(4, 3)));
                            }
                        }
                        else if (Regex.IsMatch(indate, @"^[-]*\d+$") && _inputformat.Contains("mmm"))
                        {
                            if (indate[0] == '-') negative = true;
                            // this puts the time on the current date ... which seems the default behaviour of the parse function and this needs to be consistent
                            int mins = Math.Abs(Int32.Parse(indate));
                            DateTime now = DateTime.Now;
                            dt = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                            dt = dt.AddMinutes(mins);
                        }
                        else
                        {
                            // parse the date/time
                            dt = DateTime.ParseExact(indate, _inputformat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (_useinvaliddatevalue)
                        {
                            ColumnLine cl = new ColumnLine();
                            cl.AddEnd(_invaliddatevalue);

                            return cl;
                        }
                        else if (_preservevaluewheninvalid)
                        {
                            ColumnLine cl = new ColumnLine();
                            cl.AddEnd(indate);

                            return cl;
                        }
                        else
                        {
                            ColumnLine cl = new ColumnLine();
                            cl.AddEnd("Date invalid : '" + _inputformat.ToString() + "' : '" + indate + "' : " + ex.Message);

                            return cl;
                        }
                    }
                }

                // if we are to adjust it by a fixed amount
                if (_adjustByFixed)
                {
                    // adjust it
                    dt = dt + _adjustBy;
                }
                else
                {
                    // if we are to use a column containing a timespan
                    if (_adjustByColumnTimeSpan != null)
                    {
                        // get the column value
                        string cv = _adjustByColumnTimeSpan.Value(line);

                        // parse the timespan
                        TimeSpan adjustBy = TimeSpan.Parse(cv);

                        // adjust the date
                        dt = dt + adjustBy;
                    }
                    // if we are to adjust by a number of seconds
                    else if (_adjustByColumnSeconds != null)
                    {
                        string cv = _adjustByColumnSeconds.Value(line);

                        // read the seconds and convert it to a timespan
                        double secs = Convert.ToDouble(cv);
                        int h = (int)(secs / 3600);
                        secs = secs - h * 3600;
                        int m = (int)(secs / 60);
                        secs = secs - m * 60;
                        int s = (int)secs;
                        int msecs = (int)(secs * 1000) - (s * 1000);
                        TimeSpan adjustBy = new TimeSpan(0, h, m, s, msecs);

                        // adjust the time
                        dt = dt + adjustBy;
                    }
                }

                if (_UTCToLocal)
                {
                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc).ToLocalTime();
                }

                if (_LocalToUTC)
                {
                    dt = DateTime.SpecifyKind(dt, DateTimeKind.Local).ToUniversalTime();
                }

                if (_weekcommencing != -1)
                {
                    int dow = -1;
                    switch (dt.DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            dow = 0;
                            break;
                        case DayOfWeek.Tuesday:
                            dow = 1;
                            break;
                        case DayOfWeek.Wednesday:
                            dow = 2;
                            break;
                        case DayOfWeek.Thursday:
                            dow = 3;
                            break;
                        case DayOfWeek.Friday:
                            dow = 4;
                            break;
                        case DayOfWeek.Saturday:
                            dow = 5;
                            break;
                        case DayOfWeek.Sunday:
                            dow = 6;
                            break;
                    }
                    if (dow < _weekcommencing)
                    {
                        dt = dt.AddDays(-1 * (7 - (_weekcommencing - dow)));
                    }
                    else if (dow > _weekcommencing)
                    {
                        dt = dt.AddDays(-1 * (dow - _weekcommencing));
                    }
                }
                else if (_weekending != -1)
                {
                    int dow = -1;
                    switch (dt.DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            dow = 0;
                            break;
                        case DayOfWeek.Tuesday:
                            dow = 1;
                            break;
                        case DayOfWeek.Wednesday:
                            dow = 2;
                            break;
                        case DayOfWeek.Thursday:
                            dow = 3;
                            break;
                        case DayOfWeek.Friday:
                            dow = 4;
                            break;
                        case DayOfWeek.Saturday:
                            dow = 5;
                            break;
                        case DayOfWeek.Sunday:
                            dow = 6;
                            break;
                    }
                    if (dow < _weekending)
                    {
                        dt = dt.AddDays(_weekending - dow);
                    }
                    else if (dow > _weekending)
                    {
                        dt = dt.AddDays(7 - (dow - _weekending));
                    }
                }

                string output = null;

                if (_outputformat == "hhh:mm")
                {
                    DateTime now = DateTime.Now;
                    DateTime zero = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                    TimeSpan diff = dt - zero;
                    output = ((int)diff.TotalMinutes / 60).ToString() + ":" + ((int)diff.TotalMinutes % 60).ToString("0#");
                }
                else
                {
                    output = dt.ToString(_outputformat);
                }

                if (negative)
                {
                    output = "-" + output;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "DateColumn: {" + Id + "} '" + orig + "' --> '" + output + "'" + ANSIHelper.White);
                }

                ColumnLine cl2 = new ColumnLine();
                cl2.AddEnd(output);

                PostProcess();

                // format the date and return it
                return cl2;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a date column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public datecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "DateColumn";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "now")
                        {
                            // we want the time now
                            _now = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Now/>");
                            }
                        }
                        else if (n.Name.ToLower() == "trim")
                        {
                            // trim the date
                            _trim = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Trim/>");
                            }
                        }
                        else if (n.Name.ToLower() == "informat")
                        {
                            // get the input format
                            _inputformat.Add(n.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InFormat>" + n.InnerText + "</InFormat>");
                            }
                        }
                        else if (n.Name.ToLower() == "weekcommencing")
                        {
                            switch (n.InnerText.ToLower().Trim())
                            {
                                case "m":
                                case "mo":
                                case "mon":
                                case "monday":
                                    _weekcommencing = 0;
                                    break;
                                case "tu":
                                case "tue":
                                case "tues":
                                case "tuesday":
                                    _weekcommencing = 1;
                                    break;
                                case "w":
                                case "we":
                                case "wed":
                                case "wednesday":
                                    _weekcommencing = 2;
                                    break;
                                case "th":
                                case "thu":
                                case "thur":
                                case "thurs":
                                case "thursday":
                                    _weekcommencing = 3;
                                    break;
                                case "f":
                                case "fr":
                                case "fri":
                                case "friday":
                                    _weekcommencing = 4;
                                    break;
                                case "sa":
                                case "sat":
                                case "saturday":
                                    _weekcommencing = 5;
                                    break;
                                case "su":
                                case "sun":
                                case "sunday":
                                    _weekcommencing = 6;
                                    break;
                                default:
                                    throw new ApplicationException("DateColumn {" + Id + "} Unreckognised week commencing " + n.InnerText);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <WeekCommencing>" + n.InnerText + "</WeekCommencing>");
                            }
                        }
                        else if (n.Name.ToLower() == "weekending")
                        {
                            switch (n.InnerText.ToLower().Trim())
                            {
                                case "m":
                                case "mo":
                                case "mon":
                                case "monday":
                                    _weekending = 0;
                                    break;
                                case "tu":
                                case "tue":
                                case "tues":
                                case "tuesday":
                                    _weekending = 1;
                                    break;
                                case "w":
                                case "we":
                                case "wed":
                                case "wednesday":
                                    _weekending = 2;
                                    break;
                                case "th":
                                case "thu":
                                case "thur":
                                case "thurs":
                                case "thursday":
                                    _weekending = 3;
                                    break;
                                case "f":
                                case "fr":
                                case "fri":
                                case "friday":
                                    _weekending = 4;
                                    break;
                                case "sa":
                                case "sat":
                                case "saturday":
                                    _weekending = 5;
                                    break;
                                case "su":
                                case "sun":
                                case "sunday":
                                    _weekending = 6;
                                    break;
                                default:
                                    throw new ApplicationException("DateColumn {" + Id + "} Unreckognised week ending " + n.InnerText);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <WeekEnding>" + n.InnerText + "</WeekEnding>");
                            }
                        }
                        else if (n.Name.ToLower() == "invaliddatevalue")
                        {
                            _useinvaliddatevalue = true;
                            // get the input format
                            _invaliddatevalue = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InvalidDateValue>" + _invaliddatevalue + "</InvalidDateValue>");
                            }
                        }
                        else if (n.Name.ToLower() == "preservevaluewheninvalid")
                        {
                            _preservevaluewheninvalid = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <PreserveValueWhenInvalid/>");
                            }
                        }
                        else if (n.Name.ToLower() == "outformat")
                        {
                            // get the output format
                            _outputformat = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outputformat + "</OutFormat>");
                            }
                        }
                        else if (n.Name.ToLower() == "utctolocal")
                        {
                            _UTCToLocal = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <UTCToLocal/>");
                            }
                        }
                        else if (n.Name.ToLower() == "localtoutc")
                        {
                            _LocalToUTC = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LocalToUTC/>");
                            }
                        }
                        else if (n.Name.ToLower() == "adjustby")
                        {
                            // parse the adjustby timespan
                            try
                            {
                                _adjustBy = TimeSpan.Parse(n.InnerText);
                                _adjustByFixed = true;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error parsing DateColumn adjustment", ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <AdjustBy>" + _adjustBy.ToString() + "</AdjustBy>");
                            }
                        }
                        else if (n.Name.ToLower() == "adjustbycolumntimespan")
                        {
                            // read the column containing the timespan to adjust by
                            try
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <AdjustByColumnTimespan>");
                                }
                                _adjustByColumnTimeSpan = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </AdjustByColumnTimespan>");
                                }
                                _adjustByFixed = false;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading adjust by column for datecolumn.\n" + n.InnerText, ex);
                            }
                        }
                        else if (n.Name.ToLower() == "adjustbycolumnseconds")
                        {
                            // read the column containing the seconds to adjust by
                            try
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  <AdjustByColumnSeconds>");
                                }
                                _adjustByColumnSeconds = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </AdjustByColumnSeconds>");
                                }
                                _adjustByFixed = false;
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error reading adjust by column for datecolumn.\n" + n.InnerText, ex);
                            }
                        }
                    }
                }

                if (_LocalToUTC && _UTCToLocal)
                {
                    throw new ApplicationException("<LocalToUTC/> and <UTCToLocal/> cannot be both specified.");
                }

                if (_LocalToUTC)
                {
                    DateTime dt = DateTime.Now;
                    TimeSpan ts = DateTime.SpecifyKind(dt, DateTimeKind.Local).ToUniversalTime() - dt;
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "DateColumn <LocalToUTC/> {" + Id + "} adjusting time by " + Timer.FormatTimeSpan(ts, "HH:mm") + ANSIHelper.White);
                }

                if (_UTCToLocal)
                {
                    DateTime dt = DateTime.Now;
                    TimeSpan ts = DateTime.SpecifyKind(dt, DateTimeKind.Utc).ToLocalTime() - dt;
                    CSVProcessor.DisplayError(ANSIHelper.Warning + "DateColumn <UTCToLocal/> {" + Id + "} adjusting time by " + Timer.FormatTimeSpan(ts, "HH:mm") + ANSIHelper.White);
                }

                if (_weekcommencing != -1 && _weekending != -1)
                {
                    throw new ApplicationException("WeekCommencing and WeekEnding cannot be both specified.");
                }

                if (_useinvaliddatevalue && _preservevaluewheninvalid)
                {
                    throw new ApplicationException("InvalidDateValue and PreserveValueWhenInvalid cannot be both specified.");
                }

                // must have an input format if not now
                if (_inputformat.Count == 0 && !_now)
                {
                    throw new ApplicationException("No input format specified for datecolumn.");
                }

                // must have an output format
                if (_outputformat == string.Empty)
                {
                    throw new ApplicationException("No output format specified for datecolumn.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating DateColumn: {" + Id + "}", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Copy an input column
    /// </summary>
    public class copycolumn : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn = null; // column to copy
        CSVJob _j = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public copycolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            _j = j;
            Name = "CopyColumn";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            // extract the source column
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, "   " + prefix);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>" + _srccolumn.ToString() + "</Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating CopyColumn: {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                _srccolumn = new ColumnNumberOrColumn(null, j, p, od, tracexml, "   " + prefix);
            }

            // if we are doing all columns force it to safe ... i think this is ok
            if (_srccolumn.IsColumnNumber() && _srccolumn.ColumnNumber.All)
            {
                _safe = true;
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        // this is a great idea but it wont work as it is called before the first line is read and the column names are allocated ... damn
        //public override string Title
        //{
        //   get
        //   {
        //      if (base.Title == string.Empty)
        //      {
        //         if (_srccolumn.All)
        //         {
        //            return ColumnNumber.GetColNames(_p.Job).ToString();
        //         }
        //         else
        //         {
        //            return ColumnNumber.GetColNames(_p.Job)[_srccolumn].ToString();
        //         }
        //      }
        //      else
        //      {
        //         return base.Title;
        //      }
        //   }
        //}

        public override ColumnLine ProcessMulti(ColumnLine line)
        {
            if (_srccolumn.IsColumnNumber() && _srccolumn.ColumnNumber.All)
            {
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "CopyColumn: {" + Id + "}  '" + line.ToString() + "'" + ANSIHelper.White);
                }

                return line;
            }
            else
            {
                throw new ApplicationException("CopyColumn:ProcessMulti Should not get here!");
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            // if all
            if (_srccolumn.IsColumnNumber() && _srccolumn.ColumnNumber.All)
            {
                throw new UseMultItemProcess();
                //if (_trace)
                //{
                //	CSVProcessor.DisplayError(ANSIHelper.Magenta + "CopyColumn:" + Id + "  '" + line.Raw + "'" + ANSIHelper.White);
                //}

                // return the raw line
                //return line.Raw;
            }
            else
            {
                try
                {
                    // get the column value
                    string cv = string.Empty;
                    if (_srccolumn.IsColumnNumber() && _srccolumn.ColumnNumber.TagValue)
                    {
                        Debug.Assert(_p != null);
                        Debug.Assert(_p.Tags != null);
                        cv = _p.Tags.GetTagValue(_srccolumn.ColumnNumber.Name, line);
                    }
                    else
                    {
                        ColumnNumber cn = null;
                        if (_srccolumn.IsColumnNumber())
                        {
                            cn = _srccolumn.ColumnNumber;
                        }
                        else
                        {
                            cn = new ColumnNumber(_j, _srccolumn.Value(line));
                        }
                        if (cn.Variable)
                        {
                            cv = cn.GetVariableValue(_p);
                        }
                        else
                        {
                            cv = line[cn];
                        }
                    }

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "CopyColumn: {" + Id + "}  '" + cv + "'" + ANSIHelper.White);
                    }

                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(cv);

                    PostProcess();

                    // return it
                    return cl;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// 
    /// A static text column
    /// </summary>
    public class literalcolumn : Column
    {
        #region Member Variables
        string _literal; // static text to output
        #endregion

        #region Constructors
        /// <summary>
        /// Create a static text column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public literalcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Literal";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "literal")
                        {
                            // save the text
                            _literal = n.InnerText;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Literal>" + _literal + "</Literal>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating LiteralColumn: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "literal":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(_literal);

            PostProcess();

            return cl;
        }
        #endregion
    }

    public class delimitercolumn : Column
    {
        #region Member Variables
        char _delimiter = ','; // static text to output
        #endregion

        #region Constructors
        /// <summary>
        /// Create a static text column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public delimitercolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
            : base(node, j, p, od, tracexml, prefix)
        {
            Name = "DelimiterColumn";

            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "outputdelimiter")
                        {
                            // save the output delimiter to use for this file
                            try
                            {
                                if (n.InnerText.Length == 0)
                                {
                                }
                                else
                                {
                                    _delimiter = n.InnerText.Substring(0, 1)[0];

                                    // If it is a \ then it is a special character
                                    if (_delimiter == '\\')
                                    {
                                        if (n.InnerText.Substring(0, 2).ToLower() == "\\t")
                                        {
                                            _delimiter = '\t';
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new ApplicationException("Error extracting outputdelimiter : " + n.InnerText, ex);
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <OutputDelimiter>" + _delimiter + "</OutputDelimiter>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating CommaColumn: {" + Id + "}", ex);
            }

            _safe = true;
            _title = _delimiter.ToString();
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(",");

            PostProcess();

            return cl;
        }
        #endregion
    }
    /// <summary>
    /// Lookup an ip address and get the DNS entry
    /// </summary>
    public class iplookupdns : Column
    {
        #region Member Variables
        ColumnNumberOrColumn _srccolumn = null; // column containing IP address to lookup
        static Dictionary<IPAddress, string> _cache = new Dictionary<IPAddress, string>();
        #endregion

        #region Constructors
        /// <summary>
        /// Create a lookup to decode an IP into a server name
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public iplookupdns(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "IPLookupDNS";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            // grab the column to lookup
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating IPLookupDNS: {" + Id + "}", ex);
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating IPLookupDNS - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                // parse the ip address
                IPAddress ip = IPAddress.Parse(cv);

                string s = string.Empty;

                if (_cache.ContainsKey(ip))
                {
                    s = _cache[ip];
                }
                else
                {
                    s = Dns.GetHostEntry(ip).HostName;
                    lock (_cache)
                    {
                        _cache[ip] = s;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "IPLookupDNS: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    /// <summary>
    /// Get the value of a system or command line variable
    /// </summary>
    public class systemvariable : Column
    {
        #region Member Variables
        string _variable = string.Empty; // name of the variable
        static string[] _cmdLineVars = new string[10]; // the command line parameters
        static string _ipaddress = "xxx";
        #endregion

        #region Static Functions
        /// <summary>
        /// Save a variable fromt the command line parameters
        /// </summary>
        /// <param name="n"></param>
        /// <param name="val"></param>
        public static void SetVariable(int n, string val)
        {
            // only accept 0-9
            if (n < 0 || n > 9)
            {
                throw new ApplicationException("Command line variables must be numbered 0-9.");
            }

            // save it
            _cmdLineVars[n] = val;
        }

        public static string GetVariable(string var)
        {
            if (var.Length == 2 && (var[0] == 'v' || var[0] == 'V'))
            {
                int n = Int32.Parse(var.Substring(1));
                if (n >= 0 && n <= 9) return _cmdLineVars[n];
            }
            return "";
        }

        public static bool IsReservedName(string name)
        {
            switch (name.ToLower())
            {
                case "currentdirectory":
                case "desktopdirectory":
                case "mydocumentsdirectory":
                case "currentuser":
                case "machinename":
                case "osversion":
                case "ipaddress":
                case "domainname":
                case "starttime":
                case "v0":
                case "v1":
                case "v2":
                case "v3":
                case "v4":
                case "v5":
                case "v6":
                case "v7":
                case "v8":
                case "v9":
                    return true;
                default:
                    return false;
            }
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Get the variable value
        /// </summary>
        /// <returns></returns>
        string Value()
        {
            // it depends on the variable name
            switch (_variable)
            {
                // current directory
                case "currentdirectory":
                    return Environment.CurrentDirectory;
                // desktop directory
                case "desktopdirectory":
                    return Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory);
                // my documents
                case "mydocumentsdirectory":
                    return Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                // logged in user
                case "currentuser":
                    return Environment.UserName;
                // machine name
                case "machinename":
                    return Environment.MachineName;
                // Operating system version
                case "osversion":
                    return Environment.OSVersion.ToString();
                // IP Address
                case "ipaddress":
                    string ips = string.Empty; // ip address(s) to return

                    if (_ipaddress == "xxx")
                    {
                        try
                        {
                            // loop through each ip address
                            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                            {
                                // if not empty delimiter it with a ;
                                if (ips != string.Empty)
                                {
                                    ips = ips + ";";
                                }

                                // add the ip address
                                ips = ips + ip.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            // problem getting the ip address
                            throw new ApplicationException("Error getting ip address", ex);
                        }
                    }
                    else
                    {
                        ips = _ipaddress;
                    }

                    // return the ip address
                    return ips;
                // domain name of logged in user
                case "domainname":
                    return Environment.UserDomainName;
                // when this current program was started
                case "starttime":
                    return System.Diagnostics.Process.GetCurrentProcess().StartTime.ToString();
                // system variables 0-9
                case "v0":
                    return _cmdLineVars[0];
                case "v1":
                    return _cmdLineVars[1];
                case "v2":
                    return _cmdLineVars[2];
                case "v3":
                    return _cmdLineVars[3];
                case "v4":
                    return _cmdLineVars[4];
                case "v5":
                    return _cmdLineVars[5];
                case "v6":
                    return _cmdLineVars[6];
                case "v7":
                    return _cmdLineVars[7];
                case "v8":
                    return _cmdLineVars[8];
                case "v9":
                    return _cmdLineVars[9];
                default:
                    try
                    {
                        return _p.ProcessParameters.PromptVariable(_variable);
                    }
                    catch
                    {
                        try
                        {
                            return _p.JobParameters.PromptVariable(_variable);
                        }
                        catch (Exception ex2)
                        {
                            try
                            {
                                return Constants.Constant(_variable);
                            }
                            catch (Exception)
                            {
                                try
                                {
                                    return RuntimeVariable.GetValue(_variable);
                                }
                                catch (Exception)
                                {
                                    // we dont recognise that variable
                                    throw new ApplicationException("Variable name not recognised : " + _variable, ex2);
                                }
                            }
                        }
                    }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a system variable column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public systemvariable(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "SystemVariable";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "variable")
                        {
                            // get the name of the variable
                            _variable = n.InnerText.Trim().ToLower();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Variable>" + _variable + "</Variable>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating SystemVariable: {" + Id + "}", ex);
            }

            if (_variable == string.Empty)
            {
                throw new ApplicationException("Error Creating SystemVariable - Missing variable: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "variable":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // return the column value
                string s = Value();

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "SystemVariable: {" + Id + "}  '" + _variable + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    /// <summary>
    /// Lookup an ip address and get the DNS entry
    /// </summary>
    public class iplookupwhois : Column
    {
        #region Member Variables
        enum WHOIS { NONE, COUNTRY, REGISTRAR, OWNER, NETNAME, RAW, NETTYPE };
        ColumnNumberOrColumn _srccolumn = null; // column containing IP address to lookup
        WHOIS _what = WHOIS.NONE;
        static Dictionary<string, basewhois> _cache = new Dictionary<string, basewhois>();
        #endregion

        #region Private Functions
        basewhois GetWhoIs(IPAddress ip)
        {
            basewhois rc = null;

            try
            {
                rwhois rw = new rwhois(ip, rwhois.REGISTRAR.ARIN);

                if (rw.Owner == "African Network Information Center")
                {
                    rc = new whois(ip, whois.REGISTRAR.AFRINIC, "");
                }
                else if (rw.Owner == "RIPE Network Coordination Centre")
                {
                    rc = new whois(ip, whois.REGISTRAR.RIPE, "");
                }
                else if (rw.Owner == "Asia Pacific Network Information Centre")
                {
                    rc = new whois(ip, whois.REGISTRAR.APNIC, "");
                }
                else if (rw.Owner == "Latin American and Caribbean IP address Regional Registry")
                {
                    rc = new whois(ip, whois.REGISTRAR.LACNIC, "");
                }
                else if (rw.Owner == string.Empty)
                {
                    // this handles a very funky case where a netname is returned but no other details.
                    rc = new whois(ip, whois.REGISTRAR.ARIN, rw.NetName);

                    if (rc.Owner == string.Empty)
                    {
                        return null;
                    }
                }
                else
                {
                    rc = rw;
                }
            }
            catch (Exception)
            {
                return null;
            }

            return rc;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a lookup to decode an IP into a server name
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public iplookupwhois(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "IPLookupWhoIs";
            try
            {
                // read the XML
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "column")
                        {
                            // grab the column to lookup
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>");
                            }
                            _srccolumn = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Column>");
                            }
                        }
                        else if (n.Name.ToLower() == "country")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.COUNTRY;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Country/>");
                            }
                        }
                        else if (n.Name.ToLower() == "netname")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.NETNAME;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <NetName/>");
                            }
                        }
                        else if (n.Name.ToLower() == "owner")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.OWNER;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Owner/>");
                            }
                        }
                        else if (n.Name.ToLower() == "registrar")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.REGISTRAR;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Registrar/>");
                            }
                        }
                        else if (n.Name.ToLower() == "nettype")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.NETTYPE;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <NetType/>");
                            }
                        }
                        else if (n.Name.ToLower() == "raw")
                        {
                            if (_what != WHOIS.NONE)
                            {
                                throw new ApplicationException("IPLookupWhoIs can only look up one thing.");
                            }
                            // grab the column to lookup
                            _what = WHOIS.RAW;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Raw/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating IPLookupWhoIs: {" + Id + "}", ex);
            }

            if (_what == WHOIS.NONE)
            {
                throw new ApplicationException("Error Creating IPLookupWhoIs - must specify something to look for : {" + Id + "}");
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("Error Creating IPLookupWhoIs - Missing column number: {" + Id + "}");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "owner":
                case "country":
                case "registrar":
                case "netname":
                case "nettype":
                case "raw":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            basewhois w = null;

            PreProcess();

            try
            {
                // get the column value
                string cv = _srccolumn.Value(line);

                // parse the ip address
                IPAddress ip = null;
                try
                {
                    ip = IPAddress.Parse(cv);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing IP address " + cv, ex);
                }

                string s = string.Empty;

                if (_cache.ContainsKey(ip.ToString()))
                {
                    w = _cache[ip.ToString()];
                }
                else
                {
                    w = GetWhoIs(ip);
                    lock (_cache)
                    {
                        _cache.Add(ip.ToString(), w);
                    }
                }

                if (w == null)
                {
                    s = "Could not access whois data.";
                }
                else
                {
                    switch (_what)
                    {
                        case WHOIS.COUNTRY:
                            s = w.Country;
                            break;
                        case WHOIS.NETNAME:
                            s = w.NetName;
                            break;
                        case WHOIS.NETTYPE:
                            s = w.NetType;
                            break;
                        case WHOIS.OWNER:
                            s = w.Owner;
                            break;
                        case WHOIS.RAW:
                            s = w.Full;
                            break;
                        case WHOIS.REGISTRAR:
                            s = w.Registrar;
                            break;
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "IPLookupWhoIs: {" + Id + "}  '" + cv + "' --> '" + s + "'" + ANSIHelper.White);
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(s);

                PostProcess();

                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    /// <summary>
    /// This column type is used to chain column processing together. Once you choose your input subsequent processes can only refer to column 0
    /// </summary>
    public class chaincolumn : Column
    {
        #region Member Variables
        List<Column> _sources = new List<Column>(); // the source columns
        List<Column> _chainColumns = new List<Column>(); // an ordered list of chained processes
        #endregion

        #region Constructors
        /// <summary>
        /// Create a chaine column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public chaincolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "ChainColumn";
            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The source node gets the starting data for the chain
                        if (n.Name.ToLower() == "source")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Source>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    // store the source column processor
                                    _sources.Add(Column.Create(n1, j, p, od, tracexml, prefix + "    "));
                                }
                            }

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Source>");
                            }
                        }
                        else if (n.Name.ToLower() == "id" || n.Name.ToLower() == "title" || n.Name.ToLower() == "trace" || n.Name.ToLower() == "safe")
                        {
                            // do nothing
                        }
                        else
                        {
                            // add the column processor to the chain
                            _chainColumns.Add(Column.Create(n, j, p, od, tracexml, prefix + "  "));
                        }
                    }
                }

                // if we have no sources throw an error
                if (_sources.Count == 0)
                {
                    throw new ApplicationException("ChainColumn is missing a <source>.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating ChainColumn: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "source":
                case "trace":
                    return true;
            }

            if (OutputColumnProcess.SupportsCols().Contains(s))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            ColumnLine s = new ColumnLine();
            //string[] s = new string[_sources.Count]; // source columns

            try
            {
                // create a string array of source columns
                for (int i = 0; i < _sources.Count; i++)
                {
                    s.AddEnd(((Column)_sources[i]).Process(line));
                    //s[i] = ((Column)_sources[i]).Process(line).ToString(false);
                }

                // if we are tracing write out column 0 only
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Chain start data: {" + Id + "}  : " + s.ToString() + ANSIHelper.White);
                }

                // now process each of the chained processors
                foreach (Column c in _chainColumns)
                {
                    // process it creating a dummy line each time with the result so far
                    ColumnLine cl = new ColumnLine(s);
                    //ChainedLine cl = new ChainedLine(s, line.LineNumber, line.FileInfo);
                    //cl.Parse();
                    s[0] = c.Process(cl)[0];

                    // if we are tracing write out our progress
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Chain step: {" + Id + "}  " + c.ToString() + " output : " + s[0] + ANSIHelper.White);
                    }
                }

                ColumnLine cl2 = new ColumnLine();
                cl2.AddEnd(s[0]);

                PostProcess();

                return cl2;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    public class blankcolumn : Column
    {
        #region Constructors
        public blankcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "BlankColumn";
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            return false;
        }

        public override ColumnLine Process(ColumnLine line)
        {
            ColumnLine cl = new ColumnLine();
            cl.AddEnd(string.Empty);

            return cl;
        }
        #endregion
    }

    /// <summary>
    /// If then else column processing
    /// </summary>
    public class ifcolumn : Column
    {
        #region Member Variables
        LogicFilterNode _f = new AndFilterNode(); // holds the default AND filter
        Column _true = null; // output column if statement is true
        Column _false = null; // output column if statement is false
        #endregion

        #region Private Functions
        /// <summary>
        /// Parse the if condition
        /// </summary>
        /// <param name="fn"></param>
        /// <param name="n"></param>
        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, _p, tracexml, prefix + "  ")));
                    }
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create an if ... then ... else column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public ifcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "IfColumn";
            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The condition part of the statement
                        if (n.Name.ToLower() == "condition")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Condition>");
                            }
                            // parse the if condition
                            ParseFilter(_f, j, n, tracexml, prefix + "    ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Condition>");
                            }
                        }
                        // the then or true part of the statement
                        else if (n.Name.ToLower() == "then")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (_true != null)
                                    {
                                        throw new ArgumentException("Only one then condition allowed {" + Id + "}");
                                    }

                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  <Then>");
                                    }
                                    // parse the output column
                                    _true = Column.Create(n1, j, p, od, tracexml, prefix + "    ");
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  </Then>");
                                    }
                                }
                            }
                        }
                        // the else or false part of the statement
                        else if (n.Name.ToLower() == "else")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (_false != null)
                                    {
                                        throw new ArgumentException("Only one else condition allowed {" + Id + "}");
                                    }

                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  <Else>");
                                    }
                                    // parse the output column
                                    _false = Column.Create(n1, j, p, od, tracexml, prefix + "    ");
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  </Else>");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating IfColumn: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "condition":
                case "then":
                case "else":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();
            ColumnLine clout;

            try
            {
                // check the condition
                if (_f.Evaluate(line))
                {
                    // if we dont have a true output then return an empty string
                    if (_true == null)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd(string.Empty);

                        return cl;
                    }

                    // return the true output column
                    clout = _true.Process(line);

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "IfColumn: {" + Id + "} THEN --> '" + clout.ToString() + "'" + ANSIHelper.White);
                    }

                }
                else
                {
                    // if we dont have a false output then return an empty string
                    if (_false == null)
                    {
                        ColumnLine cl = new ColumnLine();
                        cl.AddEnd(string.Empty);

                        return cl;
                    }

                    // return the false output column
                    clout = _false.Process(line);

                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "IfColumn: {" + Id + "} ELSE --> '" + clout.ToString() + "'" + ANSIHelper.White);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            PostProcess();

            return clout;

        }
        #endregion
    }

    /// <summary>
    /// select case column processing
    /// </summary>
    public class selectcolumn : Column
    {
        #region Private Classes
        class SelectValue
        {
            #region Member Variables
            List<StringOrColumn> _colvalues = new List<StringOrColumn>();
            Column _output = null;
            #endregion

            #region Public Functions
            public bool equals(string s, ColumnLine l)
            {
                foreach (StringOrColumn v in _colvalues)
                {
                    if (s.ToLower() == v.Value(l).ToLower())
                    {
                        return true;
                    }
                }
                return false;
            }
            public bool contains(string s, ColumnLine l)
            {
                foreach (StringOrColumn v in _colvalues)
                {
                    if (s.ToLower().Contains(v.Value(l).ToLower()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public bool endswith(string s, ColumnLine l)
            {
                foreach (StringOrColumn v in _colvalues)
                {
                    if (s.ToLower().EndsWith(v.Value(l).ToLower()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public bool beginswith(string s, ColumnLine l)
            {
                foreach (StringOrColumn v in _colvalues)
                {
                    if (s.ToLower().StartsWith(v.Value(l).ToLower()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public ColumnLine Value(ColumnLine l)
            {
                return _output.Process(l);
            }
            #endregion

            #region Constructors
            public SelectValue(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
            {
                try
                {
                    // parse the xml
                    foreach (XmlNode n in node.ChildNodes)
                    {
                        if (n.NodeType != XmlNodeType.Comment)
                        {
                            if (n.Name.ToLower() == "testvalue")
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "   <TestValue>");
                                }
                                _colvalues.Add(new StringOrColumn(n, j, p, od, tracexml, prefix + "   "));
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "   </TestValue>");
                                }
                            }
                            else if (n.Name.ToLower() == "then")
                            {
                                if (_output != null)
                                {
                                    throw new ApplicationException("selectColumn Case case can only have one <then>");
                                }
                                else
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "   <Then>");
                                    }
                                    foreach (XmlNode n1 in n.ChildNodes)
                                    {
                                        if (n1.NodeType != XmlNodeType.Comment)
                                        {
                                            if (_output != null)
                                            {
                                                throw new ApplicationException("SelectCase Then node can only have one child node.");
                                            }
                                            _output = Column.Create(n1, j, p, od, tracexml, prefix + "   ");
                                        }
                                    }
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "   </Then>");
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error creating SelectCase case.", ex);
                }

                if (_colvalues.Count == 0)
                {
                    throw new ApplicationException("SelectCase::Constructor case must contain at least testvalue.");
                }
            }
            #endregion
        }
        #endregion

        #region Member Variables
        List<SelectValue> _cases = new List<SelectValue>();
        Column _default = null;
        ColumnNumberOrColumn _colnum = null;
        enum SCComparator { SCEQUALS, SCBEGINSWITH, SCENDSWITH, SCCONTAINS };
        SCComparator _comparator = SCComparator.SCEQUALS;
        #endregion

        #region Constructors
        /// <summary>
        /// Create an select column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public selectcolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
         : base(node, j, p, od, tracexml, prefix)
        {
            Name = "SelectColumn";
            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The condition part of the statement
                        if (n.Name.ToLower() == "case")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Case>");
                            }
                            _cases.Add(new SelectValue(n, j, p, od, tracexml, "   " + prefix));
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Case>");
                            }
                        }
                        else if (n.Name.ToLower() == "equals")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   <Equals/>");
                            }
                            _comparator = SCComparator.SCEQUALS;
                        }
                        else if (n.Name.ToLower() == "beginswith" || n.Name.ToLower() == "startswith")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   <BeginsWith/>");
                            }
                            _comparator = SCComparator.SCBEGINSWITH;
                        }
                        else if (n.Name.ToLower() == "endswith")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   <EndsWith/>");
                            }
                            _comparator = SCComparator.SCENDSWITH;
                        }
                        else if (n.Name.ToLower() == "contains")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   <Contains/>");
                            }
                            _comparator = SCComparator.SCCONTAINS;
                        }
                        // the then or true part of the statement
                        else if (n.Name.ToLower() == "default")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (_default != null)
                                    {
                                        throw new ApplicationException("SelectColumn can only have one default.");
                                    }
                                    else
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "   <Default>");
                                        }

                                        _default = Column.Create(n1, j, p, od, tracexml, "   " + prefix);
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "   </Default>");
                                        }
                                    }
                                }
                            }
                        }
                        else if (n.Name.ToLower() == "column")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   <Column>");
                            }
                            _colnum = new ColumnNumberOrColumn(n, j, p, od, tracexml, prefix + "   ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "   </Column>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating SelectColumn: {" + Id + "}", ex);
            }

            if (_cases.Count == 0)
            {
                throw new ApplicationException("SelectCase::Constructor must contain at least case.");
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "column":
                case "equals":
                case "contains":
                case "endswith":
                case "startswith":
                case "beginswith":
                case "case":
                case "testvaue":
                case "then":
                case "default":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            ColumnLine cl = new ColumnLine();
            bool set = false;
            PreProcess();
            string s = string.Empty;

            try
            {
                string cv = _colnum.Value(line);

                foreach (SelectValue sv in _cases)
                {
                    if ((_comparator == SCComparator.SCEQUALS && sv.equals(cv, line)) ||
                        (_comparator == SCComparator.SCCONTAINS && sv.contains(cv, line)) ||
                        (_comparator == SCComparator.SCBEGINSWITH && sv.beginswith(cv, line)) ||
                        (_comparator == SCComparator.SCENDSWITH && sv.endswith(cv, line)))
                    {
                        cl.AddEnd(sv.Value(line));
                        set = true;
                    }
                }

                if (!set)
                {
                    if (_default != null)
                    {
                        cl.AddEnd(_default.Process(line));
                    }
                    else
                    {
                        // add an empty value if nothing matched as we have to return something
                        cl.AddEnd(string.Empty);
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "SelectColumn: {" + Id + "}  " + cv + " -> " + cl.ToString(true) + ANSIHelper.White);
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            PostProcess();

            return cl;

        }
        #endregion
    }

    public class assert : OutputColumnProcess
    {
        #region Member Variables
        LogicFilterNode _f = new AndFilterNode(); // holds the default AND filter
        #endregion

        #region Private Functions
        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }

                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, _p, tracexml, prefix + "  ")));
                    }
                }
            }
        }
        #endregion

        #region Constructors
        public assert(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Assert";
            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The condition part of the statement
                        if (n.Name.ToLower() == "condition")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Condition>");
                            }

                            // parse the if condition
                            ParseFilter(_f, j, n, tracexml, prefix + "  ");
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Condition>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating Assert: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "condition":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                // check the condition
                if (_f.Evaluate(line))
                {
                    ColumnLine cl = new ColumnLine();
                    cl.AddEnd(string.Empty);

                    PostProcess();

                    // all good
                    return cl;
                }
                else
                {
                    Debug.Fail("Assert {" + Id + "} Failed.");
                    throw new ApplicationException("Assert {" + Id + "} Failed.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion
    }

    public class runcmd : OutputColumnProcess
    {
        #region Member Variables
        Column _buildCmd = null;
        Column _parameters = null;
        bool _asynch = false;
        #endregion

        #region Constructors
        public runcmd(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RunCmd";
            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The condition part of the statement
                        if (n.Name.ToLower() == "command" || n.Name.ToLower() == "cmd")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Command>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (_buildCmd != null)
                                    {
                                        throw new ApplicationException("RunCmd can only have one node under the <Command>");
                                    }
                                }

                                _buildCmd = Column.Create(n1, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </Command>");
                                }
                            }
                        }
                        else if (n.Name.ToLower() == "parms" || n.Name.ToLower() == "parameters")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Parameters>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (_parameters != null)
                                    {
                                        throw new ApplicationException("RunCmd can only have one node under the <Parameters>");
                                    }
                                }

                                _parameters = Column.Create(n1, j, p, od, tracexml, prefix + "   ");
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "  </Parameters>");
                                }
                            }
                        }
                        else if (n.Name.ToLower() == "asynch" || n.Name.ToLower() == "asynchronous")
                        {
                            _asynch = true;
                        }
                    }
                }
                if (_buildCmd == null)
                {
                    throw new ApplicationException("RunCmd must have at least one node under <Command>");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating RunCmd: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "command":
                case "cmd":
                case "parms":
                case "parameters":
                case "asynchronous":
                case "asynch":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                string cmd = _buildCmd.Process(line).ToString(false);
                string parms = string.Empty;
                if (_parameters != null)
                {
                    parms = _parameters.Process(line).ToString(false);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RunCmd: {" + Id + "}  " + cmd + " " + parms + ANSIHelper.White);
                }

                System.Diagnostics.Process p = System.Diagnostics.Process.Start(cmd, parms);

                if (!_asynch)
                {
                    p.WaitForExit();
                }

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(string.Empty);

                PostProcess();

                // all good
                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion
    }

    public class runtimevariableoc : OutputColumnProcess
    {
        #region Member Variables
        RuntimeVariable _var = null;
        #endregion

        #region Constructors
        public runtimevariableoc(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "RuntimeVariableOC";
            _var = new RuntimeVariable(node, j, p, od, tracexml, prefix);
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            switch (s)
            {
                case "variable":
                case "var":
                case "value":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            try
            {
                string pre = _var.Value;
                _var.Process(line);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "RuntimeVariableOC: {" + Id + "} was " + pre + " -> now " + _var.Value + ANSIHelper.White);
                }

                PostProcess();

                ColumnLine cl = new ColumnLine();
                cl.AddEnd(string.Empty);

                // all good
                return cl;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Column to join multiple columns together
    /// </summary>
    public class concatenatecolumn : Column
    {
        #region Member Variables
        List<OutputColumnProcess> _joinColumns = new List<OutputColumnProcess>(); // collection of columns to join
        #endregion

        #region Constructors
        concatenatecolumn() : base()
        {
        }

        /// <summary>
        /// Construct the object
        /// </summary>
        /// <param name="node">xml node containing the parameters for this function</param>
        public concatenatecolumn(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "ConcatenateColumn";

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "safe")
                    {
                    }
                    else if (n.Name.ToLower() == "id")
                    {
                    }
                    else if (n.Name.ToLower() == "trace")
                    {
                    }
                    else if (n.Name.ToLower() == "title")
                    {
                    }
                    else
                    {
                        // parse the column and add it to our list
                        _joinColumns.Add(Column.Create(n, j, p, od, tracexml, prefix + "  "));
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            if (OutputColumnProcess.SupportsCols().Contains(s))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            string s = string.Empty; // blank start

            try
            {
                // for each column to join
                foreach (OutputColumnProcess c in _joinColumns)
                {
                    if (c.GetType().IsSubclassOf(Column.Type))
                    {
                        // process it an add it to the result
                        try
                        {
                            s = s + c.Process(line).ToString(!((Column)c).Safe);
                        }
                        catch (UseMultItemProcess)
                        {
                            s = s + ((Column)c).ProcessMulti(line).ToString(!((Column)c).Safe);
                        }

                    }
                    else
                    {
                        c.Process(line);
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Concatenate Column: {" + Id + "} output : " + s + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            ColumnLine cl = new ColumnLine();
            cl.AddEnd(s);

            PostProcess();

            // return it
            return cl;
        }
        public override string Title
        {
            get
            {
                if (base.Title == string.Empty)
                {
                    string title = string.Empty;

                    foreach (Column c in _joinColumns)
                    {
                        title += c.Title;
                    }

                    return title;
                }
                else
                {
                    return base.Title;
                }
            }
        }
        #endregion
    }

    public class output : Column
    {
        #region Member Variables
        OutputDefinition _colod = null;
        #endregion

        #region Constructors
        public output(XmlNode node, CSVJob j, CSVProcess p, OutputDefinition od, bool tracexml, string prefix)
           : base(node, j, p, od, tracexml, prefix)
        {
            Name = "Output";
            _colod = new OutputDefinition(node, j, p, tracexml, prefix + "  ");
        }
        #endregion

        #region Public Functions
        public new static bool Supports(string s)
        {
            if (OutputColumnProcess.SupportsCols().Contains(s))
            {
                return true;
            }

            return false;
        }

        public override string Title
        {
            get
            {
                return _colod.Titles.ToString();
            }
        }

        public override ColumnLine Process(ColumnLine line)
        {
            PreProcess();

            ColumnLine cl; // blank start

            try
            {

                cl = _colod.Process(line);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Output: {" + Id + "} --> '" + cl.ToString() + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" " + Name + " {" + Id + "} ", ex);
            }

            PostProcess();

            // return it
            return cl;
        }
        #endregion
    }

    #endregion

    #endregion

    #region Output File Definition Class

    /// <summary>
    /// This class holds the output file definition
    /// </summary>
    public class OutputDefinition
    {
        #region Member Variables
        CSVProcess _csvProcess = null; // owning process
        List<OutputColumnProcess> _cols = new List<OutputColumnProcess>(); // holds the column definitions
        long _outputLineNumber = 0; // line we are up to 
        string _id = string.Empty;
        bool _timePerformance = false;
        bool _trace = false;
        int _unique = UniqueIdClass.Allocate();
        #endregion


        public string UniqueId
        {
            get
            {
                return "clusterOD" + _unique.ToString();
            }
        }

        public string FirstUniqueId
        {
            get
            {
                if (_cols.Count > 0)
                {
                    return _cols[0].UniqueId;
                }

                Debug.Fail("No output column to link to for dot output.");
                return string.Empty;
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
            dotWriter.WriteLine(prefix + "   color=yellow;");
            dotWriter.WriteLine(prefix + "   style=filled;");
            dotWriter.WriteLine(prefix + "   edge [dir=none];");
            dotWriter.WriteLine(prefix + "   node [shape=rect];");
            foreach (OutputColumnProcess o in _cols)
            {
                o.WriteDot(dotWriter, "   " + prefix);
            }
            for (int i = 1; i < _cols.Count; i++)
            {
                dotWriter.WriteLine(prefix + "   " + _cols[i - 1].UniqueId + " -> " + _cols[i].UniqueId + ";");
            }
            if (Id != string.Empty)
            {
                dotWriter.WriteLine(prefix + "   label=\"OutputDefinition::" + Id + "\";");
            }
            else
            {
                dotWriter.WriteLine(prefix + "   label=\"OutputDefinition\";");
            }
            dotWriter.WriteLine(prefix + "   }");
        }

        #region Accessors
        /// <summary>
        /// The line number we are writing out
        /// </summary>
        public long OutputLineNumber
        {
            get
            {
                return _outputLineNumber;
            }
        }

        public CSVFileLine Titles
        {
            get
            {
                CSVFileLine clOut = new CSVFileLine();
                clOut.Data = false;

                foreach (OutputColumnProcess c in _cols)
                {
                    if (c.GetType().IsSubclassOf(Column.Type))
                    {
                        if (((Column)c).Safe)
                        {
                            // if it is safe we may need to parse it out
                            CSVFileLine cl = new CSVFileLine(((Column)c).Title, 0);
                            cl.Parse();
                            clOut.AddEnd(cl);
                        }
                        else
                        {
                            clOut.AddEnd(((Column)c).Title);
                        }
                    }
                }

                return clOut;
            }
        }

        /// <summary>
        /// Get our owning processor
        /// </summary>
        public CSVProcess Processor
        {
            get
            {
                return _csvProcess;
            }
        }
        #endregion

        #region Constructors

        public OutputDefinition(CSVProcess process)
        {
            // save the owning process
            _csvProcess = process;

            Performance.CreateCounterInstance("CSVProcessor_OutputColumns", "Output Lines", Id);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="doc">Xml Document holding the output definition</param>
        public OutputDefinition(XmlNode node, CSVJob j, CSVProcess process, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<OutputColumns>");
            }
            // save the owning process
            _csvProcess = process;

            // set the output line number to 0
            _outputLineNumber = 0;

            // look at the children
            foreach (XmlNode n3 in node.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "id")
                    {
                        _id = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</id>");
                        }
                    }
                    else if (n3.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TimePerformance/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Trace/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "safe" && node.Name.ToLower() == "output")
                    {
                        // ignore
                    }
                    else if (n3.Name.ToLower() == "title" && node.Name.ToLower() == "output")
                    {
                        // ignore
                    }
                    else if (n3.Name.ToLower() == "templatedcolumn" || n3.Name.ToLower() == "templatedcolumns")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TemplatedColumns>");
                        }
                        string template = string.Empty;
                        List<string> instances = new List<string>();
                        string tid = string.Empty;
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (n4.Name.ToLower() == "template")
                                {
                                    using (var sw = new StringWriter())
                                    {
                                        using (var xw = new XmlTextWriter(sw))
                                        {
                                            n4.WriteContentTo(xw);
                                        }
                                        template = "<template>" + sw.ToString() + "</template>";
                                    }
                                }
                                else if (n4.Name.ToLower() == "id")
                                {
                                    tid = n4.InnerText;
                                }
                                else if (n4.Name.ToLower() == "instances")
                                {
                                    foreach (XmlNode n5 in n4.ChildNodes)
                                    {
                                        if (n5.NodeType != XmlNodeType.Comment)
                                        {
                                            if (n5.Name.ToLower() == "instance")
                                            {
                                                instances.Add(n5.InnerText);
                                            }
                                            else
                                            {
                                                throw new ApplicationException("TemplatedColumns: Unrecognised node " + n5.Name);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException("TemplatedColumns: Unrecognised node " + n4.Name);
                                }
                            }
                        }

                        if (template == string.Empty)
                        {
                            throw new ApplicationException("TemplatedColumns: Template not provided.");
                        }
                        if (instances.Count == 0)
                        {
                            throw new ApplicationException("TemplatedColumns: No instances defined.");
                        }
                        if (template.IndexOf("%TEMPLATE%") == -1)
                        {
                            throw new ApplicationException("TemplatedColumns: template has no %TEMPLATE% to replace.");
                        }

                        int index = 0;
                        foreach (string s in instances)
                        {
                            string t = template;
                            t = t.Replace("%TEMPLATE%", s);
                            t = t.Replace("%TEMPLATEINDEX%", index.ToString());
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(t);
                            foreach (XmlNode nn in doc.DocumentElement)
                            {
                                if (nn.NodeType != XmlNodeType.Comment)
                                {
                                    try
                                    {
                                        _cols.Add(OutputColumnProcess.Create(nn, j, process, this, tracexml, prefix + "     "));
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ApplicationException("TemplatedColumns {" + tid + "} failure applying template for '" + s + "'", ex);
                                    }
                                }
                            }
                            index++;
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </TemplatedColumns>");
                        }
                    }
                    else
                    {
                        // each node here should be a column derived class defining one output column
                        _cols.Add(OutputColumnProcess.Create(n3, j, process, this, tracexml, prefix + "  "));
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</OutputColumns>");
            }
            if (_timePerformance && Id == "")
            {
                throw new ApplicationException("OutputDefinition <TimePerformance\\> requires that you also specify an <Id>.");
            }

            Performance.CreateCounterInstance("CSVProcessor_OutputColumns", "Output Lines", Id);

            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_OutputColumns", "OutputColumns Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_OutputColumns", "OutputColumns Average Duration Base", Id);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// This function takes an input line and produces the transformed output line
        /// </summary>
        /// <param name="l">CSV input line</param>
        /// <returns>output line</returns>
        public CSVFileLine Process(ColumnLine l)
        {
            long starttime = 0;
            Performance.QueryPerformanceCounter(ref starttime);
            Performance.Increment("Output Lines", Id);

            CSVFileLine clOut = new CSVFileLine();

            // increment the output line number
            _outputLineNumber++;
            Performance.SetCounter("Output Lines", Id, (int)_outputLineNumber);

            // start with an empty result
            string s = string.Empty;

            // this is the first column

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Input Line : {" + Id + "} : " + ANSIHelper.Trace + l.ToString() + ANSIHelper.White);
            }

            // for each defined output column
            foreach (OutputColumnProcess c in _cols)
            {
                if (c.GetType().IsSubclassOf(Column.Type))
                {
                    try
                    {
                        clOut.AddEnd(((Column)c).Process(l));
                    }
                    catch (UseMultItemProcess)
                    {
                        clOut.AddEnd(((Column)c).ProcessMulti(l));
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    c.Process(l);
                }

            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Information + "Output Line : " + ANSIHelper.Trace + clOut.ToString() + ANSIHelper.White);
            }
            if (_timePerformance)
            {
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                Performance.IncrementBy("OutputColumns Average Duration", Id, endtime - starttime);
                Performance.Increment("OutputColumns Average Duration Base", Id);
            }
            return clOut;
        }

        public string Id
        {
            get
            {
                if (_id == string.Empty)
                {
                    if (_csvProcess != null)
                    {
                        return _csvProcess.Id;
                    }
                    else
                    {
                        return "OutputDefinition";
                    }
                }
                else
                {
                    return _id;
                }
            }
        }
        #endregion
    }

    #endregion
}
