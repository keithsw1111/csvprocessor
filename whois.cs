﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace CSVProcessor
{
    abstract class basewhois
    {
        public abstract string Owner
        {
            get;
        }
        public abstract string NetName
        {
            get;
        }
        public abstract string NetType
        {
            get;
        }
        public abstract string Country
        {
            get;
        }
        public abstract string Full
        {
            get;
        }
        public abstract string Registrar
        {
            get;
        }
        public abstract string ReferTo
        {
            get;
        }
        public abstract IPAddress IP
        {
            get;
        }
    }

    class whois : basewhois
    {
        #region whois registrars
        static string _APNIC = "whois.apnic.net";
        static string _ARIN = "whois.arin.net";
        static string _RIPE = "whois.ripe.net";
        static string _LACNIC = "whois.lacnic.net";
        static string _AFRINIC = "whois.afrinic.net";
        #endregion

        #region Member Variables
        public enum REGISTRAR { UNKNOWN, ARIN, APNIC, RIPE, LACNIC, AFRINIC };
        string _full;
        REGISTRAR _registrar = REGISTRAR.UNKNOWN;
        IPAddress _ip;
        #endregion

        #region Accessors
        static Regex _orgnameregex = new Regex("(O|o)rg(-|)(n|N)ame:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _custnameregex = new Regex("(C|c)ust(-|)(n|N)ame:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _ownerregex = new Regex("owner:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _descrregex = new Regex("descr:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string Owner
        {
            get
            {
                string owner = _orgnameregex.Match(_full).Groups["val"].Value;

                if (owner == string.Empty)
                {
                    owner = _custnameregex.Match(_full).Groups["val"].Value;
                }

                if (owner == string.Empty)
                {
                    owner = _ownerregex.Match(_full).Groups["val"].Value;
                }

                if (owner == string.Empty)
                {
                    foreach (Match m in _descrregex.Matches(_full))
                    {
                        if (owner != string.Empty)
                        {
                            owner += " ";
                        }
                        owner += m.Groups["val"].Value;
                    }
                }

                return owner;
            }
        }

        static Regex _countryregex = new Regex("(C|c)ountry:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string Country
        {
            get
            {
                return _countryregex.Match(_full).Groups["val"].Value;
            }
        }

        static Regex _netnameregex = new Regex("(n|N)et(-|)(n|N)ame:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _owneridregex = new Regex("ownerid:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string NetName
        {
            get
            {
                string netname = _netnameregex.Match(_full).Groups["val"].Value;

                if (netname == string.Empty)
                {
                    netname = _owneridregex.Match(_full).Groups["val"].Value;
                }

                return netname;
            }
        }

        static Regex _nettyperegex = new Regex("(N|n)et(-|)(t|T)ype:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _statusegex = new Regex("(S|s)tatus:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string NetType
        {
            get
            {
                string nettype;

                if (_registrar == REGISTRAR.ARIN)
                {
                    nettype = _nettyperegex.Match(_full).Groups["val"].Value;
                }
                else if (_registrar == REGISTRAR.APNIC ||
                         _registrar == REGISTRAR.RIPE ||
                         _registrar == REGISTRAR.LACNIC ||
                         _registrar == REGISTRAR.AFRINIC)
                {
                    nettype = _statusegex.Match(_full).Groups["val"].Value;
                }
                else
                {
                    nettype = "Cant extract as whois provider not recognised " + _registrar;
                }

                return nettype;
            }
        }

        public override string Registrar
        {
            get
            {
                return _registrar.ToString();
            }
        }

        public override string Full
        {
            get
            {
                return _full;
            }
        }

        static Regex _referto = new Regex("ReferralServer:[ ]*whois:\\/\\/(?'val'.*?)(:|.$)", RegexOptions.Multiline);
        public override string ReferTo
        {
            get
            {
                return _referto.Match(_full).Groups["val"].Value;
            }
        }

        public override IPAddress IP
        {
            get { return _ip; }
        }

        #endregion

        #region  Constructors

        public whois(IPAddress ip, REGISTRAR reg, string netname)
        {
            string server;
            string prefix = string.Empty;

            _registrar = reg;
            _ip = ip;

            switch (reg)
            {
                case REGISTRAR.AFRINIC:
                    server = _AFRINIC;
                    break;
                case REGISTRAR.APNIC:
                    server = _APNIC;
                    break;
                case REGISTRAR.ARIN:
                    prefix = "n ";
                    server = _ARIN;
                    break;
                case REGISTRAR.LACNIC:
                    server = _LACNIC;
                    break;
                case REGISTRAR.RIPE:
                    server = _RIPE;
                    break;
                default:
                    _full = "Unknown registrar";
                    return;
            }

            TcpClient tcpWhois;
            NetworkStream nsWhois;
            BufferedStream bfWhois;
            StreamWriter swSend;
            StreamReader srReceive;

            try
            {
                // The TcpClient should connect to the who-is server, on port 43 (default who-is)
                tcpWhois = new TcpClient(server, 43);

                // Set up the network stream
                nsWhois = tcpWhois.GetStream();

                // Hook up the buffered stream to the network stream
                bfWhois = new BufferedStream(nsWhois);
            }
            catch
            {
                _full = "Could not open a connection to the Who-Is server.";
                return;
            }

            if (ip != null && netname == string.Empty)
            {
                // Send to the server the hostname that we want to get information on
                swSend = new StreamWriter(bfWhois);
                swSend.WriteLine(prefix + ip.ToString());
                swSend.Flush();
            }
            else
            {
                swSend = new StreamWriter(bfWhois);
                swSend.WriteLine(netname);
                swSend.Flush();
            }

            try
            {
                srReceive = new StreamReader(bfWhois);
                string strResponse;

                // Read the response line by line and put it in the textbox
                while ((strResponse = srReceive.ReadLine()) != null)
                {
                    _full += strResponse + "\r\n";
                }
            }
            catch
            {
                _full = "Could not read data from the Who-Is server.";
                return;
            }

            // We're done with the connection
            tcpWhois.Close();
        }
        #endregion
    }

    class rwhois : basewhois
    {
        #region rwhois registrars
        static string _ARIN = "rwhois.arin.net";
        #endregion

        #region Member Variables
        public enum REGISTRAR { UNKNOWN, ARIN, REFERRAL };
        string _full;
        REGISTRAR _registrar = REGISTRAR.UNKNOWN;
        IPAddress _ip;
        #endregion

        #region Accessors
        static Regex _ownerregex = new Regex("(O|o)rg(-|)(N|n)ame:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string Owner
        {
            get
            {
                return _ownerregex.Match(_full).Groups["val"].Value;
            }
        }

        static Regex _handleregex = new Regex("Handle:[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _netnameregex = new Regex("(Network-Name|netname):[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        static Regex _lastresortnetnameregex = new Regex("\\((?'val'NET[^\\)]*?)\\)", RegexOptions.Multiline);
        public override string NetName
        {
            get
            {
                string networkname = _netnameregex.Match(_full).Groups["val"].Value;

                if (networkname == string.Empty)
                {
                    networkname = _handleregex.Match(_full).Groups["val"].Value;
                }

                if (networkname == string.Empty)
                {
                    networkname = _lastresortnetnameregex.Match(_full).Groups["val"].Value;
                }

                return networkname;
            }
        }

        static Regex _countryregex = new Regex("(C|c)ountry(-Code|):[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string Country
        {
            get
            {
                return _countryregex.Match(_full).Groups["val"].Value;
            }
        }

        static Regex _nettyperegex = new Regex("(Class-Name|NetType):[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string NetType
        {
            get
            {
                return _nettyperegex.Match(_full).Groups["val"].Value;
            }
        }

        public override string Registrar
        {
            get
            {
                return _registrar.ToString();
            }
        }

        public override string Full
        {
            get
            {
                return _full;
            }
        }

        static Regex _referralregex = new Regex("(R|r)eferral(Server:|)[ ]*(?'val'.*?).$", RegexOptions.Multiline);
        public override string ReferTo
        {
            get
            {
                return _referralregex.Match(_full).Groups["val"].Value;
            }
        }

        public override IPAddress IP
        {
            get { return _ip; }
        }
        #endregion

        void GetData(IPAddress ip, string server)
        {
            TcpClient tcpWhois;
            NetworkStream nsWhois;
            BufferedStream bfWhois;
            StreamWriter swSend;
            StreamReader srReceive;

            try
            {
                // The TcpClient should connect to the who-is server, on port 43 (default who-is)
                tcpWhois = new TcpClient(server, 43 /*4321*/);

                // Set up the network stream
                nsWhois = tcpWhois.GetStream();

                // Hook up the buffered stream to the network stream
                bfWhois = new BufferedStream(nsWhois);
            }
            catch
            {
                _full = "Could not open a connection to the R-Who-Is server.";
                return;
            }

            if (ip != null)
            {
                // Send to the server the hostname that we want to get information on
                swSend = new StreamWriter(bfWhois);
                //swSend.WriteLine("-forward on");
                swSend.Write("n ");
                swSend.WriteLine(ip.ToString());
                swSend.Flush();
            }

            try
            {
                srReceive = new StreamReader(bfWhois);
                string strResponse;

                // Read the response line by line and put it in the textbox
                while ((strResponse = srReceive.ReadLine()) != null)
                {
                    _full += strResponse + "\r\n";
                }
            }
            catch
            {
                _full = "Could not read data from the RWho-Is server.";
                return;
            }

            // We're done with the connection
            tcpWhois.Close();
        }

        #region Constructor

        public rwhois(IPAddress ip, string url)
        {
            _registrar = REGISTRAR.REFERRAL;
            _ip = ip;

            GetData(null, url);
        }
        public rwhois(IPAddress ip, REGISTRAR reg)
        {
            string server;

            _registrar = reg;
            _ip = ip;

            switch (reg)
            {
                case REGISTRAR.ARIN:
                    server = _ARIN;
                    break;
                default:
                    _full = "Unknown registrar";
                    return;
            }

            GetData(ip, server);
        }
        #endregion
    }
}
