// Code last tidied up September 4, 2010

using System;
using System.Xml;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Text;
using System.Data;
using System.IO;

namespace CSVProcessor
{
    #region Base Class for conditions
    /// <summary>
    /// Base class for conditions
    /// </summary>
    public class Condition
    {
        #region Member Variables
        protected CSVProcess _p = null; // the owning process
        protected string _id = string.Empty; // an id for the condition
        protected bool _trace = false; // trace logic flag
        bool _timePerformance = false; // true if we are to time filter variable performance
        long starttime = 0; // start time for an operation
        protected string _name = string.Empty;
        int _unique = UniqueIdClass.Allocate();
        #endregion

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string UniqueId
        {
            get
            {
                return "F" + _unique.ToString();
            }
        }

        virtual public string GraphDetail
        {
            get
            {
                return string.Empty;
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (GraphDetail != string.Empty)
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + Name + "::" + Id + "::" + GraphDetail + "\"];");
            }
            else
            {
                dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + Name + "::" + Id + "\"];");
            }
        }

        #region Constructor
        /// <summary>
        /// Create a condition
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public Condition(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix, string[] ignore)
        {
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "id")
                    {
                        _id = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
                        }
                    }
                    else if (n.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trace/>");
                        }
                    }
                    else if (n.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<TimePerformance/>");
                        }
                    }
                    else
                    {
                        bool found = false;
                        foreach (string s in ignore)
                        {
                            if (s.ToLower() == n.Name.ToLower() || s == "*")
                            {
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Condition::Constructor Ignoring xml node : " + n.Name + "\n" + ANSIHelper.White);
                        }
                    }
                }
            }

            if (_timePerformance && _id == string.Empty)
            {
                throw new ApplicationException("Condition::Constructor <TimePerformance> requires <Id> to also be specified.");
            }

            // save the owning filter
            _p = p;

            // create performance counters
            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_Condition", "Condition Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_Condition", "Condition Average Duration Base", Id);
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Before we process a condition
        /// </summary>
        protected void PreProcess()
        {
            // get the start time
            if (_timePerformance)
            {
                Performance.QueryPerformanceCounter(ref starttime);
            }
        }

        /// <summary>
        /// After we process a condition
        /// </summary>
        protected void PostProcess()
        {
            if (_timePerformance)
            {
                // get the end time
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                // update the performance counters
                Performance.IncrementBy("Condition Average Duration", Id, endtime - starttime);
                Performance.Increment("Condition Average Duration Base", Id);
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Id of the condition
        /// </summary>
        public string Id
        {
            get
            {
                return "{" + _id + "}";
            }
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Evaluate a condition
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public virtual bool True(ColumnLine line)
        {
            Debug.Fail("Condition succeeding because not directed to a condition.");
            return true;
        }
        #endregion

        #region Static Functions

        /// <summary>
        /// Get a list of supported Conditions
        /// </summary>
        /// <returns></returns>
        public static List<string> Supports()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if ((t.BaseType.Name == "Condition" || t.BaseType.Name == "ConditionColumnValue") && t.Name != "ConditionColumnValue")
                {
                    s.Add("Condition::" + t.Name);
                }
            }

            return s;
        }

        /// <summary>
        /// Create a condition derived object
        /// </summary>
        /// <param name="n"></param>
        /// <param name="fd"></param>
        /// <returns></returns>
        public static Condition Create(XmlNode n, CSVJob j, CSVProcess p, bool tracexml, string prefix)
        {
            // create a fully specified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            try
            {
                // get the type to create
                Type t = Type.GetType(classname);

                if (t == null)
                {
                    throw new ApplicationException("Condition::Create - " + n.Name + " is not a valid condition type.");
                }

                // create the object
                Condition o = (Condition)Activator.CreateInstance(t, new object[] { n, j, p, tracexml, prefix + "  " });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object
                return o;
            }
            catch (Exception ex)
            {
                // this is not good. We tried to create a node that did not exist.
                throw new ApplicationException("Condition::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
        #endregion
    }
    #endregion

    #region A condition with a column and a value
    /// <summary>
    /// Condition with a column and a value parameter
    /// </summary>
    class ConditionColumnValue : Condition
    {
        #region Member variables
        protected ColumnNumberOrColumn _col; // column to use
        protected StringOrColumn _value = null; // column processor which generates the column
        #endregion

        public override string GraphDetail
        {
            get
            {
                if (_value.IsColumn)
                {
                    return string.Empty;
                }
                else
                {
                    return _value.Value();
                }
            }
        }

        #region Protected Functions

        /// <summary>
        /// Get the value
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        protected string Value(ColumnLine line)
        {
            return _value.Value(line);
        }

        /// <summary>
        /// Get the column value
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        protected string ColumnValue(ColumnLine line)
        {
            return _col.Value(line);
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Create a condition with a column and a value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public ConditionColumnValue(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix, string[] ignore)
            : base(node, j, p, tracexml, prefix, ignore)
        {
            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "column")
                    {
                        // extract the column number
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>");
                        }
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Column>");
                        }
                    }
                    // if we have a value node
                    else if (n.Name.ToLower() == "value")
                    {
                        // extract the column number
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Value>");
                        }
                        _value = new StringOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Value>");
                        }
                    }
                }
            }
        }
        #endregion
    }
    #endregion

    #region Filter Evaluation Nodes
    /// <summary>
    /// Check if the column starts with a string
    /// </summary>
    class stringstartswith : ConditionColumnValue
    {
        bool _casesensitive = false;

        #region Constructor
        public stringstartswith(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "casesensitive" })
        {
            _name = "StringStartsWith";
            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate condition
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            string v1 = string.Empty;
            string v2 = string.Empty;

            if (_casesensitive)
            {
                v1 = ColumnValue(line);
                v2 = Value(line);
            }
            else
            {
                v1 = ColumnValue(line).ToLower();
                v2 = Value(line).ToLower();
            }

            // check the string starts with the substring
            bool b = (v1.StartsWith(v2));

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StringStartsWith:" + Id + " : '" + v1 + "' starts with '" + v2 + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    class stringbeginswith : stringstartswith
    {
        public stringbeginswith(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix)
        {
        }
    }

    /// <summary>
    /// Check if the column ends with the string
    /// </summary>
    class stringendswith : ConditionColumnValue
    {
        bool _casesensitive = false;

        #region Constructor
        public stringendswith(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "casesensitive" })
        {
            _name = "StringEndsWith";
            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        public override bool True(ColumnLine line)
        {
            PreProcess();

            string v1 = string.Empty;
            string v2 = string.Empty;

            if (_casesensitive)
            {
                v1 = ColumnValue(line);
                v2 = Value(line);
            }
            else
            {
                v1 = ColumnValue(line).ToLower();
                v2 = Value(line).ToLower();
            }

            // check if the column value ends with the string
            bool b = (v1.EndsWith(v2));

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StringEndsWith:" + Id + " : '" + v1 + "' ends with '" + v2 + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Check is the column contains the string
    /// </summary>
    class stringcontains : ConditionColumnValue
    {
        bool _casesensitive = false;
        bool _lessthan32 = false;
        bool _greaterthan126 = false;

        #region Constructor
        public stringcontains(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "casesensitive" })
        {
            _name = "StringContains";
            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                    else if (n.Name.ToLower() == "lessthan32")
                    {
                        _lessthan32 = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<LessThan32/>");
                        }
                    }
                    else if (n.Name.ToLower() == "greaterthan126")
                    {
                        _greaterthan126 = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<GreaterThan126/>");
                        }
                    }
                }
            }

            if (_value == null && !_lessthan32 && !_greaterthan126)
            {
                throw new ApplicationException("StringContains::Constructor Must specify a string to search for or LessThan32 or GreaterThan126.");
            }
        }
        #endregion

        #region Public Functions
        public override bool True(ColumnLine line)
        {
            PreProcess();

            string cv = string.Empty;
            string search = string.Empty;
            if (_casesensitive)
            {
                cv = ColumnValue(line);
                search = Value(line);
            }
            else
            {
                cv = ColumnValue(line).ToLower();
                search = Value(line).ToLower();
            }


            // check if the column value contains the string
            bool b = false;
            if (search != string.Empty)
            {
                b = (cv.IndexOf(search) != -1);
            }

            if (!b)
            {
                foreach (char c in cv)
                {
                    if (_lessthan32 && c < 32)
                    {
                        b = true;
                        break;
                    }
                    else if (_greaterthan126 && c > 126)
                    {
                        b = true;
                        break;
                    }
                }
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StringContains:" + Id + " : '" + cv + "' contains '" + search + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Check is the column contains the string
    /// </summary>
    class inlist : Condition
    {
        bool _casesensitive = false;
        List<string> _list = new List<string>();
        enum InListMode { EQUALS, CONTAINS, STARTSWITH, ENDSWITH };
        InListMode _mode = InListMode.EQUALS;
        ColumnNumberOrColumn _col = null;

        public override string GraphDetail
        {
            get
            {
                string rs = _mode.ToString() + "\n";
                foreach (string s in _list)
                {
                    rs += s + "\n";
                }
                return rs;
            }
        }

        #region Constructor
        public inlist(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "list", "equals", "contains", "startswith", "beginswith", "endswith", "casesensitive" })
        {
            _name = "InList";
            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "column")
                    {
                        // extract the column number
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>");
                        }
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Column>");
                        }
                    }
                    else if (n.Name.ToLower() == "list")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<List>");
                        }

                        foreach (XmlNode n1 in n.ChildNodes)
                        {
                            if (n1.NodeType != XmlNodeType.Comment)
                            {
                                if (n1.Name.ToLower() == "item")
                                {
                                    _list.Add(n1.InnerText);

                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "   <Item>" + n1.InnerText + "</Item>");
                                    }
                                }
                            }
                        }

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</List>");
                        }
                    }
                    else if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                    else if (n.Name.ToLower() == "equals")
                    {
                        _mode = InListMode.EQUALS;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Equals/>");
                        }
                    }
                    else if (n.Name.ToLower() == "contains")
                    {
                        _mode = InListMode.CONTAINS;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Contains/>");
                        }
                    }
                    else if (n.Name.ToLower() == "endswith")
                    {
                        _mode = InListMode.ENDSWITH;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<EndsWith/>");
                        }
                    }
                    else if (n.Name.ToLower() == "startswith" || n.Name.ToLower() == "beginswith")
                    {
                        _mode = InListMode.STARTSWITH;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<StartsWith/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        public override bool True(ColumnLine line)
        {
            PreProcess();

            string cv = string.Empty;
            if (_casesensitive)
            {
                cv = _col.Value(line);
            }
            else
            {
                cv = _col.Value(line).ToLower();
            }

            bool b = false;

            switch (_mode)
            {
                case InListMode.EQUALS:
                    foreach (string s in _list)
                    {
                        if (_casesensitive)
                        {
                            b = (s == cv);
                        }
                        else
                        {
                            b = (s.ToLower() == cv);
                        }

                        if (b)
                        {
                            break;
                        }
                    }
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "InList:Equals" + Id + " : '" + cv + "' --> " + b.ToString() + ANSIHelper.White);
                    }

                    break;
                case InListMode.CONTAINS:
                    foreach (string s in _list)
                    {
                        if (_casesensitive)
                        {
                            b = (cv.IndexOf(s) != -1);
                        }
                        else
                        {
                            b = (cv.IndexOf(s.ToLower()) != -1);
                        }

                        if (b)
                        {
                            break;
                        }
                    }
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "InList:Contains" + Id + " : '" + cv + "' --> " + b.ToString() + ANSIHelper.White);
                    }
                    break;
                case InListMode.STARTSWITH:
                    foreach (string s in _list)
                    {
                        if (_casesensitive)
                        {
                            b = cv.StartsWith(s);
                        }
                        else
                        {
                            b = cv.StartsWith(s.ToLower());
                        }

                        if (b)
                        {
                            break;
                        }
                    }
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "InList:StartsWith" + Id + " : '" + cv + "' --> " + b.ToString() + ANSIHelper.White);
                    }
                    break;
                case InListMode.ENDSWITH:
                    foreach (string s in _list)
                    {
                        if (_casesensitive)
                        {
                            b = cv.EndsWith(s);
                        }
                        else
                        {
                            b = cv.EndsWith(s.ToLower());
                        }

                        if (b)
                        {
                            break;
                        }
                    }
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "InList:EndsWith" + Id + " : '" + cv + "' --> " + b.ToString() + ANSIHelper.White);
                    }
                    break;
            }


            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Run a custom code function
    /// </summary>
    class customcodefilter : Condition
    {
        #region Member Variables
        MethodInfo _method = null; // method
        string _class = string.Empty; // class created
        string _code = string.Empty; // code for the class
        #endregion

        #region Private Functions
        /// <summary>
        /// Calls a code function in form ...
        /// 
        /// namespace CSVProcessor
        /// {
        ///		class xyx
        ///		{
        ///			public static bool True(string[] cols)
        ///			{
        ///				return (cols[0] == "A");
        ///			}
        ///		}	
        ///	}
        ///	
        ///	This example would only include rows where the first column was "A"
        ///	
        /// </summary>
        /// <param name="code">The code to compile</param>
        /// <returns>A method to call</returns>
        MethodInfo BuildAssembly(string code)
        {
            // code is in c#
            System.CodeDom.Compiler.CodeDomProvider provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp");

            // set the compiler parameters
            CompilerParameters compilerparams = new CompilerParameters();
            compilerparams.GenerateExecutable = false;
            compilerparams.GenerateInMemory = true;

            // compile the function
            CompilerResults results =
                provider.CompileAssemblyFromSource(compilerparams, code);

            // if we have errors
            if (results.Errors.HasErrors)
            {
                // create an error message
                StringBuilder errors = new StringBuilder("Compiler Errors :\r\n");
                foreach (CompilerError error in results.Errors)
                {
                    errors.AppendFormat("Line {0},{1}\t: {2}\n",
                        error.Line, error.Column, error.ErrorText);
                }

                // throw it
                throw new ApplicationException(errors.ToString());
            }
            else
            {
                // get the type of the class
                Type type = results.CompiledAssembly.GetType("CSVProcessor." + _class);

                // get the evaluate method
                MethodInfo method = type.GetMethod("True");

                // if the method is not found throw an error
                if (method == null)
                {
                    throw new ApplicationException("True() method not found in CSVProcessor." + _class);
                }

                // return the method
                return method;
            }
        }

        /// <summary>
        /// Run the function
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        bool RunFunction(ColumnLine line)
        {
            // convert the line to a string array
            string[] cols = line.Cols;

            // call the function
            return (bool)_method.Invoke(null, new object[] { cols });
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a custom code filter
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public customcodefilter(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "class", "code" })
        {
            _name = "CustomCodeFilter";

            // read the code from an XML file
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // get the class name
                    if (n.Name.ToLower() == "class")
                    {
                        _class = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Class>" + _class + "</Class>");
                        }
                    }
                    // get the code
                    else if (n.Name.ToLower() == "code")
                    {
                        _code = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Code>" + _code + "</Code>");
                        }
                    }
                }
            }

            try
            {
                // compile the code
                _method = BuildAssembly(_code);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error constructing CustomCodeFilter:" + Id.ToString(), ex);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate the custom code
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = false;

            // run the custom function
            try
            {
                b = RunFunction(line);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing CustomCodeFilter:" + Id.ToString(), ex);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "CustomCodeFilter:" + Id + " : value '" + b.ToString() + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Check for a match to a regular expression
    /// </summary>
    class stringregexmatch : Condition
    {
        #region Member Variables
        Regex _regex; // the regular expression
        string _regexString = string.Empty; // the regex string
        ColumnNumberOrColumn _col; // column to use
        StringOrColumn _value = null; // column processor which generates the value
        bool _lowercase = false;
        #endregion

        public override string GraphDetail
        {
            get
            {
                return _regexString;
            }
        }


        #region Private Functions
        /// <summary>
        /// Get the regulare expression
        /// </summary>
        /// <returns></returns>
        string RegexValue()
        {
            return _value.Value();
        }

        /// <summary>
        /// Get the column value
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        string ColumnValue(ColumnLine line)
        {
            if (_lowercase)
            {
                return _col.Value(line).ToLower();
            }
            else
            {
                return _col.Value(line);
            }
        }
        #endregion

        #region Constructor
        public stringregexmatch(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "regex", "lowercase" })
        {
            _name = "StringRegexMatch";

            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>");
                        }
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Column>");
                        }
                    }
                    // if we have a value node
                    else if (n.Name.ToLower() == "regex")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Regex>");
                        }
                        _value = new StringOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Regex>");
                        }
                    }
                    else if (n.Name.ToLower() == "lowercase")
                    {
                        _lowercase = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<LowerCase/>");
                        }
                    }
                }
            }

            try
            {
                _regexString = RegexValue(); //_stringvalue;

                // create the regular expression
                try
                {
                    _regex = new Regex(_regexString, RegexOptions.Singleline | RegexOptions.Compiled);
                }
                catch (Exception e)
                {
                    throw new ApplicationException("StringRegexMatch error parsing regex : " + _regexString, e);
                }

                if (_regex == null)
                {
                    throw new ApplicationException("StringRegexMatch No regex found : " + _regexString);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error constructing StringRegexMatch:" + Id.ToString(), ex);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if the regex matches the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = false;

            try
            {
                string newregex = _value.Value(line);
                if (_regexString != newregex)
                {
                    try
                    {
                        _regex = new Regex(newregex);
                        _regexString = newregex;
                    }
                    catch (Exception ex)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Error + "StringRegexMatch:" + Id + " : ' tried to update regex but failed '" + newregex + "' " + ex.Message + ANSIHelper.White);
                    }
                }

                // check it matches
                b = (_regex.IsMatch(ColumnValue(line)));

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "StringRegexMatch:" + Id + " : '" + ColumnValue(line) + "' matches '" + _regexString + "' --> " + b.ToString() + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing StringRegexMatch:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Check for a value in a lookup file
    /// </summary>
    class inlookup : Condition
    {
        #region Member Variables
        List<ColumnNumberOrColumn> _keySrc = new List<ColumnNumberOrColumn>(); // source file key columns
        Column _key = null; // output column to generate the key
        DecodeTable _dt = null; // the lookup file to use
        bool _useinputrules = false;
        string _file = string.Empty;
        enum InLookupMode { EQUALS, CONTAINS, STARTSWITH, ENDSWITH };
        InLookupMode _mode = InLookupMode.EQUALS;
        bool _casesensitive = false;
        bool _fastfailblank = false;
        bool _dumpLookup = false;

        #endregion

        public override string GraphDetail
        {
            get
            {
                return _file;
            }
        }

        #region Constructor
        public inlookup(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "fastfailifblank", "sourcekeys", "targetkeys", "dumplookup", "lookupfile", "useinputparsingrules", "casesensitive", "equals", "contains", "startswith", "beginswith", "endswith" })
        {
            _name = "InLookup";

            List<String> keyTgt = null; // list of key columns in  the lookup file

            try
            {
                // iterate through the nodes
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        if (n.Name.ToLower() == "sourcekeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <SourceKeys>");
                            }
                            foreach (XmlNode n2 in n.ChildNodes)
                            {
                                if (n2.NodeType != XmlNodeType.Comment)
                                {
                                    if (n2.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the source key columns
                                        try
                                        {
                                            ColumnNumberOrColumn cn = new ColumnNumberOrColumn(n2, j, p, null, tracexml, prefix + "   ");
                                            _keySrc.Add(cn);
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading inlookup source column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        _key = Column.Create(n2, j, p, null, tracexml, prefix + "  ");
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </SourceKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "targetkeys")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <TargetKeys>");
                            }
                            // create a target key list
                            keyTgt = new List<String>();

                            foreach (XmlNode n3 in n.ChildNodes)
                            {
                                if (n3.NodeType != XmlNodeType.Comment)
                                {
                                    if (n3.Name.ToLower() == "key")
                                    {
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    <Key>");
                                        }
                                        // add the target key columns
                                        try
                                        {
                                            StringOrColumn cn = new StringOrColumn(n3, j, p, null, tracexml, prefix + "   ");
                                            keyTgt.Add(cn.Value());
                                        }
                                        catch (Exception ex)
                                        {
                                            throw new ApplicationException("Error reading inlookup target column", ex);
                                        }
                                        if (tracexml)
                                        {
                                            CSVProcessor.DisplayError(prefix + "    </Key>");
                                        }
                                    }
                                    else
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Warning + "InLookup::Constructor Ignoring xml node : " + n3.Name + "\n" + ANSIHelper.White);
                                    }
                                }
                            }
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </TargetKeys>");
                            }
                        }
                        else if (n.Name.ToLower() == "useinputparsingrules")
                        {
                            _useinputrules = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <UseInputParsingRules/>");
                            }
                        }
                        else if (n.Name.ToLower() == "fastfailifblank")
                        {
                            _fastfailblank = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <FastFailIfBlank/>");
                            }
                        }
                        else if (n.Name.ToLower() == "dumplookup")
                        {
                            _dumpLookup = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <DumpLookup/>");
                            }
                        }
                        else if (n.Name.ToLower() == "lookupfile")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <LookupFile>");
                            }

                            StringOrColumn sc = new StringOrColumn(n, j, p, null, tracexml, prefix + "   ");

                            _file = sc.Value();

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "    " + _file);
                                CSVProcessor.DisplayError(prefix + "  </LookupFile>");
                            }
                        }
                        else if (n.Name.ToLower() == "casesensitive")
                        {
                            _casesensitive = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                            }
                        }
                        else if (n.Name.ToLower() == "equals")
                        {
                            _mode = InLookupMode.EQUALS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Equals/>");
                            }
                        }
                        else if (n.Name.ToLower() == "contains")
                        {
                            _mode = InLookupMode.CONTAINS;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Contains/>");
                            }
                        }
                        else if (n.Name.ToLower() == "endswith")
                        {
                            _mode = InLookupMode.ENDSWITH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<EndsWith/>");
                            }
                        }
                        else if (n.Name.ToLower() == "startswith" || n.Name.ToLower() == "beginswith")
                        {
                            _mode = InLookupMode.STARTSWITH;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<StartsWith/>");
                            }
                        }
                    }
                }

                if (_keySrc.Count == 0 && _key == null)
                {
                    throw new ApplicationException("InLookup does not have a source key column.");
                }
                else if (_keySrc.Count > 0 && _key != null)
                {
                    throw new ApplicationException("InLookup can't have source keys and a source output column.");
                }

                // if we got a file name and a target key then load the lookup file
                if (_file != string.Empty && keyTgt != null)
                {
                    try
                    {
                        if (_useinputrules)
                        {
                            // load the lookup file
                            _dt = new DecodeTable(_file, keyTgt, _dumpLookup, false, p.Input.Delimiter, p.Input.Quote, p.Input.QuoteEscape);
                        }
                        else
                        {
                            // load the lookup file
                            _dt = new DecodeTable(_file, keyTgt, _dumpLookup, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Problem loading lookup file : " + _file, ex);
                    }
                }
                else
                {
                    throw new ApplicationException("Lookup file incompletely specified.");
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating InLookup: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if the regex matches the column
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();
            bool b = false;

            try
            {
                string key = string.Empty;

                // if we have an output column key
                if (_key != null)
                {
                    // generate the key using the output column
                    key = _key.Process(line).ToString(false).Trim();
                }
                else
                {
                    // construct the key
                    foreach (ColumnNumberOrColumn n in _keySrc)
                    {
                        key += n.Value(line).Trim();
                    }
                }

                if (!_casesensitive)
                {
                    key = key.ToLower();
                }

                if (_fastfailblank && key == string.Empty)
                {
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "InLoolup:FastFailIfBlank" + Id + " : '" + key + "' --> " + b.ToString() + ANSIHelper.White);
                    }
                }
                else
                {
                    // check if the key exists
                    //b = _dt.Contains(key);
                    switch (_mode)
                    {
                        case InLookupMode.EQUALS:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    b = (s == key);
                                }
                                else
                                {
                                    b = (s.ToLower() == key);
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "InLoolup:Equals" + Id + " : '" + key + "' --> " + b.ToString() + ANSIHelper.White);
                            }

                            break;
                        case InLookupMode.CONTAINS:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    b = (key.IndexOf(s) != -1);
                                }
                                else
                                {
                                    b = (key.IndexOf(s.ToLower()) != -1);
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "InLookup:Contains" + Id + " : '" + key + "' --> " + b.ToString() + ANSIHelper.White);
                            }
                            break;
                        case InLookupMode.STARTSWITH:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    b = key.StartsWith(s);
                                }
                                else
                                {
                                    b = key.StartsWith(s.ToLower());
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "InLookup:StartsWith" + Id + " : '" + key + "' --> " + b.ToString() + ANSIHelper.White);
                            }
                            break;
                        case InLookupMode.ENDSWITH:
                            foreach (string s in _dt.Keys)
                            {
                                if (_casesensitive)
                                {
                                    b = key.EndsWith(s);
                                }
                                else
                                {
                                    b = key.EndsWith(s.ToLower());
                                }

                                if (b)
                                {
                                    break;
                                }
                            }
                            if (_trace)
                            {
                                CSVProcessor.DisplayError(ANSIHelper.Trace + "InLookup:EndsWith" + Id + " : '" + key + "' --> " + b.ToString() + ANSIHelper.White);
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Processing InLookup: {" + Id + "}", ex);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    class inputcolumnexists : Condition
    {
        string _colname = string.Empty;

        public override string GraphDetail
        {
            get
            {
                return _colname;
            }
        }

        #region Public Functions
        /// <summary>
        /// Checks the condition
        /// </summary>
        /// <param name="line">The input columns</param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();
            bool b = false;

            try
            {
                b = ColumnNumber.ColumnNameExists(_p.Job, _colname);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "InputColumnExists:" + Id + " : '" + _colname + "' --> " + b.ToString() + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing InputColumnExists(" + _colname + ") Filter:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create an inputcolumnexists column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public inputcolumnexists(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "colname", "columnname" })
        {
            _name = "InputColumnExists";

            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // if we have a column node
                        if (n.Name.ToLower() == "colname" || n.Name.ToLower() == "columnname")
                        {
                            _colname = n.InnerText.ToLower().Trim();
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<ColumnName>" + n.InnerText + "</ColumnName>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating InputColumnExists Filter:" + Id.ToString(), ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Evaluate an equation with arithmatic or logical operations
    /// </summary>
    class expression : Condition
    {
        #region Member Variables
        System.Collections.ArrayList _formulaDecoded; // the formula to run
        string _formula = string.Empty;
        bool _stripCurrencyPct = false; // flag to indicate if $,% should be stripped from columns before they are processed
        string _format = "F6";
        #endregion

        public override string GraphDetail
        {
            get
            {
                return _formula;
            }
        }

        #region Private Functions

        /// <summary>
        /// Calculate a boolean function
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool CalcBoolean(string s)
        {
            // create a data table
            DataTable dt = new DataTable("tbl");

            // add a column with our formula with a boolean result
            dt.Columns.Add(new DataColumn("calc", Type.GetType("System.Boolean"), s));

            // add a row
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);

            // return the value
            return Convert.ToBoolean(dt.Rows[0]["calc"]);
        }

        /// <summary>
        /// Decode the formula
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static System.Collections.ArrayList DecodeFormula(string f, CSVJob j, CSVProcess p)
        {
            System.Collections.ArrayList result = new System.Collections.ArrayList(); // the decoded formula
            int i = 0; // an iterator
            int state = 0; // parsing state
            string s = string.Empty; // a parse substring

            // scan each character
            for (i = 0; i < f.Length; i++)
            {
                // get the current character
                string c = f.Substring(i, 1);

                // if we are in beginning state
                if (state == 0)
                {
                    // if it is a column name start char
                    if (c == "[")
                    {
                        // if we have a substring
                        if (s != string.Empty)
                        {
                            // add it to the parse array
                            result.Add(s);
                        }

                        // now in column name state
                        state = 1;

                        // clear the substring
                        s = string.Empty;
                    }
                    // if it is a column name end char
                    else if (c == "]")
                    {
                        // this is not valid as we must be in state 1
                        throw new ApplicationException("Invalid formula in filter : " + f);
                    }
                    else
                    {
                        // add the character to the substring
                        s = s + c;
                    }
                }
                // if we are in the column name state
                else if (state == 1)
                {
                    // if it is a column name start char
                    if (c == "[")
                    {
                        // we are trying to read a column name ... you cant start another one until you finish this one
                        throw new ApplicationException("Invalid formula in filter : " + f);
                    }
                    // if it is a column name end char
                    else if (c == "]")
                    {
                        // if we have something
                        if (s != string.Empty)
                        {
                            // it is a column number so create one and add it
                            if (p == null)
                            {
                                result.Add(new ColumnNumber(j, s));
                            }
                            else
                            {
                                result.Add(new ColumnNumber(p.Job, s));
                            }
                        }

                        // move back to start state
                        state = 0;

                        // clear the substring
                        s = string.Empty;
                    }
                    else
                    {
                        // add the character to the column name
                        s = s + c;
                    }
                }
            }

            // if we are in the start state
            if (state == 0)
            {
                // if we have a substring
                if (s != string.Empty)
                {
                    // add it to our formula
                    result.Add(s);
                }
            }
            else
            {
                // we are in column name state but did not find a ']'
                throw new ApplicationException("Invalid formula in filter : " + f);
            }

            // return the decoded formula
            return result;
        }

        public static string Substitute(System.Collections.ArrayList formulaDecoded, ColumnLine line, CSVProcess p, string format, bool stripCurrencyPct)
        {
            string formula = string.Empty; // populate the formula

            // for each component in the formula
            foreach (object o in formulaDecoded)
            {
                // if it is a string component
                if (o.GetType() == typeof(System.String))
                {
                    // add it
                    formula = formula + (string)o;
                }
                else
                {
                    // it must ve a column number
                    Debug.Assert(o.GetType() == typeof(ColumnNumber));

                    // Cast it to a column number
                    ColumnNumber cn = (ColumnNumber)o;

                    // get the value of the column
                    string cv = string.Empty;
                    if (cn.TagValue)
                    {
                        if (p != null)
                        {
                            cv = p.Tags.GetTagValue(cn.Name, line);
                        }
                        else
                        {
                            throw new ApplicationException("Error processing Expression Filter: Cant use tag values : " + formula);
                        }
                    }
                    else if (cn.Variable)
                    {
                        cv = cn.GetVariableValue(p);
                    }
                    else
                    {
                        cv = line[cn];
                    }

                    // add the column value to a formula
                    if (stripCurrencyPct)
                    {
                        cv = cv.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
                        cv = cv.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol, "");
                    }

                    // Anything that wont parse as a number becomes zero
                    try
                    {
                        float f = float.Parse(cv);
                        cv = f.ToString(format);
                    }
                    catch
                    {
                        cv = ((float)(0)).ToString(format);
                    }

                    formula = formula + cv;
                }
            }
            return formula;
        }

        #endregion

            #region Public Functions
            /// <summary>
            /// Checks the condition
            /// </summary>
            /// <param name="line">The input columns</param>
            /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();
            bool b = false;
            string formula = string.Empty;

            try
            {
                formula = Substitute(_formulaDecoded, line, _p, _format, _stripCurrencyPct);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Expression:" + Id + " : '" + formula + ANSIHelper.White);
                }

                // calculate the value
                b = CalcBoolean(formula);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Expression:" + Id + " : '" + formula + "' --> " + b.ToString() + "'" + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing Expression Filter: " + Id.ToString() + " : " + formula, ex);
            }

            PostProcess();

            return b;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create an expression column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public expression(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "formula", "format" })
        {
            _name = "Expression";

            try
            {
                // parse the xml
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // if we have a column node
                        if (n.Name.ToLower() == "formula")
                        {
                            // decode the formula
                            _formula = n.InnerText;
                            _formulaDecoded = DecodeFormula(n.InnerText, j, p);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Formula>" + n.InnerText + "</Formula>");
                            }
                        }
                        else if (n.Name.ToLower() == "format")
                        {
                            _format = n.InnerText; ;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<Format>" + _format + "</Format>");
                            }
                        }
                        else if (n.Name.ToLower() == "stripcurrencypct" || n.Name.ToLower() == "stripcurrencypercent")
                        {
                            _stripCurrencyPct = true;
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "<StripCurrencyPct/>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating Expression Filter:" + Id.ToString(), ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// Check if the column is equal to the given value
    /// </summary>
    class stringequals : ConditionColumnValue
    {
        bool _casesensitive = false;
        bool _trim = false;

        #region Constructors

        /// <summary>
        /// Create a string equals condition
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public stringequals(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "casesensitive", "trim" })
        {
            _name = "StringEquals";

            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                    else if (n.Name.ToLower() == "trim")
                    {
                        _trim = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trim/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate whether the column equals the given string
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            // check if it matches
            bool b = false;

            string v1 = string.Empty;
            string v2 = string.Empty;

            if (_casesensitive)
            {
                v1 = ColumnValue(line);
                v2 = Value(line);
            }
            else
            {
                v1 = ColumnValue(line).ToLower();
                v2 = Value(line).ToLower();
            }

            if (_trim)
            {
                v1 = v1.Trim();
                v2 = v2.Trim();
            }

            b = (v1.CompareTo(v2) == 0);

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StringEquals:" + Id + " : '" + v1 + "' equals '" + v2 + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Check if the column is blank
    /// </summary>
    class isblank : Condition
    {
        bool _trim = false;
        protected ColumnNumberOrColumn _col; // column to use

        #region Constructors

        /// <summary>
        /// Create a string equals condition
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public isblank(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "trim" })
        {
            _name = "IsBlank";

            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "trim")
                    {
                        _trim = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trim/>");
                        }
                    }
                    else if (n.Name.ToLower() == "column")
                    {
                        // extract the column number
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>");
                        }
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Column>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate whether the column equals the given string
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            // check if it matches
            bool b = false;

            string v1 = _col.Value(line);

            if (_trim)
            {
                v1 = v1.Trim();
            }

            b = (v1.CompareTo("") == 0);

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "IsBlank:" + Id + " : '" + v1 + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// This column type is used to chain column processing together. Once you choose your input subsequent can only refer to columns in the source
    /// </summary>
    class chainfilter : Condition
    {
        #region Member Variables
        List<Column> _sources = new List<Column>(); // the source columns
        List<Column> _chainColumns = new List<Column>(); // an ordered list of chained processes
        LogicFilterNode _f = new AndFilterNode(); // holds the default AND filter
        #endregion

        // TODO!!!! Need to dump this to .dot file better

        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, _p, tracexml, prefix + "  ")));
                    }
                }
            }
        }

        #region Constructors
        /// <summary>
        /// Create a chaine column
        /// </summary>
        /// <param name="node"></param>
        /// <param name="od"></param>
        public chainfilter(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "source", "condition" })
        {
            _name = "ChainFilter";

            try
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.NodeType != XmlNodeType.Comment)
                    {
                        // The source node gets the starting data for the chain
                        if (n.Name.ToLower() == "source")
                        {
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <Source>");
                            }

                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    // store the source column processor
                                    _sources.Add(Column.Create(n1, j, p, null, tracexml, prefix + "    "));
                                }
                            }

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  </Source>");
                            }
                        }
                        else if (n.Name.ToLower() == "condition")
                        {
                            foreach (XmlNode n1 in n.ChildNodes)
                            {
                                if (n1.NodeType != XmlNodeType.Comment)
                                {
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  <Condition>");
                                    }
                                    // parse the condition
                                    ParseFilter(_f, j, n, tracexml, prefix + "    ");
                                    if (tracexml)
                                    {
                                        CSVProcessor.DisplayError(prefix + "  </Condition>");
                                    }
                                }
                            }
                        }
                        else if (n.Name.ToLower() == "id" || n.Name.ToLower() == "title" || n.Name.ToLower() == "trace" || n.Name.ToLower() == "safe")
                        {
                            // do nothing
                        }
                        else
                        {
                            // add the column processor to the chain
                            _chainColumns.Add(Column.Create(n, j, p, null, tracexml, prefix + "  "));
                        }
                    }
                }

                // if we have no sources throw an error
                if (_sources.Count == 0)
                {
                    throw new ApplicationException("ChainFilter is missing a <source>.");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Creating ChainFilter: {" + Id + "}", ex);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            ColumnLine s = new ColumnLine();
            //string[] s = new string[_sources.Count]; // source columns

            try
            {
                // create a string array of source columns
                for (int i = 0; i < _sources.Count; i++)
                {
                    s.AddEnd(((Column)_sources[i]).Process(line));
                    //s[i] = ((Column)_sources[i]).Process(line).ToString(false);
                }

                // if we are tracing write out column 0 only
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "ChainFilter start data: {" + Id + "}  : " + s.ToString() + ANSIHelper.White);
                }

                // now process each of the chained processors
                foreach (Column c in _chainColumns)
                {
                    // process it creating a dummy line each time with the result so far
                    ColumnLine cl = new ColumnLine(s);
                    //ChainedLine cl = new ChainedLine(s, line.LineNumber, line.FileInfo);
                    //cl.Parse();
                    s[0] = c.Process(cl)[0];

                    // if we are tracing write out our progress
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Chain step: {" + Id + "}  " + c.ToString() + " output : " + s[0] + ANSIHelper.White);
                    }
                }

                ColumnLine cl2 = new ColumnLine(s);
                //ChainedLine cl2 = new ChainedLine(s, line.LineNumber, line.FileInfo);
                //cl2.Parse();
                bool b = _f.Evaluate(cl2);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "ChainFilter:" + Id + " : '" + s[0] + "' --> " + b.ToString() + ANSIHelper.White);
                }

                PostProcess();

                return b;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(" {" + Id + "} ", ex);
            }

        }
        #endregion
    }

    /// <summary>
    /// Check if the column contains a number
    /// </summary>
    class isnumber : Condition
    {
        #region Member Variables
        //ColumnNumber _col = null; // the column to check
        ColumnNumberOrColumn _col = null; // the source column we will be trimming
        Regex _r = new Regex("[^0-9\\.\\-]", RegexOptions.Singleline | RegexOptions.Compiled); // regex for checking numbers
        #endregion

        #region Constructors
        /// <summary>
        /// Create an isnumber
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public isnumber(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column" })
        {
            _name = "IsNumber";

            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "column")
                    {
                        // get the column number
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>" + _col.ToString() + "</Column>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if the column is a number
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();
            // get the column value
            string cv = _col.Value(line);

            // trim the column value
            string s = cv.Trim();

            // Check if the numbers regex matches
            string res = _r.Match(s).Value;

            // if nothing matches then it is a number
            bool b = (s != string.Empty && res == string.Empty);

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "IsNumber:" + Id + " : '" + cv + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    class isdate : Condition
    {
        #region Member Variables
        List<string> _informat = new List<string>(); // the input format of the dates
        //ColumnNumber _col = null; // the column to check
        ColumnNumberOrColumn _col = null; // the column to check
        #endregion

        #region Constructors
        /// <summary>
        /// Check if a date is valid
        /// </summary>
        /// <param name="node"></param>
        /// <param name="p"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        public isdate(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "informat" })
        {
            _name = "IsDate";

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // get the input format
                    if (n.Name.ToLower() == "informat")
                    {
                        _informat.Add(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<InFormat>" + _informat[_informat.Count - 1] + "</InFormat>");
                        }
                    }
                    // if we have a column node
                    else if (n.Name.ToLower() == "column")
                    {
                        // get the column number
                        _col = new ColumnNumberOrColumn(n, j, p, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Column>" + _col.ToString() + "</Column>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check the date is less than the given value
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = true;

            // get the column value
            string cv = _col.Value(line);

            // trim the column value
            string s = cv.Trim();

            try
            {
                DateTime val = DateTime.MinValue;

                try
                {
                    // parse the date from the column
                    val = DateTime.ParseExact(s, _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                }
                catch (Exception)
                {
                    b = false;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "IsDate:" + Id + " : '" + s + "' --> " + b.ToString() + ANSIHelper.White);
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing IsDate:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    class isdatea : ConditionColumnValue
    {
        #region Member Variables
        List<string> _informat = new List<string>(); // the input format of the dates
        bool _UTCToLocal = false;
        bool _LocalToUTC = false;
        string _weekend = "satsun";
        string _weekday = "montuewedthufri";
        string _holidayfile;
        Holiday _holiday = null;
        bool _isweekend = false;
        bool _isweekday = false;
        bool _ismonday = false;
        bool _istuesday = false;
        bool _iswednesday = false;
        bool _isthursday = false;
        bool _isfriday = false;
        bool _issaturday = false;
        bool _issunday = false;
        bool _isholiday = false;
        int _nth = 0;
        string _what = string.Empty;
        #endregion

        #region Constructors
        public isdatea(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "weekend", "weekday", "holiday", "holidays", "informat", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "utctolocal", "localtoutc", "nth" })
        {
            _name = "IsDateA";

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // get the input format
                    if (n.Name.ToLower() == "informat")
                    {
                        _informat.Add(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<InFormat>" + _informat[_informat.Count - 1] + "</InFormat>");
                        }
                    }
                    else if (n.Name.ToLower() == "utctolocal")
                    {
                        _UTCToLocal = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<UTCToLocal/>");
                        }
                    }
                    else if (n.Name.ToLower() == "localtoutc")
                    {
                        _LocalToUTC = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<LocalToUTC/>");
                        }
                    }
                    else if (n.Name.ToLower() == "nth")
                    {
                        _nth = int.Parse(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Nth>" + _nth.ToString() + "</Nth>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekend")
                    {
                        if (n.InnerText.Trim() != string.Empty)
                        {
                            _weekend = n.InnerText.ToLower().Trim();
                        }
                        _isweekend = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Weekend>" + _weekend + "</Weekend>");
                        }
                    }
                    else if (n.Name.ToLower() == "weekday")
                    {
                        if (n.InnerText.Trim() != string.Empty)
                        {
                            _weekday = n.InnerText.ToLower().Trim();
                        }
                        _isweekday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Weekday>" + _weekday + "</Weekday>");
                        }
                    }
                    else if (n.Name.ToLower() == "monday")
                    {
                        _ismonday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Monday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "tuesday")
                    {
                        _istuesday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Tuesday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "wednesday")
                    {
                        _iswednesday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Wednesday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "thursday")
                    {
                        _isthursday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Thursday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "friday")
                    {
                        _isfriday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Friday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "saturday")
                    {
                        _issaturday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Saturday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "sunday")
                    {
                        _issunday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Sunday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "holiday")
                    {
                        _isholiday = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Holiday/>");
                        }
                    }
                    else if (n.Name.ToLower() == "holidays")
                    {
                        // look at the children
                        foreach (XmlNode n6 in n.ChildNodes)
                        {
                            // If this is a text node then they have just given us a default value
                            if (n6.NodeType == XmlNodeType.Text || n6.NodeType == XmlNodeType.CDATA)
                            {
                                // read the output filename
                                _holidayfile = n6.InnerText;
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "      <!--Holidays>" + _holidayfile + "</Holidays-->");
                                }
                                _holiday = new Holiday(_holidayfile, j, p, tracexml, prefix + "     ");
                            }
                            else if (n6.NodeType != XmlNodeType.Comment)
                            {
                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "      <Holidays>");
                                }
                                _holiday = new Holiday(n, j, p, tracexml, prefix + "        ");

                                if (tracexml)
                                {
                                    CSVProcessor.DisplayError(prefix + "      </Holidays>");
                                }

                            }
                        }
                    }
                }
            }

            if (_col == null)
            {
                throw new ApplicationException("<IsDateA> <column> must be specified.");
            }

            if (_informat.Count == 0)
            {
                throw new ApplicationException("<IsDateA> <InFormat> must be specified.");
            }

            if (_isweekend)
            {
                _what = " a Weekend";
            }

            if (_isweekday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Weekday";
            }

            if (_isholiday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Holiday";
            }

            if (_ismonday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Monday";
            }

            if (_istuesday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Tuesday";
            }

            if (_iswednesday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Wednesday";
            }

            if (_isthursday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Thursday";
            }

            if (_isfriday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Friday";
            }

            if (_issaturday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Saturday";
            }

            if (_issunday)
            {
                if (_what == string.Empty)
                {
                    _what += " or";
                }
                _what += " a Sunday";
            }
        }
        #endregion
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = false;

            try
            {
                DateTime val = DateTime.MinValue;

                try
                {
                    // parse the date from the column
                    val = DateTime.ParseExact(ColumnValue(line).Trim(), _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                    if (_UTCToLocal)
                    {
                        val = DateTime.SpecifyKind(val, DateTimeKind.Utc).ToLocalTime();
                    }

                    if (_LocalToUTC)
                    {
                        val = DateTime.SpecifyKind(val, DateTimeKind.Local).ToUniversalTime();
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing date from file column " + _col.ToString() + " '" + ColumnValue(line) + "'", ex);
                }

                if (_isweekend)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (_weekend.Contains(d))
                    {
                        b = true;
                    }
                }

                if (!b && _isweekday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (_weekday.Contains(d))
                    {
                        b = true;
                    }
                }

                if (_ismonday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "mon")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_istuesday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "tue")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_iswednesday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "wed")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_isthursday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "thu")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_isfriday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "fri")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_issaturday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "sat")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (_issunday)
                {
                    string d = val.ToString("ddd").ToLower();
                    if (d == "sun")
                    {
                        if (_nth == 0)
                        {
                            b = true;
                        }
                        else
                        {
                            int n = (val.Day - 1) / 7;
                            if (n == _nth)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                        }
                    }
                }

                if (!b && _isholiday)
                {
                    if (_holiday != null)
                    {
                        if (_holiday.IsAHoliday(val))
                        {
                            b = true;
                        }
                    }
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "IsDateA:" + Id + " : '" + val.ToString() + "' '" + _what + "' --> " + b.ToString() + ANSIHelper.White);
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing IsDateA:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
    }

    /// <summary>
    /// Check if a date is less than a given value
    /// </summary>
    class datelt : ConditionColumnValue
    {
        #region Member Variables
        List<string> _informat = new List<string>(); // the input format of the dates
        bool _orequals = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Check if a date is less than a value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="p"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        public datelt(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "informat", "orequals" })
        {
            _name = "DateLT";

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // get the input format
                    if (n.Name.ToLower() == "informat")
                    {
                        _informat.Add(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<InFormat>" + _informat[_informat.Count - 1] + "</InFormat>");
                        }
                    }
                    else if (n.Name.ToLower() == "orequals")
                    {
                        _orequals = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<OrEquals/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check the date is less than the given value
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = false;

            try
            {
                DateTime val = DateTime.MinValue;

                try
                {
                    // parse the date from the column
                    val = DateTime.ParseExact(ColumnValue(line).Trim(), _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing date from file column " + _col.ToString() + " '" + ColumnValue(line) + "' using format '" + _informat + "'", ex);
                }

                DateTime compareTo = DateTime.MinValue;

                try
                {
                    // parse out the date to compare to
                    compareTo = DateTime.ParseExact(Value(line).Trim(), _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing date from parameters '" + Value(line) + "' using format '" + _informat + "'", ex);
                }

                // compare the dates
                if (_orequals)
                {
                    b = (val.CompareTo(compareTo) <= 0);
                }
                else
                {
                    b = (val.CompareTo(compareTo) < 0);
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "DateLT:" + Id + " : '" + val.ToString() + "' < '" + compareTo.ToString() + "' --> " + b.ToString() + ANSIHelper.White);
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing DateLT:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Compare two dates
    /// </summary>
    class dateequals : ConditionColumnValue
    {
        #region Member Variables
        List<string> _informat = new List<string>(); // the input format of the dates
        bool _trim = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Check if a date is equal to a value
        /// </summary>
        /// <param name="node"></param>
        /// <param name="p"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        public dateequals(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "informat", "Trim" })
        {
            _name = "DateEquals";

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // get the date input format
                    if (n.Name.ToLower() == "informat")
                    {
                        _informat.Add(n.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<InFormat>" + _informat[_informat.Count - 1] + "</InFormat>");
                        }
                    }
                    else if (n.Name.ToLower() == "trim")
                    {
                        _trim = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Trim/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if two dates are equal
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            bool b = false;

            try
            {
                string cv = ColumnValue(line);

                if (_trim)
                {
                    cv = cv.Trim();
                }

                DateTime val = DateTime.MinValue;
                try
                {
                    // parse the date to compare to
                    val = DateTime.ParseExact(cv, _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing date from file '" + ColumnValue(line) + "'", ex);
                }

                DateTime compareTo = DateTime.MinValue;

                try
                {
                    compareTo = DateTime.ParseExact(Value(line), _informat.ToArray(), System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error parsing date from parameters '" + Value(line) + "'", ex);
                }

                // compare the dates
                b = (val.CompareTo(compareTo) == 0);

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "DateEquals:" + Id + " : '" + val.ToString() + "' = '" + compareTo.ToString() + "' --> " + b.ToString() + ANSIHelper.White);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing DateEquals:" + Id.ToString(), ex);
            }

            PostProcess();

            return b;
        }
        #endregion
    }

    /// <summary>
    /// Compare strings to check they are less
    /// </summary>
    class stringlt : ConditionColumnValue
    {
        bool _casesensitive = false;

        #region Constructors
        /// <summary>
        /// Create a string less than node
        /// </summary>
        /// <param name="node"></param>
        /// <param name="fd"></param>
        public stringlt(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
            : base(node, j, p, tracexml, prefix, new string[] { "column", "value", "casesensitive" })
        {
            _name = "StringLT";

            // parse the xml
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    // if we have a column node
                    if (n.Name.ToLower() == "casesensitive")
                    {
                        _casesensitive = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<CaseSensitive/>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if string is less than
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public override bool True(ColumnLine line)
        {
            PreProcess();

            string v1 = string.Empty;
            string v2 = string.Empty;

            if (_casesensitive)
            {
                v1 = ColumnValue(line);
                v2 = Value(line);
            }
            else
            {
                v1 = ColumnValue(line).ToLower();
                v2 = Value(line).ToLower();
            }

            // compare the strings
            bool b = (v1.CompareTo(v2) < 0);

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StringLT:" + Id + " : '" + v1 + "' < '" + v2 + "' --> " + b.ToString() + ANSIHelper.White);
            }

            PostProcess();

            return b;
        }
        #endregion
    }
    #endregion


    #region Base class of all filter nodes
    /// <summary>
    /// Filter Node Base Class
    /// </summary>
    public class FilterNode
    {
        #region Member Variables
        Condition _f = null; // the node condition
        int _unique = UniqueIdClass.Allocate();
        #endregion

        virtual public string FirstUniqueId
        {
            get
            {
                if (_f == null)
                {
                    return _unique.ToString();
                }
                else
                {
                    return _f.UniqueId;
                }
            }
        }

        public string UniqueId
        {
            get
            {
                if (_f == null)
                {
                    return "clusterFN" + _unique.ToString();
                }
                else
                {
                    return _f.UniqueId;
                }
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (_f != null)
            {
                _f.WriteDot(dotWriter, prefix);
            }
            else
            {
                Debug.Fail("Should not reach here.");
            }
        }

        virtual public bool SomethingToDo
        {
            get
            {
                if (_f != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region Constructors
        /// <summary>
        /// Create an empty filter node
        /// </summary>
        public FilterNode()
        {
            // no actual condition
            _f = null;
        }

        /// <summary>
        /// Create a filter node with a condition
        /// </summary>
        /// <param name="f"></param>
        public FilterNode(Condition f)
        {
            // save the condition
            _f = f;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate the condition
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public virtual bool Evaluate(ColumnLine l)
        {
            // if we have no condition
            if (_f == null)
            {
                // always true but this should not happen
                Debug.Fail("Evaluate filter node succeeding because not directed to a filter.");
                return true;
            }
            else
            {
                // evaluate the condition and return the result
                return _f.True(l);
            }
        }
        #endregion
    }
    #endregion

    #region Logic Nodes
    #region Logic Nodes Base Class

    /// <summary>
    /// Base class for logic nodes
    /// </summary>
    public class LogicFilterNode : FilterNode
    {
        #region Member Variables
        protected List<FilterNode> _filters = new List<FilterNode>(); // logic sub nodes
        string _name = string.Empty;
        #endregion

        public override string FirstUniqueId
        {
            get
            {
                if (_filters.Count > 0)
                {
                    return _filters[0].FirstUniqueId;
                }
                else
                {
                    Debug.Fail("No unique id");
                    return string.Empty;
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public override bool SomethingToDo
        {
            get
            {
                foreach (FilterNode n in _filters)
                {
                    if (n.SomethingToDo)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        #region Constructor
        /// <summary>
        /// Create a logic node
        /// </summary>
        public LogicFilterNode()
        {
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Add a sub node
        /// </summary>
        /// <param name="fn"></param>
        public void Add(FilterNode fn)
        {
            // add the node to the sub nodes
            _filters.Add(fn);
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// And logic node
    /// </summary>
    class AndFilterNode : LogicFilterNode
    {
        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (_filters.Count == 1)
            {
                // if only one subnode then just write it out ... basically collapse it.
                _filters[0].WriteDot(dotWriter, "   " + prefix);
            }
            else
            {
                dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
                dotWriter.WriteLine(prefix + "   fillcolor=lightgrey;");
                dotWriter.WriteLine(prefix + "   color=black;");
                dotWriter.WriteLine(prefix + "   style=filled;");
                dotWriter.WriteLine(prefix + "   label=\"AND\";");

                foreach (FilterNode f in _filters)
                {
                    f.WriteDot(dotWriter, "   " + prefix);
                }

                dotWriter.WriteLine(prefix + "   }");
            }
        }

        #region Constructors
        /// <summary>
        /// Create an and node
        /// </summary>
        public AndFilterNode()
        {
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate the condition
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public override bool Evaluate(ColumnLine l)
        {
            // for each sub node
            foreach (FilterNode fn in _filters)
            {
                // if a node is false
                if (!fn.Evaluate(l))
                {
                    // the and is false
                    return false;
                }
            }

            // all sub nodes are true so this is true
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Not logic node
    /// </summary>
    class NotFilterNode : LogicFilterNode
    {
        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
            dotWriter.WriteLine(prefix + "   fillcolor=lightgrey;");
            dotWriter.WriteLine(prefix + "   color=black;");
            dotWriter.WriteLine(prefix + "   style=filled;");
            dotWriter.WriteLine(prefix + "   label=\"NOT\";");

            foreach (FilterNode f in _filters)
            {
                f.WriteDot(dotWriter, "   " + prefix);
            }

            dotWriter.WriteLine(prefix + "   }");
        }

        #region Constructors
        /// <summary>
        /// Create a not node
        /// </summary>
        public NotFilterNode()
        {
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate the not node
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public override bool Evaluate(ColumnLine l)
        {
            // must have only one sub node
            if (_filters.Count != 1)
            {
                throw new ApplicationException("NOT only works on 1 argument");
            }
            else
            {
                // evaluate the subnode and return the inverted result
                return (!((FilterNode)_filters[0]).Evaluate(l));
            }
        }
        #endregion
    }

    /// <summary>
    /// Or logic node
    /// </summary>
    class OrFilterNode : LogicFilterNode
    {
        public override void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (_filters.Count == 1)
            {
                // if only one subnode then just write it out ... basically collapse it.
                _filters[0].WriteDot(dotWriter, "   " + prefix);
            }
            else
            {
                dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
                dotWriter.WriteLine(prefix + "   fillcolor=lightgrey;");
                dotWriter.WriteLine(prefix + "   color=black;");
                dotWriter.WriteLine(prefix + "   style=filled;");
                dotWriter.WriteLine(prefix + "   label=\"OR\";");

                foreach (FilterNode f in _filters)
                {
                    f.WriteDot(dotWriter, "   " + prefix);
                }

                dotWriter.WriteLine(prefix + "   }");
            }
        }

        #region Constructors
        /// <summary>
        /// Create an or node
        /// </summary>
        public OrFilterNode()
        {
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Evaluate the or node
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public override bool Evaluate(ColumnLine l)
        {
            // for each sub node
            foreach (FilterNode fn in _filters)
            {
                // if the sub node evaluates to true
                if (fn.Evaluate(l))
                {
                    // it is all true
                    return true;
                }
            }

            // nothing was true so this must be false
            return false;
        }
        #endregion
    }
    #endregion

    #region Filter Definition
    /// <summary>
    /// Summary description for FilterDefinition.
    /// </summary>
    public class FilterDefinition
    {
        #region Member Variables
        CSVProcess _csvProcess = null; // owning processor
        LogicFilterNode _f = new AndFilterNode(); // holds the default AND filter
        string _id = string.Empty; // id of the filter
        bool _timePerformance = false; // true if we are to time performance
        long _filteredIn = 0; // count the records we filtered in
        long _filteredOut = 0; // count the records we filtered out
        int _unique = UniqueIdClass.Allocate();
        #endregion

        public string FirstUniqueId
        {
            get
            {
                if (UniqueId.Contains("cluster"))
                {
                    return _f.FirstUniqueId;
                }
                else
                {
                    return UniqueId;
                }
            }
        }

        public string UniqueId
        {
            get
            {
                return "clusterFD" + _unique.ToString();
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            if (SomethingToDo)
            {
                dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
                dotWriter.WriteLine(prefix + "   color=red;");
                dotWriter.WriteLine(prefix + "   style=filled;");
                dotWriter.WriteLine(prefix + "   label=\"Filter\";");
                _f.WriteDot(dotWriter, "   " + prefix);
                dotWriter.WriteLine(prefix + "   }");
            }
        }

        public bool SomethingToDo
        {
            get
            {
                if (_f == null)
                {
                    return false;
                }
                else
                {
                    return _f.SomethingToDo;
                }
            }
        }

        #region Accessors
        /// <summary>
        /// Get the owning processor
        /// </summary>
        public CSVProcess Processor
        {
            get
            {
                return _csvProcess;
            }
        }
        /// <summary>
        ///  Get the id of the filter
        /// </summary>
        protected string Id
        {
            get
            {
                // if we have an id return it
                if (_id != string.Empty)
                {
                    return _id;
                }
                else
                {
                    // else return the process id if we have a process
                    if (_csvProcess != null)
                    {
                        return _csvProcess.Id;
                    }
                    else
                    {
                        // nothing just use a default name
                        return "InputFilter";
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Parse the filter XML
        /// </summary>
        /// <param name="fn">Owning node</param>
        /// <param name="n">XML node</param>
        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process each child node
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // create an and node
                        LogicFilterNode newFn = new AndFilterNode();

                        // add the and node
                        fn.Add(newFn);

                        // parse the and contents
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // create an or node
                        LogicFilterNode newFn = new OrFilterNode();

                        // add the or node
                        fn.Add(newFn);

                        // parse the or contents
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // create a not node
                        LogicFilterNode newFn = new NotFilterNode();

                        // add the not node
                        fn.Add(newFn);

                        // parse the not node
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else if (n2.Name.ToLower() == "id")
                    {
                        _id = n2.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</id>");
                        }
                    }
                    else if (n2.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<TimePerformance/>");
                        }
                    }
                    else
                    {
                        // it must be an evaluation node

                        // create the evaluation node and add it
                        fn.Add(new FilterNode(Condition.Create(n2, j, Processor, tracexml, prefix)));
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Check if we should process this line
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public bool In(ColumnLine l)
        {
            bool result = true; // default to including the line

            // record our start time
            long starttime = 0;
            Performance.QueryPerformanceCounter(ref starttime);

            try
            {
                if (_f != null)
                {
                    // evaluate the filter
                    result = _f.Evaluate(l);
                }

                // track if it was filtered in or out
                if (result)
                {
                    _filteredIn++;
                }
                else
                {
                    _filteredOut++;
                }

                // update the performance counters
                Debug.Assert(Id != string.Empty);
                Performance.SetCounter("Filtered Out %", Id, (int)(_filteredOut / (_filteredIn + _filteredOut)));
                Performance.SetCounter("Filtered In Lines", Id, (int)_filteredIn);

                // if we are timing throughput ... update the counters
                if (_timePerformance)
                {
                    long endtime = 0;
                    Performance.QueryPerformanceCounter(ref endtime);

                    Performance.IncrementBy("Filter Average Duration", Id, endtime - starttime);
                    Performance.Increment("Filter Average Duration Base", Id);
                }
            }
            catch (Exception e)
            {
                throw new ApplicationException("Filter encountered error on Input line " + l.LineNumber.ToString() + " : " + l.Raw, e);
            }

            // return the result
            return result;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a filter
        /// </summary>
        /// <param name="node"></param>
        /// <param name="process"></param>
        public FilterDefinition(XmlNode node, CSVJob j, CSVProcess process, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Filter>");
            }

            // save the owning process
            _csvProcess = process;

            // parse out the filter
            ParseFilter(_f, j, node, tracexml, prefix + "  ");

            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Filter>");
            }

            if (_timePerformance && Id == "")
            {
                throw new ApplicationException("FilterDefinition <TimePerformance\\> requires that you also specify an <Id>.");
            }

            // create the performance counters
            Performance.CreateCounterInstance("CSVProcessor_Filter", "Filtered Out %", Id);
            Performance.CreateCounterInstance("CSVProcessor_Filter", "Filtered In Lines", Id);
            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_Filter", "Filter Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_Filter", "Filter Average Duration Base", Id);
            }
        }

        public FilterDefinition(CSVProcess process)
        {
            // save the owning process
            _csvProcess = process;

            // Create the performance counters
            Performance.CreateCounterInstance("CSVProcessor_Filter", "Filtered Out %", Id);
            Performance.CreateCounterInstance("CSVProcessor_Filter", "Filtered In Lines", Id);
        }
        #endregion
    }
    #endregion
}
