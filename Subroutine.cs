using System;
using System.Xml;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

namespace CSVProcessor
{
	/// <summary>
	/// Class holding all our subroutines
	/// </summary>
	public class Subroutines
	{
		CSVProcess _p = null; // owning processor
		Dictionary<string, Subroutine> _subroutines = new Dictionary<string, Subroutine>(); // the table of subroutine

		virtual public void WriteDot(StreamWriter dotWriter, string prefix)
		{
			foreach (Subroutine s in _subroutines.Values)
			{
				dotWriter.WriteLine(prefix + s.DotName + " [shape=trapezium];");
				dotWriter.WriteLine(prefix + s.DotName + " [label=\"" + s.Name + "\"];");
			}
		}

		#region Accessors

		/// <summary>
		/// Call a subroutine
		/// </summary>
		public ColumnLine Call(string subroutine, ColumnLine l)
		{
			// if we have a subroutine with that name call it
			try
			{
				Subroutine sub = _subroutines[subroutine];
				return sub.Process(l);
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Subroutines.Call:Unknown subroutine : " + subroutine, ex);
			}
		}

		#endregion

		#region Public Functions

		public void SetOutputDefinition(OutputDefinition od)
		{
			foreach (var de in _subroutines)
			{
				de.Value.SetOutputDefinition(od);
			}
		}
		#endregion

		#region Constructors

		/// <summary>
		/// Create a subroutine
		/// </summary>
		/// <param name="p"></param>
		public Subroutines(CSVProcess p)
		{
			// save the owning processor
			_p = p;
		}

		/// <summary>
		/// Create a subroutine
		/// </summary>
		/// <param name="node"></param>
		/// <param name="p"></param>
		public Subroutines(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
		{
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "<Subroutines>");
			}

			// save the owning processor
			_p = p;

			// process each node
			foreach (XmlNode n1 in node.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "subroutine")
					{
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  <Subroutine>");
						}
						try
						{
							// create the subroutine
							Subroutine sub = new Subroutine(n1, j, p, tracexml, prefix + "    ");

							// add it to our list
							_subroutines.Add(sub.Name, sub);
						}
						catch (Exception ex)
						{
							throw new ApplicationException("Error parsing Subroutine", ex);
						}
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "  </Subroutine>");
						}
					}
					else
					{
						CSVProcessor.DisplayError(ANSIHelper.Warning + "Subroutines::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
					}
				}
			}
			if (tracexml)
			{
				CSVProcessor.DisplayError(prefix + "</Subroutines>");
			}
		}
		#endregion
	}

	/// <summary>
	/// A single subroutine.
	/// </summary>
	public class Subroutine
	{
		CSVProcess _p = null; // owning processor
		string _name = string.Empty; // the name of the subroutine
		Column _output = null; // the outputcolumn to call
		bool _trace = false;
		string _id = string.Empty;

		public string DotName
		{
			get
			{
				return "Subroutine_" + Name;
			}
		}

		#region Accessors
		/// <summary>
		/// The Subroutines name
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}
		#endregion

		#region Public Functions

		public void SetOutputDefinition(OutputDefinition od)
		{
			_output.SetOutputDefinition(od);
		}

		protected void PreAccess()
		{
		}

		protected void PostAccess()
		{
		}

		/// <summary>
		/// Call the subroutine
		/// </summary>
		/// <param name="l"></param>
		/// <param name="line"></param>
		/// <param name="s"></param>
		public ColumnLine Process(ColumnLine l)
		{
			ColumnLine cl = new ColumnLine();

			PreAccess();

			// if the have a column definition
			if (_output != null)
			{
				if (_trace)
				{
					CSVProcessor.DisplayError(ANSIHelper.Trace + "Calling subroutine " + _name + "(" + l.ToString() + ")" + ANSIHelper.White);
				}
				// process it to get our new value
				cl = _output.Process(l);
			}

			PostAccess();

			return cl;
		}

		public string Id
		{
			get
			{
				return "{" + _id + "}";
			}
		}

		#endregion

		#region Constructors
		/// <summary>
		/// Create a subroutine
		/// </summary>
		/// <param name="node"></param>
		/// <param name="p"></param>
		public Subroutine(XmlNode node, CSVJob j, CSVProcess p, bool tracexml, string prefix)
		{
			// save the owning processor
			_p = p;

			// process each node
			foreach (XmlNode n1 in node.ChildNodes)
			{
				if (n1.NodeType != XmlNodeType.Comment)
				{
					if (n1.Name.ToLower() == "name")
					{
						// save the name
						_name = n1.InnerText.ToLower();
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Name>" + _name + "</Name>");
						}
					}
					else if (n1.Name.ToLower() == "output")
					{
						foreach (XmlNode n2 in n1.ChildNodes)
						{
							if (n2.NodeType != XmlNodeType.Comment)
							{
								// check this has not already been defined
								if (_output != null)
								{
									throw new ApplicationException("Only one output can be specified in a Subroutine.");
								}
								if (tracexml)
								{
									CSVProcessor.DisplayError(prefix + "<Output>");
								}

								// save the output
								_output = Column.Create(n2, j, p, null, tracexml, prefix + "  ");

								if (tracexml)
								{
									CSVProcessor.DisplayError(prefix + "</Output>");
								}
							}
						}
					}
					else if (n1.Name.ToLower() == "trace")
					{
						_trace = true;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Trace/>");
						}
					}
					else if (n1.Name.ToLower() == "id")
					{
						_id = n1.InnerText;
						if (tracexml)
						{
							CSVProcessor.DisplayError(prefix + "<Id>" + _id + "</Id>");
						}
					}
					else
					{
						CSVProcessor.DisplayError(ANSIHelper.Warning + "Subroutine::Constructor Ignoring xml node : " + n1.Name + ANSIHelper.White);
					}
				}
			}

			if (_output == null)
			{
				throw new ApplicationException("Subroutine::Constructor : No output specified.");
			}
		}
		#endregion
	}
}