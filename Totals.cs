using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;

namespace CSVProcessor
{
    #region Total Accumulator ... holds TotalItems for a given key
    /// <summary>
    /// this contains all the total accumulators for a given instance 
    /// </summary>
    public class TotalAccum
    {
        #region Member Variables
        string _key = string.Empty; // the key the totals are for
        List<TotalItem> _items = new List<TotalItem>(); // to total items for this key
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">The key for this accumulator</param>
        /// <param name="cols">The number of totals</param>
        public TotalAccum(string key, int cols)
        {
            // save the key
            _key = key;

            // create null accumulators
            for (int i = 0; i < cols; i++)
            {
                _items.Add(null);
            }
        }
        #endregion

        #region Accessors

        /// <summary>
        /// Get this rows key
        /// </summary>
        public string Key
        {
            get
            {
                return _key;
            }
        }

        /// <summary>
        /// Get/Set a total item
        /// </summary>
        public TotalItem this[int index]
        {
            get
            {
                return (TotalItem)_items[index];
            }
            set
            {
                _items[index] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Add a total item
        /// </summary>
        /// <param name="ti"></param>
        public void Add(TotalItem ti)
        {
            _items.Add(ti);
        }
        #endregion
    }
    #endregion

    #region TotalItems ... represents the values for a total column for a key

    #region Base Class

    /// <summary>
    /// The base total item
    /// </summary>
    public class TotalItem
    {
        #region Constructor
        public TotalItem()
        {
        }
        #endregion
    }
    #endregion

    #region Derived Classes
    /// <summary>
    /// A total item with just an integer
    /// </summary>
    public class TotalItem_Int64 : TotalItem
    {
        Int64 _int64 = 0; // the integer

        #region Constructors
        public TotalItem_Int64(int initialvalue)
            : base()
        {
            _int64 = initialvalue;
        }
        public TotalItem_Int64(string s)
            : base()
        {
            try
            {
                _int64 = Convert.ToInt64(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to an integer.", ex);
            }
        }
        public TotalItem_Int64()
            : base()
        {
        }
        #endregion

        #region Public Methods
        public Int64 Add(Int64 i)
        {
            _int64 += i;
            return _int64;
        }
        public Int64 Add(string s)
        {
            try
            {
                _int64 += Convert.ToInt64(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to an integer.", ex);
            }

            return _int64;
        }
        #endregion

        #region Accessors for the values
        public Int64 GetSet
        {
            get
            {
                return _int64;
            }
            set
            {
                _int64 = value;
            }
        }
        #endregion
    }

    /// <summary>
    /// A total item with a collection of counts
    /// </summary>
    public class TotalItem_ItemCount : TotalItem
    {
        #region Member Variables
        SortedList od = null; // our list of values and their counts
        int total = 0; // the total number of values counted
        #endregion

        #region Constructors
        public TotalItem_ItemCount(object o, int initialvalue)
            : base()
        {
            od = new SortedList();
            od[o] = initialvalue;
            total = initialvalue;
        }
        public TotalItem_ItemCount()
            : base()
        {
        }
        #endregion

        #region Public Methods
        public void AddCount(object o, int count)
        {
            try
            {
                od[o] = ((int)od[o]) + count;
            }
            catch
            {
                od[o] = count;
            }
            total += count;
        }
        #endregion

        #region Accessors for the values
        public SortedList Items
        {
            get
            {
                return od;
            }
        }
        public int Total
        {
            get
            {
                return total;
            }
        }
        #endregion
    }

    /// <summary>
    /// A total item with just a double
    /// </summary>
    public class TotalItem_Double : TotalItem
    {
        #region Member Variables
        double _double = 0; // the double accumulator
        #endregion

        #region Constructors
        public TotalItem_Double(double initialvalue)
            : base()
        {
            _double = initialvalue;
        }
        public TotalItem_Double(string s)
            : base()
        {
            try
            {
                _double = Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }
        }
        public TotalItem_Double()
            : base()
        {
        }
        #endregion

        #region Public Methods
        public double Add(double d)
        {
            _double += d;
            return _double;
        }
        public double Add(string s)
        {
            try
            {
                _double += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }
            return _double;
        }
        #endregion

        #region Accessors
        public double GetSet
        {
            get
            {
                return _double;
            }
            set
            {
                _double = value;
            }
        }
        #endregion
    }

    /// <summary>
    /// A total item with lots of accumulators
    /// </summary>
    public class TotalItem_FiveDouble_Int64 : TotalItem
    {
        #region Member Variables
        double _double1 = 0;
        double _double2 = 0;
        double _double3 = 0;
        double _double4 = 0;
        double _double5 = 0;
        Int64 _int64 = 0;
        #endregion

        #region Constructors

        public TotalItem_FiveDouble_Int64(double initialvalue_d1, Int64 initialvalue_i)
            : base()
        {
            _double1 = initialvalue_d1;
            _int64 = initialvalue_i;
        }

        public TotalItem_FiveDouble_Int64(double initialvalue_d1, double initialvalue_d2, Int64 initialvalue_i)
            : base()
        {
            _double1 = initialvalue_d1;
            _double2 = initialvalue_d2;
            _int64 = initialvalue_i;
        }

        public TotalItem_FiveDouble_Int64(double initialvalue_d1, double initialvalue_d2, double initialvalue_d3, double initialvalue_d4, double initialvalue_d5, Int64 initialvalue_i)
            : base()
        {
            _double1 = initialvalue_d1;
            _double2 = initialvalue_d2;
            _double3 = initialvalue_d3;
            _double4 = initialvalue_d4;
            _double5 = initialvalue_d5;
            _int64 = initialvalue_i;
        }

        public TotalItem_FiveDouble_Int64()
            : base()
        {
        }

        #endregion

        #region Public Methods

        public double AddDouble1(double d)
        {
            _double1 += d;
            return _double1;
        }
        public double AddDouble2(double d)
        {
            _double2 += d;
            return _double2;
        }
        public double AddDouble3(double d)
        {
            _double3 += d;
            return _double3;
        }
        public double AddDouble4(double d)
        {
            _double4 += d;
            return _double4;
        }
        public double AddDouble5(double d)
        {
            _double5 += d;
            return _double5;
        }
        public Int64 AddInt64(Int64 i)
        {
            _int64 += i;
            return _int64;
        }
        public double AddDouble1(string s)
        {
            try
            {
                _double1 += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }
            return _double1;
        }
        public double AddDouble2(string s)
        {
            try
            {
                _double2 += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }

            return _double2;
        }
        public double AddDouble3(string s)
        {
            try
            {
                _double3 += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }
            return _double3;
        }
        public double AddDouble4(string s)
        {
            try
            {
                _double4 += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }

            return _double4;
        }
        public double AddDouble5(string s)
        {
            try
            {
                _double5 += Convert.ToDouble(s);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + s + "' to a double.", ex);
            }
            return _double5;
        }
        #endregion

        #region Accessors
        public double GetSetDouble1
        {
            get
            {
                return _double1;
            }
            set
            {
                _double1 = value;
            }
        }
        public double GetSetDouble2
        {
            get
            {
                return _double2;
            }
            set
            {
                _double2 = value;
            }
        }
        public double GetSetDouble3
        {
            get
            {
                return _double3;
            }
            set
            {
                _double3 = value;
            }
        }
        public double GetSetDouble4
        {
            get
            {
                return _double4;
            }
            set
            {
                _double4 = value;
            }
        }
        public double GetSetDouble5
        {
            get
            {
                return _double5;
            }
            set
            {
                _double5 = value;
            }
        }
        public Int64 GetSetInt64
        {
            get
            {
                return _int64;
            }
            set
            {
                _int64 = value;
            }
        }
        #endregion
    }

    /// <summary>
    /// A date accumulator
    /// </summary>
    public class TotalItem_DateTime : TotalItem
    {
        #region Member Variables
        DateTime _datetime = DateTime.MinValue; // the date
        #endregion

        #region Constructors
        public TotalItem_DateTime(DateTime initialvalue)
            : base()
        {
            _datetime = initialvalue;
        }
        public TotalItem_DateTime(string s, string informat)
            : base()
        {
            _datetime = DateTime.ParseExact(s, informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
        }
        public TotalItem_DateTime()
            : base()
        {
        }
        #endregion

        #region Accessors
        public DateTime GetSet
        {
            get
            {
                return _datetime;
            }
            set
            {
                _datetime = value;
            }
        }
        #endregion
    }

    /// <summary>
    /// A total item with a string accumulator
    /// </summary>
    public class TotalItem_String : TotalItem
    {
        #region Member Variables
        string _string = string.Empty;
        #endregion

        #region Constructors
        public TotalItem_String(string s)
            : base()
        {
            _string = s;
        }
        public TotalItem_String()
            : base()
        {
        }
        #endregion

        #region Accessors
        public string GetSet
        {
            get
            {
                return _string;
            }
            set
            {
                _string = value;
            }
        }
        #endregion
    }
    public class TotalItem_StringInt : TotalItem
    {
        #region Member Variables
        string _string = string.Empty;
        int _int = 0;
        #endregion

        #region Constructors
        public TotalItem_StringInt(string s)
            : base()
        {
            _string = s;
        }
        public TotalItem_StringInt(string s, int i)
            : base()
        {
            _string = s;
            _int = i;
        }
        public TotalItem_StringInt()
            : base()
        {
        }
        #endregion

        #region Accessors
        public string GetSetString
        {
            get
            {
                return _string;
            }
            set
            {
                _string = value;
            }
        }
        public int GetSetInt
        {
            get
            {
                return _int;
            }
            set
            {
                _int = value;
            }
        }
        #endregion
    }

    #endregion

    #endregion

    #region Total Classes ... represent a total output column

    #region Base Class
    /// <summary>
    /// A base for all total types
    /// </summary>
    public class Total
    {
        #region Member Variables
        protected Totals _totals = null; // hold a pointer to the owning totals object
        protected bool _safe = false; // true if we dont have ,
        protected bool _trace = false; // true if we are to trace processing
        protected string _id = string.Empty; // our total id
        bool _timePerformance = false; // true if we are to time performance
        long starttime = 0; // the time we started this operation
        string _title = string.Empty; // the total column title
        LogicFilterNode _condition = new AndFilterNode(); // holds the default AND filter
        int _unique = UniqueIdClass.Allocate();
        protected string _name = string.Empty;
        protected CSVProcess _p = null;

        #endregion

        public string UniqueId
        {
            get
            {
                return "T" + _unique.ToString();
            }
        }

        virtual public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + UniqueId + " [label=\"" + _name + "::" + _title + "::" + Id + "\"];");
        }

        #region Accessors
        /// <summary>
        /// The total column title
        /// </summary>
        public string Title
        {
            get
            {
                return _title;
            }
        }
        /// <summary>
        /// The Id of the total
        /// </summary>
        public string Id
        {
            get
            {
                if (_id != string.Empty)
                {
                    return "{" + _id + "}";
                }
                else
                {
                    return "{" + Title + "}";
                }
            }
        }
        /// <summary>
        /// Indicates if the column should not have quotes placed around it even if it contains a column separator
        /// </summary>
        public bool Safe
        {
            get
            {
                return _safe;
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Create a total
        /// </summary>
        /// <param name="node"></param>
        /// <param name="totals"></param>
        /// <param name="tracexml"></param>
        /// <param name="prefix"></param>
        /// <param name="ignore"></param>
        public Total(XmlNode node, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix, string[] ignore)
        {
            // save the owning totals object
            _totals = totals;
            _p = p;

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    if (n.Name.ToLower() == "safe")
                    {
                        _safe = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Safe/>");
                        }

                    }
                    else if (n.Name.ToLower() == "id")
                    {
                        _id = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</Id>");
                        }

                    }
                    else if (n.Name.ToLower() == "condition")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Condition>");
                        }
                        // parse the if condition
                        ParseFilter(_condition, j, n, tracexml, prefix + "    ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Condition>");
                        }
                    }
                    else if (n.Name.ToLower() == "title")
                    {
                        _title = n.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Title>" + _title + "</Title>");
                        }
                    }
                    else if (n.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TimePerformance/>");
                        }
                    }
                    else if (n.Name.ToLower() == "trace")
                    {
                        _trace = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Trace/>");
                        }
                    }
                    else
                    {
                        bool found = false;
                        foreach (string s in ignore)
                        {
                            if (s.ToLower() == n.Name.ToLower() || s == "*")
                            {
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Warning + "Total::Constructor Ignoring xml node : " + n.Name + ANSIHelper.White);
                        }
                    }
                }
            }

            if (_timePerformance && _id == "")
            {
                throw new ApplicationException("Total::Constructor <TimePerformance> requires <Id> to also be specified.");
            }

            // Create performance counters
            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_TotalAccumulator", "Total Accumulator Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_TotalAccumulator", "Total Accumulator Average Duration Base", Id);
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Parse the if condition
        /// </summary>
        /// <param name="fn"></param>
        /// <param name="n"></param>
        void ParseFilter(LogicFilterNode fn, CSVJob j, XmlNode n, bool tracexml, string prefix)
        {
            // process the xml
            foreach (XmlNode n2 in n.ChildNodes)
            {
                if (n2.NodeType != XmlNodeType.Comment)
                {
                    // if it is an and
                    if (n2.Name.ToLower() == "and")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<And>");
                        }
                        // add an and node
                        LogicFilterNode newFn = new AndFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");

                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</And>");
                        }
                    }
                    // if it is an or
                    else if (n2.Name.ToLower() == "or")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Or>");
                        }
                        // add an or node
                        LogicFilterNode newFn = new OrFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Or>");
                        }
                    }
                    // if it is a not
                    else if (n2.Name.ToLower() == "not")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "<Not>");
                        }
                        // add a not node
                        LogicFilterNode newFn = new NotFilterNode();
                        fn.Add(newFn);

                        // recurse
                        ParseFilter(newFn, j, n2, tracexml, prefix + "  ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "</Not>");
                        }
                    }
                    else
                    {
                        // must be a real filter function so create it
                        fn.Add(new FilterNode(Condition.Create(n2, j, _totals.Processor, tracexml, prefix + "  ")));
                    }
                }
            }
        }

        /// <summary>
        /// Pre calculating a total
        /// </summary>
        protected void PreProcess()
        {
            // save the start time
            Performance.QueryPerformanceCounter(ref starttime);
        }

        /// <summary>
        /// Post calculating a total
        /// </summary>
        protected void PostProcess()
        {
            if (_timePerformance)
            {
                // get the end time
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                // update the performance counters
                Performance.IncrementBy("Total Accumulator Average Duration", Id, endtime - starttime);
                Performance.Increment("Total Accumulator Average Duration Base", Id);
            }
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Check if this total should be processed for this input line
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public bool True(ColumnLine line)
        {
            return _condition.Evaluate(line);
        }

        /// <summary>
        /// This is the function called to produce the total aggregation
        /// In this base class it does nothing and it should never be reached.
        /// </summary>
        /// <param name="line">The CSV input line</param>
        public virtual TotalItem Process(ColumnLine line, TotalItem item, string key)
        {
            throw new ApplicationException("Derived classes from Total must provide their own Process() implementation.");
        }

        /// <summary>
        /// This is the function used to return the result for a given key
        /// In this base class it should never be called
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual string Result(/*string rowkey, string colkey*/ TotalItem ti, TotalAccum ta)
        {
            throw new ApplicationException("Derived classes from Total must provide their own Result() implementation.");
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// Get a list of supported Totals
        /// </summary>
        /// <returns></returns>
        public static List<string> Supports()
        {
            List<string> s = new List<string>();

            Type[] types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();

            foreach (Type t in types)
            {
                if ((t.BaseType.Name == "Total" || t.BaseType.Name == "TotalWithColumn") && t.Name != "TotalWithColumn")
                {
                    s.Add("Total::" + t.Name);
                }
            }

            return s;
        }

        /// <summary>
        /// This is the coolest function. It extracts from an xml node the name of a class derived from Column
        /// and creates an instance of it. If no node mentioning a class is found in the xml file then no
        /// instance of the class will be created.
        /// </summary>
        /// <param name="n">The node containing the name of the class to create</param>
        /// <returns></returns>
        public static Total Create(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<" + n.Name + ">");
            }

            // work out the fully classified class name
            string classname = "CSVProcessor." + n.Name.ToLower();

            try
            {
                // get the type we want to create. If the class does not exist in this assembly then this will fail
                Type t = Type.GetType(classname);

                if (t == null)
                {
                    throw new ApplicationException("Total::Create - " + n.Name + " is not a valid total type.");
                }

                // now construct the object passing in our xml node
                Total o = (Total)Activator.CreateInstance(t, new object[] { n, j, p, totals, tracexml, prefix + "  " });

                if (tracexml)
                {
                    CSVProcessor.DisplayError(prefix + "</" + n.Name + ">");
                }

                // return the object we created
                return o;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Total::Create - Attempt to create object from class " + classname + " failed. Please check the documentation to ensure it is spelled correctly", ex);
            }
        }
        #endregion
    }

    /// <summary>
    /// A base for totals that read from 1 column
    /// </summary>
    public class TotalWithColumn : Total
    {
        ColumnNumberOrColumn _srccolumn = null; // the column we are reading from

        #region Protected Functions
        protected string ColumnValue(ColumnLine l)
        {
            return _srccolumn.Value(l);
        }
        #endregion

        #region Constructors
        public TotalWithColumn(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix, string[] ignore)
            : base(n, j, p, totals, tracexml, prefix, ignore)
        {
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "column")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column>");
                        }
                        _srccolumn = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column>");
                        }
                    }
                }
            }

            if (_srccolumn == null)
            {
                throw new ApplicationException("<Column> must be specified.");
            }
        }
        #endregion
    }
    #endregion

    #region Derived Classes

    /// <summary>
    /// Sum up a column using integers
    /// </summary>
    public class sumint : TotalWithColumn
    {
        #region Constructors
        public sumint(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "SumInt";
        }
        #endregion

        #region Override functions
        /// <summary>
        /// Process an input line
        /// </summary>
        /// <param name="line"></param>
        /// <param name="ti"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();

            // get the totals for this key
            TotalItem_Int64 TI = ti as TotalItem_Int64;

            // get the column value
            string cv = ColumnValue(line);

            // create a new node or add it
            if (TI == null)
            {
                TI = new TotalItem_Int64(cv);
            }
            else
            {
                TI.Add(cv);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "SumInt:" + Id + " [" + key + "] adding '" + cv + "' --> '" + TI.GetSet + "'" + ANSIHelper.White);
            }

            PostProcess();

            return TI;
        }

        /// <summary>
        /// Grab the result
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Int64 TI = ti as TotalItem_Int64;
            return TI.GetSet.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Sum up a column using integers
    /// </summary>
    public class nthvalue : TotalWithColumn
    {
        #region Member Variables
        int _n = -1;
        #endregion

        #region Constructors
        public nthvalue(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "n" })
        {
            _name = "NthValue";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "n")
                    {
                        _n = Int32.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <n>" + _n.ToString() + "</n>");
                        }
                    }
                }
            }

            if (_n < 1)
            {
                throw new ApplicationException("NthValue - no <n> specified greater than or equal to 1.");
            }
        }
        #endregion

        #region Override functions
        /// <summary>
        /// Process an input line
        /// </summary>
        /// <param name="line"></param>
        /// <param name="ti"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();

            // get the totals for this key
            TotalItem_StringInt TI = ti as TotalItem_StringInt;

            // get the column value
            string cv = ColumnValue(line);

            if (TI == null)
            {
                TI = new TotalItem_StringInt();
            }

            TI.GetSetInt++;

            if (TI.GetSetInt == _n)
            {
                TI.GetSetString = cv;
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "NthValue:" + Id + " [" + key + "] value '" + cv + "'" + ANSIHelper.White);
                }
            }

            PostProcess();

            return TI;
        }

        /// <summary>
        /// Grab the result
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_StringInt TI = ti as TotalItem_StringInt;
            return TI.GetSetString;
        }
        #endregion
    }

    /// <summary>
    /// Maximum value in a column
    /// </summary>
    public class max : TotalWithColumn
    {
        #region Constructors
        public max(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "Max";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();

            TotalItem_Double TI = ti as TotalItem_Double;
            string cv = ColumnValue(line);
            double d = 0;
            try
            {
                d = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }

            if (TI == null)
            {
                TI = new TotalItem_Double(d);
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Max:" + Id + " [" + key + "] new max '" + cv + "'" + ANSIHelper.White);
                }
            }
            else
            {
                if (d > TI.GetSet)
                {
                    TI.GetSet = d;
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Max:" + Id + " [" + key + "] new max '" + cv + "'" + ANSIHelper.White);
                    }
                }
            }
            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Double TI = ti as TotalItem_Double;
            return TI.GetSet.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Find the maximum date/time in a column
    /// </summary>
    public class maxdate : TotalWithColumn
    {
        #region Member Variables
        string _informat = string.Empty;
        string _outformat = string.Empty;
        #endregion

        #region Constructors
        public maxdate(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "informat", "outformat" })
        {
            _name = "MaxDate";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</InFormat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "outformat")
                    {
                        _outformat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outformat + "</OutFormat>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();

            TotalItem_DateTime TI = ti as TotalItem_DateTime;
            string cv = ColumnValue(line);

            try
            {
                DateTime dtCurrent = DateTime.ParseExact(cv, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);

                if (TI == null)
                {
                    TI = new TotalItem_DateTime(dtCurrent);
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "MaxDate:" + Id + " [" + key + "] new max '" + cv + "'" + ANSIHelper.White);
                    }
                }
                else
                {
                    if (dtCurrent > TI.GetSet)
                    {
                        TI.GetSet = dtCurrent;
                        if (_trace)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Trace + "MaxDate:" + Id + " [" + key + "] new max '" + cv + "'" + ANSIHelper.White);
                        }
                    }
                }
            }
            catch
            {
                // ignore if not a date
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_DateTime TI = ti as TotalItem_DateTime;
            return TI.GetSet.ToString(_outformat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
        }
        #endregion
    }

    /// <summary>
    /// Find the minimum date/time in a column
    /// </summary>
    public class mindate : TotalWithColumn
    {
        #region Member Variables
        string _informat = string.Empty;
        string _outformat = string.Empty;
        #endregion

        #region Constructors
        public mindate(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "informat", "outformat" })
        {
            _name = "MinDate";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</InFormat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "outformat")
                    {
                        _outformat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <OutFormat>" + _outformat + "</OutFormat>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_DateTime TI = ti as TotalItem_DateTime;
            string cv = ColumnValue(line);

            try
            {
                DateTime dtCurrent = DateTime.ParseExact(cv, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);

                if (TI == null)
                {
                    TI = new TotalItem_DateTime(dtCurrent);
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "MinDate:" + Id + " [" + key + "] new min '" + cv + "'" + ANSIHelper.White);
                    }
                }
                else
                {
                    if (dtCurrent < TI.GetSet)
                    {
                        TI.GetSet = dtCurrent;
                        if (_trace)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Trace + "MinDate:" + Id + " [" + key + "] new min '" + cv + "'" + ANSIHelper.White);
                        }
                    }
                }
            }
            catch
            {
                // ignore if not a date
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_DateTime TI = ti as TotalItem_DateTime;
            return TI.GetSet.ToString(_outformat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
        }
        #endregion
    }

    /// <summary>
    /// Get the first non blank value in the column
    /// </summary>
    public class firstnonblank : TotalWithColumn
    {
        #region Constructor
        public firstnonblank(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "FirstNonBlank";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_String TI = ti as TotalItem_String;
            string cv = ColumnValue(line);

            if (cv.Trim() != string.Empty)
            {
                if (TI == null)
                {
                    TI = new TotalItem_String(cv);
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "FirstNonBlank:" + Id + " [" + key + "] value '" + cv + "'" + ANSIHelper.White);
                    }
                }
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_String TI = ti as TotalItem_String;
            return TI.GetSet;
        }
        #endregion
    }

    /// <summary>
    /// Get the first value in the column
    /// </summary>
    public class firstvalue : TotalWithColumn
    {
        #region Constructor
        public firstvalue(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "FirstValue";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_String TI = ti as TotalItem_String;
            string cv = ColumnValue(line);

            if (TI == null)
            {
                TI = new TotalItem_String(cv);
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "FirstValue:" + Id + " [" + key + "] value '" + cv + "'" + ANSIHelper.White);
                }
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_String TI = ti as TotalItem_String;
            return TI.GetSet;
        }
        #endregion
    }

    public class collect : TotalWithColumn
    {
        string _delimiter = "|";
        int _mincollected = -1;
        int _maxcollected = -1;
        int _currentmax = -1;
        bool _padtolargest = false;
        bool _unique = false;
        bool _suppressblanks = false;

        #region Constructor
        public collect(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "suppressblanks", "delimiter", "minimumtocollect", "maximumtocollect", "padtolargest", "unique" })
        {
            _name = "Collect";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "delimiter")
                    {
                        switch (n1.InnerText.Trim())
                        {
                            case "\\t":
                            case "\\T":
                                _delimiter = "\t";
                                break;
                            default:
                                _delimiter = n1.InnerText;
                                break;
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Delimiter>" + _delimiter + "</Delimiter>");
                        }
                    }
                    else if (n1.Name.ToLower() == "minimumtocollect")
                    {
                        try
                        {
                            _mincollected = Int32.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MinimumToCollect>" + _mincollected.ToString() + "</MinimumToCollect>");
                            }
                            if (_mincollected < 1)
                            {
                                throw new ApplicationException("Collect::Construct <MinimumToCollect> value less than 0 makes no sense: " + _mincollected);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Collect::Constructor Error parsing MinimumToCollect : " + n1.InnerText, ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "maximumtocollect")
                    {
                        try
                        {
                            _maxcollected = Int32.Parse(n1.InnerText);
                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <MaximumToCollect>" + _maxcollected.ToString() + "</MaximumToCollect>");
                            }
                            if (_maxcollected < 1)
                            {
                                throw new ApplicationException("Collect::Construct <MaximumToCollect> value less than 1 makes no sense: " + _maxcollected);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Collect::Constructor Error parsing MinimumToCollect : " + n1.InnerText, ex);
                        }
                    }
                    else if (n1.Name.ToLower() == "padtolargest")
                    {
                        _padtolargest = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <PadToLargest/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "unique")
                    {
                        _unique = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Unique/>");
                        }
                    }
                    else if (n1.Name.ToLower() == "suppressblanks")
                    {
                        _suppressblanks = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <SuppressBlanks/>");
                        }
                    }
                }
            }

            if (_padtolargest && (_maxcollected != -1 || _mincollected != -1))
            {
                throw new ApplicationException("Collect::Constructor PadToLargest cannot be specified with MinimumToCollect or MaximumToCollect.");
            }
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_StringInt TI = ti as TotalItem_StringInt;
            string cv = ColumnValue(line);

            if (TI == null)
            {
                TI = new TotalItem_StringInt(cv);
                if (_currentmax < 1)
                {
                    _currentmax = 1;
                }
            }
            else
            {
                if (_suppressblanks && cv == string.Empty)
                {
                    // dont collect
                }
                else
                {
                    if (_maxcollected < 0 || TI.GetSetInt < _maxcollected - 1)
                    {
                        if (!_unique || (TI.GetSetString != cv && !TI.GetSetString.StartsWith(cv + _delimiter) && !TI.GetSetString.EndsWith(_delimiter + cv) && !TI.GetSetString.Contains(_delimiter + cv + _delimiter)))
                        {
                            TI.GetSetString = TI.GetSetString + _delimiter + cv;
                            TI.GetSetInt++;
                            if (TI.GetSetInt > _currentmax)
                            {
                                _currentmax = TI.GetSetInt;
                            }
                        }
                    }
                }
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_StringInt TI = ti as TotalItem_StringInt;

            string rc = TI.GetSetString;


            if (_padtolargest)
            {
                for (int i = TI.GetSetInt; i < _currentmax; i++)
                {
                    rc += _delimiter;
                }
            }
            else
            {
                // pad out any missing items
                for (int i = TI.GetSetInt; i < _mincollected - 1; i++)
                {
                    rc += _delimiter;
                }
            }

            return rc;
        }
        #endregion
    }

    /// <summary>
    /// Get the last non blank value in the column
    /// </summary>
    public class lastnonblank : TotalWithColumn
    {
        #region Constructors
        public lastnonblank(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "LastNonBlank";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_String TI = ti as TotalItem_String;
            string cv = ColumnValue(line);

            if (cv.Trim() != string.Empty)
            {
                if (TI == null)
                {
                    TI = new TotalItem_String(cv);
                }
                else
                {
                    TI.GetSet = cv;
                }

                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "LastNonBlank:" + Id + " [" + key + "] new value '" + cv + "'" + ANSIHelper.White);
                }
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_String TI = ti as TotalItem_String;
            return TI.GetSet;
        }
        #endregion
    }

    /// <summary>
    /// Get the last value in a column
    /// </summary>
    public class lastvalue : TotalWithColumn
    {
        #region Constructors
        public lastvalue(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "LastValue";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_String TI = ti as TotalItem_String;
            string cv = ColumnValue(line);

            if (TI == null)
            {
                TI = new TotalItem_String(cv);
            }
            else
            {
                TI.GetSet = cv;
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "LastValue:" + Id + " [" + key + "] new value '" + cv + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_String TI = ti as TotalItem_String;
            return TI.GetSet;
        }
        #endregion
    }

    /// <summary>
    /// Get the minimum value in a column
    /// </summary>
    public class min : TotalWithColumn
    {
        #region Constructor
        public min(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "Min";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_Double TI = ti as TotalItem_Double;
            string cv = ColumnValue(line);
            double d = 0;
            try
            {
                d = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }

            if (TI == null)
            {
                TI = new TotalItem_Double(d);
                if (_trace)
                {
                    CSVProcessor.DisplayError(ANSIHelper.Trace + "Min:" + Id + " [" + key + "] new min '" + cv + "'" + ANSIHelper.White);
                }
            }
            else
            {
                if (d < TI.GetSet)
                {
                    TI.GetSet = d;
                    if (_trace)
                    {
                        CSVProcessor.DisplayError(ANSIHelper.Trace + "Min:" + Id + " [" + key + "] new min '" + cv + "'" + ANSIHelper.White);
                    }
                }
            }
            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Double TI = ti as TotalItem_Double;
            return TI.GetSet.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Count the number of entries
    /// </summary>
    public class count : Total
    {
        #region Constructors
        public count(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { })
        {
            _name = "Count";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_Int64 TI = ti as TotalItem_Int64;

            if (TI == null)
            {
                TI = new TotalItem_Int64(1);
            }
            else
            {
                TI.Add(1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Count:" + Id + " [" + key + "] new count '" + TI.GetSet.ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Int64 TI = ti as TotalItem_Int64;
            return TI.GetSet.ToString();
        }
        #endregion
    }

    public class formula : Total
    {
        double _initialValue = 0;
        bool _float = false;
        string _formula;
        System.Collections.ArrayList _formulaDecoded = null;

        #region Constructors
        public formula(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "formula", "float", "initialvalue" })
        {
            _name = "Formula";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "formula")
                    {
                        _formula = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Formula>" + _formula + "</Formula>");
                        }
                        _formula = _formula.Replace("[-1]", "!xyzzy!");
                        _formulaDecoded = arithmatic.DecodeFormula(_formula, j, p);
                    }
                    else if (n1.Name.ToLower() == "initialvalue")
                    {
                        _initialValue = Double.Parse(n1.InnerText);
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InitialValue>" + _initialValue.ToString() + "</InitialValue>");
                        }
                    }
                    else if (n1.Name.ToLower() == "float")
                    {
                        _float = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Float/>");
                        }
                    }
                }
            }

            if (_formula == "")
            {
                throw new ApplicationException("Totals Formula not specified.");
            }
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();

            TotalItem_Double TI = ti as TotalItem_Double;

            if (TI == null)
            {
                TI = new TotalItem_Double(_initialValue);
            }

            string curValue;
            if (_float)
                curValue = TI.GetSet.ToString();
            else
                curValue = ((int)TI.GetSet).ToString();

            string s = arithmatic.Substitute(_formulaDecoded, line, _p, false);
            s = s.Replace("!xyzzy!", curValue);

            if (_float)
                TI.GetSet = arithmatic.CalcFloat(s, arithmatic.round.round_none);
            else
                TI.GetSet = arithmatic.CalcInt(s, arithmatic.round.round_none);

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Formula:" + Id + " [" + key + "] new value '" + TI.GetSet.ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Double TI = ti as TotalItem_Double;

            if (_float)
                return TI.GetSet.ToString();
            else
                return ((int)TI.GetSet).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Work out the average of a column
    /// </summary>
    public class average : TotalWithColumn
    {
        #region Constructors
        public average(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "Average";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;

            if (TI == null)
            {
                double d = 0;
                try
                {
                    d = Convert.ToDouble(cv);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
                }

                TI = new TotalItem_FiveDouble_Int64(d, 1);
            }
            else
            {
                TI.AddDouble1(cv);
                TI.AddInt64(1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Average:" + Id + " [" + key + "] new average '" + (TI.GetSetDouble1 / TI.GetSetInt64).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            return (TI.GetSetDouble1 / TI.GetSetInt64).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Work out a sum and then turns it into a percentage against another sum total
    /// </summary>
    public class percentofrowtotal : Total
    {
        private string _thecolumnname = "";
        private string _ofcolumnname = "";
        private int _thecolumnnumber = -1;
        private int _ofcolumnnumber = -1;

        #region Constructors
        public percentofrowtotal(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "ofcolumn" })
        {
            _name = "PercentOfTowTotal";

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "column")
                    {
                        if (n1.InnerText.ToLower().Trim().StartsWith("totalname:"))
                        {
                            _thecolumnname = n1.InnerText.ToLower().Trim().Substring(10);
                        }
                        else
                        {
                            _thecolumnnumber = Convert.ToInt16(n1.InnerText.Trim());
                        }
                        if (tracexml)
                        {
                            if (_thecolumnname != "")
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>" + _thecolumnname + "</Column>");
                            }
                            else
                            {
                                CSVProcessor.DisplayError(prefix + "  <Column>" + _thecolumnnumber.ToString() +
                                                          "</Column>");
                            }
                        }
                    }
                    else if (n1.Name.ToLower() == "ofcolumn")
                    {
                        if (n1.InnerText.ToLower().Trim().StartsWith("totalname:"))
                        {
                            _ofcolumnname = n1.InnerText.ToLower().Trim().Substring(10);
                        }
                        else
                        {
                            _ofcolumnnumber = Convert.ToInt16(n1.InnerText.Trim());
                        }
                        if (tracexml)
                        {
                            if (_ofcolumnname != "")
                            {
                                CSVProcessor.DisplayError(prefix + "  <OfColumn>" + _ofcolumnname + "</OfColumn>");
                            }
                            else
                            {
                                CSVProcessor.DisplayError(prefix + "  <OfColumn>" + _ofcolumnnumber.ToString() +
                                                          "</OfColumn>");
                            }
                        }
                    }
                }
            }

            if (_thecolumnname == "" && _thecolumnnumber == -1)
            {
                throw new ApplicationException("PercentOfRowTotal::Constructor Column not specified.");
            }
            if (_ofcolumnname == "" && _ofcolumnnumber == -1)
            {
                throw new ApplicationException("PercentOfRowTotal::Constructor OfColumn not specified.");
            }
        }
        #endregion

        private Total OfColumn(ref int num)
        {
            if (_ofcolumnname != "")
            {
                int i = 0;
                foreach (Total t in _totals.TheTotals)
                {
                    if (t.Title.ToLower() == _ofcolumnname.ToLower().Trim())
                    {
                        num = i;
                        CSVProcessor.DisplayError(ANSIHelper.Information + "TotalColumn Number '" + _ofcolumnname + "' assigned to column " + num.ToString() + ".\n" + ANSIHelper.Normal);
                        return t;
                    }
                    i++;
                }
            }
            else
            {
                num = _ofcolumnnumber;
                return _totals.TheTotals[_ofcolumnnumber];
            }

            return null;
        }

        private Total TheColumn(ref int num)
        {
            if (_thecolumnname != "")
            {
                int i = 0;
                foreach (Total t in _totals.TheTotals)
                {
                    if (t.Title.ToLower() == _thecolumnname.ToLower().Trim())
                    {
                        num = i;
                        CSVProcessor.DisplayError(ANSIHelper.Information + "TotalColumn Number '" + _thecolumnname + "' assigned to column " + num.ToString() + ".\n" + ANSIHelper.Normal);
                        return t;
                    }
                    i++;
                }
            }
            else
            {
                num = _thecolumnnumber;
                return _totals.TheTotals[_thecolumnnumber];
            }

            return null;
        }

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            // I dont need to do anything
            PreProcess();
            TotalItem_Double TI = ti as TotalItem_Double;
            if (TI == null)
            {
                TI = new TotalItem_Double(0);
            }
            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            int thecn = -1;
            Total the = TheColumn(ref thecn);

            int ofcn = -1;
            Total of = OfColumn(ref ofcn);

            if (of == null)
            {
                return "OfColumn " + _ofcolumnname + " was not found.";
            }
            else if (the == null)
            {
                return "Column " + _thecolumnname + " was not found.";
            }
            else
            {
                return (Convert.ToDouble(the.Result(ta[thecn], ta)) * 100.0 / Convert.ToDouble(of.Result(ta[ofcn], ta))).ToString("0.000");
            }
        }
        #endregion
    }

    /// <summary>
    /// Work out the median of a column
    /// </summary>
    public class median : TotalWithColumn
    {
        #region Member Variables
        string _type = string.Empty; // the type of variable we are calculating the median of
        string _informat = string.Empty; // if it is a date then the input date format
        #endregion

        #region Constructors
        public median(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "type", "informat" })
        {
            _name = "Median";
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "type")
                    {
                        _type = n1.InnerText.ToLower();

                        switch (_type)
                        {
                            case "int":
                            case "string":
                            case "datetime":
                            case "float":
                                break;
                            default:
                                throw new ApplicationException("Median::Constructor unsupported type " + n1.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Type>" + _type + "</Type>");
                        }
                    }
                    else if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</Informat>");
                        }
                    }

                    if (_type == "datetime" && _informat == string.Empty)
                    {
                        throw new ApplicationException("Median::Constructor DateTime requires an InFormat.");
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        object Median(TotalItem_ItemCount TI)
        {
            int sum = 0;
            int total = TI.Total;

            foreach (DictionaryEntry de in TI.Items)
            {
                sum += (int)de.Value;

                if (sum > total / 2)
                {
                    return de.Key;
                }
            }

            return null;
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);

            object o = null;

            switch (_type)
            {
                case "int":
                    try
                    {
                        o = Convert.ToInt32(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error converting '" + cv + "' to an integer.", ex);
                    }

                    break;
                case "string":
                    o = cv;
                    break;
                case "datetime":
                    o = DateTime.ParseExact(cv, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                    break;
                case "float":
                case "":
                    try
                    {
                        o = Convert.ToSingle(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error converting '" + cv + "' to a float.", ex);
                    }
                    break;
            }

            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;

            if (TI == null)
            {
                TI = new TotalItem_ItemCount(o, 1);
            }
            else
            {
                TI.AddCount(o, 1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Median:" + Id + " [" + key + "] new median '" + Median(TI).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;
            return Median(TI).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Work out the median of a column
    /// </summary>
    public class percentile : TotalWithColumn
    {
        #region Member Variables
        string _type = string.Empty; // the type of variable we are calculating the median of
        string _informat = string.Empty; // if it is a date then the input date format
        float _percent = 50;
        #endregion

        #region Constructors
        public percentile(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "type", "informat", "percent" })
        {
            _name = "Percentile";
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "type")
                    {
                        _type = n1.InnerText.ToLower();

                        switch (_type)
                        {
                            case "int":
                            case "string":
                            case "datetime":
                            case "float":
                                break;
                            default:
                                throw new ApplicationException("Percentile::Constructor unsupported type " + n1.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Type>" + _type + "</Type>");
                        }
                    }
                    else if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</Informat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "percent")
                    {
                        try
                        {
                            _percent = Convert.ToSingle(n1.InnerText);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Error parsing Percentile '" + n1.InnerText + "' to a float.", ex);
                        }
                    }
                    if (_type == "datetime" && _informat == string.Empty)
                    {
                        throw new ApplicationException("Median::Constructor DateTime requires an InFormat.");
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        object Percentile(TotalItem_ItemCount TI)
        {
            int sum = 0;
            int total = TI.Total;

            foreach (DictionaryEntry de in TI.Items)
            {
                sum += (int)de.Value;

                if (sum > total * _percent / 100)
                {
                    return de.Key;
                }
            }

            return null;
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);

            object o = null;

            switch (_type)
            {
                case "int":
                    try
                    {
                        o = Convert.ToInt32(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Percentile::Error converting '" + cv + "' to an integer.", ex);
                    }

                    break;
                case "string":
                    o = cv;
                    break;
                case "datetime":
                    o = DateTime.ParseExact(cv, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                    break;
                case "":
                case "float":
                    try
                    {
                        o = Convert.ToSingle(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Percentile::Error converting '" + cv + "' to a float.", ex);
                    }
                    break;
            }

            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;

            if (TI == null)
            {
                TI = new TotalItem_ItemCount(o, 1);
            }
            else
            {
                TI.AddCount(o, 1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Percentile:" + Id + " [" + key + "] new " + _percent.ToString() + "th percentile '" + Percentile(TI).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;
            return Percentile(TI).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Work out the mode of a column
    /// </summary>
    public class mode : TotalWithColumn
    {
        #region Member Variables
        string _type = string.Empty;
        string _informat = string.Empty;
        bool _forceone = false;
        #endregion

        #region Constructors
        public mode(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "type", "informat", "forceone" })
        {
            _name = "Mode";
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "type")
                    {
                        _type = n1.InnerText.ToLower();

                        switch (_type)
                        {
                            case "int":
                            case "string":
                            case "datetime":
                            case "float":
                                break;
                            default:
                                throw new ApplicationException("Mode::Constructor unsupported type " + n1.InnerText);
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Type>" + _type + "</Type>");
                        }
                    }
                    else if (n1.Name.ToLower() == "informat")
                    {
                        _informat = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <InFormat>" + _informat + "</Informat>");
                        }
                    }
                    else if (n1.Name.ToLower() == "forceone")
                    {
                        _forceone = true;
                    }

                    if (_type == "datetime" && _informat == string.Empty)
                    {
                        throw new ApplicationException("Mode::Constructor DateTime requires an InFormat.");
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        ArrayList Mode(TotalItem_ItemCount TI)
        {
            ArrayList maxo = new ArrayList();
            int maxcount = -1;

            foreach (DictionaryEntry de in TI.Items)
            {
                if ((int)de.Value > maxcount)
                {
                    maxo.Clear();
                    maxo.Add(de.Key);
                    maxcount = (int)de.Value;
                }
                else if ((int)de.Value == maxcount)
                {
                    maxo.Add(de.Key);
                }
            }

            if (_forceone && maxo.Count > 1)
            {
                // remove first and last until we only have one left
                while (maxo.Count > 1)
                {
                    maxo.RemoveAt(0);
                    if (maxo.Count > 1)
                    {
                        maxo.RemoveAt(maxo.Count - 1);
                    }
                }
            }

            return maxo;
        }

        string Flatten(ArrayList al)
        {
            string s = string.Empty;
            foreach (object o in al)
            {
                if (s != string.Empty)
                {
                    s = s + "+";
                }
                s = s + o.ToString();
            }
            return s;
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);

            object o = null;

            switch (_type)
            {
                case "int":
                    try
                    {
                        o = Convert.ToInt32(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error converting '" + cv + "' to an integer.", ex);
                    }
                    break;
                case "string":
                    o = cv;
                    break;
                case "datetime":
                    o = DateTime.ParseExact(cv, _informat, System.Globalization.DateTimeFormatInfo.InvariantInfo);
                    break;
                case "float":
                    try
                    {
                        o = Convert.ToSingle(cv);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error converting '" + cv + "' to a float.", ex);
                    }
                    break;
            }

            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;

            if (TI == null)
            {
                TI = new TotalItem_ItemCount(o, 1);
            }
            else
            {
                TI.AddCount(o, 1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "Mode:" + Id + " [" + key + "] new mode '" + Flatten(Mode(TI)) + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_ItemCount TI = ti as TotalItem_ItemCount;
            return Flatten(Mode(TI));
        }
        #endregion
    }

    /// <summary>
    /// Work out the column std deviation
    /// </summary>
    public class standarddeviation : TotalWithColumn
    {

        #region Constructors
        public standarddeviation(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "StandardDeviation";
        }
        #endregion

        #region Private Functions
        double stddev(TotalItem ti)
        {
            // _sumvsquared - double1
            // _sumv - double2
            // _count - int64

            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            return Math.Sqrt((TI.GetSetInt64 * TI.GetSetDouble1 - TI.GetSetDouble2 * TI.GetSetDouble2) / (TI.GetSetInt64 * (TI.GetSetInt64 - 1)));
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);
            double dcv = 0;
            try
            {
                dcv = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;

            if (TI == null)
            {
                TI = new TotalItem_FiveDouble_Int64(dcv * dcv, dcv, 1);
            }
            else
            {
                TI.AddInt64(1);
                TI.AddDouble2(dcv);
                TI.AddDouble1(dcv * dcv);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StandardDeviation:" + Id + " [" + key + "] new stddev '" + stddev(TI).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            return stddev(ti).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Work out the coefficient of variation
    /// </summary>
    public class coefficientofvariation : TotalWithColumn
    {

        #region Constructors
        public coefficientofvariation(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "CoefficientOfVariation";
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Calculate the std dev
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        double stddev(TotalItem ti)
        {
            // _sumvsquared - double1
            // _sumv - double2
            // _count - int64
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            return Math.Sqrt((TI.GetSetInt64 * TI.GetSetDouble1 - TI.GetSetDouble2 * TI.GetSetDouble2) / (TI.GetSetInt64 * (TI.GetSetInt64 - 1)));
        }

        /// <summary>
        /// Calculate the average
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        double average(TotalItem ti)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            return TI.GetSetDouble2 / TI.GetSetInt64;
        }

        /// <summary>
        /// Calculate the coefficient
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        double coefficient(TotalItem ti)
        {
            return stddev(ti) * 100 / average(ti);
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);
            double dcv = 0;
            try
            {
                dcv = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;

            if (TI == null)
            {
                TI = new TotalItem_FiveDouble_Int64(dcv * dcv, dcv, 1);
            }
            else
            {
                TI.AddInt64(1);
                TI.AddDouble2(dcv);
                TI.AddDouble1(dcv * dcv);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "CoefficientOfVariation:" + Id + " [" + key + "] new coefficient '" + coefficient(TI).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            return coefficient(ti).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Calculate the coefficient of correlation on the column
    /// </summary>
    public class coefficientofcorrelation : Total
    {
        #region Member Variables
        ColumnNumberOrColumn _col1 = null; // column x
        ColumnNumberOrColumn _col2 = null; // column y
        #endregion

        #region Constructors
        public coefficientofcorrelation(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column1", "column2" })
        {
            _name = "CoefficientOfCorrelation";
            // _count - Int64
            // _sumxbyy - double1
            // _sumx - double2
            // _sumy - double3
            // _sumxsquared - double4
            // _sumysquared - double5

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "column1")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column1>");
                        }
                        _col1 = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column1>");
                        }
                    }
                    else if (n1.Name.ToLower() == "column2")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column2>");
                        }
                        _col2 = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column2>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        protected string Column1Value(ColumnLine l)
        {
            return _col1.Value(l);
        }

        protected string Column2Value(ColumnLine l)
        {
            return _col2.Value(l);
        }


        double coefficient(TotalItem ti)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            double numerator = TI.GetSetInt64 * TI.GetSetDouble1 - (TI.GetSetDouble2 * TI.GetSetDouble3);
            double denominator = Math.Sqrt(TI.GetSetInt64 * TI.GetSetDouble4 - TI.GetSetDouble2 * TI.GetSetDouble2) * Math.Sqrt(TI.GetSetInt64 * TI.GetSetDouble5 - TI.GetSetDouble3 * TI.GetSetDouble3);

            return numerator / denominator;
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            string cv1 = Column1Value(line);
            string cv2 = Column2Value(line);
            double dcv1 = 0;
            try
            {
                dcv1 = Convert.ToDouble(cv1);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv1 + "' to a double.", ex);
            }
            double dcv2 = 0;
            try
            {
                dcv2 = Convert.ToDouble(cv2);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv2 + "' to a double.", ex);
            }

            if (TI == null)
            {
                TI = new TotalItem_FiveDouble_Int64(dcv1 * dcv2, dcv1, dcv2, dcv1 * dcv1, dcv2 * dcv2, 1);
            }
            else
            {
                TI.AddDouble1(dcv1 * dcv2);
                TI.AddDouble2(dcv1);
                TI.AddDouble3(dcv2);
                TI.AddDouble4(dcv1 * dcv1);
                TI.AddDouble5(dcv2 * dcv2);
                TI.AddInt64(1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "CoefficientOfCorrelation:" + Id + " [" + key + "] new coefficient '" + coefficient(TI).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            return coefficient(ti).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Calculate a straight line fit formula
    /// </summary>
    public class straightlinefit : Total
    {
        #region Member Variables
        ColumnNumberOrColumn _col1 = null; // column x
        ColumnNumberOrColumn _col2 = null; // column y
        #endregion

        #region Constructors
        public straightlinefit(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column1", "column2" })
        {
            _name = "StraightLineFit";
            // _count - Int64
            // _sumxbyy - double1
            // _sumx - double2
            // _sumy - double3
            // _sumxsquared - double4
            // _sumysquared - double5

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "column1")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column1>");
                        }
                        _col1 = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column1>");
                        }
                    }
                    else if (n1.Name.ToLower() == "column2")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Column2>");
                        }
                        _col2 = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Column2>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        protected string Column1Value(ColumnLine l)
        {
            return _col1.Value(l);
        }

        protected string Column2Value(ColumnLine l)
        {
            return _col2.Value(l);
        }

        double a(TotalItem ti)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            double numerator = TI.GetSetDouble3 * TI.GetSetDouble4 - TI.GetSetDouble2 * TI.GetSetDouble1;
            double denominator = TI.GetSetInt64 * TI.GetSetDouble4 - TI.GetSetDouble2 * TI.GetSetDouble2;

            return numerator / denominator;
        }

        double b(TotalItem ti)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            double numerator = TI.GetSetInt64 * TI.GetSetDouble1 - TI.GetSetDouble2 * TI.GetSetDouble3;
            double denominator = TI.GetSetInt64 * TI.GetSetDouble4 - TI.GetSetDouble2 * TI.GetSetDouble2;

            return numerator / denominator;
        }

        string straightline(TotalItem ti)
        {
            return a(ti).ToString() + " + " + b(ti).ToString() + " * <column2>";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            string cv1 = Column1Value(line);
            string cv2 = Column2Value(line);
            double dcv1 = 0;
            try
            {
                dcv1 = Convert.ToDouble(cv1);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv1 + "' to a double.", ex);
            }
            double dcv2 = 0;
            try
            {
                dcv2 = Convert.ToDouble(cv2);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv2 + "' to a double.", ex);
            }

            if (TI == null)
            {
                TI = new TotalItem_FiveDouble_Int64(dcv1 * dcv2, dcv1, dcv2, dcv1 * dcv1, dcv2 * dcv2, 1);
            }
            else
            {
                TI.AddDouble1(dcv1 * dcv2);
                TI.AddDouble2(dcv1);
                TI.AddDouble3(dcv2);
                TI.AddDouble4(dcv1 * dcv1);
                TI.AddDouble5(dcv2 * dcv2);
                TI.AddInt64(1);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "StraightLineFit:" + Id + " [" + key + "] new fit formula '" + straightline(TI) + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            return straightline(ti);
        }
        #endregion
    }

    /// <summary>
    /// Calculate the weighted average
    /// </summary>
    public class weightedaverage : TotalWithColumn
    {
        #region Member Variables
        ColumnNumberOrColumn _weightedcolumnnumber;
        #endregion

        #region Constructors
        public weightedaverage(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column", "weightcolumn" })
        {
            _name = "WeightedAverage";
            //_numerator - double1
            //_denominator - double2

            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "weightcolumn")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <WeightColumn>");
                        }
                        _weightedcolumnnumber = new ColumnNumberOrColumn(n1, j, totals.Processor, null, tracexml, prefix + "   ");
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </WeightColumn>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        string WeightedColumnValue(ColumnLine l)
        {
            return _weightedcolumnnumber.Value(l);
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            string cv = ColumnValue(line);
            string wcv = WeightedColumnValue(line);

            double dcv = 0;
            double dwcv = 0;
            try
            {
                dcv = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }

            try
            {
                dwcv = Convert.ToDouble(wcv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + wcv + "' to a double.", ex);
            }

            if (TI == null)
            {
                TI = new TotalItem_FiveDouble_Int64(dcv * dwcv, dwcv, 0);
            }
            else
            {
                TI.GetSetDouble1 += dcv * dwcv;
                TI.GetSetDouble2 += dwcv;
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "WeightedAverage:" + Id + " [" + key + "] new weighted average '" + (TI.GetSetDouble1 / TI.GetSetDouble2).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_FiveDouble_Int64 TI = ti as TotalItem_FiveDouble_Int64;
            return (TI.GetSetDouble1 / TI.GetSetDouble2).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Calculate the % of the column totls
    /// </summary>
    public class pctoftotal : TotalWithColumn
    {
        #region Member Variables
        double _total = 0.0;
        #endregion

        #region Constructors
        public pctoftotal(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "PctOfTotal";
        }
        #endregion

        #region Override Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            string cv = ColumnValue(line);
            TotalItem_Double TI = ti as TotalItem_Double;

            if (TI == null)
            {
                TI = new TotalItem_Double(cv);
            }
            else
            {
                TI.Add(cv);
            }
            double d = 0;

            try
            {
                d = Convert.ToDouble(cv);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error converting '" + cv + "' to a double.", ex);
            }

            _total += d;
            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "PctOfTotal:" + Id + " [" + key + "] new value '" + (100 * TI.GetSet / _total).ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Double TI = ti as TotalItem_Double;

            return (100 * TI.GetSet / _total).ToString();
        }
        #endregion
    }

    /// <summary>
    /// Sum up a column in a floating point value
    /// </summary>
    public class sumfloat : TotalWithColumn
    {
        #region Constructors
        public sumfloat(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "column" })
        {
            _name = "SumFloat";
        }
        #endregion

        #region Overridden Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem_Double TI = ti as TotalItem_Double;
            string cv = ColumnValue(line);

            if (TI == null)
            {
                TI = new TotalItem_Double(cv);
            }
            else
            {
                TI.Add(cv);
            }

            if (_trace)
            {
                CSVProcessor.DisplayError(ANSIHelper.Trace + "SumFloat:" + Id + " [" + key + "] adding '" + cv + "' --> '" + TI.GetSet.ToString() + "'" + ANSIHelper.White);
            }

            PostProcess();
            return TI;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            TotalItem_Double TI = ti as TotalItem_Double;
            return TI.GetSet.ToString();
        }
        #endregion
    }

    /// <summary>
    /// A hard coded total column value
    /// </summary>
    public class literal : Total
    {
        #region Member Variables
        string _literal = string.Empty;
        #endregion

        #region Constructors
        public literal(XmlNode n, CSVJob j, CSVProcess p, Totals totals, bool tracexml, string prefix)
            : base(n, j, p, totals, tracexml, prefix, new string[] { "literal" })
        {
            _name = "Literal";
            foreach (XmlNode n1 in n.ChildNodes)
            {
                if (n1.NodeType != XmlNodeType.Comment)
                {
                    if (n1.Name.ToLower() == "literal")
                    {
                        _literal = n1.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Literal>" + _literal + "</Literal>");
                        }
                    }
                }
            }
        }
        #endregion

        #region Overridable Functions
        public override TotalItem Process(ColumnLine line, TotalItem ti, string key)
        {
            PreProcess();
            TotalItem s = ti;
            PostProcess();
            return s;
        }

        public override string Result(TotalItem ti, TotalAccum ta)
        {
            return _literal;
        }
        #endregion
    }
    #endregion

    #endregion

    /// <summary>
    /// Summary description for Totals.
    /// </summary>
    public class Totals
    {
        #region Member Variables
        CSVProcess _csvProcess = null; // The CSVProcess this totals belongs to
        Column _rowkey = null; // The key column
        Column _colkey = null; // The key column
        Dictionary<string, TotalAccum> _groups = null; // The accumulators
        //List<string> _rowkeys = new List<string>(); // A collection of all the keys we have totals for
        SortedDictionary<string, int> _rowkeys = new SortedDictionary<string, int>(); // A collection of all the keys we have totals for
        //List<string> _colkeys = new List<string>(); // A collection of all the keys we have totals for
        SortedDictionary<string, int> _colkeys = new SortedDictionary<string, int>(); // A collection of all the keys we have totals for
        List<Total> _totals = new List<Total>(); // holds the total definitions
        List<Column> _columns = new List<Column>(); // holds some column definitions for output which is based on the key
        string _rowkeytitles = string.Empty; // given row key titles
        string _colkeytitles = string.Empty; // given column key titles
        string _totalstitles = string.Empty; // given totals titles
        bool _usetotalstitles = false; // true if we are to get column titles from total elements
        int _initialCapacity = 1000; // default initial hash table size
        bool _useSpecialHashtable = false; // true if the user wants to manually manage hashtable size
        bool _timePerformance = false; // true if we are do performance measurement
        string _id = string.Empty; // the id for totals
        long _processed = 0; // count of processed rows
        int _unique = UniqueIdClass.Allocate();
        #endregion

        public List<Total> TheTotals
        {
            get { return _totals; }
        }

        public string UniqueId
        {
            get
            {
                return "cluster" + _unique.ToString();
            }
        }

        public string FirstUniqueId
        {
            get
            {
                if (_totals.Count > 0)
                {
                    return _totals[0].UniqueId;
                }

                Debug.Fail("No total column to link to for dot output.");
                return string.Empty;
            }
        }

        public void WriteDot(StreamWriter dotWriter, string prefix)
        {
            dotWriter.WriteLine(prefix + "   subgraph " + UniqueId + " {");
            dotWriter.WriteLine(prefix + "   color=green;");
            dotWriter.WriteLine(prefix + "   style=filled;");
            dotWriter.WriteLine(prefix + "   edge [dir=none];");
            dotWriter.WriteLine(prefix + "   node [shape=rect];");
            foreach (Total t in _totals)
            {
                t.WriteDot(dotWriter, "   " + prefix);
            }
            for (int i = 1; i < _totals.Count; i++)
            {
                dotWriter.WriteLine(prefix + "   " + _totals[i - 1].UniqueId + " -> " + _totals[i].UniqueId + ";");
            }
            if (Id != string.Empty)
            {
                dotWriter.WriteLine(prefix + "   label=\"Totals::" + Id + "\";");
            }
            else
            {
                dotWriter.WriteLine(prefix + "   label=\"Totals\";");
            }
            dotWriter.WriteLine(prefix + "   }");
        }

        #region Accessors
        /// <summary>
        /// The owning process
        /// </summary>
        public CSVProcess Processor
        {
            get
            {
                return _csvProcess;
            }
        }

        /// <summary>
        /// The totals Id
        /// </summary>
        public string Id
        {
            get
            {
                if (_id != null && _id != string.Empty)
                {
                    return _id;
                }
                else
                {
                    return _csvProcess.Id;
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="doc">Xml Document holding the output definition</param>
        public Totals(XmlNode node, CSVJob j, CSVProcess process, bool tracexml, string prefix)
        {
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "<Totals>");
            }

            _csvProcess = process;

            // look at the children
            foreach (XmlNode n3 in node.ChildNodes)
            {
                if (n3.NodeType != XmlNodeType.Comment)
                {
                    if (n3.Name.ToLower() == "rowgroup")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RowGroup>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (_rowkey != null)
                                {
                                    throw new ApplicationException("Only one group column can be specified. Use concatenate column to join columns to make a key");
                                }
                                else
                                {
                                    _rowkey = Column.Create(n4, j, process, null, tracexml, prefix + "    ");
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </RowGroup>");
                        }
                    }
                    else if (n3.Name.ToLower() == "id")
                    {
                        _id = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Id>" + _id + "</id>");
                        }
                    }
                    else if (n3.Name.ToLower() == "timeperformance")
                    {
                        _timePerformance = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TimePerformance/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "initialcapacity")
                    {
                        try
                        {
                            _initialCapacity = Convert.ToInt32(n3.InnerText);
                            _useSpecialHashtable = true;

                            if (tracexml)
                            {
                                CSVProcessor.DisplayError(prefix + "  <InitialCapacity>" + _initialCapacity.ToString() + "</InitialCapacity>");
                            }
                        }
                        catch
                        {
                            throw new ApplicationException("Totals::Constructor error parsing initial capacity.");
                        }
                    }
                    else if (n3.Name.ToLower() == "colgroup")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ColGroup>");
                        }
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                if (_colkey != null)
                                {
                                    throw new ApplicationException("Only one column group column can be specified. Use concatenate column to join columns to make a key");
                                }
                                else
                                {
                                    _colkey = Column.Create(n4, j, process, null, tracexml, prefix + "    ");
                                }
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </ColGroup>");
                        }
                    }
                    else if (n3.Name.ToLower() == "rowkeytitles" || n3.Name.ToLower() == "rowgrouptitles")
                    {
                        _rowkeytitles = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <RowKeyTitles>" + _rowkeytitles + "</RowKeyTitles>");
                        }
                    }
                    else if (n3.Name.ToLower() == "colkeytitles" || n3.Name.ToLower() == "colgrouptitles")
                    {
                        _colkeytitles = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <ColKeyTitles>" + _colkeytitles + "</ColKeyTitles>");
                        }
                    }
                    else if (n3.Name.ToLower() == "totalstitles")
                    {
                        _totalstitles = n3.InnerText;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <TotalsTitles>" + _totalstitles + "</TotalsTitles>");
                        }
                    }
                    else if (n3.Name.ToLower() == "usetotalstitles")
                    {
                        _usetotalstitles = true;
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <UseTotalsTitles/>");
                        }
                    }
                    else if (n3.Name.ToLower() == "outputcolumns")
                    {
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  <Output>");
                        }
                        // look at the children
                        foreach (XmlNode n4 in n3.ChildNodes)
                        {
                            if (n4.NodeType != XmlNodeType.Comment)
                            {
                                // each node here should be a column derived class defining one output column
                                _columns.Add(Column.Create(n4, j, process, null, tracexml, prefix + "    "));
                            }
                        }
                        if (tracexml)
                        {
                            CSVProcessor.DisplayError(prefix + "  </Output>");
                        }
                    }
                    else
                    {
                        // each node here should be a column derived class defining one output column
                        _totals.Add(Total.Create(n3, j, process, this, tracexml, prefix + "  "));
                    }
                }
            }
            if (tracexml)
            {
                CSVProcessor.DisplayError(prefix + "</Totals>");
            }

            if (_timePerformance && Id == "")
            {
                throw new ApplicationException("Totals <TimePerformance\\> requires that you also specify an <Id>.");
            }

            if (_useSpecialHashtable)
            {
                _groups = new Dictionary<string, TotalAccum>(_initialCapacity);
            }
            else
            {
                _groups = new Dictionary<string, TotalAccum>();
            }

            //if (_usetotalstitles)
            //{
            //    _totalstitles = string.Empty;

            //    foreach (Total t in _totals)
            //    {
            //        if (_totalstitles != string.Empty)
            //        {
            //            _totalstitles = _totalstitles + ",";
            //        }

            //        _totalstitles = _totalstitles + t.Title;
            //    }
            //}

            Performance.CreateCounterInstance("CSVProcessor_Totals", "Total Groups", Id);
            Performance.CreateCounterInstance("CSVProcessor_Totals", "Total Lines", Id);

            if (_timePerformance)
            {
                Performance.CreateCounterInstance("CSVProcessor_Totals", "Totals Average Duration", Id);
                Performance.CreateCounterInstance("CSVProcessor_Totals", "Totals Average Duration Base", Id);
            }
        }
        #endregion

        #region Public Methods
        public List<ColumnLine> Result()
        {
            List<ColumnLine> sc = new List<ColumnLine>();

            // create title line 1
            if (_totalstitles != string.Empty)
            {
                ColumnLine tt = new ColumnLine(_totalstitles);
                tt.Parse();
                tt.Data = false;
                sc.Add(tt);
            }
            else
            {
                CSVFileLine rlt = null;
                if (_rowkeytitles == string.Empty && _usetotalstitles && _rowkey != null)
                {
                    rlt = new CSVFileLine(_rowkey.Title, 0, new CSVFileInfo());
                }
                else
                {
                    rlt = new CSVFileLine(_rowkeytitles, 0, new CSVFileInfo());
                }
                rlt.Parse();

                CSVFileLine clt = null;
                if (_colkeytitles == string.Empty && _usetotalstitles && _colkey != null)
                {
                    clt = new CSVFileLine(_colkey.Title, 0, new CSVFileInfo());
                }
                else
                {
                    clt = new CSVFileLine(_colkeytitles, 0, new CSVFileInfo());
                }
                clt.Parse();

                ColumnLine title = new ColumnLine(rlt);
                title.Data = false;

                if ((clt.Count == 0) || ((clt.Count == 1) && (clt[0] == string.Empty)))
                {
                    if (_usetotalstitles)
                    {
                        for (int i = 0; i < _totals.Count; i++)
                        {
                            title.AddEnd(_totals[i].Title);
                        }
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, int> kvp in _colkeys)
                    {
                        if (_totals.Count == 1)
                        {
                            if (kvp.Key != string.Empty)
                            {
                                title.AddEnd(kvp.Key + " - " + _totals[0].Title);
                            }
                            else
                            {
                                title.AddEnd(_totals[0].Title);
                            }
                        }
                        else
                        {
                            ColumnLine cl = new ColumnLine(kvp.Key);
                            cl.Parse();
                            title.AddEnd(cl);

                            for (int j = 0; j < _totals.Count - 1; j++)
                            {
                                title.AddEnd("");
                            }
                        }
                    }
                }
                bool blankcolumns = true;
                foreach (string s in title.Items)
                {
                    if (s != string.Empty)
                    {
                        blankcolumns = false;
                    }
                }
                if (!blankcolumns)
                {
                    sc.Add(title);
                }

                // Display total line 2
                if (_totals.Count > 1 && _usetotalstitles && !((clt.Count == 0) || ((clt.Count == 1) && (clt[0] == string.Empty))))
                {
                    title = new ColumnLine();
                    title.Data = false;
                    for (int i = 0; i < rlt.Count; i++)
                    {
                        title.AddEnd("");
                    }
                    foreach (KeyValuePair<string, int> kvp in _colkeys)
                    {
                        for (int j = 0; j < _totals.Count; j++)
                        {
                            title.AddEnd(_totals[j].Title);
                        }
                    }
                    blankcolumns = true;
                    foreach (string s in title.Items)
                    {
                        if (s != string.Empty)
                        {
                            blankcolumns = false;
                        }
                    }
                    if (!blankcolumns)
                    {
                        sc.Add(title);
                    }
                }
            }

            //foreach (string rowkey in _rowkeys)
            foreach (KeyValuePair<string, int> kvp in _rowkeys)
            {
                //string s = rowkey;
                string s = kvp.Key;
                ColumnLine cl = new ColumnLine();
                if (_rowkey != null && _rowkey.Safe)
                {
                    //CSVFileLine clNew = new CSVFileLine(rowkey, 0);
                    CSVFileLine clNew = new CSVFileLine(kvp.Key, 0);
                    try
                    {
                        clNew.Parse();
                        cl.AddEnd(clNew);
                    }
                    catch (Exception ex)
                    {
                        //CSVProcessor.DisplayError(ANSIHelper.Red + "Error parsing total rowkey: " + rowkey + " : " + ex.Message + ANSIHelper.White);
                        CSVProcessor.DisplayError(ANSIHelper.Red + "Error parsing total rowkey: " + kvp.Key + " : " + ex.Message + ANSIHelper.White);
                    }
                }
                else if (_rowkey != null)
                {
                    //cl.AddEnd(rowkey);
                    cl.AddEnd(kvp.Key);
                }

                foreach (Column c in _columns)
                {
                    if (c.Safe)
                    {
                        CSVFileLine clNew = new CSVFileLine(c.Process(cl), 0);
                        try
                        {
                            clNew.Parse();
                            cl.AddEnd(clNew);
                        }
                        catch (Exception ex)
                        {
                            CSVProcessor.DisplayError(ANSIHelper.Red + "Error parsing total column: " + clNew.Raw + " : " + ex.Message + ANSIHelper.White);
                        }
                    }
                    else
                    {
                        cl.AddEnd(c.Process(cl));
                    }
                }

                if (_colkeys.Count == 0)
                {
                    //string key = rowkey + Convert.ToChar(127);
                    string key = kvp.Key + Convert.ToChar(127);

                    for (int i = 0; i < _totals.Count; i++)
                    {
                        try
                        {
                            string res = ((Total)_totals[i]).Result(((TotalAccum)_groups[key])[i], ((TotalAccum)_groups[key]));
                            if (((Total)_totals[i]).Safe)
                            {
                                CSVFileLine clNew = new CSVFileLine(res, 0);
                                try
                                {
                                    clNew.Parse();
                                    cl.AddEnd(clNew);
                                }
                                catch (Exception ex)
                                {
                                    CSVProcessor.DisplayError(ANSIHelper.Red + "Error parsing total value: " + res + " : " + ex.Message + ANSIHelper.White);
                                }
                            }
                            else
                            {
                                cl.AddEnd(res);
                            }
                        }
                        catch
                        {
                            // missing total column so put in a blank
                            cl.AddEnd(string.Empty);
                        }
                    }
                }
                else
                {
                    //foreach (string colkey in _colkeys)
                    foreach (KeyValuePair<string, int> kvpcol in _colkeys)
                    {
                        string colkey = kvpcol.Key;
                        //string key = rowkey + Convert.ToChar(127) + colkey;
                        string key = kvp.Key + Convert.ToChar(127) + colkey;

                        for (int i = 0; i < _totals.Count; i++)
                        {
                            try
                            {
                                string res = ((Total)_totals[i]).Result(((TotalAccum)_groups[key])[i], ((TotalAccum)_groups[key]));
                                if (((Total)_totals[i]).Safe)
                                {
                                    CSVFileLine clNew = new CSVFileLine(res, 0);
                                    try
                                    {
                                        clNew.Parse();
                                        cl.AddEnd(clNew);
                                    }
                                    catch (Exception ex)
                                    {
                                        CSVProcessor.DisplayError(ANSIHelper.Red + "Error parsing total value: " + res + " : " + ex.Message + ANSIHelper.White);
                                    }
                                }
                                else
                                {
                                    cl.AddEnd(res);
                                }
                            }
                            catch
                            {
                                // missing total column so put in a blank
                                cl.AddEnd(string.Empty);
                            }
                        }
                    }
                }

                sc.Add(cl);
            }

            return sc;
        }

        /// <summary>
        /// This function takes an input line and adds it to the totals
        /// </summary>
        /// <param name="l">CSV input line</param>
        public void Process(ColumnLine l)
        {
            // save the process start time
            long starttime = 0;
            Performance.QueryPerformanceCounter(ref starttime);

            // update our performance counter
            _processed++;
            Performance.SetCounter("Total Lines", Id, (int)_processed);

            // build the row and column keys
            string rowkey = string.Empty;
            string colkey = string.Empty;
            if (_rowkey != null)
            {
                try
                {
                    rowkey = _rowkey.Process(l).ToString(false);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Problem extracting totals grouping key", ex);
                }
            }
            if (_colkey != null)
            {
                try
                {
                    colkey = _colkey.Process(l).ToString(false);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Problem extracting totals grouping column key", ex);
                }
            }

            // add the row key
            //if (!_rowkeys.Contains(rowkey))
            //{
            //    _rowkeys.Add(rowkey);
            //}
            if (!_rowkeys.ContainsKey(rowkey))
            {
                _rowkeys.Add(rowkey, 1);
            }

            // add the column key
            //if (!_colkeys.Contains(colkey))
            if (!_colkeys.ContainsKey(colkey))
            {
                _colkeys.Add(colkey, 1);
            }

            // make the overall key
            string key = rowkey + Convert.ToChar(127) + colkey;

            // get or create the totals accumulator
            TotalAccum ta = null;
            try
            {
                ta = _groups[key];
            }
            catch
            {
                ta = new TotalAccum(key, _totals.Count);
                _groups[key] = ta;

                // update our performance counter
                Performance.SetCounter("Total Groups", Id, _groups.Count);
            }

            // for each defined total
            for (int i = 0; i < _totals.Count; i++)
            {
                // process the row
                try
                {
                    if (_totals[i].True(l))
                    {
                        if (ta[i] == null)
                        {
                            ta[i] = ((Total)_totals[i]).Process(l, ta[i], key);
                        }
                        else
                        {
                            ((Total)_totals[i]).Process(l, ta[i], key);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Problem accumulating total", ex);
                }
            }

            // if we are timing performance do the calculation
            if (_timePerformance)
            {
                long endtime = 0;
                Performance.QueryPerformanceCounter(ref endtime);

                Performance.IncrementBy("Totals Average Duration", Id, endtime - starttime);
                Performance.Increment("Totals Average Duration Base", Id);
            }
        }
        #endregion
    }
}

