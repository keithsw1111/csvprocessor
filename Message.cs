using System;
using System.Threading;
using Queues;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSVProcessor
{
	/// <summary>
	/// A message for passing lines between threads.
	/// </summary>
	public class CSVMessage
	{
		public enum STATUS { UNKNOWN, NEWFILE, LINE, STOP, SYNCH, MESSAGE, PROGRESS, PROGRESSPCT, REJECT };

		ColumnLine _cl = null; // the line to process
		long _line = -1; // the line number
		bool _firstFile = false; // the first file being processed
		ManualResetEvent _mre = null; // a reset event if this is a synch message
		STATUS _status = STATUS.UNKNOWN;
		string _string = string.Empty;
		float _float = 0;
		long _long = 0;
		bool _bool = false;

		#region Accessors
		public STATUS Status
		{
			get
			{
				return _status;
			}
		}
		public string String
		{
			get
			{
				return _string;
			}
			set
			{
				_string = value;
			}
		}
		public bool Bool
		{
			get
			{
				return _bool;
			}
		}
		public float Float
		{
			get
			{
				return _float;
			}
		}
		public float Long
		{
			get
			{
				return _long;
			}
		}

		/// <summary>
		/// Synchronisation object
		/// </summary>
		public ManualResetEvent Synch
		{
			get
			{
				return _mre;
			}
		}

		/// <summary>
		/// Line number being processed
		/// </summary>
		public long LineNumber
		{
			get
			{
				return _line;
			}
		}

		/// <summary>
		/// Line being processed
		/// </summary>
		public ColumnLine Line
		{
			get
			{
				return _cl;
			}
		}

		/// <summary>
		/// Checks if this is a stop message
		/// </summary>
		public bool Finished
		{
			get
			{
				return _status == STATUS.STOP;
			}
		}

		/// <summary>
		/// Checks if this is a line from the first file
		/// </summary>
		public bool FirstFile
		{
			get
			{
				return _firstFile;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor for a line to be processed
		/// </summary>
		/// <param name="l">line</param>
		/// <param name="line">line number</param>
		public CSVMessage(ColumnLine l, long line, bool firstfile)
		{
			// save the values
			_cl = l;
			_line = line;
			_firstFile = firstfile;
			_status = STATUS.LINE;
		}

		/// <summary>
		/// Constructor for a synch message
		/// </summary>
		/// <param name="mre"></param>
		public CSVMessage(ManualResetEvent mre)
		{
			// save the values
			_mre = mre;
			_status = STATUS.SYNCH;
		}

		public CSVMessage(string s, STATUS status)
		{
			// save the values
			_string = s;
			_status = status;
		}

		public CSVMessage(string s, bool b, STATUS status)
		{
			// save the values
			_string = s;
			_bool = b;
			_status = status;
		}

		public CSVMessage(long l, STATUS status)
		{
			// save the values
			_long = l;
			_status = status;
		}

		public CSVMessage(float f, STATUS status)
		{
			// save the values
			_float = f;
			_status = status;
		}

		public CSVMessage(STATUS status)
		{
			_status = status;
		}

		/// <summary>
		/// Void constructor
		/// </summary>
		public CSVMessage()
		{
			_status = STATUS.UNKNOWN;
		}
		#endregion
	}

	public class NamedCSVMessageQueue
	{
		#region Static Variables
		static List<NamedCSVMessageQueue> __queues = new List<NamedCSVMessageQueue>(); // a list of queues in existence
		static int __defaultQueueLength = 1000; // length of the queue
		#endregion

		#region Member Variables
		string _name = string.Empty; // name of the queue
		protected BlockingQueue _mq = null; // queue to use for multithreaded running
		int _queueLength = NamedCSVMessageQueue.__defaultQueueLength;
		#endregion

		#region Static Functions
		static public NamedCSVMessageQueue GetQueue(string name)
		{
			return GetQueue(name, __defaultQueueLength);
		}

		static public NamedCSVMessageQueue GetQueue(string name, int queueLength)
		{
			NamedCSVMessageQueue q = null;

			foreach (NamedCSVMessageQueue nmq in __queues)
			{
				if (nmq._name == name.ToLower())
				{
					q = nmq;
					break;
				}
			}

			if (q == null)
			{
				q = new NamedCSVMessageQueue(name, queueLength);
			}

			return q;
		}
		#endregion

		#region Constructors
		public NamedCSVMessageQueue(string name, int queueLength)
		{
			Debug.WriteLine("Creating named queue:" + name);

			_queueLength = queueLength;
			_name = name.ToLower();

			// add us to the list of the available queues
			__queues.Add(this);

			// create the queue
			_mq = new BlockingQueue("CSVProcessor_NamedMessageQueue", _name, _queueLength);
		}
		#endregion

		#region Accessors

		public string Name
		{
			get
			{
				return _name;
			}
		}

		public BlockingQueue Queue
		{
			get
			{
				return _mq;
			}
		}

		#endregion
	}
}
